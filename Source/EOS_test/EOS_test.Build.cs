// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class EOS_test : ModuleRules
{
	public EOS_test(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
