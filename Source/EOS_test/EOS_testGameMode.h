// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EOS_testGameMode.generated.h"

UCLASS(minimalapi)
class AEOS_testGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEOS_testGameMode();
};



