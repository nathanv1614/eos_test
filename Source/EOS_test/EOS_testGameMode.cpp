// Copyright Epic Games, Inc. All Rights Reserved.

#include "EOS_testGameMode.h"
#include "EOS_testCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEOS_testGameMode::AEOS_testGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
