// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyEOSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class EOS_TEST_API UMyEOSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
