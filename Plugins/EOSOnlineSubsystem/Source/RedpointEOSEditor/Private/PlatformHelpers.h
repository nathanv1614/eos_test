// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

TArray<FName> GetConfidentialPlatformNames();