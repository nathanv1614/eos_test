// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ISettingsSection.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"

#include "OnlineSubsystemEOSEditorConfig.generated.h"

/**
 * Configuration for Epic Online Services. The developer portal is located at https://dev.epicgames.com/portal/en-US/.
 */
UCLASS(Config = Engine, DefaultConfig)
class UOnlineSubsystemEOSEditorConfig : public UObject
{
    GENERATED_BODY()

public:
    UOnlineSubsystemEOSEditorConfig();

    virtual void LoadEOSConfig();
    virtual void SaveEOSConfig();
    virtual void ConfigureProject();

    static const FName GetAuthenticationGraphPropertyName();
    static const FName GetEditorAuthenticationGraphPropertyName();
    static const FName GetCrossPlatformAccountProviderPropertyName();

    /**
     * The product name to send with requests to Epic Online Services. This can be any arbitrary string, but should
     * probably be the name of your game.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString ProductName;

    /**
     * The product version to send with requests to Epic Online Services. If unsure, you can leave this as the default.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString ProductVersion;

    /**
     * The product ID. You can find this under "Product Settings" in the portal.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString ProductId;

    /**
     * The sandbox ID. You can find this under "Product Settings" -> "Sandboxes" in the portal. If you are not an Epic
     * Games Store developer, this should be the ID of the Live sandbox.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString SandboxId;

    /**
     * The deployment ID. You can find this under "Product Settings" -> "Sandboxes" -> (SANDBOX NAME) -> "Deployments"
     * in the portal.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString DeploymentId;

    /**
     * The client ID. You can find this under "Product Settings" -> "Client Credentials" (bottom of the page)
     * in the portal.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString ClientId;

    /**
     * The client secret. You can find this under "Product Settings" -> "Client Credentials" (bottom of the page) ->
     * "..." -> "Client Details" -> "Secret key" in the portal.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    FString ClientSecret;

    /**
     * Require the Epic Games Launcher to launch the game. This option should only be turned on for games that ship on
     * the Epic Games Store.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Core Configuration")
    bool RequireEpicGamesLauncher;

    /**
     * The authentication graph to use for this application. Refer to
     * https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_configuration for a list of available options.
     * This defaults to "Default", whose behaviour may change with API versions.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication")
    FName AuthenticationGraph;

    /**
     * The authentication graph to use when running this application in the editor.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication")
    FName EditorAuthenticationGraph;

    /**
     * The cross-platform account provider. This will typically be either "None" or "EpicGames" but you can define your
     * own first-party, cross-platform account systems in C++ and leverage the existing authentication logic across
     * platforms.
     *
     * If this is set to "None", then you will get the same behaviour as the NoEAS option that was previously available.
     * If this is set to "EpicGames", then you'll get either EASOptional or EASRequired depending on the
     * RequireCrossPlatformAccount setting.
     *
     * Refer to https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_configuration for a list of available
     * options.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication")
    FName CrossPlatformAccountProvider;

    /**
     * Require the user to have linked a cross-platform account in order to play the game.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication")
    bool RequireCrossPlatformAccount;

    /**
     * The user interface to use when the player needs to select which account they want to sign in with.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication|User Interface",
        NoClear,
        meta =
            (DisplayName = "On Select EOS Account",
             MetaClass = "UserWidget",
             MustImplement = "EOSUserInterface_SelectEOSAccount"))
    FSoftClassPath WidgetClass_SelectEOSAccount;

    /**
     * The user interface to use when the player needs to choose what other identities they want to link with their
     * current account.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication|User Interface",
        NoClear,
        meta =
            (DisplayName = "On Link EOS Accounts Against Cross Platform",
             MetaClass = "UserWidget",
             MustImplement = "EOSUserInterface_LinkEOSAccountsAgainstCrossPlatform"))
    FSoftClassPath WidgetClass_LinkEOSAccountsAgainstCrossPlatform;

    /**
     * The user interface to use when the player is being prompted to switch to a cross-platform account.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication|User Interface",
        NoClear,
        meta =
            (DisplayName = "On Switch To Cross Platform Account",
             MetaClass = "UserWidget",
             MustImplement = "EOSUserInterface_SwitchToCrossPlatformAccount"))
    FSoftClassPath WidgetClass_SwitchToCrossPlatformAccount;

    /**
     * The user interface to use when the player is being prompted to choose between signing into an existing account or
     * creating a new account.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication|User Interface",
        NoClear,
        meta =
            (DisplayName = "On Sign In or Create Account",
             MetaClass = "UserWidget",
             MustImplement = "EOSUserInterface_SignInOrCreateAccount"))
    FSoftClassPath WidgetClass_SignInOrCreateAccount;

    /**
     * The user interface to use when the player is being prompted to obtain and enter a PIN code (used for
     * authentication on consoles).
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication|User Interface",
        NoClear,
        meta =
            (DisplayName = "On Enter Device PIN Code",
             MetaClass = "UserWidget",
             MustImplement = "EOSUserInterface_EnterDevicePinCode"))
    FSoftClassPath WidgetClass_EnterDevicePinCode;

    /**
     * If true, players will be able to login anonymously on their device. You can find out more information about this
     * option in the documentation: https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_configuration
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication Graph: Monolithic",
        meta = (DisplayName = "Allow Device Id Accounts (Allow Anonymous Users)"))
    bool AllowDeviceIdAccounts;

    /**
     * If false, native platforms accounts will not be used to sign into EOS or an Epic Games account. This setting
     * should only be used for testing purposes.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication Graph: Monolithic")
    bool AllowNativePlatformAccounts;

    /**
     * If true, anonymous accounts will be deleted when the player logs out of the device. This setting should only be
     * used for testing purposes. You can find out more information about this option in the documentation:
     * https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_configuration
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Authentication Graph: Monolithic",
        meta = (DisplayName = "Delete Device Id On Logout (Delete Anonymous User On Logout)"))
    bool DeleteDeviceIdOnLogout;

    /**
     * If true, player logins will not be persistently saved when players sign in with an Epic Games account. This
     * setting should only be used for testing purposes. You can find out more information about this option in the
     * documentation: https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_configuration
     */
    UPROPERTY(EditAnywhere, Config, Category = "Cross Platform Account Provider: Epic Games")
    bool DisablePersistentLogin;

    /**
     * The login URL that you have set up for Simple First Party authentication. Follow the documentation on how to
     * configure this.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Cross Platform Account Provider: Simple First Party")
    FString SimpleFirstPartyLoginUrl;

    /**
     * Specifies the host and port as "host:port" that the plugin will use to connect to the Developer Authentication
     * Tool. Refer to the documentation for more information about this setting:
     * https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_dev_auth_tool_usage
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication Graph: Monolithic")
    FString DevAuthToolAddress;

    /**
     * Specifies the default credential name that the plugin will try to use in the Developer Authentication
     * Tool if the credential for the play-in-editor context didn't work. Refer to the documentation for more
     * information about this setting:
     * https://redpointgames.gitlab.io/eos-online-subsystem/docs/identity_dev_auth_tool_usage
     */
    UPROPERTY(EditAnywhere, Config, Category = "Authentication Graph: Monolithic")
    FString DevAuthToolDefaultCredentialName;

    /**
     * The encryption key used to encrypt and decrypt data from Player Data Storage and Title Storage. This should be
     * a 64-character hexadecimal string; see the EOS documentation for more information:
     * https://dev.epicgames.com/docs/services/en-US/Interfaces/PlayerDataStorage/index.html
     */
    UPROPERTY(EditAnywhere, Config, Category = "Storage")
    FString PlayerDataEncryptionKey;

    /**
     * In EOS, you can only have one object advertised with presence; this can either be a party or a session, but not
     * both. This configuration option determines what gets advertised in the presence system.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Presence")
    EPresenceAdvertisementType PresenceAdvertises;

    /**
     * A comma-separated list of subsystems that friends will be included from (in addition to Epic Account Services).
     * Refer to the documentation for more information on this setting:
     * https://redpointgames.gitlab.io/eos-online-subsystem/docs/friends_delegated_subsystems
     */
    UPROPERTY(EditAnywhere, Config, Category = "Cross-Platform")
    TArray<FString> DelegatedSubsystems;

    /**
     * Constrains the effect of accepting cross-platform party invites by a player. This setting has no effect on your
     * game code calling JoinParty; it only affects synthetic party invites that were sent and accepted over the native
     * platform.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Cross-Platform")
    EPartyJoinabilityConstraint PartyJoinabilityConstraint;

    /**
     * When new versions of the plugin are released, sometimes the default behaviour changes. When you first use the
     * plugin, it sets the current latest API version in the configuration so that when you upgrade, the behaviour does
     * not change for you until you decide to upgrade.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Compatibility")
    EEOSApiVersion ApiVersion;

    /**
     * The EOS networking stack to use.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    EEOSNetworkingStack NetworkingStack;

    /**
     * Turn this on to enable Anti-Cheat. Enabling anti-cheat will also turn on identity checks on listen servers and
     * enable trusted dedicated servers.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    bool EnableAntiCheat;

    /**
     * A list of trusted platforms.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    TSet<FName> TrustedPlatforms;

    /**
     * The public signing key for trusted clients (such as consoles). This is used to by other clients and servers to
     * verify that the connecting player is allowed to join without Anti-Cheat.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString TrustedClientPublicKey;

    /**
     * The private signing key for trusted clients (such as consoles). This is used to sign a proof, enabling the player
     * to connect to an Anti-Cheat protected server without running Anti-Cheat.
     *
     * This value is only set in Build/NoRedist/EOSTrustedClient.ini and in each
     * Platforms/<Platform>/Config/<Platform>Engine.ini for the trusted platforms chosen in Project Settings.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString TrustedClientPrivateKey;

    /**
     * If enabled, listen servers and trusted dedicated servers will check the connecting player for any BAN sanctions.
     *
     * Turning this option on requires creating a custom policy in the Epic Games Dev Portal with permissions to check
     * sanctions of other players. If you don't set up the custom policy properly, players will not be able to connect.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    bool EnableSanctionChecks;

    /**
     * If enabled, listen servers will check the product user ID of connecting players to ensure that they exist as a
     * real account. No credentials are sent, so this doesn't prevent impersonation, but it does prevent players from
     * using junk IDs.
     *
     * This option is required for Anti-Cheat to work.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    bool EnableIdentityChecksOnListenServers;

    /**
     * Specifies how dedicated servers are distributed for this project. Depending on how dedicated servers are
     * distributed changes how the server IDs, secrets and private keys are stored in configuration.
     *
     * This configuration option is stored in DefaultEditor.ini, not DefaultEngine.ini. The value is not present in
     * packaged versions of the game.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    EDedicatedServersDistributionMode DedicatedServerDistributionMode;

    /**
     * If enabled, connecting players will be verified when they connect to dedicated servers using their EOS Connect
     * token. Trusted dedicated servers will also be able to write to Player Data Storage on a connected player's
     * behalf.
     *
     * Trusted dedicated servers required the client-server connection to be encrypted. You can turn on automatic
     * encryption below or manually configure network encryption in your project.
     *
     * This option is required for Anti-Cheat to work.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    bool EnableTrustedDedicatedServers;

    /**
     * If enabled, player connections to trusted dedicated servers will automatically negotiate an AES-GCM encrypted
     * connection using the public and private keys that have been set up below. This is vastly simpler than setting up
     * encryption manually, and is suitable for almost all games. The only cases where you might want to configure
     * encryption manually is if you have platform-specific requirements that dictate an encryption algorithm other than
     * AES-GCM.
     *
     * If this option is off, but trusted dedicated servers is on, you MUST configure encryption manually. Trusted
     * dedicated servers can not work without connections being encrypted.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    bool EnableAutomaticEncryptionOnTrustedDedicatedServers;

    /**
     * The client ID to use when running as a dedicated server. This value should only ever be set in
     * DedicatedServerEngine.ini to prevent it from being shipped with game clients.
     *
     * For additional security, you can leave this option blank and pass in the client ID on the command line (refer to
     * the documentation).
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString DedicatedServerClientId;

    /**
     * The client secret to use when running as a dedicated server. This value should only ever be set in
     * DedicatedServerEngine.ini to prevent it from being shipped with game clients.
     *
     * For additional security, you can leave this option blank and pass in the client secret on the command line (refer
     * to the documentation).
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString DedicatedServerClientSecret;

    /**
     * The public signing key for dedicated servers. This is used by clients to verify that network
     * commands are actually being sent by trusted dedicated servers. This value is set into DefaultEngine.ini and
     * should be present on all clients.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString DedicatedServerPublicKey;

    /**
     * The private signing key of dedicated servers. This value should only ever be set in
     * DedicatedServerEngine.ini to prevent it from being shipped with game clients.
     *
     * For additional security, you can leave this option blank and pass in the private key on the command line (refer
     * to the documentation).
     */
    UPROPERTY(EditAnywhere, Config, Category = "Networking")
    FString DedicatedServerPrivateKey;

    /**
     * The EOS client ID to use when acting as a player connected to a trusted dedicated server. The client ID and
     * secret you use for your dedicated server will not be able to have User Cloud enabled on the policy in the Epic
     * Games Developer Portal, so this is an alternate client ID and secret that will be used to perform operations on a
     * player's behalf. This value typically matches the client ID set for the game client above.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Networking",
        meta = (DisplayName = "Dedicated Server 'Act As' Client Id"))
    FString DedicatedServerActAsClientId;

    /**
     * The EOS client secret to use when acting as a player connected to a trusted dedicated server. The client ID and
     * secret you use for your dedicated server will not be able to have User Cloud enabled on the policy in the Epic
     * Games Developer Portal, so this is an alternate client ID and secret that will be used to perform operations on a
     * player's behalf. This value typically matches the client secret set for the game client above.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Networking",
        meta = (DisplayName = "Dedicated Server 'Act As' Client Secret"))
    FString DedicatedServerActAsClientSecret;

    /**
     * When enabled, the voice chat system will echo back audio input through your speakers so you can test voice chat
     * without setting up multiple machines.
     *
     * This option only applies to parties with voice chat enabled, and only takes effect in non-Shipping builds.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Voice Chat",
        meta = (DisplayName = "Enable Echo in Parties (Development Only)"))
    bool EnableVoiceChatEchoInParties;

    /**
     * When enabled, the voice chat system will enable noise cancellation by default, if SetSetting has not been called
     * on the IVoiceChatUser to explicitly state otherwise.
     */
    UPROPERTY(
        EditAnywhere,
        Config,
        Category = "Voice Chat",
        meta = (DisplayName = "Enable Platform AEC (Noise Cancellation) By Default"))
    bool EnableVoiceChatPlatformAECByDefault;
};

UCLASS(Config = Engine, DefaultConfig)
class UOnlineSubsystemEOSEditorConfigFreeEdition : public UOnlineSubsystemEOSEditorConfig
{
    GENERATED_BODY()

public:
    virtual void LoadEOSConfig() override;
    virtual void SaveEOSConfig() override;

    /**
     * The license key for EOS Online Subsystem (Free Edition). You can obtain a license key from
     * https://licensing.redpoint.games/get/eos-online-subsystem-free/
     */
    UPROPERTY(EditAnywhere, Config, Category = "Free Edition")
    FString FreeEditionLicenseKey;
};

class FOnlineSubsystemEOSEditorConfig
{
private:
    TWeakObjectPtr<UOnlineSubsystemEOSEditorConfig> Config;
    ISettingsSectionPtr SettingsSection;

    bool HandleSettingsSaved();

public:
    void RegisterSettings();
    void UnregisterSettings();
};
