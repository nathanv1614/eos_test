// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"

#include "OnlineSubsystemEOSEditorPrefs.generated.h"

/**
 * Editor preferences for Epic Online Services.
 */
UCLASS(Config = EditorPerProjectUserSettings, meta = (DisplayName = "Epic Online Services"))
class UOnlineSubsystemEOSEditorPrefs : public UDeveloperSettings
{
    GENERATED_BODY()

public:
    UOnlineSubsystemEOSEditorPrefs(const FObjectInitializer &ObjectInitializer);

    /**
     * If turned on, the EOS dropdown will not be visible in the editor toolbar.
     */
    UPROPERTY(EditAnywhere, Config, Category = "Toolbar")
    bool bHideDropdownInEditorToolbar;
};