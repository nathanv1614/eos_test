// Copyright June Rhodes. All Rights Reserved.

#include "RedpointEOSEditorModule.h"

#include "CoreGlobals.h"
#include "DesktopPlatform/Public/DesktopPlatformModule.h"
#include "DesktopPlatform/Public/IDesktopPlatform.h"
#include "DetailCustomizations.h"
#include "Editor.h"
#include "EditorDirectories.h"
#include "HAL/FileManager.h"
#include "ISettingsModule.h"
#include "Interfaces/IPluginManager.h"
#include "Interfaces/IProjectManager.h"
#include "LevelEditor.h"
#include "Misc/FeedbackContext.h"
#include "Misc/FileHelper.h"
#include "Misc/MessageDialog.h"
#include "Misc/ScopedSlowTask.h"
#include "ObjectEditorUtils.h"
#include "OnlineSubsystemEOSEditorCommands.h"
#include "OnlineSubsystemEOSEditorConfig.h"
#include "OnlineSubsystemEOSEditorConfigDetails.h"
#include "OnlineSubsystemEOSEditorPrefs.h"
#include "OnlineSubsystemEOSEditorStyle.h"
#include "OnlineSubsystemUtils.h"
#include "OnlineSubsystemUtils/Private/OnlinePIESettings.h"
#include "SWebBrowser.h"
#include "ToolMenus.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include <regex>
#include <string>
#if defined(EOS_IS_FREE_EDITION)
#include "HttpModule.h"
#include "Interfaces/IHttpResponse.h"
#include "Interfaces/IMainFrameModule.h"
#endif

DEFINE_LOG_CATEGORY(LogEOSEditor);

static const FName OnlineSubsystemEOSLicenseAgreementTabName("License Agreement");

#define LOCTEXT_NAMESPACE "OnlineSubsystemEOSEditorModule"

#if PLATFORM_WINDOWS || PLATFORM_MAC
bool bIsDevToolRunning = false;
bool bNeedsToCheckIfDevToolRunning = true;
FDelegateHandle ResetCheckHandle;
#endif

void FRedpointEOSEditorModule::StartupModule()
{
#if PLATFORM_WINDOWS || PLATFORM_MAC
    bIsDevToolRunning = false;
    bNeedsToCheckIfDevToolRunning = true;
    ResetCheckHandle.Reset();
#endif

    // Set up editor styles & setup window.
    SettingsInstance = MakeShared<FOnlineSubsystemEOSEditorConfig>();
    SettingsInstance->RegisterSettings();
    FOnlineSubsystemEOSEditorStyle::Initialize();
    FOnlineSubsystemEOSEditorStyle::ReloadTextures();
    FOnlineSubsystemEOSEditorCommands::Register();

    // Map commands to actions.
    PluginCommands = MakeShareable(new FUICommandList);
    PluginCommands->MapAction(
        FOnlineSubsystemEOSEditorCommands::Get().ViewDocumentation,
        FExecuteAction::CreateRaw(this, &FRedpointEOSEditorModule::ViewDocumentation),
        FCanExecuteAction());
#if defined(EOS_IS_FREE_EDITION)
    PluginCommands->MapAction(
        FOnlineSubsystemEOSEditorCommands::Get().ViewLicenseAgreementInBrowser,
        FExecuteAction::CreateRaw(this, &FRedpointEOSEditorModule::ViewLicenseAgreementInBrowser),
        FCanExecuteAction());
#else
    PluginCommands->MapAction(
        FOnlineSubsystemEOSEditorCommands::Get().AccessSupport,
        FExecuteAction::CreateRaw(this, &FRedpointEOSEditorModule::AccessSupport),
        FCanExecuteAction());
#endif
    PluginCommands->MapAction(
        FOnlineSubsystemEOSEditorCommands::Get().ViewWebsite,
        FExecuteAction::CreateRaw(this, &FRedpointEOSEditorModule::ViewWebsite),
        FCanExecuteAction());

    // When the toolbar menus startup, let us register our menu items.
    UToolMenus::RegisterStartupCallback(
        FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FRedpointEOSEditorModule::RegisterMenus));

#if PLATFORM_WINDOWS || PLATFORM_MAC
    FEditorDelegates::PreBeginPIE.AddLambda([](const bool bIsSimulating) {
        FRedpointEOSEditorModule::ResyncLoginsIfEnabled();
    });
#endif

#if defined(EOS_IS_FREE_EDITION)
    // Show the license agreement.
    if (!HasAcceptedLicenseAgreement())
    {
        IMainFrameModule &MainFrame = FModuleManager::LoadModuleChecked<IMainFrameModule>("MainFrame");
        if (MainFrame.IsWindowInitialized())
        {
            this->ViewLicenseAgreementInEditor();
        }
        else
        {
            MainFrame.OnMainFrameCreationFinished().AddLambda([this](TSharedPtr<SWindow>, bool) {
                this->ViewLicenseAgreementInEditor();
            });
        }
    }

    // Fetch latest version number.
    auto HttpRequest = FHttpModule::Get().CreateRequest();
    HttpRequest->SetURL(TEXT("https://licensing.redpoint.games/api/product-version/eos-online-subsystem-free"));
    HttpRequest->SetVerb(TEXT("GET"));
    HttpRequest->SetHeader(TEXT("Content-Length"), TEXT("0"));
    HttpRequest->OnProcessRequestComplete().BindLambda(
        [this](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) {
            if (HttpResponse.IsValid())
            {
                FString VersionNumberList = HttpResponse->GetContentAsString();

#if PLATFORM_WINDOWS
                FString VersionPath = FString(FPlatformMisc::GetEnvironmentVariable(TEXT("USERPROFILE"))) /
                                      ".eos-free-edition-latest-version";
#else
                FString VersionPath =
                    FString(FPlatformMisc::GetEnvironmentVariable(TEXT("HOME"))) / ".eos-free-edition-latest-version";
#endif
                FFileHelper::SaveStringToFile(VersionNumberList.TrimStartAndEnd(), *VersionPath);

#if defined(EOS_BUILD_VERSION_NAME)
                FString CurrentVersion = FString(EOS_BUILD_VERSION_NAME);
                TArray<FString> AllowedVersions;
                VersionNumberList.TrimStartAndEnd().ParseIntoArrayLines(AllowedVersions, true);
                if (AllowedVersions.Num() > 0)
                {
                    bool bAllowedVersionFound = false;
                    for (auto AllowedVersion : AllowedVersions)
                    {
                        if (AllowedVersion == CurrentVersion)
                        {
                            bAllowedVersionFound = true;
                            break;
                        }
                    }

                    if (!bAllowedVersionFound)
                    {
                        this->ShowUpgradePrompt();
                    }
                }
#endif
            }
        });
    HttpRequest->ProcessRequest();
#endif

    // Add custom detail for the EOS Online Subsystem project settings page.
    FPropertyEditorModule &PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
    PropertyModule.RegisterCustomClassLayout(
        "OnlineSubsystemEOSEditorConfig",
        FOnGetDetailCustomizationInstance::CreateStatic(&FOnlineSubsystemEOSEditorConfigDetails::MakeInstance));
}

void FRedpointEOSEditorModule::ShutdownModule()
{
    UToolMenus::UnRegisterStartupCallback(this);
    UToolMenus::UnregisterOwner(this);
    FOnlineSubsystemEOSEditorStyle::Shutdown();
    FOnlineSubsystemEOSEditorCommands::Unregister();
    SettingsInstance->UnregisterSettings();
}

void FRedpointEOSEditorModule::ViewDocumentation()
{
    FPlatformProcess::LaunchURL(TEXT("https://redpointgames.gitlab.io/eos-online-subsystem/docs/"), nullptr, nullptr);
}

void FRedpointEOSEditorModule::ViewWebsite()
{
    FPlatformProcess::LaunchURL(TEXT("https://redpointgames.gitlab.io/eos-online-subsystem/"), nullptr, nullptr);
}

#if defined(EOS_IS_FREE_EDITION)
void FRedpointEOSEditorModule::ViewLicenseAgreementInBrowser()
{
    if (!HasAcceptedLicenseAgreement())
    {
        // User hasn't accepted in editor yet, so they need to be able to click the I Accept button instead.
        ViewLicenseAgreementInEditor();
        return;
    }

    FPlatformProcess::LaunchURL(TEXT("https://redpoint.games/eos-online-subsystem-free-eula/"), nullptr, nullptr);
}

void FRedpointEOSEditorModule::ViewLicenseAgreementInEditor()
{
    if (this->LicenseAgreementWindow.IsValid())
    {
        // @todo: Figure out how to focus existing window.
        return;
    }

    if (!FSlateApplicationBase::IsInitialized())
    {
        // UI not available.
        return;
    }

    static FWindowStyle DialogStyle = FCoreStyle::Get().GetWidgetStyle<FWindowStyle>("Window");
    DialogStyle.SetBackgroundBrush(DialogStyle.ChildBackgroundBrush);

    this->LicenseAgreementLoaded = false;

    const FVector2D WindowSize(800.f, 600.f);
    this->LicenseAgreementWindow =
        SNew(SWindow)
            .Title(LOCTEXT("EOSLicenseAgreement", "EOS Online Subsystem (Free Edition): License Agreement"))
            .ClientSize(WindowSize)
            .HasCloseButton(false)
            .SupportsMinimize(false)
            .SupportsMaximize(false)
            .FocusWhenFirstShown(true)
            .AutoCenter(EAutoCenter::PreferredWorkArea)
            .Style(&DialogStyle);
    this->LicenseAgreementWindow->SetContent(
        SNew(SVerticalBox) +
        SVerticalBox::Slot().AutoHeight().Padding(FMargin(
            10,
            10,
            10,
            10))[SNew(STextBlock)
                     .Font(FSlateFontInfo(FPaths::EngineContentDir() / TEXT("Slate/Fonts/Roboto-Bold.ttf"), 16))
                     .Text(LOCTEXT("LicenseAgreementTitle", "Please review the license agreement"))] +
        SVerticalBox::Slot().AutoHeight().Padding(FMargin(
            10,
            0,
            10,
            10))[SNew(STextBlock)
                     .AutoWrapText(true)
                     .Text(LOCTEXT(
                         "LicenseAgreementPreamble",
                         "Before you can use EOS Online Subsystem (Free Edition), you must agree to the license."))] +
        SVerticalBox::Slot().FillHeight(1).Padding(
            FMargin(10, 0, 10, 10))[SNew(SWebBrowser)
                                        .InitialURL("https://redpoint.games/eos-online-subsystem-free-eula/")
                                        .ShowControls(false)
                                        .ShowAddressBar(false)
                                        .OnLoadCompleted_Lambda([this]() {
                                            this->LicenseAgreementLoaded = true;
                                        })] +
        SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 0, 10, 10))
            [SNew(STextBlock)
                 .AutoWrapText(true)
                 .Text(LOCTEXT("LicenseAgreementEnd", "If you agree to the above terms, click 'I accept' below."))] +
        SVerticalBox::Slot()
            .AutoHeight()
            .Padding(FMargin(10, 0, 10, 10))
            .HAlign(EHorizontalAlignment::HAlign_Fill)
                [SNew(SHorizontalBox) +
                 SHorizontalBox::Slot().AutoWidth().Padding(FMargin(0, 0, 10, 0))
                     [SNew(SButton)
                          .ContentPadding(FMargin(10, 5))
                          .Text(LOCTEXT("LicenseAgreementAccept", "I accept"))
                          .IsEnabled_Lambda([this]() {
                              return this->LicenseAgreementLoaded;
                          })
                          .OnClicked(FOnClicked::CreateRaw(this, &FRedpointEOSEditorModule::AcceptLicenseTerms))] +
                 SHorizontalBox::Slot().HAlign(EHorizontalAlignment::HAlign_Fill) +
                 SHorizontalBox::Slot().AutoWidth().HAlign(EHorizontalAlignment::HAlign_Right)
                     [SNew(SButton)
                          .ContentPadding(FMargin(10, 5))
                          .Text(LOCTEXT("LicenseAgreementDoNotAccept", "I do not accept (Quit UE4 Editor)"))
                          .OnClicked(FOnClicked::CreateRaw(this, &FRedpointEOSEditorModule::RejectLicenseTerms))]]);

    this->LicenseAgreementWindow->SetOnWindowClosed(FOnWindowClosed::CreateLambda([this](const TSharedRef<SWindow> &) {
        this->LicenseAgreementWindow.Reset();
    }));

    FSlateApplication::Get().AddWindow(this->LicenseAgreementWindow.ToSharedRef(), true);
}

FReply FRedpointEOSEditorModule::AcceptLicenseTerms()
{
    if (this->LicenseAgreementWindow.IsValid())
    {
#if PLATFORM_WINDOWS
        FString FlagPath =
            FString(FPlatformMisc::GetEnvironmentVariable(TEXT("USERPROFILE"))) / ".eos-free-edition-agreed";
#else
        FString FlagPath = FString(FPlatformMisc::GetEnvironmentVariable(TEXT("HOME"))) / ".eos-free-edition-agreed";
#endif
        FFileHelper::SaveStringToFile(TEXT("Accepted-v1"), *FlagPath);

        this->LicenseAgreementWindow->RequestDestroyWindow();
    }

    return FReply::Handled();
}

FReply FRedpointEOSEditorModule::RejectLicenseTerms()
{
    // Close the editor, as the user is not agreeing to the license terms.
    FGenericPlatformMisc::RequestExit(false);

    return FReply::Handled();
}

bool FRedpointEOSEditorModule::HasAcceptedLicenseAgreement()
{
#if PLATFORM_WINDOWS
    FString FlagPath = FString(FPlatformMisc::GetEnvironmentVariable(TEXT("USERPROFILE"))) / ".eos-free-edition-agreed";
#else
    FString FlagPath = FString(FPlatformMisc::GetEnvironmentVariable(TEXT("HOME"))) / ".eos-free-edition-agreed";
#endif
    return FPaths::FileExists(FlagPath);
}

void FRedpointEOSEditorModule::ShowUpgradePrompt()
{
    if (this->UpgradePromptWindow.IsValid())
    {
        // @todo: Figure out how to focus existing window.
        return;
    }

    if (!FSlateApplicationBase::IsInitialized())
    {
        // UI not available.
        return;
    }

    static FWindowStyle DialogStyle = FCoreStyle::Get().GetWidgetStyle<FWindowStyle>("Window");
    DialogStyle.SetBackgroundBrush(DialogStyle.ChildBackgroundBrush);

    const FVector2D WindowSize(500.f, 105.f);
    this->UpgradePromptWindow = SNew(SWindow)
                                    .Title(LOCTEXT("EOSUpgradeRequired", "EOS Upgrade Required"))
                                    .ClientSize(WindowSize)
                                    .SupportsMaximize(false)
                                    .FocusWhenFirstShown(true)
                                    .AutoCenter(EAutoCenter::PreferredWorkArea)
                                    .Style(&DialogStyle);
    this->UpgradePromptWindow->SetContent(
        SNew(SVerticalBox) +
        SVerticalBox::Slot().AutoHeight().Padding(FMargin(10, 10, 10, 10))
            [SNew(STextBlock)
                 .LineHeightPercentage(1.1f)
                 .Text(LOCTEXT(
                     "UpgradeRequiredDescription",
                     "Your version of EOS Online Subsystem (Free Edition) is too old. Please upgrade to the\nlatest "
                     "version. This version check only applies in the editor, and does not apply to\npackaged "
                     "games."))] +
        SVerticalBox::Slot()
            .AutoHeight()
            .Padding(FMargin(10, 0, 10, 10))
            .HAlign(EHorizontalAlignment::HAlign_Fill)
                [SNew(SHorizontalBox) +
                 SHorizontalBox::Slot().AutoWidth().Padding(FMargin(0, 0, 10, 0))
                     [SNew(SButton)
                          .ContentPadding(FMargin(10, 5))
                          .Text(LOCTEXT("UpgradeRequiredDownload", "Download latest version"))
                          .OnClicked(FOnClicked::CreateRaw(this, &FRedpointEOSEditorModule::FollowUpgradePrompt))]]);

    this->UpgradePromptWindow->SetOnWindowClosed(FOnWindowClosed::CreateLambda([this](const TSharedRef<SWindow> &) {
        this->UpgradePromptWindow.Reset();
    }));

    FSlateApplication::Get().AddWindow(this->UpgradePromptWindow.ToSharedRef(), true);
}

FReply FRedpointEOSEditorModule::FollowUpgradePrompt()
{
    FPlatformProcess::LaunchURL(
        TEXT("https://licensing.redpoint.games/get/eos-online-subsystem-free"),
        nullptr,
        nullptr);

    return FReply::Handled();
}
#else
void FRedpointEOSEditorModule::AccessSupport()
{
    FPlatformProcess::LaunchURL(
        TEXT("https://redpointgames.gitlab.io/eos-online-subsystem/docs/accessing_support"),
        nullptr,
        nullptr);
}
#endif

TSharedRef<SWidget> FRedpointEOSEditorModule::GenerateOnlineSettingsMenu(
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FUICommandList> InCommandList)
{
    FToolMenuContext MenuContext(InCommandList);

    return UToolMenus::Get()->GenerateWidget(
        "LevelEditor.LevelEditorToolBar.LevelToolbarOnlineSubsystemEOS",
        MenuContext);
}

#if PLATFORM_WINDOWS || PLATFORM_MAC
bool IsDevAuthToolRunning()
{
    // If the project is configured to use a remote host (not localhost) as the developer
    // tool address, then we assume it is running on the remote machine.
    // NOTE: We could try to do full host:port parsing here, but it's complicated
    // with IPv6 and Unreal doesn't have any good built-in utilities to do it.
    if (!FEOSConfigIni().GetDeveloperToolAddress().StartsWith("localhost:"))
    {
        return true;
    }

    if (!bNeedsToCheckIfDevToolRunning)
    {
        return bIsDevToolRunning;
    }

    ResetCheckHandle = FTicker::GetCoreTicker().AddTicker(
        FTickerDelegate::CreateLambda([](float DeltaTime) {
            bNeedsToCheckIfDevToolRunning = true;
            return false;
        }),
        10.0f);

#if PLATFORM_WINDOWS
    UE_LOG(LogEOSEditor, Verbose, TEXT("Checking if EOS_DevAuthTool.exe process is running"));
    bIsDevToolRunning = FPlatformProcess::IsApplicationRunning(TEXT("EOS_DevAuthTool.exe"));
#else
    UE_LOG(LogEOSEditor, Verbose, TEXT("Checking if EOS_DevAuthTool process is running"));
    bIsDevToolRunning = FPlatformProcess::IsApplicationRunning(TEXT("EOS_DevAuthTool"));
#endif
    bNeedsToCheckIfDevToolRunning = false;

    return bIsDevToolRunning;
}

FString GetPathToToolsFolder()
{
    TArray<FString> SearchPaths;
    FString BaseDir = IPluginManager::Get().FindPlugin("OnlineSubsystemRedpointEOS")->GetBaseDir();

    FString ToolsFolder = FString(TEXT("Tools"));

#if PLATFORM_WINDOWS
    FString CommonAppData = FPlatformMisc::GetEnvironmentVariable(TEXT("PROGRAMDATA"));
    FString UserProfile = FPlatformMisc::GetEnvironmentVariable(TEXT("USERPROFILE"));
    FString SystemDrive = FPlatformMisc::GetEnvironmentVariable(TEXT("SYSTEMDRIVE"));

    SearchPaths.Add(FPaths::ConvertRelativePathToFull(BaseDir / TEXT("Source") / TEXT("ThirdParty") / ToolsFolder));
    SearchPaths.Add(FPaths::ConvertRelativePathToFull(
        BaseDir / TEXT("..") / TEXT("..") / TEXT("EOS-SDK-" EOS_SDK_LINK_VERSION) / TEXT("SDK") / TEXT("Bin")));
    if (!CommonAppData.IsEmpty())
    {
        SearchPaths.Add(CommonAppData / TEXT("EOS-SDK-" EOS_SDK_LINK_VERSION) / TEXT("SDK") / ToolsFolder);
    }
    if (!UserProfile.IsEmpty())
    {
        SearchPaths.Add(
            UserProfile / TEXT("Downloads") / TEXT("EOS-SDK-" EOS_SDK_LINK_VERSION) / TEXT("SDK") / ToolsFolder);
    }
    if (!SystemDrive.IsEmpty())
    {
        SearchPaths.Add(SystemDrive / TEXT("EOS-SDK-" EOS_SDK_LINK_VERSION) / TEXT("SDK") / ToolsFolder);
    }
#else
    FString Home = FPlatformMisc::GetEnvironmentVariable(TEXT("HOME"));

    SearchPaths.Add(FPaths::ConvertRelativePathToFull(BaseDir / TEXT("Source") / TEXT("ThirdParty") / ToolsFolder));
    if (!Home.IsEmpty())
    {
        SearchPaths.Add(Home / TEXT("Downloads") / TEXT("EOS-SDK-" EOS_SDK_LINK_VERSION) / TEXT("SDK") / ToolsFolder);
    }
#endif

    for (const auto &SearchPath : SearchPaths)
    {
#if PLATFORM_WINDOWS
        if (FPaths::FileExists(SearchPath / TEXT("EOS_DevAuthTool-win32-x64-1.0.1") / TEXT("EOS_DevAuthTool.exe")))
        {
            return SearchPath.Replace(TEXT("/"), TEXT("\\"));
        }
        if (FPaths::FileExists(SearchPath / TEXT("EOS_DevAuthTool-win32-x64-1.0.1.zip")))
        {
            return SearchPath.Replace(TEXT("/"), TEXT("\\"));
        }
#else
        if (FPaths::FileExists(
                SearchPath / TEXT("EOS_DevAuthTool-darwin-x64-1.0.1") / TEXT("EOS_DevAuthTool.app") / TEXT("Contents") /
                TEXT("MacOS") / TEXT("EOS_DevToolApp")))
        {
            return SearchPath;
        }
        if (FPaths::FileExists(SearchPath / TEXT("EOS_DevAuthTool-darwin-x64-1.0.1.zip")))
        {
            return SearchPath;
        }
#endif
    }

    return TEXT("");
}
#endif

void FRedpointEOSEditorModule::RegisterMenus()
{
    // Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
    FToolMenuOwnerScoped OwnerScoped(this);

    {
        UToolMenu *Menu =
            UToolMenus::Get()->RegisterMenu("LevelEditor.LevelEditorToolBar.LevelToolbarOnlineSubsystemEOS");

        struct Local
        {
            static void OpenSettings(FName ContainerName, FName CategoryName, FName SectionName)
            {
                FModuleManager::LoadModuleChecked<ISettingsModule>("Settings")
                    .ShowViewer(ContainerName, CategoryName, SectionName);
            }
        };

        {
            FToolMenuSection &Section =
                Menu->AddSection("OnlineSection", LOCTEXT("OnlineSection", "Epic Online Services"));

            Section.AddMenuEntry(
                "OnlineSettings",
                LOCTEXT("OnlineSettingsMenuLabel", "Settings..."),
                LOCTEXT("OnlineSettingsMenuToolTip", "Configure the EOS Online Subsystem plugin."),
                FSlateIcon(),
                FUIAction(FExecuteAction::CreateStatic(
                    &Local::OpenSettings,
                    FName("Project"),
                    FName("Game"),
                    FName("Epic Online Services"))));
        }

#if PLATFORM_WINDOWS || PLATFORM_MAC
        {
            FToolMenuSection &Section =
                Menu->AddSection("AuthenticationSection", LOCTEXT("AuthenticationSection", "Authentication"));
            Section.AddEntry(FToolMenuEntry::InitMenuEntry(
                "StartDevAuthTool",
                LOCTEXT("StartDevAuthTool", "Start Developer Authentication Tool"),
                LOCTEXT(
                    "StartDevAuthToolTooltip",
                    "Launches the Developer Authentication Tool, which can be used to automatically sign each "
                    "play-in-editor instance into a different Epic Games account."),
                FSlateIcon(),
                FUIAction(
                    FExecuteAction::CreateLambda([]() {
                        FString Path = ::GetPathToToolsFolder();
                        if (!Path.IsEmpty())
                        {
#if PLATFORM_WINDOWS
                            FString ExePath =
                                Path / TEXT("EOS_DevAuthTool-win32-x64-1.0.1") / TEXT("EOS_DevAuthTool.exe");
                            FString ExeDirPath = Path / TEXT("EOS_DevAuthTool-win32-x64-1.0.1");
                            FString ZipPath = Path / TEXT("EOS_DevAuthTool-win32-x64-1.0.1.zip");
#else
                            FString ExePath = Path / TEXT("EOS_DevAuthTool.app") / TEXT("Contents") / TEXT("MacOS") /
                                              TEXT("EOS_DevAuthTool");
                            FString ExeDirPath = Path;
                            FString ZipPath = Path / TEXT("EOS_DevAuthTool-darwin-x64-1.0.1.zip");
#endif
                            if (!FPaths::FileExists(ExePath))
                            {
                                if (!FPaths::FileExists(ZipPath))
                                {
                                    UE_LOG(LogEOSEditor, Error, TEXT("Have path to ZIP, but ZIP doesn't exist"));
                                    return;
                                }

                                FText ExtractTitle = LOCTEXT("DevToolPromptTitle", "Extraction required");
                                FMessageDialog::Open(
                                    EAppMsgType::Ok,
                                    LOCTEXT(
                                        "DevToolExtraction",
                                        "The Developer Authentication Tool will now be extracted from the SDK."),
                                    &ExtractTitle);

#if PLATFORM_WINDOWS
                                FString ExtractionDir = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(
                                    *((IPluginManager::Get().FindPlugin("OnlineSubsystemRedpointEOS")->GetBaseDir() /
                                       TEXT("Resources"))
                                          .Replace(TEXT("/"), TEXT("\\"))));
                                FString ExtractionBat = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(
                                    *((ExtractionDir / TEXT("ExtractDevAuthTool.bat")).Replace(TEXT("/"), TEXT("\\"))));
#else
                                FString ExtractionDir = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(
                                    *((IPluginManager::Get().FindPlugin("OnlineSubsystemRedpointEOS")->GetBaseDir() /
                                       TEXT("Resources"))));
                                FString ExtractionBat = IFileManager::Get().ConvertToAbsolutePathForExternalAppForRead(
                                    *((ExtractionDir / TEXT("ExtractDevAuthTool.command"))));
#endif

                                UE_LOG(LogEOSEditor, Verbose, TEXT("Executing: %s"), *ExtractionBat);
                                UE_LOG(LogEOSEditor, Verbose, TEXT("With arguments: %s"), *Path);
                                UE_LOG(LogEOSEditor, Verbose, TEXT("In working directory: %s"), *ExtractionDir);

                                int32 ReturnCode;
                                FString Stdout;
                                FString Stderr;
#if PLATFORM_WINDOWS
                                FPlatformProcess::ExecProcess(
                                    *ExtractionBat,
                                    *Path,
                                    &ReturnCode,
                                    &Stdout,
                                    &Stderr,
                                    *ExtractionDir);
#else
                                FString BashArgs = FString::Printf(TEXT("%s \"%s\""), *ExtractionBat, *Path);
                                FPlatformProcess::ExecProcess(
                                    TEXT("/bin/bash"),
                                    *BashArgs,
                                    &ReturnCode,
                                    &Stdout,
                                    &Stderr,
                                    *ExtractionDir);
#endif
                                UE_LOG(LogEOSEditor, Verbose, TEXT("ZIP extraction return code: %d"), ReturnCode);
                                UE_LOG(LogEOSEditor, Verbose, TEXT("ZIP extraction stdout: %s"), *Stdout);
                                UE_LOG(LogEOSEditor, Verbose, TEXT("ZIP extraction stderr: %s"), *Stderr);

                                if (!FPaths::FileExists(ExePath))
                                {
                                    UE_LOG(
                                        LogEOSEditor,
                                        Error,
                                        TEXT("ZIP extraction failed to produce executable at path: %s"),
                                        *ExePath);
                                    return;
                                }
                            }

                            FText StartTitle =
                                LOCTEXT("DevToolPromptTitle", "How to use the Developer Authentication Tool");
                            FMessageDialog::Open(
                                EAppMsgType::Ok,
                                LOCTEXT(
                                    "DevToolPrompt",
                                    "When prompted, use port 6300.\n\n"
                                    "Name each credential Context_1, Context_2, etc.\n\n"
                                    "Do not use the same Epic Games account "
                                    "more than once.\n\n"
                                    "For more information, check the documentation."),
                                &StartTitle);

                            UE_LOG(
                                LogEOSEditor,
                                Verbose,
                                TEXT("Launching Developer Authentication Tool located at: %s"),
                                *ExePath);
                            uint32 ProcID;
#if PLATFORM_WINDOWS
                            FPlatformProcess::CreateProc(
                                *ExePath,
                                TEXT("-port 6300"),
                                true,
                                false,
                                false,
                                &ProcID,
                                0,
                                *ExeDirPath,
                                nullptr,
                                nullptr);
#else
                            FPlatformProcess::CreateProc(
                                TEXT("/usr/bin/open"),
                                TEXT("-a EOS_DevAuthTool"),
                                true,
                                false,
                                false,
                                &ProcID,
                                0,
                                *ExeDirPath,
                                nullptr,
                                nullptr);
#endif
                            ResetCheckHandle = FTicker::GetCoreTicker().AddTicker(
                                FTickerDelegate::CreateLambda([](float DeltaTime) {
                                    bNeedsToCheckIfDevToolRunning = true;
                                    return false;
                                }),
                                10.0f);
                            bIsDevToolRunning = true;
                            bNeedsToCheckIfDevToolRunning = false;
                        }
                    }),
                    FCanExecuteAction::CreateLambda([]() {
                        return !::IsDevAuthToolRunning() && !::GetPathToToolsFolder().IsEmpty();
                    }),
                    FGetActionCheckState(),
                    FIsActionButtonVisible()),
                EUserInterfaceActionType::Button,
                "StartDevAuthToolEAS"));
            Section.AddEntry(FToolMenuEntry::InitMenuEntry(
                "ToggleAutoLogin",
                LOCTEXT("AutoLogin", "Login before play-in-editor"),
                LOCTEXT(
                    "AutoLoginTooltip",
                    "If enabled, the editor will authenticate each play-in-editor instance against the Developer "
                    "Authentication Tool before play starts. This is recommended if you are testing anything other "
                    "than your login screen."),
                FSlateIcon(),
                FUIAction(
                    FExecuteAction::CreateLambda([]() {
                        IOnlineSubsystemUtils *Utils = Online::GetUtils();
                        FRedpointEOSEditorModule::SetLoginsEnabled(!Utils->IsOnlinePIEEnabled());
                        FRedpointEOSEditorModule::ResyncLoginsIfEnabled();
                    }),
                    FCanExecuteAction::CreateLambda([]() {
                        return ::IsDevAuthToolRunning();
                    }),
                    FGetActionCheckState::CreateLambda([]() {
                        IOnlineSubsystemUtils *Utils = Online::GetUtils();
                        return (Utils->IsOnlinePIEEnabled() && ::IsDevAuthToolRunning()) ? ECheckBoxState::Checked
                                                                                         : ECheckBoxState::Unchecked;
                    }),
                    FIsActionButtonVisible()),
                EUserInterfaceActionType::ToggleButton,
                "ToggleAutoLoginEAS"));
        }
#endif

        {
            FToolMenuSection &Section =
                Menu->AddSection("PluginSection", LOCTEXT("PluginSection", "EOS Online Subsystem Plugin"));

            Section.AddMenuEntry(FOnlineSubsystemEOSEditorCommands::Get().ViewDocumentation);
#if defined(EOS_IS_FREE_EDITION)
            Section.AddMenuEntry(FOnlineSubsystemEOSEditorCommands::Get().ViewLicenseAgreementInBrowser);
#else
            Section.AddMenuEntry(FOnlineSubsystemEOSEditorCommands::Get().AccessSupport);
#endif
            Section.AddMenuEntry(FOnlineSubsystemEOSEditorCommands::Get().ViewWebsite);
        }
    }

    {
#if defined(UE_5_0_OR_LATER)
        UToolMenu *Toolbar = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar.SettingsToolbar");
#else
        UToolMenu *Toolbar = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
#endif
        {
            FToolMenuSection &SettingsSection = Toolbar->AddSection("Settings");
            {
                FUIAction Act;
                Act.IsActionVisibleDelegate = FIsActionButtonVisible::CreateLambda([]() {
                    const UOnlineSubsystemEOSEditorPrefs *Prefs = GetDefault<UOnlineSubsystemEOSEditorPrefs>();
                    return !Prefs || !Prefs->bHideDropdownInEditorToolbar;
                });
                FToolMenuEntry ComboButton = FToolMenuEntry::InitComboButton(
                    "LevelToolbarOnlineSubsystemEOS",
                    Act,
                    FOnGetContent::CreateStatic(
                        &FRedpointEOSEditorModule::GenerateOnlineSettingsMenu,
                        PluginCommands.ToSharedRef()),
#if defined(UE_5_0_OR_LATER)
                    LOCTEXT("SettingsCombo", "Epic Online Services"),
#else
                    LOCTEXT("SettingsCombo", "Online (EOS)"),
#endif
                    LOCTEXT("SettingsCombo_ToolTip", "Manage Epic Online Services."),
                    FSlateIcon(FOnlineSubsystemEOSEditorStyle::GetStyleSetName(), "LevelEditor.OnlineSettings"),
                    false,
                    "LevelToolbarOnlineSubsystemEOS");
#if defined(UE_5_0_OR_LATER)
                ComboButton.StyleNameOverride = "CalloutToolbar";
#endif
                SettingsSection.AddEntry(ComboButton);
            }
        }
    }
}

#if PLATFORM_WINDOWS || PLATFORM_MAC
void FRedpointEOSEditorModule::SetLoginsEnabled(bool bEnabled)
{
    IOnlineSubsystemUtils *Utils = Online::GetUtils();
    Utils->SetShouldTryOnlinePIE(true);

    UE_LOG(LogEOSEditor, Verbose, TEXT("Setting PIE logins enabled to %s"), bEnabled ? TEXT("true") : TEXT("false"));

    UClass *PIESettingsClass = FindObject<UClass>(ANY_PACKAGE, TEXT("OnlinePIESettings"));
    UObject *PIESettings = PIESettingsClass->GetDefaultObject(true);

#if defined(UE_4_25_OR_LATER)
    FProperty *Prop = PIESettingsClass->FindPropertyByName("bOnlinePIEEnabled");
#else
    UProperty *Prop = PIESettingsClass->FindPropertyByName("bOnlinePIEEnabled");
#endif
    bool *SourceAddr = Prop->ContainerPtrToValuePtr<bool>(PIESettings);
    *SourceAddr = bEnabled;
}

void FRedpointEOSEditorModule::ResyncLoginsIfEnabled()
{
    const ULevelEditorPlaySettings *PlayInSettings = GetDefault<ULevelEditorPlaySettings>();
    int32 PlayNumberOfClients(0);
    if (!PlayInSettings->GetPlayNumberOfClients(PlayNumberOfClients))
    {
        return;
    }

    IOnlineSubsystemUtils *Utils = Online::GetUtils();
    if (Utils->IsOnlinePIEEnabled())
    {
        if (!::IsDevAuthToolRunning())
        {
            UE_LOG(
                LogEOSEditor,
                Verbose,
                TEXT("Turning off PIE automatic login because the Developer Authentication Tool is not running"));
            Utils->SetShouldTryOnlinePIE(false);
            return;
        }

        UE_LOG(
            LogEOSEditor,
            Verbose,
            TEXT("Synchronising automatic login credentials because PIE automatic login is enabled"));

        UClass *PIESettingsClass = FindObject<UClass>(ANY_PACKAGE, TEXT("OnlinePIESettings"));
        UObject *PIESettings = PIESettingsClass->GetDefaultObject(true);

#if defined(UE_4_25_OR_LATER)
        FProperty *Prop = PIESettingsClass->FindPropertyByName("Logins");
#else
        UProperty *Prop = PIESettingsClass->FindPropertyByName("Logins");
#endif
        TArray<FPIELoginSettingsInternal> *Arr =
            Prop->ContainerPtrToValuePtr<TArray<FPIELoginSettingsInternal>>(PIESettings);

        while (Arr->Num() < PlayNumberOfClients)
        {
            FPIELoginSettingsInternal Settings;
            Settings.Id = TEXT("DEV_TOOL_AUTO_LOGIN");
            Settings.Token = TEXT("DEV_TOOL_AUTO_LOGIN");
            Settings.Type = TEXT("DEV_TOOL_AUTO_LOGIN");
            Arr->Add(Settings);
        }
        while (Arr->Num() > PlayNumberOfClients)
        {
            Arr->RemoveAt(Arr->Num() - 1);
        }
    }
}
#endif

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FRedpointEOSEditorModule, RedpointEOSEditor)