// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemEOSEditorConfig.h"

#include "ISettingsContainer.h"
#include "ISettingsModule.h"
#include "Misc/Base64.h"
#include "Misc/ConfigCacheIni.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "Modules/ModuleManager.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSDefines.h"
#include "PlatformHelpers.h"
#include "RedpointLibHydrogen.h"
#include "UObject/UObjectGlobals.h"
#include "Widgets/Text/STextBlock.h"

#define LOCTEXT_NAMESPACE "OnlineSubsystemEOSEditorModule"

#define EOS_SECTION TEXT("EpicOnlineServices")

UOnlineSubsystemEOSEditorConfig::UOnlineSubsystemEOSEditorConfig()
{
    // NOTE: These default values need to be kept in sync with EOSConfig.cpp.
    this->ProductName = TEXT("Product Name Not Set");
    this->ProductVersion = TEXT("0.0.0");
    this->PlayerDataEncryptionKey = TEXT("4dc4ad8a46823586f4044225d6cf1b1e7ee32b3d7dff1c63b6ad5807671c4a3f");
    this->DevAuthToolAddress = TEXT("localhost:6300");
    this->DevAuthToolDefaultCredentialName = TEXT("Context_1");
    this->AuthenticationGraph = NAME_Default;
    this->EditorAuthenticationGraph = NAME_Default;
    this->CrossPlatformAccountProvider = NAME_None;
    this->RequireCrossPlatformAccount = false;
    this->AllowNativePlatformAccounts = true;
    this->AllowDeviceIdAccounts = false;
    this->DeleteDeviceIdOnLogout = false;
    this->DisablePersistentLogin = false;
    this->PresenceAdvertises = EPresenceAdvertisementType::Party;
    this->ApiVersion = (EEOSApiVersion)0;
    this->NetworkingStack = EEOSNetworkingStack::Default;
    this->RequireEpicGamesLauncher = false;
    this->SimpleFirstPartyLoginUrl = TEXT("");
    this->PartyJoinabilityConstraint = EPartyJoinabilityConstraint::AllowPlayersInMultipleParties;

    this->EnableAntiCheat = false;
    this->TrustedPlatforms.Empty();
    this->TrustedClientPrivateKey.Empty();
    this->TrustedClientPublicKey.Empty();
    this->EnableSanctionChecks = false;
    this->EnableIdentityChecksOnListenServers = true;
    this->DedicatedServerDistributionMode = EDedicatedServersDistributionMode::DevelopersAndPlayers;
    this->EnableTrustedDedicatedServers = true;
    this->EnableAutomaticEncryptionOnTrustedDedicatedServers = true;

    this->EnableVoiceChatEchoInParties = false;
    this->EnableVoiceChatPlatformAECByDefault = false;

    // NOTE: These defaults need to be kept in sync with the authentication graph.
    this->WidgetClass_SelectEOSAccount = FSoftClassPath(
        FString(TEXT("/OnlineSubsystemRedpointEOS/"
                     "EOSDefaultUserInterface_SelectEOSAccount.EOSDefaultUserInterface_SelectEOSAccount_C")));
    this->WidgetClass_LinkEOSAccountsAgainstCrossPlatform = FSoftClassPath(
        FString(TEXT("/OnlineSubsystemRedpointEOS/"
                     "EOSDefaultUserInterface_LinkEOSAccountsAgainstCrossPlatform.EOSDefaultUserInterface_"
                     "LinkEOSAccountsAgainstCrossPlatform_C")));
    this->WidgetClass_SwitchToCrossPlatformAccount =
        FSoftClassPath(FString(TEXT("/OnlineSubsystemRedpointEOS/"
                                    "EOSDefaultUserInterface_SwitchToCrossPlatformAccount.EOSDefaultUserInterface_"
                                    "SwitchToCrossPlatformAccount_C")));
    this->WidgetClass_EnterDevicePinCode = FSoftClassPath(
        FString(TEXT("/OnlineSubsystemRedpointEOS/"
                     "EOSDefaultUserInterface_EnterDevicePinCode.EOSDefaultUserInterface_EnterDevicePinCode_C")));
    this->WidgetClass_SignInOrCreateAccount =
        FSoftClassPath(FString(TEXT("/OnlineSubsystemRedpointEOS/"
                                    "EOSDefaultUserInterface_SignInOrCreateAccount.EOSDefaultUserInterface_"
                                    "SignInOrCreateAccount_C")));

    this->LoadEOSConfig();
}

void UOnlineSubsystemEOSEditorConfig::LoadEOSConfig()
{
    auto Filename = this->GetDefaultConfigFilename();

    auto EditorFilename = Filename / TEXT("..") / TEXT("DefaultEditor.ini");

    FString Value;
    bool bValue;
    if (GConfig->GetString(EOS_SECTION, TEXT("ProductName"), Value, Filename))
    {
        this->ProductName = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ProductVersion"), Value, Filename))
    {
        this->ProductVersion = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ProductId"), Value, Filename))
    {
        this->ProductId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("SandboxId"), Value, Filename))
    {
        this->SandboxId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DeploymentId"), Value, Filename))
    {
        this->DeploymentId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ClientId"), Value, Filename))
    {
        this->ClientId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ClientSecret"), Value, Filename))
    {
        this->ClientSecret = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("AuthenticationGraph"), Value, Filename))
    {
        this->AuthenticationGraph = FName(*Value);
    }
    else if (GConfig->GetString(EOS_SECTION, TEXT("AuthenticationBehaviour"), Value, Filename))
    {
        // We are migrating this project from the old AuthenticationBehaviour value to the new settings.
        if (Value == "NoEAS")
        {
            this->CrossPlatformAccountProvider = NAME_None;
            this->RequireCrossPlatformAccount = false;
        }
        else if (Value == "EASOptional")
        {
            this->CrossPlatformAccountProvider = FName(TEXT("EpicGames"));
            this->RequireCrossPlatformAccount = false;
        }
        else if (Value == "EASRequired")
        {
            this->CrossPlatformAccountProvider = FName(TEXT("EpicGames"));
            this->RequireCrossPlatformAccount = true;
        }
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("EditorAuthenticationGraph"), Value, Filename))
    {
        this->EditorAuthenticationGraph = FName(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("CrossPlatformAccountProvider"), Value, Filename))
    {
        this->CrossPlatformAccountProvider = FName(*Value);
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("RequireCrossPlatformAccount"), bValue, Filename))
    {
        this->RequireCrossPlatformAccount = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("AllowDeviceIdAccounts"), bValue, Filename))
    {
        this->AllowDeviceIdAccounts = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("AllowNativePlatformAccounts"), bValue, Filename))
    {
        this->AllowNativePlatformAccounts = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("DeleteDeviceIdOnLogout"), bValue, Filename))
    {
        this->DeleteDeviceIdOnLogout = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("DisablePersistentLogin"), bValue, Filename))
    {
        this->DisablePersistentLogin = bValue;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DevAuthToolAddress"), Value, Filename))
    {
        this->DevAuthToolAddress = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DevAuthToolDefaultCredentialName"), Value, Filename))
    {
        this->DevAuthToolDefaultCredentialName = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("PlayerDataEncryptionKey"), Value, Filename))
    {
        this->PlayerDataEncryptionKey = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("PresenceAdvertises"), Value, Filename))
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EPresenceAdvertisementType"), true);
        this->PresenceAdvertises =
            (EPresenceAdvertisementType)Enum->GetValueByName(FName(*Value), EGetByNameFlags::ErrorIfNotFound);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DelegatedSubsystems"), Value, Filename))
    {
        Value.ParseIntoArray(this->DelegatedSubsystems, TEXT(","), true);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ApiVersion"), Value, Filename))
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EEOSApiVersion"), true);
        this->ApiVersion = (EEOSApiVersion)Enum->GetValueByName(FName(*Value), EGetByNameFlags::ErrorIfNotFound);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_SelectEOSAccount"), Value, Filename))
    {
        this->WidgetClass_SelectEOSAccount = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_LinkEOSAccountsAgainstEAS"), Value, Filename))
    {
        this->WidgetClass_LinkEOSAccountsAgainstCrossPlatform = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_LinkEOSAccountsAgainstCrossPlatform"), Value, Filename))
    {
        this->WidgetClass_LinkEOSAccountsAgainstCrossPlatform = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_SwitchToEASAccount"), Value, Filename))
    {
        this->WidgetClass_SwitchToCrossPlatformAccount = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_SwitchToCrossPlatformAccount"), Value, Filename))
    {
        this->WidgetClass_SwitchToCrossPlatformAccount = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_EnterDevicePinCode"), Value, Filename))
    {
        this->WidgetClass_EnterDevicePinCode = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("WidgetClass_SignInOrCreateAccount"), Value, Filename))
    {
        this->WidgetClass_SignInOrCreateAccount = FSoftClassPath(*Value);
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("NetworkingStack"), Value, Filename))
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EEOSNetworkingStack"), true);
        this->NetworkingStack =
            (EEOSNetworkingStack)Enum->GetValueByName(FName(*Value), EGetByNameFlags::ErrorIfNotFound);
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("RequireEpicGamesLauncher"), bValue, Filename))
    {
        this->RequireEpicGamesLauncher = bValue;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("SimpleFirstPartyLoginUrl"), Value, Filename))
    {
        this->SimpleFirstPartyLoginUrl = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("PartyJoinabilityConstraint"), Value, Filename))
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EPartyJoinabilityConstraint"), true);
        this->PartyJoinabilityConstraint =
            (EPartyJoinabilityConstraint)Enum->GetValueByName(FName(*Value), EGetByNameFlags::ErrorIfNotFound);
    }

    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableVoiceChatEchoInParties"), bValue, Filename))
    {
        this->EnableVoiceChatEchoInParties = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableVoiceChatPlatformAECByDefault"), bValue, Filename))
    {
        this->EnableVoiceChatPlatformAECByDefault = bValue;
    }

    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableAntiCheat"), bValue, Filename))
    {
        this->EnableAntiCheat = bValue;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("TrustedClientPublicKey"), Value, Filename))
    {
        this->TrustedClientPublicKey = Value;
    }
    auto TrustedClientEngineIni =
        Filename / TEXT("..") / TEXT("..") / TEXT("Build") / TEXT("NoRedist") / TEXT("TrustedEOSClient.ini");
    if (GConfig->GetString(EOS_SECTION, TEXT("TrustedClientPrivateKey"), Value, TrustedClientEngineIni))
    {
        this->TrustedClientPrivateKey = Value;
    }

    TSet<FName> TrustedPlatformsSet;
    for (const auto &PlatformName : GetConfidentialPlatformNames())
    {
        auto PlatformFilename = Filename / TEXT("..") / TEXT("..") / TEXT("Platforms") / *PlatformName.ToString() /
                                TEXT("Config") / FString::Printf(TEXT("%sEngine.ini"), *PlatformName.ToString());
        FString TrustedClientPrivateKeyStr;
        GConfig->GetString(EOS_SECTION, TEXT("TrustedClientPrivateKey"), TrustedClientPrivateKeyStr, PlatformFilename);
        if (!TrustedClientPrivateKeyStr.IsEmpty() && TrustedClientPrivateKeyStr == this->TrustedClientPrivateKey)
        {
            TrustedPlatformsSet.Add(PlatformName);
        }
    }
    this->TrustedPlatforms.Empty();
    for (const auto &TrustedPlatform : TrustedPlatformsSet)
    {
        this->TrustedPlatforms.Add(TrustedPlatform);
    }

    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableSanctionChecks"), bValue, Filename))
    {
        this->EnableSanctionChecks = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableIdentityChecksOnListenServers"), bValue, Filename))
    {
        this->EnableIdentityChecksOnListenServers = bValue;
    }

    if (GConfig->GetString(EOS_SECTION, TEXT("DedicatedServerDistributionMode"), Value, EditorFilename))
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EDedicatedServersDistributionMode"), true);
        this->DedicatedServerDistributionMode =
            (EDedicatedServersDistributionMode)Enum->GetValueByName(FName(*Value), EGetByNameFlags::ErrorIfNotFound);
    }

    FString ServerFilename;
    if (this->DedicatedServerDistributionMode == EDedicatedServersDistributionMode::DevelopersOnly)
    {
        ServerFilename = Filename / TEXT("..") / TEXT("DedicatedServerEngine.ini");
    }
    else
    {
        // For PlayersOnly, we still store these values in Build/NoRedist so that if someone is messing around with
        // Project Settings they can't lose their public/private keypair settings.
        ServerFilename =
            Filename / TEXT("..") / TEXT("..") / TEXT("Build") / TEXT("NoRedist") / TEXT("DedicatedServerEngine.ini");
    }

    if (GConfig->GetString(EOS_SECTION, TEXT("DedicatedServerPublicKey"), Value, Filename))
    {
        this->DedicatedServerPublicKey = Value;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableTrustedDedicatedServers"), bValue, Filename))
    {
        this->EnableTrustedDedicatedServers = bValue;
    }
    if (GConfig->GetBool(EOS_SECTION, TEXT("EnableAutomaticEncryptionOnTrustedDedicatedServers"), bValue, Filename))
    {
        this->EnableAutomaticEncryptionOnTrustedDedicatedServers = bValue;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ClientId"), Value, ServerFilename))
    {
        this->DedicatedServerClientId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("ClientSecret"), Value, ServerFilename))
    {
        this->DedicatedServerClientSecret = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DedicatedServerPrivateKey"), Value, ServerFilename))
    {
        this->DedicatedServerPrivateKey = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DedicatedServerActAsClientId"), Value, ServerFilename))
    {
        this->DedicatedServerActAsClientId = Value;
    }
    if (GConfig->GetString(EOS_SECTION, TEXT("DedicatedServerActAsClientSecret"), Value, ServerFilename))
    {
        this->DedicatedServerActAsClientSecret = Value;
    }

    bool bNeedsSave = false;

    if (this->DedicatedServerPublicKey.IsEmpty() && this->DedicatedServerPrivateKey.IsEmpty() &&
        this->DedicatedServerDistributionMode != EDedicatedServersDistributionMode::PlayersOnly)
    {
        // Generate a dedicated server public/private key pair.
        hydro_sign_keypair server_key_pair;
        hydro_sign_keygen(&server_key_pair);

        TArray<uint8> PrivateKey(server_key_pair.sk, sizeof(server_key_pair.sk));
        TArray<uint8> PublicKey(server_key_pair.pk, sizeof(server_key_pair.pk));

        this->DedicatedServerPrivateKey = FBase64::Encode(PrivateKey);
        this->DedicatedServerPublicKey = FBase64::Encode(PublicKey);

        // Save immediately.
        bNeedsSave = true;
    }

    if (this->EnableAntiCheat && (this->TrustedClientPrivateKey.IsEmpty() || this->TrustedClientPublicKey.IsEmpty()))
    {
        // Generate a trusted client public/private key pair.
        hydro_sign_keypair client_key_pair;
        hydro_sign_keygen(&client_key_pair);

        TArray<uint8> PrivateKey(client_key_pair.sk, sizeof(client_key_pair.sk));
        TArray<uint8> PublicKey(client_key_pair.pk, sizeof(client_key_pair.pk));

        this->TrustedClientPrivateKey = FBase64::Encode(PrivateKey);
        this->TrustedClientPublicKey = FBase64::Encode(PublicKey);

        // Save immediately.
        bNeedsSave = true;
    }

    // Check if we need to immediately save to fix up any missing config values.
    if (this->EnableTrustedDedicatedServers)
    {
        FString EncryptionComponent = TEXT("");
        GConfig->GetString(TEXT("PacketHandlerComponents"), TEXT("EncryptionComponent"), EncryptionComponent, Filename);
        if (EncryptionComponent.IsEmpty())
        {
            bNeedsSave = true;
        }
    }

    if (bNeedsSave)
    {
        this->SaveEOSConfig();
    }
}

void UOnlineSubsystemEOSEditorConfig::SaveEOSConfig()
{
    auto Filename = this->GetDefaultConfigFilename();
    auto File = GConfig->Find(Filename, true);

    auto EditorFilename = Filename / TEXT("..") / TEXT("DefaultEditor.ini");
    auto EditorFile = GConfig->Find(EditorFilename, true);

    GConfig->SetString(EOS_SECTION, TEXT("ProductName"), *this->ProductName, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("ProductVersion"), *this->ProductVersion, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("ProductId"), *this->ProductId, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("SandboxId"), *this->SandboxId, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("DeploymentId"), *this->DeploymentId, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("ClientId"), *this->ClientId, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("ClientSecret"), *this->ClientSecret, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("AuthenticationGraph"), *this->AuthenticationGraph.ToString(), Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("EditorAuthenticationGraph"),
        *this->EditorAuthenticationGraph.ToString(),
        Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("CrossPlatformAccountProvider"),
        *this->CrossPlatformAccountProvider.ToString(),
        Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("RequireCrossPlatformAccount"), this->RequireCrossPlatformAccount, Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("AllowDeviceIdAccounts"), this->AllowDeviceIdAccounts, Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("AllowNativePlatformAccounts"), this->AllowNativePlatformAccounts, Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("DeleteDeviceIdOnLogout"), this->DeleteDeviceIdOnLogout, Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("DisablePersistentLogin"), this->DisablePersistentLogin, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("DevAuthToolAddress"), *this->DevAuthToolAddress, Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("DevAuthToolDefaultCredentialName"),
        *this->DevAuthToolDefaultCredentialName,
        Filename);
    GConfig->SetString(EOS_SECTION, TEXT("PlayerDataEncryptionKey"), *this->PlayerDataEncryptionKey, Filename);
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EPresenceAdvertisementType"), true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("PresenceAdvertises"),
            *Enum->GetNameStringByValue((int64)this->PresenceAdvertises),
            Filename);
    }
    GConfig->SetString(
        EOS_SECTION,
        TEXT("DelegatedSubsystems"),
        *FString::Join(this->DelegatedSubsystems, TEXT(",")),
        Filename);
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EEOSApiVersion"), true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("ApiVersion"),
            *Enum->GetNameStringByValue((int64)this->ApiVersion),
            Filename);
    }
    GConfig->SetString(
        EOS_SECTION,
        TEXT("WidgetClass_SelectEOSAccount"),
        *this->WidgetClass_SelectEOSAccount.ToString(),
        Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("WidgetClass_LinkEOSAccountsAgainstCrossPlatform"),
        *this->WidgetClass_LinkEOSAccountsAgainstCrossPlatform.ToString(),
        Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("WidgetClass_SwitchToCrossPlatformAccount"),
        *this->WidgetClass_SwitchToCrossPlatformAccount.ToString(),
        Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("WidgetClass_EnterDevicePinCode"),
        *this->WidgetClass_EnterDevicePinCode.ToString(),
        Filename);
    GConfig->SetString(
        EOS_SECTION,
        TEXT("WidgetClass_SignInOrCreateAccount"),
        *this->WidgetClass_SignInOrCreateAccount.ToString(),
        Filename);
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EEOSNetworkingStack"), true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("NetworkingStack"),
            *Enum->GetNameStringByValue((int64)this->NetworkingStack),
            Filename);
    }
    GConfig->SetBool(EOS_SECTION, TEXT("RequireEpicGamesLauncher"), this->RequireEpicGamesLauncher, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("SimpleFirstPartyLoginUrl"), *this->SimpleFirstPartyLoginUrl, Filename);
    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EPartyJoinabilityConstraint"), true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("PartyJoinabilityConstraint"),
            *Enum->GetNameStringByValue((int64)this->PartyJoinabilityConstraint),
            Filename);
    }
    GConfig->SetBool(EOS_SECTION, TEXT("EnableVoiceChatEchoInParties"), this->EnableVoiceChatEchoInParties, Filename);
    GConfig->SetBool(
        EOS_SECTION,
        TEXT("EnableVoiceChatPlatformAECByDefault"),
        this->EnableVoiceChatPlatformAECByDefault,
        Filename);

    GConfig->SetBool(EOS_SECTION, TEXT("EnableAntiCheat"), this->EnableAntiCheat, Filename);
    GConfig->SetString(EOS_SECTION, TEXT("TrustedClientPublicKey"), *this->TrustedClientPublicKey, Filename);
    {
        auto TrustedClientEngineIni =
            Filename / TEXT("..") / TEXT("..") / TEXT("Build") / TEXT("NoRedist") / TEXT("TrustedEOSClient.ini");
        auto TrustedClientFile = GConfig->Find(TrustedClientEngineIni, true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("TrustedClientPrivateKey"),
            *this->TrustedClientPrivateKey,
            TrustedClientEngineIni);
        TrustedClientFile->Write(TrustedClientEngineIni);
    }
    TSet<FName> TrustedPlatformsSet;
    for (const auto &PlatformName : GetConfidentialPlatformNames())
    {
        auto PlatformFilename = Filename / TEXT("..") / TEXT("..") / TEXT("Platforms") / *PlatformName.ToString() /
                                TEXT("Config") / FString::Printf(TEXT("%sEngine.ini"), *PlatformName.ToString());
        auto PlatformFile = GConfig->Find(PlatformFilename, true);
        if (this->TrustedPlatforms.Contains(PlatformName))
        {
            GConfig->SetString(
                EOS_SECTION,
                TEXT("TrustedClientPrivateKey"),
                *this->TrustedClientPrivateKey,
                PlatformFilename);
        }
        else
        {
            GConfig->SetString(EOS_SECTION, TEXT("TrustedClientPrivateKey"), TEXT(""), PlatformFilename);
        }
        PlatformFile->Write(PlatformFilename);
    }

    GConfig->SetBool(EOS_SECTION, TEXT("EnableSanctionChecks"), this->EnableSanctionChecks, Filename);
    GConfig->SetBool(
        EOS_SECTION,
        TEXT("EnableIdentityChecksOnListenServers"),
        this->EnableIdentityChecksOnListenServers,
        Filename);

    {
        const UEnum *Enum = FindObject<UEnum>((UObject *)ANY_PACKAGE, TEXT("EDedicatedServersDistributionMode"), true);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("DedicatedServerDistributionMode"),
            *Enum->GetNameStringByValue((int64)this->DedicatedServerDistributionMode),
            EditorFilename);
    }
    if (this->DedicatedServerDistributionMode == EDedicatedServersDistributionMode::PlayersOnly)
    {
        // Trusted dedicated servers can not be enabled if dedicated servers are only distributed to players.
        this->EnableTrustedDedicatedServers = false;
    }

    GConfig->SetString(EOS_SECTION, TEXT("DedicatedServerPublicKey"), *this->DedicatedServerPublicKey, Filename);
    GConfig->SetBool(EOS_SECTION, TEXT("EnableTrustedDedicatedServers"), this->EnableTrustedDedicatedServers, Filename);
    GConfig->SetBool(
        EOS_SECTION,
        TEXT("EnableAutomaticEncryptionOnTrustedDedicatedServers"),
        this->EnableAutomaticEncryptionOnTrustedDedicatedServers,
        Filename);

    FConfigSection *UnwantedSection =
        File->Find(TEXT("/Script/OnlineSubsystemEOSEditor.OnlineSubsystemEOSEditorConfig"));
    if (UnwantedSection != nullptr)
    {
        // Remove the section that gets automatically added by the config serialization system if it already exists.
        File->Remove(TEXT("/Script/OnlineSubsystemEOSEditor.OnlineSubsystemEOSEditorConfig"));
    }

    // Fix up any core settings that are missing or invalid.
    {
        if (this->EnableAutomaticEncryptionOnTrustedDedicatedServers && this->EnableTrustedDedicatedServers)
        {
            FString EncryptionComponent = TEXT("");
            GConfig->GetString(
                TEXT("PacketHandlerComponents"),
                TEXT("EncryptionComponent"),
                EncryptionComponent,
                Filename);
            if (EncryptionComponent.IsEmpty())
            {
                GConfig->SetString(
                    TEXT("PacketHandlerComponents"),
                    TEXT("EncryptionComponent"),
                    TEXT("AESGCMHandlerComponent"),
                    Filename);
            }
        }
    }

    File->Write(Filename);
    EditorFile->Write(EditorFilename);

    auto DevOnlyServerFilename = Filename / TEXT("..") / TEXT("DedicatedServerEngine.ini");
    auto SafeServerFilename =
        Filename / TEXT("..") / TEXT("..") / TEXT("Build") / TEXT("NoRedist") / TEXT("DedicatedServerEngine.ini");

    FString ServerWriteFilename, ServerEraseFilename;
    if (this->DedicatedServerDistributionMode == EDedicatedServersDistributionMode::DevelopersOnly)
    {
        ServerWriteFilename = DevOnlyServerFilename;
        ServerEraseFilename = SafeServerFilename;
    }
    else
    {
        ServerWriteFilename = SafeServerFilename;
        ServerEraseFilename = DevOnlyServerFilename;
    }

    {
        auto ServerFile = GConfig->Find(ServerWriteFilename, true);
        ServerFile->NoSave = false;
        GConfig->SetString(EOS_SECTION, TEXT("ClientId"), *this->DedicatedServerClientId, ServerWriteFilename);
        GConfig->SetString(EOS_SECTION, TEXT("ClientSecret"), *this->DedicatedServerClientSecret, ServerWriteFilename);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("DedicatedServerPrivateKey"),
            *this->DedicatedServerPrivateKey,
            ServerWriteFilename);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("DedicatedServerActAsClientId"),
            *this->DedicatedServerActAsClientId,
            ServerWriteFilename);
        GConfig->SetString(
            EOS_SECTION,
            TEXT("DedicatedServerActAsClientSecret"),
            *this->DedicatedServerActAsClientSecret,
            ServerWriteFilename);
        ServerFile->Write(ServerWriteFilename);
        ServerFile->NoSave = true;

        // Ensure the file contains a strong warning around the values stored in this file.
        auto ServerWarning =
            FString("[EpicOnlineServices-SecurityWarning]\n"
                    "; This file contains secrets which should only ever be available to your dedicated servers.\n"
                    "; Do not move these values to other configuration files. For additional security, you can\n"
                    "; remove the values from here and pass them in on the command line inside your server cluster\n"
                    "; (to prevent other developers from even seeing these values).\n"
                    ";\n"
                    "; If these secrets leak, it will allow players to IMPERSONATE OTHER USERS, bypass Anti-Cheat,\n"
                    "; pretend to run dedicated servers and have full control over the session list. You must\n"
                    "; ensure they are never distributed to clients or players.");
        FString ServerFileContents = TEXT("");
        if (FFileHelper::LoadFileToString(ServerFileContents, *ServerWriteFilename))
        {
            if (!ServerFileContents.Contains(ServerWarning))
            {
                ServerFileContents = ServerWarning + TEXT("\n\n") + ServerFileContents;
                FFileHelper::SaveStringToFile(ServerFileContents, *ServerWriteFilename);
            }
        }
    }
    {
        auto ServerFile = GConfig->Find(ServerEraseFilename, true);
        ServerFile->NoSave = false;
        GConfig->SetString(EOS_SECTION, TEXT("ClientId"), TEXT(""), ServerEraseFilename);
        GConfig->SetString(EOS_SECTION, TEXT("ClientSecret"), TEXT(""), ServerEraseFilename);
        GConfig->SetString(EOS_SECTION, TEXT("DedicatedServerPrivateKey"), TEXT(""), ServerEraseFilename);
        GConfig->SetString(EOS_SECTION, TEXT("DedicatedServerActAsClientId"), TEXT(""), ServerEraseFilename);
        GConfig->SetString(EOS_SECTION, TEXT("DedicatedServerActAsClientSecret"), TEXT(""), ServerEraseFilename);
        ServerFile->Write(ServerEraseFilename);
        ServerFile->NoSave = true;

        // Ensure the file contains a notice about the secrets.
        auto ServerWarning = FString("[EpicOnlineServices-SecurityNotice]\n"
                                     "; The secrets for your dedicated servers are not stored in this file, due to "
                                     "your \"dedicated server distribution mode\" setting in Project Settings.");
        FString ServerFileContents = TEXT("");
        if (FFileHelper::LoadFileToString(ServerFileContents, *ServerEraseFilename))
        {
            if (!ServerFileContents.Contains(ServerWarning))
            {
                ServerFileContents = ServerWarning + TEXT("\n\n") + ServerFileContents;
                FFileHelper::SaveStringToFile(ServerFileContents, *ServerEraseFilename);
            }
        }
    }

    // Reload the configuration hierarchy so the defaults we just wrote will propagate
    // to the platform-specific ini file registered in GEngineIni.
#if defined(EOS_CONFIG_REQUIRES_MODERN_ITERATION)
    for (const FString &IniPairName : GConfig->GetFilenames())
    {
        FConfigFile *ConfigFile = GConfig->Find(IniPairName, false);
        if (ConfigFile != nullptr && ConfigFile->Num() > 0)
        {
            FName BaseName = ConfigFile->Name;
            verify(FConfigCacheIni::LoadLocalIniFile(*ConfigFile, *BaseName.ToString(), true, nullptr, true));
        }
    }
#else
    for (TPair<FString, FConfigFile> &IniPair : *GConfig)
    {
        FConfigFile &ConfigFile = IniPair.Value;
        if (ConfigFile.Num() > 0)
        {
            FName BaseName = ConfigFile.Name;
            verify(FConfigCacheIni::LoadLocalIniFile(ConfigFile, *BaseName.ToString(), true, nullptr, true));
        }
    }
#endif
}

void UOnlineSubsystemEOSEditorConfig::ConfigureProject()
{
    auto Filename = this->GetDefaultConfigFilename();

    // Set the DefaultPlatformService to EOS. This should really be a button in the UI, but for now this will
    // set things up correctly for most developers.
    GConfig->SetString(TEXT("OnlineSubsystem"), TEXT("DefaultPlatformService"), TEXT("RedpointEOS"), Filename);
}

const FName UOnlineSubsystemEOSEditorConfig::GetEditorAuthenticationGraphPropertyName()
{
    static const FName PropertyName =
        GET_MEMBER_NAME_CHECKED(UOnlineSubsystemEOSEditorConfig, EditorAuthenticationGraph);
    return PropertyName;
}

const FName UOnlineSubsystemEOSEditorConfig::GetAuthenticationGraphPropertyName()
{
    static const FName PropertyName = GET_MEMBER_NAME_CHECKED(UOnlineSubsystemEOSEditorConfig, AuthenticationGraph);
    return PropertyName;
}

const FName UOnlineSubsystemEOSEditorConfig::GetCrossPlatformAccountProviderPropertyName()
{
    static const FName PropertyName =
        GET_MEMBER_NAME_CHECKED(UOnlineSubsystemEOSEditorConfig, CrossPlatformAccountProvider);
    return PropertyName;
}

void UOnlineSubsystemEOSEditorConfigFreeEdition::LoadEOSConfig()
{
    Super::LoadEOSConfig();

    auto Filename = this->GetDefaultConfigFilename();

    FString Value;
    if (GConfig->GetString(EOS_SECTION, TEXT("FreeEditionLicenseKey"), Value, Filename))
    {
        this->FreeEditionLicenseKey = Value;
    }
}

void UOnlineSubsystemEOSEditorConfigFreeEdition::SaveEOSConfig()
{
    auto Filename = this->GetDefaultConfigFilename();

    GConfig->SetString(EOS_SECTION, TEXT("FreeEditionLicenseKey"), *this->FreeEditionLicenseKey, Filename);

    Super::SaveEOSConfig();
}

bool FOnlineSubsystemEOSEditorConfig::HandleSettingsSaved()
{
    this->Config->SaveEOSConfig();

    // Prevent the section that gets automatically added by the config serialization system from being added now.
    return false;
}

void FOnlineSubsystemEOSEditorConfig::RegisterSettings()
{
    if (ISettingsModule *SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
    {
        // Make the instanced version of the config (required so OverridePerObjectConfigSection will be called).
#if defined(EOS_IS_FREE_EDITION)
        this->Config = NewObject<UOnlineSubsystemEOSEditorConfigFreeEdition>();
#else
        this->Config = NewObject<UOnlineSubsystemEOSEditorConfig>();
#endif
        this->Config->AddToRoot();

        this->Config->ConfigureProject();

        // Register the settings
        this->SettingsSection = SettingsModule->RegisterSettings(
            "Project",
            "Game",
            "Epic Online Services",
            LOCTEXT("EOSSettingsName", "Epic Online Services"),
            LOCTEXT("EOSSettingsDescription", "Configure Epic Online Services in your game."),
            this->Config);

        if (SettingsSection.IsValid())
        {
            SettingsSection->OnModified().BindRaw(this, &FOnlineSubsystemEOSEditorConfig::HandleSettingsSaved);
        }
    }
}

void FOnlineSubsystemEOSEditorConfig::UnregisterSettings()
{
    if (ISettingsModule *SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings"))
    {
        SettingsModule->UnregisterSettings("Project", "Game", "Epic Online Services");
    }
}

#undef LOCTEXT_NAMESPACE