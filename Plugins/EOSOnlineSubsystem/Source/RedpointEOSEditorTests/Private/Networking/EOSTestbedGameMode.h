// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "EOSTestbedGameMode.generated.h"

UCLASS()
class AEOSTestbedGameMode : public AGameModeBase
{
    GENERATED_BODY()
};