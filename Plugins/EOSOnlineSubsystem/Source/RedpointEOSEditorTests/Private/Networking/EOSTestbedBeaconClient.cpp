// Copyright June Rhodes. All Rights Reserved.

#include "EOSTestbedBeaconClient.h"
#include "Net/UnrealNetwork.h"

#include "EOSEditorCommands.h"

AEOSTestbedBeaconClient::AEOSTestbedBeaconClient()
{
    this->bReplicates = true;
}

void AEOSTestbedBeaconClient::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AEOSTestbedBeaconClient, PIEInstance);
}

void AEOSTestbedBeaconClient::OnFailure()
{
#if defined(EOS_SUPPORTS_ASSERTS_WITH_RETURN)
    UE_LOG(LogEOSEditorTests, Error, TEXT("Beacon encountered an error."));
#endif
    Super::OnFailure();
}

void AEOSTestbedBeaconClient::ClientPing_Implementation()
{
#if defined(EOS_SUPPORTS_ASSERTS_WITH_RETURN)
    UE_LOG(LogEOSEditorTests, Verbose, TEXT("Server successfully called ping on client. Destroying beacon client."));
#endif
    this->DestroyBeacon();
}