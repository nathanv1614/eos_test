// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if defined(EOS_SUPPORTS_ASSERTS_WITH_RETURN)

#include "CoreMinimal.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "Settings/LevelEditorPlaySettings.h"
#include "TestHelpers.h"
#include "Tests/AutomationEditorCommon.h"

DECLARE_LOG_CATEGORY_EXTERN(LogEOSEditorTests, All, All);

enum class EEditorAuthMode : int8
{
    CreateOnDemand,

    RequireEpicGamesAccount,
};

DEFINE_LATENT_AUTOMATION_COMMAND_FIVE_PARAMETER(
    FStartPlayingEmptyMap,
    FAutomationTestBase *,
    Test,
    EEditorAuthMode,
    AuthMode,
    int,
    NumPlayers,
    EPlayNetMode,
    PlayMode,
    FString,
    AdditionalLaunchParameters);
DEFINE_LATENT_AUTOMATION_COMMAND(FClearAutomationConfig);

DEFINE_LATENT_AUTOMATION_COMMAND_ONE_PARAMETER(FWaitForStandalonePlayerController, FAutomationTestBase *, Test);
DEFINE_LATENT_AUTOMATION_COMMAND_ONE_PARAMETER(FWaitForConnectedPlayerController, FAutomationTestBase *, Test);
DEFINE_LATENT_AUTOMATION_COMMAND_ONE_PARAMETER(FSetupAutomationConfig, EEOSNetworkingStack, NetworkingStack);

#endif