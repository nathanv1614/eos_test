// Copyright June Rhodes. All Rights Reserved.

#include "EOSNativePlatformDiscord.h"

#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"

#if defined(NATIVE_PLATFORM_DISCORD)
#include "DiscordModule.h"
#include "Engine.h"
#include "TimerManager.h"
#endif

DECLARE_LOG_CATEGORY_EXTERN(LogEOSAuthDiscord, All, All);
DEFINE_LOG_CATEGORY(LogEOSAuthDiscord);

bool UEOSNativePlatformDiscord::HasCredentials(int32 LocalUserNum)
{
#if defined(NATIVE_PLATFORM_DISCORD) && (!defined(DISCORD_AVAILABLE) || DISCORD_AVAILABLE)
    return GetDiscord() != nullptr;
#else
    return false;
#endif
}

bool UEOSNativePlatformDiscord::CanProvideExternalAccountId(const FUniqueNetId &UserId)
{
#if defined(NATIVE_PLATFORM_DISCORD) && (!defined(DISCORD_AVAILABLE) || DISCORD_AVAILABLE)
    return UserId.GetType().IsEqual(DISCORD_SUBSYSTEM);
#else
    return false;
#endif
}

FExternalAccountIdInfoLegacy UEOSNativePlatformDiscord::GetExternalAccountId(const FUniqueNetId &UserId)
{
#if defined(NATIVE_PLATFORM_DISCORD) && (!defined(DISCORD_AVAILABLE) || DISCORD_AVAILABLE)
    if (!UserId.GetType().IsEqual(DISCORD_SUBSYSTEM))
    {
        return FExternalAccountIdInfo();
    }

    FExternalAccountIdInfoLegacy Info;
    Info.AccountIdType = (int32_t)EOS_EExternalAccountType::EOS_EAT_DISCORD;
    Info.AccountId = UserId.ToString();
    return Info;
#else
    return FExternalAccountIdInfoLegacy();
#endif
}

void UEOSNativePlatformDiscord::GetCredentials(int32 LocalUserNum, FGetCredentialsComplete OnComplete)
{
#if defined(NATIVE_PLATFORM_DISCORD) && (!defined(DISCORD_AVAILABLE) || DISCORD_AVAILABLE)
    if (GetDiscordModule().GetAccessToken() != TEXT(""))
    {
        if (GetDiscordModule().GetAccessToken() == TEXT("ERROR"))
        {
            OnComplete.Execute(false, FOnlineAccountCredentials(), FUserAuthAttributes());
            return;
        }

        FOnlineAccountCredentials Creds;
        Creds.Type = TEXT("DISCORD_ACCESS_TOKEN");
#if EOS_VERSION_AT_LEAST(1, 11, 0)
        Creds.Token = FString::Printf(TEXT("%s"), *GetDiscordModule().GetAccessToken());
#else
        Creds.Token = FString::Printf(TEXT("Bearer %s"), *GetDiscordModule().GetAccessToken());
#endif
        Creds.Id = TEXT("");
        TMap<FString, FString> UserAttrs;
        UserAttrs.Add(EOS_AUTH_ATTRIBUTE_AUTHENTICATEDWITH, TEXT("discord"));
        UserAttrs.Add(TEXT("discord.accessToken"), GetDiscordModule().GetAccessToken());
        OnComplete.Execute(true, Creds, UserAttrs);
        return;
    }

    FTicker::GetCoreTicker().AddTicker(
        FTickerDelegate::CreateLambda([=](float DeltaTime) {
            this->GetCredentials(LocalUserNum, OnComplete);
            return false;
        }),
        0.2f);
#else
    UE_LOG(LogEOSAuthDiscord, Error, TEXT("Discord native platform not available for GetCredentials"));
    OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
#endif
}

bool UEOSNativePlatformDiscord::CanRefreshCredentials(FOnlineAccountCredentials PreviousCredentials)
{
    return PreviousCredentials.Type == FString(TEXT("DISCORD_ACCESS_TOKEN"));
}

void UEOSNativePlatformDiscord::GetRefreshCredentials(
    FOnlineAccountCredentials PreviousCredentials,
    FGetCredentialsComplete OnComplete)
{
    this->GetCredentials(0, OnComplete);
}