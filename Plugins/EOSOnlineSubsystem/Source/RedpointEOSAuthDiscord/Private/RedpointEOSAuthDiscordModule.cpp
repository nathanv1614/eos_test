// Copyright June Rhodes. All Rights Reserved.

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FRedpointEOSAuthDiscordModule : public IModuleInterface
{
};

IMPLEMENT_MODULE(FRedpointEOSAuthDiscordModule, RedpointEOSAuthDiscord)