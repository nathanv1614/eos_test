// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "EOSNativePlatform.h"

#include "EOSNativePlatformDiscord.generated.h"

UCLASS()
class UEOSNativePlatformDiscord : public UObject, public IEOSNativePlatform
{
    GENERATED_BODY()

public:
    virtual FText GetDisplayName() override
    {
        return NSLOCTEXT("OnlineSubsystemEOSAuthDiscord", "Platform_Discord", "Discord");
    }

    virtual bool HasCredentials(int32 LocalUserNum) override;
    virtual void GetCredentials(int32 LocalUserNum, FGetCredentialsComplete OnComplete) override;

    virtual bool CanProvideExternalAccountId(const FUniqueNetId &UserId) override;
    virtual FExternalAccountIdInfoLegacy GetExternalAccountId(const FUniqueNetId &UserId) override;

    virtual bool CanRefreshCredentials(FOnlineAccountCredentials PreviousCredentials) override;
    virtual void GetRefreshCredentials(
        FOnlineAccountCredentials PreviousCredentials,
        FGetCredentialsComplete OnComplete) override;
};