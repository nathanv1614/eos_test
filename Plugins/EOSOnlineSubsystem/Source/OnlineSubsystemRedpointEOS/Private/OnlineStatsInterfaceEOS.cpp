// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/OnlineStatsInterfaceEOS.h"

#include "OnlineSubsystemRedpointEOS/Public/EOSError.h"
#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSErrorConv.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"
#include "OnlineSubsystemRedpointEOS/Shared/UniqueNetIdEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

void FOnlineStatsInterfaceEOS::QueryStats(
    const TSharedRef<const FUniqueNetId> LocalUserId,
    const TSharedRef<const FUniqueNetId> StatsUser,
    const FOnlineStatsQueryUserStatsComplete &Delegate)
{
    if (LocalUserId->GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("QueryStats: LocalUserId was invalid (not an EOS user)"));
        Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::InvalidUser(), nullptr);
        return;
    }

    if (StatsUser->GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("QueryStats: StatsUser was invalid (not an EOS user)"));
        Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::InvalidUser(), nullptr);
        return;
    }

    TArray<TSharedRef<const FUniqueNetId>> StatsUsers;
    StatsUsers.Add(StatsUser);

    TArray<FString> StatNames;

    this->QueryStats(
        LocalUserId,
        StatsUsers,
        StatNames,
        FOnlineStatsQueryUsersStatsComplete::CreateLambda(
            [Delegate](
                const FOnlineError &ResultState,
                const TArray<TSharedRef<const FOnlineStatsUserStats>> &UsersStatsResult) {
                if (UsersStatsResult[0]->Stats.Num() == 0)
                {
                    UE_LOG(
                        LogEOS,
                        Warning,
                        TEXT(
                            "No stats were returned by EOS for the QueryStats operation. EOS will not return empty "
                            "stats (stats you have never sent data for), and if you don't provide StatNames in your "
                            "QueryStats call, the EOS plugin can not backfill those stats with empty values for you."));
                }

                Delegate.ExecuteIfBound(ResultState, UsersStatsResult[0]);
            }));
}

void FOnlineStatsInterfaceEOS::QueryStats(
    const TSharedRef<const FUniqueNetId> LocalUserId,
    const TArray<TSharedRef<const FUniqueNetId>> &StatUsers,
    const TArray<FString> &StatNames,
    const FOnlineStatsQueryUsersStatsComplete &Delegate)
{
    if (LocalUserId->GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("QueryStats: LocalUserId was invalid (not an EOS user)"));
        Delegate.ExecuteIfBound(
            OnlineRedpointEOS::Errors::InvalidUser(),
            TArray<TSharedRef<const FOnlineStatsUserStats>>());
        return;
    }

    TSharedRef<const FUniqueNetIdEOS> LocalUserIdEOS = StaticCastSharedRef<const FUniqueNetIdEOS>(LocalUserId);

    FMultiOperation<TSharedRef<const FUniqueNetId>, TSharedRef<const FOnlineStatsUserStats>>::Run(
        StatUsers,
        [WeakThis = GetWeakThis(this), LocalUserIdEOS, StatNames](
            const TSharedRef<const FUniqueNetId> &TargetUserId,
            const std::function<void(const TSharedRef<const FOnlineStatsUserStats> OutValue)> &OnDone) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (TargetUserId->GetType() != REDPOINT_EOS_SUBSYSTEM)
                {
                    UE_LOG(LogEOS, Warning, TEXT("QueryStats: StatUser was invalid (not an EOS user)"));
                    return false;
                }

                TSharedRef<const FUniqueNetIdEOS> TargetUserIdEOS =
                    StaticCastSharedRef<const FUniqueNetIdEOS>(TargetUserId);

                EOS_Stats_QueryStatsOptions Opts = {};
                Opts.ApiVersion = EOS_STATS_QUERYSTATS_API_LATEST;
#if EOS_VERSION_AT_LEAST(1, 8, 0)
                Opts.LocalUserId = LocalUserIdEOS->GetProductUserId();
                Opts.TargetUserId = TargetUserIdEOS->GetProductUserId();
#else
                Opts.UserId = TargetUserIdEOS->GetProductUserId();
#endif
                Opts.StartTime = EOS_STATS_TIME_UNDEFINED;
                Opts.EndTime = EOS_STATS_TIME_UNDEFINED;
                EOSString_Stats_StatName::AllocateToCharList(StatNames, Opts.StatNamesCount, Opts.StatNames);

                EOSRunOperation<EOS_HStats, EOS_Stats_QueryStatsOptions, EOS_Stats_OnQueryStatsCompleteCallbackInfo>(
                    This->EOSStats,
                    &Opts,
                    EOS_Stats_QueryStats,
                    [WeakThis = GetWeakThis(This), TargetUserIdEOS, OnDone, StatNames, Opts](
                        const EOS_Stats_OnQueryStatsCompleteCallbackInfo *Data) {
                        EOSString_Stats_StatName::FreeFromCharListConst(Opts.StatNamesCount, Opts.StatNames);

                        if (auto This = PinWeakThis(WeakThis))
                        {
                            TSharedRef<FOnlineStatsUserStats> Result =
                                MakeShared<FOnlineStatsUserStats>(TargetUserIdEOS);

                            if (Data->ResultCode != EOS_EResult::EOS_Success)
                            {
                                OnDone(Result);
                                return;
                            }

                            EOS_Stats_GetStatCountOptions CountOpts = {};
                            CountOpts.ApiVersion = EOS_STATS_GETSTATCOUNT_API_LATEST;
#if EOS_VERSION_AT_LEAST(1, 8, 0)
                            CountOpts.TargetUserId = TargetUserIdEOS->GetProductUserId();
#else
                            CountOpts.UserId = TargetUserIdEOS->GetProductUserId();
#endif
                            uint32_t Count = EOS_Stats_GetStatsCount(This->EOSStats, &CountOpts);

                            for (uint32_t i = 0; i < Count; i++)
                            {
                                EOS_Stats_CopyStatByIndexOptions CopyOpts = {};
                                CopyOpts.ApiVersion = EOS_STATS_COPYSTATBYINDEX_API_LATEST;
                                CopyOpts.StatIndex = i;
#if EOS_VERSION_AT_LEAST(1, 8, 0)
                                CopyOpts.TargetUserId = TargetUserIdEOS->GetProductUserId();
#else
                                CopyOpts.UserId = TargetUserIdEOS->GetProductUserId();
#endif

                                EOS_Stats_Stat *Stat = nullptr;
                                EOS_EResult CopyResult = EOS_Stats_CopyStatByIndex(This->EOSStats, &CopyOpts, &Stat);
                                if (CopyResult != EOS_EResult::EOS_Success)
                                {
                                    UE_LOG(
                                        LogEOS,
                                        Warning,
                                        TEXT("Unable to copy stat at index %d, got error %s"),
                                        i,
                                        ANSI_TO_TCHAR(EOS_EResult_ToString(CopyResult)));
                                }

                                FString StatName = EOSString_Stats_StatName::FromAnsiString(Stat->Name);

                                FOnlineStatValue Val;
                                Val.SetValue(Stat->Value);
                                Result->Stats.Add(StatName, Val);

                                EOS_Stats_Stat_Release(Stat);
                            }

                            // For all of the stats that EOS didn't return, give them an empty entry. EOS only returns
                            // stats for which data has previously been sent.
                            for (const auto &StatName : StatNames)
                            {
                                if (!Result->Stats.Contains(StatName))
                                {
                                    Result->Stats.Add(StatName, FOnlineStatValue());
                                }
                            }

                            This->StatsCache.Add(*TargetUserIdEOS, Result);

                            OnDone(Result);
                        }
                    });
                return true;
            }

            return false;
        },
        [WeakThis = GetWeakThis(this), Delegate](const TArray<TSharedRef<const FOnlineStatsUserStats>> &OutValues) {
            Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::Success(), OutValues);
        },
        MakeShared<FOnlineStatsUserStats>(LocalUserIdEOS));
}

TSharedPtr<const FOnlineStatsUserStats> FOnlineStatsInterfaceEOS::GetStats(
    const TSharedRef<const FUniqueNetId> StatsUserId) const
{
    if (this->StatsCache.Contains(*StatsUserId))
    {
        return this->StatsCache[*StatsUserId];
    }
    return nullptr;
}

void FOnlineStatsInterfaceEOS::UpdateStats(
    const TSharedRef<const FUniqueNetId> LocalUserId,
    const TArray<FOnlineStatsUserUpdatedStats> &UpdatedUserStats,
    const FOnlineStatsUpdateStatsComplete &Delegate)
{
    if (LocalUserId->GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("UpdateStats: LocalUserId was invalid (not an EOS user)"));
        Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::InvalidUser());
        return;
    }

    TSharedRef<const FUniqueNetIdEOS> LocalUserIdEOS = StaticCastSharedRef<const FUniqueNetIdEOS>(LocalUserId);

    FMultiOperation<FOnlineStatsUserUpdatedStats, bool>::Run(
        UpdatedUserStats,
        [WeakThis = GetWeakThis(this),
         LocalUserIdEOS](FOnlineStatsUserUpdatedStats StatToUpdate, const std::function<void(bool OutValue)> &OnDone) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (StatToUpdate.Account->GetType() != REDPOINT_EOS_SUBSYSTEM)
                {
                    UE_LOG(LogEOS, Warning, TEXT("UpdateStats: Target user ID was invalid (not an EOS user)"));
                    return false;
                }

                TSharedRef<const FUniqueNetIdEOS> TargetUserIdEOS =
                    StaticCastSharedRef<const FUniqueNetIdEOS>(StatToUpdate.Account);

                TArray<FString> StatKeys;
                StatToUpdate.Stats.GetKeys(StatKeys);

                EOS_Stats_IngestStatOptions Opts = {};
                Opts.ApiVersion = EOS_STATS_INGESTSTAT_API_LATEST;
#if EOS_VERSION_AT_LEAST(1, 8, 0)
                Opts.LocalUserId = LocalUserIdEOS->GetProductUserId();
                Opts.TargetUserId = TargetUserIdEOS->GetProductUserId();
#else
                Opts.UserId = TargetUserIdEOS->GetProductUserId();
#endif
                Opts.StatsCount = StatKeys.Num();
                EOS_Stats_IngestData *ModifiableStats =
                    (EOS_Stats_IngestData *)Compat_MallocZeroed(sizeof(EOS_Stats_IngestData) * StatKeys.Num());
                Opts.Stats = ModifiableStats;

                for (int32 i = 0; i < StatKeys.Num(); i++)
                {
                    FString Key = StatKeys[i];
                    int32_t Amount;
                    StatToUpdate.Stats[Key].GetValue().GetValue(Amount);

                    ModifiableStats[i].ApiVersion = EOS_STATS_INGESTDATA_API_LATEST;
                    ModifiableStats[i].IngestAmount = Amount;
                    verify(
                        EOSString_Stats_StatName::AllocateToCharBuffer(Key, ModifiableStats[i].StatName) ==
                        EOS_EResult::EOS_Success);
                }

                EOSRunOperation<EOS_HStats, EOS_Stats_IngestStatOptions, EOS_Stats_IngestStatCompleteCallbackInfo>(
                    This->EOSStats,
                    &Opts,
                    EOS_Stats_IngestStat,
                    [WeakThis = GetWeakThis(This), OnDone, Opts](const EOS_Stats_IngestStatCompleteCallbackInfo *Data) {
                        for (uint32_t i = 0; i < Opts.StatsCount; i++)
                        {
                            const char *BufRef = Opts.Stats[i].StatName;
                            EOSString_Stats_StatName::FreeFromCharBuffer(BufRef);
                        }
                        FMemory::Free((void *)Opts.Stats);

                        if (auto This = PinWeakThis(WeakThis))
                        {
                            OnDone(Data->ResultCode == EOS_EResult::EOS_Success);
                        }
                    });
                return true;
            }

            return false;
        },
        [WeakThis = GetWeakThis(this), Delegate](const TArray<bool> &OutValues) {
            bool bAllSuccess = true;
            for (bool bSuccess : OutValues)
            {
                if (!bSuccess)
                {
                    bAllSuccess = false;
                    break;
                }
            }
            if (bAllSuccess)
            {
                Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::Success());
            }
            else
            {
                Delegate.ExecuteIfBound(OnlineRedpointEOS::Errors::InvalidRequest());
            }
        });
}

#if !UE_BUILD_SHIPPING
void FOnlineStatsInterfaceEOS::ResetStats(const TSharedRef<const FUniqueNetId> StatsUserId)
{
    UE_LOG(LogEOS, Error, TEXT("ResetStats is not supported in EOS."));
}
#endif

EOS_DISABLE_STRICT_WARNINGS
