// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

#if !defined(HAS_MALLOCZEROED)
void *Compat_MallocZeroed(size_t Size, uint32 Alignment)
{
    void *Temp = FMemory::Malloc(Size, Alignment);
    FMemory::Memzero(Temp, Size);
    return Temp;
}
#endif

EOS_DISABLE_STRICT_WARNINGS