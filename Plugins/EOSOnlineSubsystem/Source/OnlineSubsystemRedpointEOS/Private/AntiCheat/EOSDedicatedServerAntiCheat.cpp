// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/AntiCheat/EOSDedicatedServerAntiCheat.h"

#if WITH_SERVER_CODE && EOS_VERSION_AT_LEAST(1, 12, 0)

#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

FEOSDedicatedServerAntiCheatSession::FEOSDedicatedServerAntiCheatSession()
{
    this->NextHandle = (EOS_AntiCheatCommon_ClientHandle)1000;
}

EOS_AntiCheatCommon_ClientHandle FEOSDedicatedServerAntiCheatSession::AddPlayer(const FUniqueNetIdEOS &UserId)
{
    checkf(!this->PlayersToHandles.Contains(UserId), TEXT("Client is already registered!"));

    EOS_AntiCheatCommon_ClientHandle ReturnHandle = this->NextHandle;
    this->NextHandle = (EOS_AntiCheatCommon_ClientHandle)((intptr_t)this->NextHandle + 1);
    this->PlayersToHandles.Add(UserId, ReturnHandle);
    this->HandlesToPlayers.Add(ReturnHandle, StaticCastSharedRef<const FUniqueNetIdEOS>(UserId.AsShared()));
    return ReturnHandle;
}

void FEOSDedicatedServerAntiCheatSession::RemovePlayer(const FUniqueNetIdEOS &UserId)
{
    if (this->PlayersToHandles.Contains(UserId))
    {
        EOS_AntiCheatCommon_ClientHandle Handle = this->PlayersToHandles[UserId];
        this->PlayersToHandles.Remove(UserId);
        this->HandlesToPlayers.Remove(Handle);
    }
}

TSharedPtr<const FUniqueNetIdEOS> FEOSDedicatedServerAntiCheatSession::GetPlayer(
    EOS_AntiCheatCommon_ClientHandle Handle)
{
    return this->HandlesToPlayers[Handle];
}

EOS_AntiCheatCommon_ClientHandle FEOSDedicatedServerAntiCheatSession::GetHandle(const FUniqueNetIdEOS &UserId)
{
    return this->PlayersToHandles[UserId];
}

bool FEOSDedicatedServerAntiCheatSession::HasPlayer(const FUniqueNetIdEOS &UserId)
{
    return this->PlayersToHandles.Contains(UserId);
}

FEOSDedicatedServerAntiCheat::FEOSDedicatedServerAntiCheat(EOS_HPlatform InPlatform)
    : EOSACServer(EOS_Platform_GetAntiCheatServerInterface(InPlatform)){};

bool FEOSDedicatedServerAntiCheat::Init()
{
    EOS_AntiCheatServer_AddNotifyMessageToClientOptions MsgOpts = {};
    MsgOpts.ApiVersion = EOS_ANTICHEATSERVER_ADDNOTIFYMESSAGETOCLIENT_API_LATEST;
    this->NotifyMessageToClient = EOSRegisterEvent<
        EOS_HAntiCheatServer,
        EOS_AntiCheatServer_AddNotifyMessageToClientOptions,
        EOS_AntiCheatCommon_OnMessageToClientCallbackInfo>(
        this->EOSACServer,
        &MsgOpts,
        EOS_AntiCheatServer_AddNotifyMessageToClient,
        EOS_AntiCheatServer_RemoveNotifyMessageToClient,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnMessageToClientCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnSendNetworkMessage.ExecuteIfBound(
                        StaticCastSharedRef<FAntiCheatSession>(This->CurrentSession.ToSharedRef()),
                        *FUniqueNetIdEOS::DedicatedServerId(),
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        (const uint8 *)Data->MessageData,
                        Data->MessageDataSizeBytes);
                }
            }
        });

    EOS_AntiCheatServer_AddNotifyClientActionRequiredOptions ActOpts = {};
    ActOpts.ApiVersion = EOS_ANTICHEATSERVER_ADDNOTIFYCLIENTACTIONREQUIRED_API_LATEST;
    this->NotifyClientActionRequired = EOSRegisterEvent<
        EOS_HAntiCheatServer,
        EOS_AntiCheatServer_AddNotifyClientActionRequiredOptions,
        EOS_AntiCheatCommon_OnClientActionRequiredCallbackInfo>(
        this->EOSACServer,
        &ActOpts,
        EOS_AntiCheatServer_AddNotifyClientActionRequired,
        EOS_AntiCheatServer_RemoveNotifyClientActionRequired,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnClientActionRequiredCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnPlayerActionRequired.Broadcast(
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        Data->ClientAction,
                        Data->ActionReasonCode,
                        EOSString_AntiCheat_ActionReasonDetailsString::FromUtf8String(Data->ActionReasonDetailsString));
                }
            }
        });

    EOS_AntiCheatServer_AddNotifyClientAuthStatusChangedOptions AuthOpts = {};
    AuthOpts.ApiVersion = EOS_ANTICHEATSERVER_ADDNOTIFYCLIENTAUTHSTATUSCHANGED_API_LATEST;
    this->NotifyClientAuthStatusChanged = EOSRegisterEvent<
        EOS_HAntiCheatServer,
        EOS_AntiCheatServer_AddNotifyClientAuthStatusChangedOptions,
        EOS_AntiCheatCommon_OnClientAuthStatusChangedCallbackInfo>(
        this->EOSACServer,
        &AuthOpts,
        EOS_AntiCheatServer_AddNotifyClientAuthStatusChanged,
        EOS_AntiCheatServer_RemoveNotifyClientAuthStatusChanged,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnClientAuthStatusChangedCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnPlayerAuthStatusChanged.Broadcast(
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        Data->ClientAuthStatus);
                }
            }
        });

    return true;
}

void FEOSDedicatedServerAntiCheat::Shutdown()
{
    this->NotifyMessageToClient.Reset();
    this->NotifyClientActionRequired.Reset();
    this->NotifyClientAuthStatusChanged.Reset();
}

bool FEOSDedicatedServerAntiCheat::Exec(UWorld *InWorld, const TCHAR *Cmd, FOutputDevice &Ar)
{
    return false;
}

TSharedPtr<FAntiCheatSession> FEOSDedicatedServerAntiCheat::CreateSession(
    bool bIsServer,
    const FUniqueNetIdEOS &HostUserId,
    bool bIsDedicatedServerSession,
    TSharedPtr<const FUniqueNetIdEOS> ListenServerUserId)
{
    checkf(bIsServer, TEXT("Dedicated server Anti-Cheat expects bIsServer=true"));

    if (this->CurrentSession.IsValid())
    {
        this->CurrentSession->StackCount++;
        return this->CurrentSession;
    }

    EOS_AntiCheatServer_BeginSessionOptions Opts = {};
    Opts.ApiVersion = EOS_ANTICHEATSERVER_BEGINSESSION_API_LATEST;
    Opts.RegisterTimeoutSeconds = 60; // Recommended value for EOS
    Opts.ServerName = nullptr;        // @todo: Use the session ID for NAME_GameSession
    Opts.bEnableGameplayData = false; // @todo: Implement gameplay data.
    Opts.LocalUserId = nullptr;
    EOS_EResult Result = EOS_AntiCheatServer_BeginSession(this->EOSACServer, &Opts);
    if (Result != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: Unable to begin dedicated server session (got result %s)."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(Result)));
        return nullptr;
    }

    TSharedRef<FEOSDedicatedServerAntiCheatSession> Session = MakeShared<FEOSDedicatedServerAntiCheatSession>();
    Session->StackCount = 1;
    this->CurrentSession = Session;
    return Session;
}

bool FEOSDedicatedServerAntiCheat::DestroySession(FAntiCheatSession &Session)
{
    if (!this->CurrentSession.IsValid())
    {
        return true;
    }

    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in DestroySession must match current session!"));
    this->CurrentSession->StackCount--;

    if (this->CurrentSession->StackCount == 0)
    {
        EOS_AntiCheatServer_EndSessionOptions Opts = {};
        Opts.ApiVersion = EOS_ANTICHEATSERVER_ENDSESSION_API_LATEST;
        EOS_EResult Result = EOS_AntiCheatServer_EndSession(this->EOSACServer, &Opts);
        if (Result != EOS_EResult::EOS_Success)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Anti-Cheat: Unable to end dedicated server session (got result %s)."),
                ANSI_TO_TCHAR(EOS_EResult_ToString(Result)));
            return false;
        }
        this->CurrentSession.Reset();
    }

    return true;
}

bool FEOSDedicatedServerAntiCheat::RegisterPlayer(
    FAntiCheatSession &Session,
    const FUniqueNetIdEOS &UserId,
    EOS_EAntiCheatCommonClientType ClientType,
    EOS_EAntiCheatCommonClientPlatform ClientPlatform)
{
    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in RegisterPlayer must match current session!"));

    FEOSDedicatedServerAntiCheatSession &ServerSession = (FEOSDedicatedServerAntiCheatSession &)Session;

    if (ServerSession.HasPlayer(UserId))
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: The specified player has already been registered with the EAC session."));
        return false;
    }

    EOS_AntiCheatCommon_ClientHandle ClientHandle = ServerSession.AddPlayer(UserId);

    EOS_AntiCheatServer_RegisterClientOptions Opts = {};
    Opts.ApiVersion = EOS_ANTICHEATSERVER_REGISTERCLIENT_API_LATEST;
    Opts.ClientHandle = ClientHandle;
    Opts.ClientType = ClientType;
    Opts.ClientPlatform = ClientPlatform;
    EOSString_ProductUserId::AllocateToCharBuffer(UserId.GetProductUserId(), Opts.AccountId);
    Opts.IpAddress = nullptr; // We never use the IpAddress field for peers, because it will always be over EOS P2P.
    EOS_EResult Result = EOS_AntiCheatServer_RegisterClient(this->EOSACServer, &Opts);
    if (Result != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: Unable to register client (got result %s)."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(Result)));
        return false;
    }
    return true;
}

bool FEOSDedicatedServerAntiCheat::UnregisterPlayer(FAntiCheatSession &Session, const FUniqueNetIdEOS &UserId)
{
    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in UnregisterPlayer must match current session!"));

    FEOSDedicatedServerAntiCheatSession &ServerSession = (FEOSDedicatedServerAntiCheatSession &)Session;

    EOS_AntiCheatServer_UnregisterClientOptions Opts = {};
    Opts.ApiVersion = EOS_ANTICHEATSERVER_UNREGISTERCLIENT_API_LATEST;
    Opts.ClientHandle = ServerSession.GetHandle(UserId);
    EOS_EResult Result = EOS_AntiCheatServer_UnregisterClient(this->EOSACServer, &Opts);
    if (Result != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: Unable to unregister client (got result %s)."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(Result)));
        return false;
    }

    ServerSession.RemovePlayer(UserId);
    return true;
}

bool FEOSDedicatedServerAntiCheat::ReceiveNetworkMessage(
    FAntiCheatSession &Session,
    const FUniqueNetIdEOS &SourceUserId,
    const FUniqueNetIdEOS &TargetUserId,
    const uint8 *Data,
    uint32_t Size)
{
    EOS_AntiCheatServer_ReceiveMessageFromClientOptions Opts = {};
    Opts.ApiVersion = EOS_ANTICHEATSERVER_RECEIVEMESSAGEFROMCLIENT_API_LATEST;
    Opts.ClientHandle = this->CurrentSession->GetHandle(SourceUserId);
    Opts.Data = Data;
    Opts.DataLengthBytes = Size;
    EOS_EResult ReceiveResult = EOS_AntiCheatServer_ReceiveMessageFromClient(this->EOSACServer, &Opts);
    if (ReceiveResult != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("EOS_AntiCheatServer_ReceiveMessageFromClient failed: %s"),
            ANSI_TO_TCHAR(EOS_EResult_ToString(ReceiveResult)));
        return false;
    }
    return true;
}

#endif // #if WITH_SERVER_CODE && EOS_VERSION_AT_LEAST(1, 12, 0)