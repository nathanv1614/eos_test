// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/AntiCheat/EOSGameAntiCheat.h"

#if EOS_VERSION_AT_LEAST(1, 12, 0)

#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

FEOSGameAntiCheatSession::FEOSGameAntiCheatSession(TSharedRef<const FUniqueNetIdEOS> InHostUserId)
    : NextHandle((EOS_AntiCheatCommon_ClientHandle)1000),
      HandlesToPlayers(TMap<EOS_AntiCheatCommon_ClientHandle, TSharedPtr<const FUniqueNetIdEOS>>()),
      PlayersToHandles(TUserIdMap<EOS_AntiCheatCommon_ClientHandle>()), bIsServer(false),
      HostUserId(MoveTemp(InHostUserId)), bIsDedicatedServerSession(false)
{
}

EOS_AntiCheatCommon_ClientHandle FEOSGameAntiCheatSession::AddPlayer(const FUniqueNetIdEOS &UserId)
{
    checkf(!this->PlayersToHandles.Contains(UserId), TEXT("Client is already registered!"));

    EOS_AntiCheatCommon_ClientHandle ReturnHandle = this->NextHandle;
    this->NextHandle = (EOS_AntiCheatCommon_ClientHandle)((intptr_t)this->NextHandle + 1);
    this->PlayersToHandles.Add(UserId, ReturnHandle);
    this->HandlesToPlayers.Add(ReturnHandle, StaticCastSharedRef<const FUniqueNetIdEOS>(UserId.AsShared()));
    return ReturnHandle;
}

void FEOSGameAntiCheatSession::RemovePlayer(const FUniqueNetIdEOS &UserId)
{
    if (this->PlayersToHandles.Contains(UserId))
    {
        EOS_AntiCheatCommon_ClientHandle Handle = this->PlayersToHandles[UserId];
        this->PlayersToHandles.Remove(UserId);
        this->HandlesToPlayers.Remove(Handle);
    }
}

TSharedPtr<const FUniqueNetIdEOS> FEOSGameAntiCheatSession::GetPlayer(EOS_AntiCheatCommon_ClientHandle Handle)
{
    return this->HandlesToPlayers[Handle];
}

EOS_AntiCheatCommon_ClientHandle FEOSGameAntiCheatSession::GetHandle(const FUniqueNetIdEOS &UserId)
{
    return this->PlayersToHandles[UserId];
}

bool FEOSGameAntiCheatSession::HasPlayer(const FUniqueNetIdEOS &UserId)
{
    return this->PlayersToHandles.Contains(UserId);
}

FEOSGameAntiCheat::FEOSGameAntiCheat(EOS_HPlatform InPlatform)
    : EOSACClient(EOS_Platform_GetAntiCheatClientInterface(InPlatform)){};

bool FEOSGameAntiCheat::Init()
{
    EOS_AntiCheatClient_AddNotifyMessageToServerOptions MsgSrvOpts = {};
    MsgSrvOpts.ApiVersion = EOS_ANTICHEATCLIENT_ADDNOTIFYMESSAGETOSERVER_API_LATEST;
    this->NotifyMessageToServer = EOSRegisterEvent<
        EOS_HAntiCheatClient,
        EOS_AntiCheatClient_AddNotifyMessageToServerOptions,
        EOS_AntiCheatClient_OnMessageToServerCallbackInfo>(
        this->EOSACClient,
        &MsgSrvOpts,
        EOS_AntiCheatClient_AddNotifyMessageToServer,
        EOS_AntiCheatClient_RemoveNotifyMessageToServer,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatClient_OnMessageToServerCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnSendNetworkMessage.ExecuteIfBound(
                        StaticCastSharedRef<FAntiCheatSession>(This->CurrentSession.ToSharedRef()),
                        *This->CurrentSession->HostUserId,
                        *FUniqueNetIdEOS::DedicatedServerId(),
                        (const uint8 *)Data->MessageData,
                        Data->MessageDataSizeBytes);
                }
            }
        });

    EOS_AntiCheatClient_AddNotifyMessageToPeerOptions MsgOpts = {};
    MsgOpts.ApiVersion = EOS_ANTICHEATCLIENT_ADDNOTIFYMESSAGETOPEER_API_LATEST;
    this->NotifyMessageToPeer = EOSRegisterEvent<
        EOS_HAntiCheatClient,
        EOS_AntiCheatClient_AddNotifyMessageToPeerOptions,
        EOS_AntiCheatCommon_OnMessageToClientCallbackInfo>(
        this->EOSACClient,
        &MsgOpts,
        EOS_AntiCheatClient_AddNotifyMessageToPeer,
        EOS_AntiCheatClient_RemoveNotifyMessageToPeer,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnMessageToClientCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnSendNetworkMessage.ExecuteIfBound(
                        StaticCastSharedRef<FAntiCheatSession>(This->CurrentSession.ToSharedRef()),
                        *This->CurrentSession->HostUserId,
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        (const uint8 *)Data->MessageData,
                        Data->MessageDataSizeBytes);
                }
            }
        });

    EOS_AntiCheatClient_AddNotifyPeerActionRequiredOptions ActOpts = {};
    ActOpts.ApiVersion = EOS_ANTICHEATCLIENT_ADDNOTIFYPEERACTIONREQUIRED_API_LATEST;
    this->NotifyClientActionRequired = EOSRegisterEvent<
        EOS_HAntiCheatClient,
        EOS_AntiCheatClient_AddNotifyPeerActionRequiredOptions,
        EOS_AntiCheatCommon_OnClientActionRequiredCallbackInfo>(
        this->EOSACClient,
        &ActOpts,
        EOS_AntiCheatClient_AddNotifyPeerActionRequired,
        EOS_AntiCheatClient_RemoveNotifyPeerActionRequired,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnClientActionRequiredCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnPlayerActionRequired.Broadcast(
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        Data->ClientAction,
                        Data->ActionReasonCode,
                        EOSString_AntiCheat_ActionReasonDetailsString::FromUtf8String(Data->ActionReasonDetailsString));
                }
            }
        });

    EOS_AntiCheatClient_AddNotifyPeerAuthStatusChangedOptions AuthOpts = {};
    AuthOpts.ApiVersion = EOS_ANTICHEATCLIENT_ADDNOTIFYPEERAUTHSTATUSCHANGED_API_LATEST;
    this->NotifyClientAuthStatusChanged = EOSRegisterEvent<
        EOS_HAntiCheatClient,
        EOS_AntiCheatClient_AddNotifyPeerAuthStatusChangedOptions,
        EOS_AntiCheatCommon_OnClientAuthStatusChangedCallbackInfo>(
        this->EOSACClient,
        &AuthOpts,
        EOS_AntiCheatClient_AddNotifyPeerAuthStatusChanged,
        EOS_AntiCheatClient_RemoveNotifyPeerAuthStatusChanged,
        [WeakThis = GetWeakThis(this)](const EOS_AntiCheatCommon_OnClientAuthStatusChangedCallbackInfo *Data) {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (This->CurrentSession.IsValid())
                {
                    This->OnPlayerAuthStatusChanged.Broadcast(
                        *This->CurrentSession->GetPlayer(Data->ClientHandle),
                        Data->ClientAuthStatus);
                }
            }
        });

    return true;
}

void FEOSGameAntiCheat::Shutdown()
{
    this->NotifyMessageToServer.Reset();
    this->NotifyMessageToPeer.Reset();
    this->NotifyClientActionRequired.Reset();
    this->NotifyClientAuthStatusChanged.Reset();
}

bool FEOSGameAntiCheat::Exec(UWorld *InWorld, const TCHAR *Cmd, FOutputDevice &Ar)
{
    return false;
}

TSharedPtr<FAntiCheatSession> FEOSGameAntiCheat::CreateSession(
    bool bIsServer,
    const FUniqueNetIdEOS &HostUserId,
    bool bIsDedicatedServerSession,
    TSharedPtr<const FUniqueNetIdEOS> ListenServerUserId)
{
    checkf(
        !bIsServer || (bIsServer && !bIsDedicatedServerSession),
        TEXT("Invalid BeginSession call for Game Anti-Cheat."));

    if (this->CurrentSession.IsValid())
    {
        this->CurrentSession->StackCount++;
        return this->CurrentSession;
    }

    EOS_AntiCheatClient_BeginSessionOptions ClientOpts = {};
    ClientOpts.ApiVersion = EOS_ANTICHEATCLIENT_BEGINSESSION_API_LATEST;
    ClientOpts.LocalUserId = HostUserId.GetProductUserId();
    ClientOpts.Mode = bIsDedicatedServerSession ? EOS_EAntiCheatClientMode::EOS_ACCM_ClientServer
                                                : EOS_EAntiCheatClientMode::EOS_ACCM_PeerToPeer;
    EOS_EResult ClientResult = EOS_AntiCheatClient_BeginSession(this->EOSACClient, &ClientOpts);
    if (ClientResult != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: Unable to begin client session (got result %s). Parameters were: ApiVersion=%d, "
                 "LocalUserId=%s, Mode=%d."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(ClientResult)),
            ClientOpts.ApiVersion,
            *HostUserId.ToString(),
            ClientOpts.Mode);
        return nullptr;
    }

    TSharedRef<FEOSGameAntiCheatSession> Session =
        MakeShared<FEOSGameAntiCheatSession>(StaticCastSharedRef<const FUniqueNetIdEOS>(HostUserId.AsShared()));
    Session->bIsServer = bIsServer;
    Session->bIsDedicatedServerSession = bIsDedicatedServerSession;
    Session->StackCount = 1;
    Session->ListenServerUserId.Reset();
    this->CurrentSession = Session;

    if (!bIsServer && !bIsDedicatedServerSession)
    {
        // We are a client connecting to a listen server. We need to register the host as a player immediately.
        checkf(
            ListenServerUserId.IsValid(),
            TEXT("Expected listen server user ID if we are a client connecting to a listen server!"));
        Session->ListenServerUserId = ListenServerUserId;
        if (!this->RegisterPlayer(
                *Session,
                *ListenServerUserId,
                EOS_EAntiCheatCommonClientType::EOS_ACCCT_ProtectedClient,
                EOS_EAntiCheatCommonClientPlatform::EOS_ACCCP_Unknown))
        {
            this->DestroySession(*Session);
            return nullptr;
        }
    }

    return Session;
}

bool FEOSGameAntiCheat::DestroySession(FAntiCheatSession &Session)
{
    if (!this->CurrentSession.IsValid())
    {
        return true;
    }

    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in DestroySession must match current session!"));
    this->CurrentSession->StackCount--;

    if (this->CurrentSession->StackCount == 0)
    {
        if (this->CurrentSession->ListenServerUserId.IsValid())
        {
            this->UnregisterPlayer(Session, *this->CurrentSession->ListenServerUserId);
        }

        EOS_AntiCheatClient_EndSessionOptions ClientOpts = {};
        ClientOpts.ApiVersion = EOS_ANTICHEATCLIENT_ENDSESSION_API_LATEST;
        EOS_EResult ClientResult = EOS_AntiCheatClient_EndSession(this->EOSACClient, &ClientOpts);
        if (ClientResult != EOS_EResult::EOS_Success)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Anti-Cheat: Unable to end non-P2P client session (got result %s)."),
                ANSI_TO_TCHAR(EOS_EResult_ToString(ClientResult)));
            return false;
        }
        this->CurrentSession.Reset();
    }

    return true;
}

bool FEOSGameAntiCheat::RegisterPlayer(
    FAntiCheatSession &Session,
    const FUniqueNetIdEOS &UserId,
    EOS_EAntiCheatCommonClientType ClientType,
    EOS_EAntiCheatCommonClientPlatform ClientPlatform)
{
    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in RegisterPlayer must match current session!"));

    FEOSGameAntiCheatSession &GameSession = (FEOSGameAntiCheatSession &)Session;

    if (GameSession.bIsDedicatedServerSession)
    {
        // Clients connecting to dedicated servers don't register the server as a peer, and dedicated servers never use
        // this implementation.
        return true;
    }

    // Otherwise, this is called on clients when they connect to the listen server (with the listen server as the
    // UserId), and on the listen server for each connecting user.

    if (GameSession.HasPlayer(UserId))
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: The specified player has already been registered with the EAC session."));
        return false;
    }

    EOS_AntiCheatCommon_ClientHandle ClientHandle = GameSession.AddPlayer(UserId);

    EOS_AntiCheatClient_RegisterPeerOptions PeerOpts = {};
    PeerOpts.ApiVersion = EOS_ANTICHEATCLIENT_REGISTERPEER_API_LATEST;
    PeerOpts.PeerHandle = ClientHandle;
    PeerOpts.ClientType = ClientType;
    PeerOpts.ClientPlatform = ClientPlatform;
    EOSString_ProductUserId::AllocateToCharBuffer(UserId.GetProductUserId(), PeerOpts.AccountId);
    PeerOpts.IpAddress = nullptr; // We never use the IpAddress field for peers, because it will always be over EOS P2P.
    EOS_EResult PeerResult = EOS_AntiCheatClient_RegisterPeer(this->EOSACClient, &PeerOpts);
    EOSString_ProductUserId::FreeFromCharBuffer(PeerOpts.AccountId);
    if (PeerResult != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Anti-Cheat: Unable to register listen server as peer (got result %s)."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(PeerResult)));
        return false;
    }
    return true;
}

bool FEOSGameAntiCheat::UnregisterPlayer(FAntiCheatSession &Session, const FUniqueNetIdEOS &UserId)
{
    checkf(&Session == this->CurrentSession.Get(), TEXT("Session in UnregisterPlayer must match current session!"));

    FEOSGameAntiCheatSession &GameSession = (FEOSGameAntiCheatSession &)Session;

    if (GameSession.bIsDedicatedServerSession)
    {
        // Clients connecting to dedicated servers don't register the server as a peer, and dedicated servers never use
        // this implementation.
        return true;
    }

    EOS_AntiCheatClient_UnregisterPeerOptions PeerOpts = {};
    PeerOpts.ApiVersion = EOS_ANTICHEATCLIENT_UNREGISTERPEER_API_LATEST;
    PeerOpts.PeerHandle = GameSession.GetHandle(UserId);
    EOS_EResult PeerResult = EOS_AntiCheatClient_UnregisterPeer(this->EOSACClient, &PeerOpts);
    if (PeerResult != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Warning,
            TEXT("Anti-Cheat: Unable to unregister listen server as peer (got result %s)."),
            ANSI_TO_TCHAR(EOS_EResult_ToString(PeerResult)));
        return false;
    }

    GameSession.RemovePlayer(UserId);
    return true;
}

bool FEOSGameAntiCheat::ReceiveNetworkMessage(
    FAntiCheatSession &Session,
    const FUniqueNetIdEOS &SourceUserId,
    const FUniqueNetIdEOS &TargetUserId,
    const uint8 *Data,
    uint32_t Size)
{
    EOS_EResult Result;
    if (SourceUserId.IsDedicatedServer())
    {
        EOS_AntiCheatClient_ReceiveMessageFromServerOptions Opts = {};
        Opts.ApiVersion = EOS_ANTICHEATCLIENT_RECEIVEMESSAGEFROMSERVER_API_LATEST;
        Opts.Data = Data;
        Opts.DataLengthBytes = Size;
        Result = EOS_AntiCheatClient_ReceiveMessageFromServer(this->EOSACClient, &Opts);
    }
    else if (this->CurrentSession.IsValid() && this->CurrentSession->HasPlayer(SourceUserId))
    {
        EOS_AntiCheatClient_ReceiveMessageFromPeerOptions Opts = {};
        Opts.ApiVersion = EOS_ANTICHEATCLIENT_RECEIVEMESSAGEFROMPEER_API_LATEST;
        Opts.Data = Data;
        Opts.DataLengthBytes = Size;
        Opts.PeerHandle = this->CurrentSession->GetHandle(SourceUserId);
        Result = EOS_AntiCheatClient_ReceiveMessageFromPeer(this->EOSACClient, &Opts);
    }
    else
    {
        Result = EOS_EResult::EOS_NotFound;
    }
    if (Result != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("EOS_AntiCheatClient_ReceiveMessageFromPeer/EOS_AntiCheatClient_ReceiveMessageFromServer failed: %s"),
            ANSI_TO_TCHAR(EOS_EResult_ToString(Result)));
        return false;
    }
    return true;
}

#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)