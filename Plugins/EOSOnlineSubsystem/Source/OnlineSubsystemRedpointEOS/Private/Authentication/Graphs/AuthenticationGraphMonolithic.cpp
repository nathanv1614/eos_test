// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Graphs/AuthenticationGraphMonolithic.h"

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraphRegistry.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/AuthenticationGraphNodeConditional.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/AuthenticationGraphNodeUntil_Forever.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/AuthenticationGraphNodeUntil_LoginComplete.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/BailIfAlreadyAuthenticatedNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/CreateDeviceIdNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/FailAuthenticationNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/GatherEOSAccountsWithExternalCredentialsNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/GetExternalCredentialsFromEOSNativePlatformNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/LoginWithSelectedEOSAccountNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/NoopAuthenticationGraphNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptForSwitchToCrossPlatformAccountNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptToLinkEOSAccountsAgainstCrossPlatformNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptToSelectEOSAccountNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/SelectCrossPlatformAccountNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/SelectSingleSuccessfulEOSAccountNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/TryDeviceIdAuthenticationNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"

EOS_ENABLE_STRICT_WARNINGS

void FAuthenticationGraphMonolithic::Register()
{
    FAuthenticationGraphRegistry::Register(
        EOS_AUTH_GRAPH_MONOLITHIC,
        NSLOCTEXT("OnlineSubsystemRedpointEOS", "AuthGraph_Monolithic", "Monolithic"),
        MakeShared<FAuthenticationGraphMonolithic>());
}

bool Condition_NoSuccessfulCandidate_NoContinuanceToken_CrossPlatformEnabled(const FAuthenticationGraphState &State)
{
    int SuccessCount = 0;
    int ContinuanceCount = 0;
    for (const auto &Candidate : State.EOSCandidates)
    {
        if (EOSString_ProductUserId::IsValid(Candidate.ProductUserId))
        {
            SuccessCount++;
        }
        if (!EOSString_ContinuanceToken::IsNone(Candidate.ContinuanceToken))
        {
            ContinuanceCount++;
        }
    }

    // No success, no continuation token, CrossPlatform enabled/available.
    return SuccessCount == 0 && ContinuanceCount == 0 && State.CrossPlatformAccountProvider.IsValid();
}

bool Condition_AlreadyAuthenticatedWithNonCrossPlatformAccount_CrossPlatformOptional(
    const FAuthenticationGraphState &State)
{
    /* Already authenticated with non-CrossPlatform account, and mode is CrossPlatformOptional */
    return State.ExistingUserId.IsValid() && !State.ExistingCrossPlatformAccountId.IsValid() &&
           State.CrossPlatformAccountProvider.IsValid() && !State.Config->GetRequireCrossPlatformAccount();
}

TSharedRef<FAuthenticationGraphNode> FAuthenticationGraphMonolithic::CreateGraph(
    const TSharedRef<FAuthenticationGraphState> &InitialState)
{
    if (InitialState->CrossPlatformAccountProvider.IsValid() && InitialState->Config->GetRequireCrossPlatformAccount())
    {
        return MakeShared<FAuthenticationGraphNodeUntil_Forever>()
            ->Add(MakeShared<FBailIfAlreadyAuthenticatedNode>())
            ->Add(MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                      ->Add(InitialState->CrossPlatformAccountProvider->GetInteractiveAuthenticationSequence())
                      ->Add(MakeShared<FSelectCrossPlatformAccountNode>())
                      ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
            ->Add(MakeShared<FGetExternalCredentialsFromEOSNativePlatformNode>())
            ->Add(MakeShared<FGatherEOSAccountsWithExternalCredentialsNode>())
            ->Add(MakeShared<FPromptToLinkEOSAccountsAgainstCrossPlatformNode>());
    }
    else
    {
        return MakeShared<FAuthenticationGraphNodeConditional>()
            ->If(
                FAuthenticationGraphCondition::CreateStatic(&FAuthenticationGraph::Condition_Unauthenticated),
                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                    // Try to do cross-platform authentication only if we have a provider.
                    ->Add(MakeShared<FAuthenticationGraphNodeConditional>()
                              ->If(
                                  FAuthenticationGraphCondition::CreateStatic(
                                      &FAuthenticationGraph::Condition_HasCrossPlatformAccountProvider),
                                  InitialState->CrossPlatformAccountProvider.IsValid()
                                      ? InitialState->CrossPlatformAccountProvider
                                            ->GetNonInteractiveAuthenticationSequence()
                                      : MakeShared<FNoopAuthenticationGraphNode>())
                              ->Else(MakeShared<FNoopAuthenticationGraphNode>()))
                    // Now continue gathering all the other credential types.
                    ->Add(MakeShared<FTryDeviceIdAuthenticationNode>(true))
                    ->Add(MakeShared<FGetExternalCredentialsFromEOSNativePlatformNode>())
                    ->Add(MakeShared<FGatherEOSAccountsWithExternalCredentialsNode>())
                    ->Add(
                        MakeShared<FAuthenticationGraphNodeConditional>()
                            ->If(
                                FAuthenticationGraphCondition::CreateStatic(
                                    &FAuthenticationGraph::Condition_OneSuccessfulCandidate),
                                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                                    ->Add(MakeShared<FSelectSingleSuccessfulEOSAccountNode>())
                                    ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
                            ->If(
                                FAuthenticationGraphCondition::CreateStatic(
                                    &FAuthenticationGraph::Condition_MoreThanOneSuccessfulCandidate),
                                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                                    ->Add(MakeShared<FPromptToSelectEOSAccountNode>(/* ExistingMode */))
                                    ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
                            ->If(
                                FAuthenticationGraphCondition::CreateStatic(
                                    &FAuthenticationGraph::Condition_NoSuccessfulCandidate_AtLeastOneContinuanceToken),
                                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                                    ->Add(MakeShared<FPromptToSelectEOSAccountNode>(/* CreateMode */))
                                    ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
                            ->If(
                                FAuthenticationGraphCondition::CreateStatic(
                                    &FAuthenticationGraph::
                                        Condition_NoSuccessfulCandidate_NoContinuanceToken_DeviceIdsEnabled),
                                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                                    ->Add(MakeShared<FCreateDeviceIdNode>())
                                    ->Add(MakeShared<FTryDeviceIdAuthenticationNode>(true))
                                    ->Add(MakeShared<FSelectSingleSuccessfulEOSAccountNode>())
                                    ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
                            ->If(
                                FAuthenticationGraphCondition::CreateStatic(
                                    &Condition_NoSuccessfulCandidate_NoContinuanceToken_CrossPlatformEnabled),
                                MakeShared<FAuthenticationGraphNodeUntil_LoginComplete>()
                                    ->Add(
                                        InitialState->CrossPlatformAccountProvider.IsValid()
                                            ? InitialState->CrossPlatformAccountProvider
                                                  ->GetInteractiveOnlyAuthenticationSequence()
                                            : MakeShared<FNoopAuthenticationGraphNode>())
                                    ->Add(MakeShared<FSelectCrossPlatformAccountNode>())
                                    ->Add(MakeShared<FLoginWithSelectedEOSAccountNode>()))
                            ->Else(MakeShared<FFailAuthenticationNode>(
                                TEXT("No available credentials to complete login")))))
            ->If(
                FAuthenticationGraphCondition::CreateStatic(
                    &Condition_AlreadyAuthenticatedWithNonCrossPlatformAccount_CrossPlatformOptional),
                InitialState->CrossPlatformAccountProvider.IsValid()
                    ? InitialState->CrossPlatformAccountProvider
                          ->GetUpgradeCurrentAccountToCrossPlatformAccountSequence()
                    : MakeShared<FNoopAuthenticationGraphNode>())
            ->Else(MakeShared<FFailAuthenticationNode>(TEXT("Can't login user again, already authenticated")));
    }
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION