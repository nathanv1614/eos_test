// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptForSwitchToCrossPlatformAccountNode.h"

#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/EOSUserInterface_SwitchToCrossPlatformAccount.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"

EOS_ENABLE_STRICT_WARNINGS

void FPromptForSwitchToCrossPlatformAccountNode::Execute(
    TSharedRef<FAuthenticationGraphState> State,
    FAuthenticationGraphNodeOnDone OnDone)
{
    check(this->Widget == nullptr);

    this->Widget = MakeShared<TUserInterfaceRef<
        IEOSUserInterface_SwitchToCrossPlatformAccount,
        UEOSUserInterface_SwitchToCrossPlatformAccount,
        UEOSUserInterface_SwitchToCrossPlatformAccount_Context>>(
        State,
        State->Config->GetWidgetClass(
            TEXT("SwitchToCrossPlatformAccount"),
            TEXT("/OnlineSubsystemRedpointEOS/"
                 "EOSDefaultUserInterface_SwitchToCrossPlatformAccount.EOSDefaultUserInterface_"
                 "SwitchToCrossPlatformAccount_C")),
        TEXT("IEOSUserInterface_SwitchToCrossPlatformAccount"));

    if (!this->Widget->IsValid())
    {
        State->ErrorMessages.Add(this->Widget->GetInvalidErrorMessage());
        this->Widget = nullptr;
        OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    check(!State->HasCurrentUserInterfaceWidget());
    check(EOSString_EpicAccountId::IsValid(State->GetAuthenticatedEpicAccountId()));

    this->Widget->GetContext()->SetOnChoice(UEOSUserInterface_SwitchToCrossPlatformAccount_OnChoice::CreateSP(
        this,
        &FPromptForSwitchToCrossPlatformAccountNode::SelectChoice,
        State,
        OnDone));

    EOS_UserInfo_QueryUserInfoOptions UserInfoQueryOpts = {};
    UserInfoQueryOpts.ApiVersion = EOS_USERINFO_QUERYUSERINFO_API_LATEST;
    UserInfoQueryOpts.LocalUserId = State->GetAuthenticatedEpicAccountId();
    UserInfoQueryOpts.TargetUserId = State->GetAuthenticatedEpicAccountId();

    EOSRunOperation<EOS_HUserInfo, EOS_UserInfo_QueryUserInfoOptions, EOS_UserInfo_QueryUserInfoCallbackInfo>(
        State->EOSUserInfo,
        &UserInfoQueryOpts,
        &EOS_UserInfo_QueryUserInfo,
        [WeakThis = GetWeakThis(this), State, OnDone](const EOS_UserInfo_QueryUserInfoCallbackInfo *Info) {
            if (auto This = StaticCastSharedPtr<FPromptForSwitchToCrossPlatformAccountNode>(PinWeakThis(WeakThis)))
            {
                if (Info->ResultCode != EOS_EResult::EOS_Success)
                {
                    State->ErrorMessages.Add(TEXT("Unable to fetch user info for authenticated CrossPlatform account"));
                    OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
                    return;
                }

                EOS_UserInfo_CopyUserInfoOptions UserInfoCopyOpts = {};
                UserInfoCopyOpts.ApiVersion = EOS_USERINFO_COPYUSERINFO_API_LATEST;
                UserInfoCopyOpts.LocalUserId = State->GetAuthenticatedEpicAccountId();
                UserInfoCopyOpts.TargetUserId = State->GetAuthenticatedEpicAccountId();

                EOS_UserInfo *EpicAccountInfo = nullptr;

                auto Result = EOS_UserInfo_CopyUserInfo(State->EOSUserInfo, &UserInfoCopyOpts, &EpicAccountInfo);
                if (Result != EOS_EResult::EOS_Success)
                {
                    State->ErrorMessages.Add(TEXT("Unable to fetch user info for authenticated CrossPlatform account"));
                    OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
                    return;
                }

                IEOSUserInterface_SwitchToCrossPlatformAccount::Execute_SetupUserInterface(
                    This->Widget->GetWidget(),
                    This->Widget->GetContext(),
                    ANSI_TO_TCHAR(EpicAccountInfo->DisplayName));

                EOS_UserInfo_Release(EpicAccountInfo);

                This->Widget->AddToState(State);
            }
        });
}

void FPromptForSwitchToCrossPlatformAccountNode::SelectChoice(
    EEOSUserInterface_SwitchToCrossPlatformAccount_Choice SelectedChoice,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FAuthenticationGraphState> State,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FAuthenticationGraphNodeOnDone OnDone)
{
    State->LastSwitchChoice = SelectedChoice;

    this->Widget->RemoveFromState(State);
    this->Widget = nullptr;

    OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION