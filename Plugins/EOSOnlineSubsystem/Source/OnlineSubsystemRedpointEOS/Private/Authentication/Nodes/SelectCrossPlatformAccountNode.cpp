// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/SelectCrossPlatformAccountNode.h"

#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"

EOS_ENABLE_STRICT_WARNINGS

void FSelectCrossPlatformAccountNode::Execute(
    TSharedRef<FAuthenticationGraphState> State,
    FAuthenticationGraphNodeOnDone OnDone)
{
    check(!State->HasSelectedEOSCandidate());

    for (const auto &Candidate : State->EOSCandidates)
    {
        if (Candidate.Type == EAuthenticationGraphEOSCandidateType::CrossPlatform &&
            (EOSString_ProductUserId::IsValid(Candidate.ProductUserId) ||
             !EOSString_ContinuanceToken::IsNone(Candidate.ContinuanceToken)))
        {
            State->SelectEOSCandidate(Candidate);
            OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
            return;
        }
    }

    State->ErrorMessages.Add(
        TEXT("SelectCrossPlatformAccountNode ran but there were no cross-platform candidates available"));
    OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION