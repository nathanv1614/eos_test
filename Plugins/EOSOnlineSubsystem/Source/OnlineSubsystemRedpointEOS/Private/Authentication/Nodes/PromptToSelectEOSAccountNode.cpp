// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptToSelectEOSAccountNode.h"

#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/EOSUserInterface_SelectEOSAccount.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"

EOS_ENABLE_STRICT_WARNINGS

void FPromptToSelectEOSAccountNode::Execute(
    TSharedRef<FAuthenticationGraphState> State,
    FAuthenticationGraphNodeOnDone OnDone)
{
    check(this->Widget == nullptr);

    this->Widget = MakeShared<TUserInterfaceRef<
        IEOSUserInterface_SelectEOSAccount,
        UEOSUserInterface_SelectEOSAccount,
        UEOSUserInterface_SelectEOSAccount_Context>>(
        State,
        State->Config->GetWidgetClass(
            TEXT("SelectEOSAccount"),
            TEXT("/OnlineSubsystemRedpointEOS/"
                 "EOSDefaultUserInterface_SelectEOSAccount.EOSDefaultUserInterface_SelectEOSAccount_C")),
        TEXT("IEOSUserInterface_SelectEOSAccount"));

    if (!this->Widget->IsValid())
    {
        State->ErrorMessages.Add(this->Widget->GetInvalidErrorMessage());
        this->Widget = nullptr;
        OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    check(!State->HasCurrentUserInterfaceWidget());

    this->Widget->GetContext()->SetOnSelectCandidate(FEOSUserInterface_SelectEOSAccount_OnSelectCandidate::CreateSP(
        this,
        &FPromptToSelectEOSAccountNode::SelectCandidate,
        State,
        OnDone));

    TArray<FEOSUserInterface_CandidateEOSAccount> Arr;
    for (const auto &Candidate : State->EOSCandidates)
    {
        FEOSUserInterface_CandidateEOSAccount Acc;
        Acc.DisplayName = Candidate.DisplayName;
        Acc.Candidate = Candidate;
        Arr.Add(Acc);
    }

    IEOSUserInterface_SelectEOSAccount::Execute_SetupUserInterface(
        this->Widget->GetWidget(),
        this->Widget->GetContext(),
        Arr);

    this->Widget->AddToState(State);
}

void FPromptToSelectEOSAccountNode::SelectCandidate(
    // First parameter is part of blueprint event signature.
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FEOSUserInterface_CandidateEOSAccount SelectedCandidate,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FAuthenticationGraphState> State,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FAuthenticationGraphNodeOnDone OnDone)
{
    State->SelectEOSCandidate(SelectedCandidate.Candidate);

    this->Widget->RemoveFromState(State);
    this->Widget = nullptr;

    OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION