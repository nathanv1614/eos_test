// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/GetExternalCredentialsFromEOSNativePlatformNode.h"
#include "OnlineSubsystemRedpointEOS/Public/EOSNativePlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"

#include "OnlineSubsystemUtils.h"

EOS_ENABLE_STRICT_WARNINGS

class FEOSNativePlatformExternalCredentials : public IOnlineExternalCredentials
{
private:
    TSharedPtr<FEOSConfig> Config;
    FOnlineAccountCredentials CurrentCredentials;
    TMap<FString, FString> CurrentAuthAttributes;

public:
    UE_NONCOPYABLE(FEOSNativePlatformExternalCredentials);
    FEOSNativePlatformExternalCredentials(
        TSharedPtr<FEOSConfig> InConfig,
        FOnlineAccountCredentials InitialCredentials,
        TMap<FString, FString> InitialAuthAttributes);
    virtual ~FEOSNativePlatformExternalCredentials(){};
    virtual FText GetProviderDisplayName() const override;
    virtual FString GetType() const override;
    virtual FString GetId() const override;
    virtual FString GetToken() const override;
    virtual TMap<FString, FString> GetAuthAttributes() const override;
    virtual void Refresh(
        TSoftObjectPtr<UWorld> InWorld,
        int32 LocalUserNum,
        FOnlineExternalCredentialsRefreshComplete OnComplete) override;
};

FEOSNativePlatformExternalCredentials::FEOSNativePlatformExternalCredentials(
    TSharedPtr<FEOSConfig> InConfig,
    FOnlineAccountCredentials InitialCredentials,
    TMap<FString, FString> InitialAuthAttributes)
{
    this->Config = MoveTemp(InConfig);
    this->CurrentCredentials = MoveTemp(InitialCredentials);
    this->CurrentAuthAttributes = MoveTemp(InitialAuthAttributes);
}

FText FEOSNativePlatformExternalCredentials::GetProviderDisplayName() const
{
    return FText::FromString(TEXT("TODO"));
}

FString FEOSNativePlatformExternalCredentials::GetType() const
{
    return this->CurrentCredentials.Type;
}

FString FEOSNativePlatformExternalCredentials::GetId() const
{
    return this->CurrentCredentials.Id;
}

FString FEOSNativePlatformExternalCredentials::GetToken() const
{
    return this->CurrentCredentials.Token;
}

TMap<FString, FString> FEOSNativePlatformExternalCredentials::GetAuthAttributes() const
{
    return this->CurrentAuthAttributes;
}

void FEOSNativePlatformExternalCredentials::Refresh(
    TSoftObjectPtr<UWorld> InWorld,
    int32 LocalUserNum,
    FOnlineExternalCredentialsRefreshComplete OnComplete)
{
    for (auto NativePlatform : Config->GetNativePlatforms())
    {
        if (!NativePlatform->CanRefreshCredentials(this->CurrentCredentials))
        {
            continue;
        }

        UE_LOG(LogEOS, Verbose, TEXT("Fetching refresh credentials for EOS native platform..."));
        NativePlatform->GetRefreshCredentials(
            this->CurrentCredentials,
            FGetCredentialsComplete::CreateLambda([WeakThis = GetWeakThis(this), OnComplete](
                                                      bool bWasSuccessful,
                                                      const FOnlineAccountCredentials &NewCredentials,
                                                      const TMap<FString, FString> &NewUserAuthAttributes) {
                if (auto ThisBase = PinWeakThis(WeakThis))
                {
                    auto This = StaticCastSharedPtr<FEOSNativePlatformExternalCredentials>(ThisBase);

                    if (!bWasSuccessful)
                    {
                        UE_LOG(LogEOS, Error, TEXT("Unable to fetch refreshed credentials from EOS native platform"));
                        OnComplete.ExecuteIfBound(false);
                        return;
                    }

                    This->CurrentCredentials = NewCredentials;
                    This->CurrentAuthAttributes = NewUserAuthAttributes;
                    OnComplete.ExecuteIfBound(true);
                    UE_LOG(LogEOS, Verbose, TEXT("Successfully refreshed EOS native platform credentials for user"));
                }
            }));
        return;
    }

    UE_LOG(LogEOS, Error, TEXT("No EOS native platform implementation was found to refresh credentials"));
}

void FGetExternalCredentialsFromEOSNativePlatformNode::GetCredentialsCallback(
    bool bWasSuccessful,
    FOnlineAccountCredentials Credentials,
    TMap<FString, FString> UserAuthAttributes,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FAuthenticationGraphState> State,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    IEOSNativePlatform *NativePlatform,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    std::function<void(bool)> OnPlatformDone)
{
    if (!bWasSuccessful)
    {
        State->ErrorMessages.Add(TEXT("GetExternalCredentialsFromEOSNativePlatformNode: Failed to obtain "
                                      "credentials, even though the platform indicated they were available"));
        OnPlatformDone(true);
        return;
    }

    State->AvailableExternalCredentials.Add(
        MakeShared<FEOSNativePlatformExternalCredentials>(State->Config, Credentials, UserAuthAttributes));
    OnPlatformDone(true);
}

void FGetExternalCredentialsFromEOSNativePlatformNode::Execute(
    TSharedRef<FAuthenticationGraphState> State,
    FAuthenticationGraphNodeOnDone OnStepDone)
{
    if (!State->Config->GetAllowNativePlatformAccounts())
    {
        // Don't collect any native platform accounts.
        OnStepDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
        return;
    }

    FMultiOperation<IEOSNativePlatform *, bool>::Run(
        State->Config->GetNativePlatforms(),
        [WeakThis = GetWeakThis(this),
         State](IEOSNativePlatform *NativePlatform, std::function<void(bool)> OnPlatformDone) -> bool {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (NativePlatform == nullptr)
                {
                    State->ErrorMessages.Add(
                        TEXT("GetExternalCredentialsFromEOSNativePlatformNode: Missing platform implementation"));
                    return false;
                }

                if (NativePlatform->HasCredentials(State->LocalUserNum))
                {
                    NativePlatform->GetCredentials(
                        State->LocalUserNum,
                        FGetCredentialsComplete::CreateSP(
                            StaticCastSharedRef<FGetExternalCredentialsFromEOSNativePlatformNode>(This.ToSharedRef()),
                            &FGetExternalCredentialsFromEOSNativePlatformNode::GetCredentialsCallback,
                            State,
                            NativePlatform,
                            MoveTemp(OnPlatformDone)));
                    return true;
                }
                else
                {
                    State->ErrorMessages.Add(FString::Printf(
                        TEXT("GetExternalCredentialsFromEOSNativePlatformNode: Native platform '%s' did not have "
                             "credentials available"),
                        *NativePlatform->GetDisplayName().ToString()));
                    return false;
                }
            }
            return false;
        },
        [OnStepDone](const TArray<bool> &Results) {
            OnStepDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
        });
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
