// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/Nodes/PromptToLinkEOSAccountsAgainstCrossPlatformNode.h"

#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/EOSUserInterface_LinkEOSAccountsAgainstCrossPlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"

EOS_ENABLE_STRICT_WARNINGS

void FPromptToLinkEOSAccountsAgainstCrossPlatformNode::Execute(
    TSharedRef<FAuthenticationGraphState> State,
    FAuthenticationGraphNodeOnDone OnDone)
{
    check(this->Widget == nullptr);

    TArray<FEOSUserInterface_CandidateEOSAccount> AvailableCandidates;
    for (const auto &Candidate : State->EOSCandidates)
    {
        if (!EOSString_ContinuanceToken::IsNone(Candidate.ContinuanceToken))
        {
            FEOSUserInterface_CandidateEOSAccount Acc;
            Acc.DisplayName = Candidate.DisplayName;
            Acc.Candidate = Candidate;
            AvailableCandidates.Add(Acc);
        }
        else if (EOSString_ProductUserId::IsValid(Candidate.ProductUserId))
        {
            FString ProductUserIdStr;
            if (EOSString_ProductUserId::ToString(Candidate.ProductUserId, ProductUserIdStr) ==
                EOS_EResult::EOS_Success)
            {
                UE_LOG(
                    LogEOS,
                    Verbose,
                    TEXT("Candidate on platform '%s' is already associated with another account: %s"),
                    *Candidate.DisplayName.ToString(),
                    *ProductUserIdStr);
            }
        }
    }

    if (AvailableCandidates.Num() == 0)
    {
        // No available candidiates, just continue.
        OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
        return;
    }

    this->Widget = MakeShared<TUserInterfaceRef<
        IEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform,
        UEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform,
        UEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform_Context>>(
        State,
        State->Config->GetWidgetClass(
            TEXT("LinkEOSAccountsAgainstCrossPlatform"),
            TEXT("/OnlineSubsystemRedpointEOS/"
                 "EOSDefaultUserInterface_LinkEOSAccountsAgainstCrossPlatform.EOSDefaultUserInterface_"
                 "LinkEOSAccountsAgainstCrossPlatform_"
                 "C")),
        TEXT("IEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform"));

    if (!this->Widget->IsValid())
    {
        State->ErrorMessages.Add(this->Widget->GetInvalidErrorMessage());
        this->Widget = nullptr;
        OnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    check(!State->HasCurrentUserInterfaceWidget());

    this->Widget->GetContext()->SetOnSelectedCandidates(
        FEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform_OnSelectedCandidates::CreateSP(
            this,
            &FPromptToLinkEOSAccountsAgainstCrossPlatformNode::SelectedCandidates,
            State,
            OnDone));

    IEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform::Execute_SetupUserInterface(
        this->Widget->GetWidget(),
        this->Widget->GetContext(),
        AvailableCandidates);

    this->Widget->AddToState(State);
}

void FPromptToLinkEOSAccountsAgainstCrossPlatformNode::SelectedCandidates(
    TArray<FEOSUserInterface_CandidateEOSAccount> SelectedCandidates,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FAuthenticationGraphState> State,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FAuthenticationGraphNodeOnDone OnStepDone)
{
    FMultiOperation<FEOSUserInterface_CandidateEOSAccount, bool>::Run(
        MoveTemp(SelectedCandidates),
        [WeakThis = GetWeakThis(this), State](
            const FEOSUserInterface_CandidateEOSAccount &SelectedAccount,
            const std::function<void(bool)> &OnCandidateDone) -> bool {
            if (auto This = PinWeakThis(WeakThis))
            {
                EOS_Connect_LinkAccountOptions Opts = {};
                Opts.ApiVersion = EOS_CONNECT_LINKACCOUNT_API_LATEST;
                Opts.ContinuanceToken = SelectedAccount.Candidate.ContinuanceToken;
                Opts.LocalUserId = State->GetSelectedEOSCandidate().ProductUserId;

                EOSRunOperation<EOS_HConnect, EOS_Connect_LinkAccountOptions, EOS_Connect_LinkAccountCallbackInfo>(
                    State->EOSConnect,
                    &Opts,
                    EOS_Connect_LinkAccount,
                    [WeakThis = GetWeakThis(This), State, OnCandidateDone](
                        const EOS_Connect_LinkAccountCallbackInfo *Info) {
                        if (Info->ResultCode != EOS_EResult::EOS_Success)
                        {
                            State->ErrorMessages.Add(FString::Printf(
                                TEXT("Link account operation failed with result code %s"),
                                ANSI_TO_TCHAR(EOS_EResult_ToString(Info->ResultCode))));
                        }

                        // Only log errors, but otherwise continue.
                        OnCandidateDone(true);
                    });
                return true;
            }
            return false;
        },
        [WeakThis = GetWeakThis(this), State, OnStepDone](const TArray<bool> &Results) {
            if (auto This =
                    StaticCastSharedPtr<FPromptToLinkEOSAccountsAgainstCrossPlatformNode>(PinWeakThis(WeakThis)))
            {
                This->Widget->RemoveFromState(State);
                This->Widget = nullptr;

                OnStepDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
            }
        });
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION