// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/EOSControlChannel.h"

#include "Engine/ActorChannel.h"
#include "Misc/Base64.h"
#include "Misc/NetworkVersion.h"
#include "Net/DataChannel.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/IInternetAddrEOS.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketEOS.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketSubsystemEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSDefines.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSRuntimePlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineIdentityInterfaceEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineSubsystemRedpointEOS.h"
#include "OnlineSubsystemUtils.h"

UEOSControlChannel::UEOSControlChannel(const FObjectInitializer &ObjectInitializer) : UControlChannel(ObjectInitializer)
{
    this->bSeenHello = false;
    this->bClientTrustsServer = false;
    FMemory::Memzero(&this->server_connection_unique_kp, sizeof(this->server_connection_unique_kp));
    FMemory::Memzero(&this->client_session_kp, sizeof(this->client_session_kp));
    FMemory::Memzero(&this->server_session_kp, sizeof(this->server_session_kp));
#if !defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
    FMemory::Memzero(&this->AESGCMKey, sizeof(this->AESGCMKey));
#endif // #if !defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
}

FOnlineSubsystemEOS *UEOSControlChannel::GetOSS() const
{
    auto Driver = Cast<UEOSNetDriver>(this->Connection->Driver);
    if (!IsValid(Driver))
    {
        return nullptr;
    }

    auto ConnWorld = Driver->FindWorld();
    if (ConnWorld == nullptr)
    {
        return nullptr;
    }

    FOnlineSubsystemEOS *OSS = (FOnlineSubsystemEOS *)Online::GetSubsystem(ConnWorld, REDPOINT_EOS_SUBSYSTEM);
    return OSS;
}

const FEOSConfig *UEOSControlChannel::GetConfig() const
{
    FOnlineSubsystemEOS *OSS = this->GetOSS();
    if (OSS == nullptr)
    {
        return nullptr;
    }

    return &OSS->GetConfig();
}

EEOSNetDriverRole UEOSControlChannel::GetRole() const
{
    auto LocalNetDriver = Cast<UEOSNetDriver>(this->Connection->Driver);
    if (!IsValid(LocalNetDriver))
    {
        return (EEOSNetDriverRole)-1;
    }
    return LocalNetDriver->GetEOSRole();
}

void UEOSControlChannel::GetAntiCheat(
    TSharedPtr<IAntiCheat> &OutAntiCheat,
    TSharedPtr<FAntiCheatSession> &OutAntiCheatSession,
    bool &OutIsBeacon) const
{
    auto LocalNetDriver = Cast<UEOSNetDriver>(this->Connection->Driver);
    if (!IsValid(LocalNetDriver))
    {
        OutIsBeacon = false;
        return;
    }
    OutAntiCheat = LocalNetDriver->AntiCheat.Pin();
    OutAntiCheatSession = LocalNetDriver->AntiCheatSession;
    OutIsBeacon = LocalNetDriver->bIsOwnedByBeacon;
}

void UEOSControlChannel::On_NMT_Hello(uint8 IsLittleEndian, uint32 RemoteNetworkVersion)
{
    checkf(
        this->bEnableAutomaticEncryption,
        TEXT("NMT_Hello should not be intercepted if automatic encryption is not enabled."));

    if (this->GetRole() != EEOSNetDriverRole::DedicatedServer)
    {
        // We only handle NMT_Hello on dedicated servers. Passthrough.
        FString EmptyEncryptionToken = TEXT("");
        FOutBunch Bunch;
        Bunch << IsLittleEndian;
        Bunch << RemoteNetworkVersion;
        Bunch << EmptyEncryptionToken;

        // Propagate to the Notify, which will continue the connection.
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        this->Connection->Driver->Notify->NotifyControlMessage(this->Connection, NMT_Hello, InBunch);
        this->bSeenHello = false;
        return;
    }

    if (this->bSeenHello)
    {
        return;
    }

    this->bSeenHello = true;
    this->OriginalHelloIsLittleEndian = IsLittleEndian;
    this->OriginalHelloRemoteNetworkVersion = RemoteNetworkVersion;

    uint32 LocalNetworkVersion = FNetworkVersion::GetLocalNetworkVersion();
    if (!FNetworkVersion::IsNetworkCompatible(LocalNetworkVersion, RemoteNetworkVersion))
    {
        // Reconstruct for forwarding.
        FString EmptyEncryptionToken = TEXT("");
        FOutBunch Bunch;
        Bunch << IsLittleEndian;
        Bunch << RemoteNetworkVersion;
        Bunch << EmptyEncryptionToken;

        // Propagate to the Notify, which will close the connection.
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        this->Connection->Driver->Notify->NotifyControlMessage(this->Connection, NMT_Hello, InBunch);
        this->bSeenHello = false;
        return;
    }

    // Load the public/private signing keypair from configuration.
    bool bLoadedKey = false;
    hydro_sign_keypair server_key_pair = {};
    const FEOSConfig *Config = this->GetConfig();
    if (Config != nullptr)
    {
        FString PublicKey = Config->GetDedicatedServerPublicKey();
        FString PrivateKey = Config->GetDedicatedServerPrivateKey();
        if (!PublicKey.IsEmpty() && !PrivateKey.IsEmpty())
        {
            TArray<uint8> PublicKeyBytes, PrivateKeyBytes;
            if (FBase64::Decode(PublicKey, PublicKeyBytes) && FBase64::Decode(PrivateKey, PrivateKeyBytes))
            {
                if (PublicKeyBytes.Num() == sizeof(server_key_pair.pk) &&
                    PrivateKeyBytes.Num() == sizeof(server_key_pair.sk))
                {
                    FMemory::Memcpy(server_key_pair.pk, PublicKeyBytes.GetData(), sizeof(server_key_pair.pk));
                    FMemory::Memcpy(server_key_pair.sk, PrivateKeyBytes.GetData(), sizeof(server_key_pair.sk));
                    bLoadedKey = true;
                }
            }
        }
    }
    if (!bLoadedKey)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Rejecting player connection on dedicated server because the dedicated server could not load the "
                 "public/private signing keypair."));
        FString ErrorMessage(
            TEXT("Failed to verify your account or EAC integrity on connection. You have been disconnected."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        this->bSeenHello = false;
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Generate a unique public/private keypair so that the client can encrypt it's public key specifically for this
    // server (in a way that would not be interceptable by another dedicated server or even an attacker with the signing
    // private key).
    FMemory::Memzero(&this->server_connection_unique_kp, sizeof(this->server_connection_unique_kp));
    hydro_kx_keygen(&this->server_connection_unique_kp);

    // Sign the session public key with the signing key.
    uint8_t signature[hydro_sign_BYTES];
    if (hydro_sign_create(
            signature,
            this->server_connection_unique_kp.pk,
            sizeof(this->server_connection_unique_kp.pk),
            "SIGNING0",
            server_key_pair.sk) != 0)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Rejecting player connection on dedicated server because the dedicated server could not sign the "
                 "connection keypair."));
        FString ErrorMessage(
            TEXT("Failed to verify your account or EAC integrity on connection. You have been disconnected."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        this->bSeenHello = false;
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Send the signed public key to the client for them to verify and use.
    FString EncodedConnectionPublicKey = FBase64::Encode(
        TArray<uint8>(this->server_connection_unique_kp.pk, sizeof(this->server_connection_unique_kp.pk)));
    FString EncodedConnectionPublicKeySignature = FBase64::Encode(TArray<uint8>(signature, sizeof(signature)));
    FNetControlMessage<NMT_EOS_RequestClientEphemeralKey>::Send(
        this->Connection,
        EncodedConnectionPublicKey,
        EncodedConnectionPublicKeySignature);
    Connection->FlushNet(true);
}

void UEOSControlChannel::On_NMT_EOS_RequestClientEphemeralKey(
    const FString &ServerConnectionPublicKey,
    const FString &ServerConnectionPublicKeySignature)
{
#if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
    UE_LOG(
        LogEOS,
        Error,
        TEXT("Dedicated server requested automatic encryption negotiation, but this platform is not compiled with "
             "automatic encryption support. Refer to the OnlineSubsystemRedpointEOS_<Platform>.Build.cs file for "
             "instructions on how enable automatic encryption."));
    FString ErrorMessage(TEXT("Automatic encryption support not compiled into this platform."));
    FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
    Connection->FlushNet(true);
    Connection->Close();
    return;
#else
    checkf(
        this->bEnableAutomaticEncryption,
        TEXT("NMT_EOS_RequestClientEphemeralKey should not be handled if automatic encryption is not enabled."));

    if (this->GetRole() != EEOSNetDriverRole::ClientConnectedToDedicatedServer)
    {
        // Unexpected packet for anything other than a client connected to a dedicated server. Disconnect in case the
        // server is malicious.
        UE_LOG(
            LogEOS,
            Error,
            TEXT(
                "Disconnecting from remote host because NMT_EOS_RequestClientEphemeralKey was received when it was not "
                "expected."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0001)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Load the public signing key from configuration.
    bool bLoadedKey = false;
    hydro_sign_keypair server_key_pair = {};
    const FEOSConfig *Config = this->GetConfig();
    if (Config != nullptr)
    {
        FString PublicKey = Config->GetDedicatedServerPublicKey();
        if (!PublicKey.IsEmpty())
        {
            TArray<uint8> PublicKeyBytes;
            if (FBase64::Decode(PublicKey, PublicKeyBytes))
            {
                if (PublicKeyBytes.Num() == sizeof(server_key_pair.pk))
                {
                    FMemory::Memcpy(server_key_pair.pk, PublicKeyBytes.GetData(), sizeof(server_key_pair.pk));
                    bLoadedKey = true;
                }
            }
        }
    }
    if (!bLoadedKey)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because we could not load the dedicated server signing public key on "
                 "the client when handling NMT_EOS_RequestClientEphemeralKey."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0002)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Read the public key and signature.
    uint8_t temp_connection_unique_pk[hydro_kx_PUBLICKEYBYTES] = {};
    uint8_t signature[hydro_sign_BYTES] = {};
    TArray<uint8> ServerConnectionPublicKeyBytes;
    TArray<uint8> ServerConnectionPublicKeySignatureBytes;
    if (!FBase64::Decode(ServerConnectionPublicKey, ServerConnectionPublicKeyBytes) ||
        !FBase64::Decode(ServerConnectionPublicKeySignature, ServerConnectionPublicKeySignatureBytes) ||
        ServerConnectionPublicKeyBytes.Num() != sizeof(temp_connection_unique_pk) ||
        ServerConnectionPublicKeySignatureBytes.Num() != hydro_sign_BYTES)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT(
                "Disconnecting from remote host because NMT_EOS_RequestClientEphemeralKey was received but had invalid "
                "data."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0003)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }
    FMemory::Memcpy(
        &temp_connection_unique_pk,
        ServerConnectionPublicKeyBytes.GetData(),
        sizeof(temp_connection_unique_pk));
    FMemory::Memcpy(&signature, ServerConnectionPublicKeySignatureBytes.GetData(), sizeof(signature));

    // Verify that the public key is signed correctly.
    if (hydro_sign_verify(
            signature,
            temp_connection_unique_pk,
            sizeof(temp_connection_unique_pk),
            "SIGNING0",
            server_key_pair.pk) != 0)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because signature verification of provided public key in "
                 "NMT_EOS_RequestClientEphemeralKey failed (untrusted server)."));
        FString ErrorMessage(TEXT("The client does not trust you as a dedicated server."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // The connection public key is valid at this point *and* we now trust the server. Mark this connection as trusted.
    // NOTE: This doesn't mean that communications are implicitly secure yet - we are still a way off from negotiating
    // the symmetric AES key. But it does mean that when encrypted communications have been established, we trust
    // the server such that we will provide them our external platform credentials to verify our account.
    this->bClientTrustsServer = true;

    // Generate the client session keys and the packet to send the ephemeral public key back to the server.
    FMemory::Memzero(&this->client_session_kp, sizeof(this->client_session_kp));
    uint8_t client_packet[hydro_kx_N_PACKET1BYTES];
    if (hydro_kx_n_1(&this->client_session_kp, client_packet, NULL, temp_connection_unique_pk) != 0)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because generation of response packet failed from libhydrogen."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0004)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Send the ephemeral key back to the server.
    FString EphemeralKey = FBase64::Encode(TArray<uint8>(client_packet, sizeof(client_packet)));
    FNetControlMessage<NMT_EOS_DeliverClientEphemeralKey>::Send(this->Connection, EphemeralKey);
    Connection->FlushNet(true);
#endif // #if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
}

void UEOSControlChannel::On_NMT_EOS_DeliverClientEphemeralKey(const FString &ClientEphemeralPacket)
{
#if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
    UE_LOG(
        LogEOS,
        Error,
        TEXT("Dedicated server requested automatic encryption negotiation, but this platform is not compiled with "
             "automatic encryption support. Refer to the OnlineSubsystemRedpointEOS_<Platform>.Build.cs file for "
             "instructions on how enable automatic encryption."));
    FString ErrorMessage(TEXT("Automatic encryption support not compiled into this platform."));
    FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
    Connection->FlushNet(true);
    Connection->Close();
    return;
#else
    checkf(
        this->bEnableAutomaticEncryption,
        TEXT("NMT_EOS_DeliverClientEphemeralKey should not be handled if automatic encryption is not enabled."));

    if (this->GetRole() != EEOSNetDriverRole::DedicatedServer)
    {
        // Unexpected packet for anything other than a dedicated server. Disconnect the remote host.
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting the remote host because NMT_EOS_DeliverClientEphemeralKey was not an expected "
                 "packet."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0005)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Read ephemeral key packet.
    uint8_t client_ephemeral_packet[hydro_kx_N_PACKET1BYTES] = {};
    TArray<uint8> ClientEphemeralPacketBytes;
    if (!FBase64::Decode(ClientEphemeralPacket, ClientEphemeralPacketBytes) ||
        ClientEphemeralPacketBytes.Num() != sizeof(client_ephemeral_packet))
    {
        // Unexpected packet for anything other than a dedicated server. Disconnect the remote host.
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting the remote host because NMT_EOS_DeliverClientEphemeralKey was not valid."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0006)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }
    FMemory::Memcpy(&client_ephemeral_packet, ClientEphemeralPacketBytes.GetData(), sizeof(client_ephemeral_packet));

    // Verify and process ephemeral key.
    FMemory::Memzero(&this->server_session_kp, sizeof(this->server_session_kp));
    if (hydro_kx_n_2(&this->server_session_kp, client_ephemeral_packet, NULL, &this->server_connection_unique_kp) != 0)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting the remote host because NMT_EOS_DeliverClientEphemeralKey could not be verified."));
        FString ErrorMessage(TEXT("An encrypted connection could not be established (0x0007)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Generate an AES GCM key and store it.
    FMemory::Memzero(&this->AESGCMKey, sizeof(this->AESGCMKey));
    hydro_random_buf(&this->AESGCMKey, sizeof(this->AESGCMKey));

    // Send the AES GCM key encrypted with session key down to the client.
    TArray<uint8> EncryptedAESKey;
    EncryptedAESKey.SetNumZeroed(hydro_secretbox_HEADERBYTES + sizeof(this->AESGCMKey));
    if (hydro_secretbox_encrypt(
            EncryptedAESKey.GetData(),
            this->AESGCMKey,
            sizeof(this->AESGCMKey),
            0,
            "AESEXCHG",
            this->server_session_kp.tx) != 0)
    {
        UE_LOG(LogEOS, Error, TEXT("Disconnecting the remote host because we could not encrypt the shared key."));
        FString ErrorMessage(TEXT("An encrypted connection could not be established (0x0008)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Send the encrypted AES key down to the client.
    FString EncodedEncryptedAESKey = FBase64::Encode(EncryptedAESKey);
    FNetControlMessage<NMT_EOS_SymmetricKeyExchange>::Send(this->Connection, EncodedEncryptedAESKey);
    Connection->FlushNet(true);

    // Enable encryption on the server. We don't pass an encryption token into NMT_Hello or try to register
    // FNetDelegates::OnReceivedNetworkEncryptionToken because we don't need to (we've effectively done
    // the ack when sending NMT_EOS_SymmetricKeyExchange to the client).
    FEncryptionData EncryptionData;
    EncryptionData.Fingerprint = TArray<uint8>();
    EncryptionData.Identifier = TEXT("");
    EncryptionData.Key = TArray<uint8>(this->AESGCMKey, sizeof(this->AESGCMKey));
    this->Connection->EnableEncryptionServer(EncryptionData);

    UE_LOG(LogEOS, Verbose, TEXT("Encrypted connection on server established."));

    // Prepare NMT_Hello for forwarding.
    FString UnusedEncryptionToken = TEXT("");
    FOutBunch Bunch;
    Bunch << this->OriginalHelloIsLittleEndian;
    Bunch << this->OriginalHelloRemoteNetworkVersion;
    Bunch << UnusedEncryptionToken;

    // Propagate to the Notify, which will continue the connection.
    FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
    this->Connection->Driver->Notify->NotifyControlMessage(this->Connection, NMT_Hello, InBunch);
#endif // #if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
}

void UEOSControlChannel::On_NMT_EOS_SymmetricKeyExchange(const FString &EncryptedSymmetricKey)
{
#if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
    UE_LOG(
        LogEOS,
        Error,
        TEXT("Dedicated server requested automatic encryption negotiation, but this platform is not compiled with "
             "automatic encryption support. Refer to the OnlineSubsystemRedpointEOS_<Platform>.Build.cs file for "
             "instructions on how enable automatic encryption."));
    FString ErrorMessage(TEXT("Automatic encryption support not compiled into this platform."));
    FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
    Connection->FlushNet(true);
    Connection->Close();
    return;
#else
    checkf(
        this->bEnableAutomaticEncryption,
        TEXT("NMT_EOS_SymmetricKeyExchange should not be handled if automatic encryption is not enabled."));

    if (this->GetRole() != EEOSNetDriverRole::ClientConnectedToDedicatedServer)
    {
        // Unexpected packet for anything other than a client connected to a dedicated server. Disconnect.
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because NMT_EOS_SymmetricKeyExchange was received when it was not "
                 "expected."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0009)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Decode symmetric key.
    TArray<uint8> EncryptedSymmetricKeyBytes;
    if (!FBase64::Decode(EncryptedSymmetricKey, EncryptedSymmetricKeyBytes) ||
        EncryptedSymmetricKeyBytes.Num() != hydro_secretbox_HEADERBYTES + sizeof(this->AESGCMKey))
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because NMT_EOS_SymmetricKeyExchange was not valid."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0010)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Decrypt symmetric key.
    if (hydro_secretbox_decrypt(
            this->AESGCMKey,
            EncryptedSymmetricKeyBytes.GetData(),
            EncryptedSymmetricKeyBytes.Num(),
            0,
            "AESEXCHG",
            this->client_session_kp.rx) != 0)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because we could not decrypt the symmetric key in "
                 "NMT_EOS_SymmetricKeyExchange."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0011)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    // Enable encryption on the client.
    FEncryptionData EncryptionData;
    EncryptionData.Fingerprint = TArray<uint8>();
    EncryptionData.Identifier = TEXT("");
    EncryptionData.Key = TArray<uint8>(this->AESGCMKey, sizeof(this->AESGCMKey));
    this->Connection->EnableEncryption(EncryptionData);

    UE_LOG(LogEOS, Verbose, TEXT("Encrypted connection on client established."));
#endif // #if defined(EOS_AUTOMATIC_ENCRYPTION_UNAVAILABLE)
}

void UEOSControlChannel::FailUserVerification(const FUniqueNetId &UserId, const FString &ErrorMessage)
{
    UE_LOG(
        LogEOS,
        Error,
        TEXT("Server authentication: %s: Failed verification with error: %s"),
        *UserId.ToString(),
        *ErrorMessage);

    if (this->VerificationDatabase.Contains(UserId))
    {
        this->VerificationDatabase[UserId]->CurrentStatus = EUserVerificationStatus::Failed;
        this->VerificationDatabase[UserId]->ErrorMessage = ErrorMessage;
        for (const auto &Cb : this->VerificationDatabase[UserId]->CallbacksWaiting)
        {
            Cb.ExecuteIfBound(EUserVerificationStatus::Failed, ErrorMessage);
        }
        this->VerificationDatabase[UserId]->CallbacksWaiting.Empty();
    }
}

void UEOSControlChannel::PassUserVerification(const FUniqueNetId &UserId)
{
    UE_LOG(LogEOS, Verbose, TEXT("Server authentication: %s: Successfully verified user."), *UserId.ToString());

    if (this->VerificationDatabase.Contains(UserId))
    {
        this->VerificationDatabase[UserId]->CurrentStatus = EUserVerificationStatus::Verified;
        for (const auto &Cb : this->VerificationDatabase[UserId]->CallbacksWaiting)
        {
            Cb.ExecuteIfBound(EUserVerificationStatus::Verified, TEXT(""));
        }
        this->VerificationDatabase[UserId]->CallbacksWaiting.Empty();
    }
}

void UEOSControlChannel::QueueUserVerification(const FUniqueNetId &UserId, const FUserVerificationComplete &OnDone)
{
    if (this->VerificationDatabase.Contains(UserId))
    {
        if (this->VerificationDatabase[UserId]->CurrentStatus == EUserVerificationStatus::Verified ||
            this->VerificationDatabase[UserId]->CurrentStatus == EUserVerificationStatus::Failed)
        {
            OnDone.ExecuteIfBound(
                this->VerificationDatabase[UserId]->CurrentStatus,
                this->VerificationDatabase[UserId]->ErrorMessage);
            return;
        }

        this->VerificationDatabase[UserId]->CallbacksWaiting.Add(OnDone);
        return;
    }

    TSharedRef<FUserVerificationState> VerificationState = MakeShared<FUserVerificationState>();
    VerificationState->CallbacksWaiting.Add(OnDone);
    VerificationState->CurrentStatus = EUserVerificationStatus::NotStarted;
    VerificationState->bRegisteredForAntiCheat = false;
    this->VerificationDatabase.Add(UserId, VerificationState);

    UE_LOG(LogEOS, Verbose, TEXT("Server authentication: %s: Starting verification of user..."), *UserId.ToString());

    if (!IsValid(this->Connection))
    {
        this->FailUserVerification(UserId, TEXT("Connection not valid."));
        return;
    }

    EEOSNetDriverRole DriverRole = this->GetRole();
    if (DriverRole == EEOSNetDriverRole::DedicatedServer)
    {
        VerificationState->CurrentStatus = EUserVerificationStatus::CheckingAccountExistsFromDedicatedServer;

        if (!this->Connection->IsEncryptionEnabled())
        {
            this->FailUserVerification(UserId, TEXT("Connection is not encrypted."));
            return;
        }

        // Ask the client to provide us the token for the given user.
        FUniqueNetIdRepl UserIdRepl(UserId);
        FNetControlMessage<NMT_EOS_RequestClientToken>::Send(this->Connection, UserIdRepl);
    }
    else
    {
        VerificationState->CurrentStatus = EUserVerificationStatus::CheckingAccountExistsFromListenServer;

        FOnlineSubsystemEOS *OSS = this->GetOSS();
        if (OSS == nullptr)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Server authentication: %s: Disconnecting client because we could not access our online "
                     "subsystem."),
                *UserId.ToString());
            this->FailUserVerification(UserId, TEXT("Server error (0x0016)."));
            return;
        }

        FDelegateHandle Handle = OSS->GetUserInterface()->AddOnQueryUserInfoCompleteDelegate_Handle(
            0,
            FOnQueryUserInfoCompleteDelegate::CreateUObject(
                this,
                &UEOSControlChannel::OnUserInfoReceived,
                VerificationState,
                UserId.AsShared()));
        VerificationState->UserInfoHandle = Handle;
        TArray<TSharedRef<const FUniqueNetId>> UserIds;
        UserIds.Add(UserId.AsShared());
        if (!OSS->GetUserInterface()->QueryUserInfo(0, UserIds))
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Server authentication: %s: Disconnecting client because we could not call "
                     "IOnlineUser::QueryUserInfo."),
                *UserId.ToString());
            this->FailUserVerification(UserId, TEXT("Server error (0x0021)."));
            return;
        }
    }
}

void UEOSControlChannel::OnUserInfoReceived(
    int32 LocalUserNum,
    bool bWasSuccessful,
    const TArray<TSharedRef<const FUniqueNetId>> &UserIds,
    const FString &ErrorString,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FUserVerificationState> VerificationState,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<const FUniqueNetId> UserId)
{
    if (LocalUserNum != 0)
    {
        // Not the event we're interested in.
        return;
    }
    if (UserIds.Num() != 1 || *UserIds[0] != *UserId)
    {
        // Not the event we're interested in.
        return;
    }

    FOnlineSubsystemEOS *OSS = this->GetOSS();
    if (OSS == nullptr)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Disconnecting client because we could not access our online "
                 "subsystem."),
            *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Server error (0x0016)."));
        return;
    }

    OSS->GetUserInterface()->ClearOnQueryUserInfoCompleteDelegate_Handle(0, VerificationState->UserInfoHandle);

    if (!bWasSuccessful)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Disconnecting client because we could not obtain their user information: "
                 "%s."),
            *UserId->ToString(),
            *ErrorString);
        this->FailUserVerification(*UserId, TEXT("Your user account could not be found (0x0022)."));
        return;
    }

    TSharedPtr<FOnlineUser> TargetUser = OSS->GetUserInterface()->GetUserInfo(0, *UserId);
    if (!TargetUser.IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Disconnecting client because the user account doesn't exist."),
            *UserId->ToString(),
            *ErrorString);
        this->FailUserVerification(*UserId, TEXT("Your user account could not be found (0x0023)."));
        return;
    }

    // We've done as much verification as we can on a listen server. Proceed to sanctions check or pass depending on EOS
    // SDK level.
    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("Server authentication: %s: Successfully found user with IOnlineUser::GetUserInfo."),
        *UserId->ToString());
    // Set PlayerId early so that Anti-Cheat can route outbound packets in UEOSNetDriver.
    this->Connection->PlayerId = FUniqueNetIdRepl(UserId);
#if EOS_VERSION_AT_LEAST(1, 11, 0)
    if (this->bEnableSanctionChecks)
    {
        this->CheckSanctions(StaticCastSharedRef<const FUniqueNetIdEOS>(UserId));
    }
    else
    {
#if EOS_VERSION_AT_LEAST(1, 12, 0)
        const FEOSConfig *Config = this->GetConfig();
        if (Config != nullptr && Config->GetEnableAntiCheat())
        {
            this->NegotiateAntiCheat(StaticCastSharedRef<const FUniqueNetIdEOS>(UserId));
        }
        else
        {
            this->PassUserVerification(*UserId);
        }
#else
        this->PassUserVerification(*UserId);
#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)
    }
#else
    // Note: No need to handle Anti-Cheat here, because Anti-Cheat was only introduced
    // in 1.12, and therefore sanctions code has to run if Anti-Cheat is available.
    this->PassUserVerification(*UserId);
#endif
}

#if EOS_VERSION_AT_LEAST(1, 11, 0)

void UEOSControlChannel::CheckSanctions(const TSharedRef<const FUniqueNetIdEOS> &UserId)
{
    this->VerificationDatabase[*UserId]->CurrentStatus = EUserVerificationStatus::CheckingSanctions;

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("Server authentication: %s: Checking to see if user has any BAN sanctions..."),
        *UserId->ToString());

    FOnlineSubsystemEOS *OSS = this->GetOSS();
    if (OSS == nullptr)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Disconnecting client because we could not access our online subsystem."),
            *UserId->ToString());
        FString ErrorMessage(TEXT("Invalid protocol message (0x0016)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    EOS_HSanctions EOSSanctions = EOS_Platform_GetSanctionsInterface(OSS->GetPlatformInstance());

    EOS_Sanctions_QueryActivePlayerSanctionsOptions Opts = {};
    Opts.ApiVersion = EOS_SANCTIONS_QUERYACTIVEPLAYERSANCTIONS_API_LATEST;
    if (this->GetRole() == EEOSNetDriverRole::DedicatedServer)
    {
        Opts.LocalUserId = nullptr;
    }
    else
    {
        auto Identity = OSS->GetIdentityInterface();
        checkf(Identity.IsValid(), TEXT("Identity interface is not valid on listen server"));
        TArray<TSharedPtr<FUserOnlineAccount>> Accounts = Identity->GetAllUserAccounts();
        checkf(Accounts.Num() > 0, TEXT("No locally signed in account when checking sanctions on listen server"));
        Opts.LocalUserId = StaticCastSharedRef<const FUniqueNetIdEOS>(Accounts[0]->GetUserId())->GetProductUserId();
    }
    Opts.TargetUserId = UserId->GetProductUserId();

    EOSRunOperation<
        EOS_HSanctions,
        EOS_Sanctions_QueryActivePlayerSanctionsOptions,
        EOS_Sanctions_QueryActivePlayerSanctionsCallbackInfo>(
        EOSSanctions,
        &Opts,
        &EOS_Sanctions_QueryActivePlayerSanctions,
        [WeakThis = TSoftObjectPtr<UEOSControlChannel>(this), EOSSanctions, UserId](
            const EOS_Sanctions_QueryActivePlayerSanctionsCallbackInfo *Data) {
            if (WeakThis.IsValid())
            {
                auto This = WeakThis.Get();
                if (Data->ResultCode == EOS_EResult::EOS_Success)
                {
                    bool bIsDenied = false;
                    EOS_Sanctions_GetPlayerSanctionCountOptions CountOpts = {};
                    CountOpts.ApiVersion = EOS_SANCTIONS_GETPLAYERSANCTIONCOUNT_API_LATEST;
                    CountOpts.TargetUserId = UserId->GetProductUserId();
                    uint32_t SanctionCount = EOS_Sanctions_GetPlayerSanctionCount(EOSSanctions, &CountOpts);
                    for (uint32_t i = 0; i < SanctionCount; i++)
                    {
                        bool bStop = false;
                        EOS_Sanctions_PlayerSanction *Sanction = nullptr;
                        EOS_Sanctions_CopyPlayerSanctionByIndexOptions CopyOpts = {};
                        CopyOpts.ApiVersion = EOS_SANCTIONS_COPYPLAYERSANCTIONBYINDEX_API_LATEST;
                        CopyOpts.SanctionIndex = i;
                        CopyOpts.TargetUserId = UserId->GetProductUserId();
                        EOS_EResult CopyResult =
                            EOS_Sanctions_CopyPlayerSanctionByIndex(EOSSanctions, &CopyOpts, &Sanction);
                        if (CopyResult == EOS_EResult::EOS_Success)
                        {
                            if (FString(ANSI_TO_TCHAR(Sanction->Action)) == TEXT("BAN"))
                            {
                                // User is banned.
                                UE_LOG(
                                    LogEOS,
                                    Error,
                                    TEXT("Server authentication: %s: User is banned because they have an active BAN "
                                         "sanction."),
                                    *UserId->ToString());
                                This->FailUserVerification(
                                    *UserId,
                                    TEXT("You are currently banned from playing this game."));
                                bStop = true;
                                bIsDenied = true;
                            }

                            // Otherwise this is some other kind of sanction we don't handle yet.
                        }
                        else
                        {
                            UE_LOG(
                                LogEOS,
                                Error,
                                TEXT("Server authentication: %s: Failed to copy sanction at index %u, got result code "
                                     "%s on server."),
                                *UserId->ToString(),
                                i,
                                ANSI_TO_TCHAR(EOS_EResult_ToString(Data->ResultCode)));
                            This->FailUserVerification(*UserId, TEXT("Sanction check failure (0x0020)."));
                            bStop = true;
                            bIsDenied = true;
                        }
                        if (Sanction != nullptr)
                        {
                            EOS_Sanctions_PlayerSanction_Release(Sanction);
                        }
                        if (bStop)
                        {
                            // Must do this after EOS_Sanctions_PlayerSanction_Release and not use break directly.
                            break;
                        }
                    }
                    if (!bIsDenied)
                    {
                        UE_LOG(
                            LogEOS,
                            Verbose,
                            TEXT("Server authentication: %s: Did not find any BAN sanctions for user."),
                            *UserId->ToString());
#if EOS_VERSION_AT_LEAST(1, 12, 0)
                        const FEOSConfig *Config = This->GetConfig();
                        if (Config != nullptr && Config->GetEnableAntiCheat())
                        {
                            This->NegotiateAntiCheat(UserId);
                        }
                        else
                        {
                            This->PassUserVerification(*UserId);
                        }
#else
                        This->PassUserVerification(*UserId);
#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)
                    }
                }
                else
                {
                    UE_LOG(
                        LogEOS,
                        Error,
                        TEXT("Server authentication: %s: Failed to retrieve sanctions for user, got result code %s on "
                             "server."),
                        *UserId->ToString(),
                        ANSI_TO_TCHAR(EOS_EResult_ToString(Data->ResultCode)));
                    This->FailUserVerification(*UserId, TEXT("Sanction check failure (0x0019)."));
                }
            }
        });
}

#endif // #if EOS_VERSION_AT_LEAST(1, 11, 0)

#if EOS_VERSION_AT_LEAST(1, 12, 0)

void UEOSControlChannel::NegotiateAntiCheat(const TSharedRef<const FUniqueNetIdEOS> &UserId)
{
    this->VerificationDatabase[*UserId]->CurrentStatus = EUserVerificationStatus::EstablishingAntiCheatProof;

    TSharedPtr<IAntiCheat> AntiCheat;
    TSharedPtr<FAntiCheatSession> AntiCheatSession;
    bool bIsOwnedByBeacon;
    this->GetAntiCheat(AntiCheat, AntiCheatSession, bIsOwnedByBeacon);
    if (bIsOwnedByBeacon)
    {
        // Beacons do not use Anti-Cheat, because Anti-Cheat only allows one Anti-Cheat connection
        // for the main game session.
        UE_LOG(
            LogEOS,
            Verbose,
            TEXT("Server authentication: %s: Skipping Anti-Cheat connection because this is a beacon connection..."),
            *UserId->ToString());
        this->PassUserVerification(*UserId);
        return;
    }
    if (!AntiCheat.IsValid() || !AntiCheatSession.IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Failed to obtain Anti-Cheat interface or session."),
            *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Anti-Cheat interface or session not initialized."));
        return;
    }

    FUniqueNetIdRepl UserIdRepl(UserId->AsShared());

    // Check if we can request a proof.
    auto Config = this->GetConfig();
    if (Config == nullptr)
    {
        UE_LOG(LogEOS, Error, TEXT("Server authentication: %s: Failed to obtain configuration."), *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Invalid server state."));
        return;
    }
    FString TrustedClientPublicKey = Config->GetTrustedClientPublicKey();
    if (TrustedClientPublicKey.IsEmpty())
    {
        // We can't request proof verification from the client, because we don't have a public key.
        this->On_NMT_EOS_DeliverTrustedClientProof(UserIdRepl, false, TEXT(""), TEXT(""));
        return;
    }

    // Request the trusted client proof from the client. Certain platforms like consoles are able to run as
    // unprotected clients and still be secure, so they don't need Anti-Cheat active. To ensure that clients don't
    // pretend as if they're on console, we make clients send a cryptographic proof. Only the console builds contain the
    // private key necessary for signing the proof.
    UE_LOG(LogEOS, Verbose, TEXT("Server authentication: %s: Requesting trusted client proof..."), *UserId->ToString());
    char Nonce[32];
    FMemory::Memzero(&Nonce, 32);
    hydro_random_buf(&Nonce, 32);
    FString NonceEncoded = FBase64::Encode((uint8 *)&Nonce, 32);
    this->PendingNonceChecks.Add(*UserId, NonceEncoded);
    FNetControlMessage<NMT_EOS_RequestTrustedClientProof>::Send(this->Connection, UserIdRepl, NonceEncoded);
}

void UEOSControlChannel::On_NMT_EOS_RequestTrustedClientProof(FUniqueNetIdRepl UserId, const FString &EncodedNonce)
{
    bool bCanProvideProof = false;
    FString EncodedProof = TEXT("");
    FString PlatformString = TEXT("");

    auto Config = this->GetConfig();
    if (Config == nullptr)
    {
        UE_LOG(LogEOS, Warning, TEXT("Can't provide client proof, as configuration is not available."));
    }
    else
    {
        FString TrustedClientPrivateKey = Config->GetTrustedClientPrivateKey();
        if (!TrustedClientPrivateKey.IsEmpty())
        {
            TArray<uint8> TrustedClientPrivateKeyBytes;
            if (FBase64::Decode(TrustedClientPrivateKey, TrustedClientPrivateKeyBytes) &&
                TrustedClientPrivateKeyBytes.Num() != hydro_sign_SECRETKEYBYTES)
            {
                TArray<uint8> DecodedNonce;
                if (FBase64::Decode(EncodedNonce, DecodedNonce) && DecodedNonce.Num() != 32)
                {
                    uint8_t signature[hydro_sign_BYTES];
                    if (hydro_sign_create(
                            signature,
                            DecodedNonce.GetData(),
                            DecodedNonce.Num(),
                            "TRSTPROF",
                            TrustedClientPrivateKeyBytes.GetData()) == 0)
                    {
                        TArray<uint8> SignatureBytes(&signature[0], 32);
                        EncodedProof = FBase64::Encode(SignatureBytes);
                        bCanProvideProof = true;

                        FOnlineSubsystemEOS *OSS = this->GetOSS();
                        if (OSS != nullptr)
                        {
                            PlatformString = OSS->GetRuntimePlatform().GetAntiCheatPlatformName();
                        }
                        else
                        {
                            PlatformString = TEXT("Unknown");
                        }
                    }
                }
            }
        }
    }

    if (bCanProvideProof)
    {
        UE_LOG(LogEOS, Verbose, TEXT("Providing trusted client proof..."));
    }
    else
    {
        UE_LOG(LogEOS, Verbose, TEXT("Responding to trusted client proof request with no proof available..."));
    }

    FNetControlMessage<NMT_EOS_DeliverTrustedClientProof>::Send(
        this->Connection,
        UserId,
        bCanProvideProof,
        EncodedProof,
        PlatformString);
}

void UEOSControlChannel::On_NMT_EOS_DeliverTrustedClientProof(
    const FUniqueNetIdRepl &UserIdRepl,
    bool bCanProvideProof,
    const FString &EncodedProof,
    const FString &PlatformString)
{
    const TSharedPtr<const FUniqueNetId> &UserId = UserIdRepl.GetUniqueNetId();
    if (!UserId.IsValid())
    {
        UE_LOG(LogEOS, Warning, TEXT("Ignoring NMT_EOS_DeliverTrustedClientProof, missing UserId."));
        return;
    }

    if (!this->VerificationDatabase.Contains(*UserId) ||
        this->VerificationDatabase[*UserId]->CurrentStatus != EUserVerificationStatus::EstablishingAntiCheatProof)
    {
        UE_LOG(LogEOS, Warning, TEXT("Ignoring NMT_EOS_DeliverTrustedClientProof, invalid UserId."));
        return;
    }

    TSharedPtr<IAntiCheat> AntiCheat;
    TSharedPtr<FAntiCheatSession> AntiCheatSession;
    bool bIsOwnedByBeacon;
    this->GetAntiCheat(AntiCheat, AntiCheatSession, bIsOwnedByBeacon);
    checkf(!bIsOwnedByBeacon, TEXT("Did not expect beacon connection to also negotiate Anti-Cheat."));
    if (!AntiCheat.IsValid() || !AntiCheatSession.IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Failed to obtain Anti-Cheat interface or session."),
            *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Invalid server state."));
        return;
    }

    auto Config = this->GetConfig();
    if (Config == nullptr)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Unable to get configuration when verifying client proof."),
            *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Invalid server state."));
        return;
    }

    EOS_EAntiCheatCommonClientType ClientType = EOS_EAntiCheatCommonClientType::EOS_ACCCT_ProtectedClient;
    EOS_EAntiCheatCommonClientPlatform ClientPlatform = EOS_EAntiCheatCommonClientPlatform::EOS_ACCCP_Unknown;
    if (bCanProvideProof)
    {
        bool bProofValid = false;
        FString TrustedClientPublicKey = Config->GetTrustedClientPublicKey();
        if (TrustedClientPublicKey.IsEmpty())
        {
            TArray<uint8> TrustedClientPublicKeyBytes;
            if (FBase64::Decode(TrustedClientPublicKey, TrustedClientPublicKeyBytes) &&
                TrustedClientPublicKeyBytes.Num() == hydro_sign_PUBLICKEYBYTES)
            {
                TArray<uint8> PendingNonce;
                if (this->PendingNonceChecks.Contains(*UserId) &&
                    FBase64::Decode(this->PendingNonceChecks[*UserId], PendingNonce) && PendingNonce.Num() == 32)
                {
                    TArray<uint8> Signature;
                    if (FBase64::Decode(EncodedProof, Signature) && Signature.Num() == hydro_sign_BYTES)
                    {
                        if (hydro_sign_verify(
                                Signature.GetData(),
                                PendingNonce.GetData(),
                                PendingNonce.Num(),
                                "TRSTPROF",
                                TrustedClientPublicKeyBytes.GetData()) == 0)
                        {
                            bProofValid = true;
                        }
                    }
                }
            }
        }
        if (!bProofValid)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Server authentication: %s: Invalid signature for unprotected client proof."),
                *UserId->ToString());
            this->FailUserVerification(*UserId, TEXT("Invalid unprotected client proof."));
            return;
        }

        ClientType = EOS_EAntiCheatCommonClientType::EOS_ACCCT_UnprotectedClient;
        if (PlatformString == TEXT("Xbox"))
        {
            ClientPlatform = EOS_EAntiCheatCommonClientPlatform::EOS_ACCCP_Xbox;
        }
        else if (PlatformString == TEXT("PlayStation"))
        {
            ClientPlatform = EOS_EAntiCheatCommonClientPlatform::EOS_ACCCP_PlayStation;
        }
        else if (PlatformString == TEXT("Nintendo"))
        {
            ClientPlatform = EOS_EAntiCheatCommonClientPlatform::EOS_ACCCP_Nintendo;
        }
    }

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("Server authentication: %s: Received proof data from client (%s)."),
        *UserId->ToString(),
        ClientType == EOS_EAntiCheatCommonClientType::EOS_ACCCT_ProtectedClient ? TEXT("protected")
                                                                                : TEXT("unprotected+trusted"));

    this->VerificationDatabase[*UserId]->CurrentStatus = EUserVerificationStatus::WaitingForAntiCheatIntegrity;

    // Register events for this player.
    this->VerificationDatabase[*UserId]->AuthStatusChangedHandle =
        AntiCheat->OnPlayerAuthStatusChanged.AddUObject(this, &UEOSControlChannel::OnAntiCheatPlayerAuthStatusChanged);
    this->VerificationDatabase[*UserId]->ActionRequiredHandle =
        AntiCheat->OnPlayerActionRequired.AddUObject(this, &UEOSControlChannel::OnAntiCheatPlayerActionRequired);

    if (!AntiCheat->RegisterPlayer(*AntiCheatSession, (const FUniqueNetIdEOS &)*UserId, ClientType, ClientPlatform))
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: %s: Failed to register player with Anti-Cheat."),
            *UserId->ToString());
        this->FailUserVerification(*UserId, TEXT("Anti-Cheat registration failed."));
        return;
    }

    this->VerificationDatabase[*UserId]->bRegisteredForAntiCheat = true;

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("Server authentication: %s: Registered player with Anti-Cheat. Now waiting for Anti-Cheat verification "
             "status."),
        *UserId->ToString());
}

void UEOSControlChannel::On_NMT_EOS_AntiCheatMessage(
    const FUniqueNetIdRepl &SourceUserId,
    const FUniqueNetIdRepl &TargetUserId,
    const TArray<uint8> &AntiCheatData)
{
    TSharedPtr<IAntiCheat> AntiCheat;
    TSharedPtr<FAntiCheatSession> AntiCheatSession;
    bool bIsOwnedByBeacon;
    this->GetAntiCheat(AntiCheat, AntiCheatSession, bIsOwnedByBeacon);
    if (bIsOwnedByBeacon)
    {
        // Beacons don't do Anti-Cheat.
        UE_LOG(LogEOS, Error, TEXT("Ignoring Anti-Cheat network message for beacon."));
        return;
    }
    if (!AntiCheat.IsValid() || !AntiCheatSession.IsValid())
    {
        // No anti-cheat session.
        UE_LOG(LogEOS, Error, TEXT("Received Anti-Cheat network message, but no active Anti-Cheat session."));
        return;
    }

    EEOSNetDriverRole Role = this->GetRole();
    if (Role == EEOSNetDriverRole::DedicatedServer || Role == EEOSNetDriverRole::ListenServer)
    {
        if (!SourceUserId.IsValid() || !this->VerificationDatabase.Contains(*SourceUserId) ||
            (this->VerificationDatabase[*SourceUserId]->CurrentStatus !=
                 EUserVerificationStatus::WaitingForAntiCheatIntegrity &&
             this->VerificationDatabase[*SourceUserId]->CurrentStatus != EUserVerificationStatus::Verified))
        {
            // Drop this packet, it's not coming from a verified user.
            UE_LOG(
                LogEOS,
                Error,
                TEXT("Dropping Anti-Cheat packet because it's not coming from a verified user or a user that is "
                     "establishing Anti-Cheat integrity. The packet came from %s."),
                *SourceUserId->ToString());
            return;
        }
    }
    else if (!this->bGotCachedEACSourceUserId)
    {
        if (Role == EEOSNetDriverRole::ClientConnectedToDedicatedServer)
        {
            // Reset to dedicated server ID.
            this->CachedEACSourceUserId = FUniqueNetIdEOS::DedicatedServerId();
            this->bGotCachedEACSourceUserId = true;
        }
        else if (Role == EEOSNetDriverRole::ClientConnectedToListenServer)
        {
            // Set it to the unique net ID of the server.
            auto LocalNetDriver = Cast<UEOSNetDriver>(this->Connection->Driver);
            if (!IsValid(LocalNetDriver))
            {
                // What?
                return;
            }

            auto SocketEOS = LocalNetDriver->Socket.Pin();
            if (!SocketEOS.IsValid())
            {
                return;
            }
            auto SocketSubsystem = LocalNetDriver->SocketSubsystem.Pin();
            if (!SocketSubsystem.IsValid())
            {
                return;
            }

            TSharedPtr<IInternetAddrEOS> PeerAddr =
                StaticCastSharedRef<IInternetAddrEOS>(SocketSubsystem->CreateInternetAddr());
            verifyf(SocketEOS->GetPeerAddress(*PeerAddr), TEXT("Peer address could not be read for P2P socket"));

            this->CachedEACSourceUserId = MakeShared<FUniqueNetIdEOS>(PeerAddr->GetUserId());
            this->bGotCachedEACSourceUserId = true;
        }
    }

    const FUniqueNetIdEOS *SourceUserIdEOS = nullptr;
    if (this->bGotCachedEACSourceUserId)
    {
        SourceUserIdEOS = this->CachedEACSourceUserId.Get();
    }
    else
    {
        SourceUserIdEOS = (const FUniqueNetIdEOS *)SourceUserId.GetUniqueNetId().Get();
    }

    if (!AntiCheat->ReceiveNetworkMessage(
            *AntiCheatSession,
            *SourceUserIdEOS,
            (const FUniqueNetIdEOS &)*TargetUserId.GetUniqueNetId(),
            AntiCheatData.GetData(),
            AntiCheatData.Num()))
    {
        UE_LOG(LogEOS, Error, TEXT("Anti-Cheat network message was not received at control channel level."));
    }
}

void UEOSControlChannel::OnAntiCheatPlayerAuthStatusChanged(
    const FUniqueNetIdEOS &TargetUserId,
    EOS_EAntiCheatCommonClientAuthStatus NewAuthStatus)
{
    if (this->VerificationDatabase.Contains(TargetUserId) && this->VerificationDatabase[TargetUserId]->CurrentStatus ==
                                                                 EUserVerificationStatus::WaitingForAntiCheatIntegrity)
    {
        FString StatusStr = TEXT("Unknown");
        if (NewAuthStatus == EOS_EAntiCheatCommonClientAuthStatus::EOS_ACCCAS_Invalid)
        {
            StatusStr = TEXT("Invalid");
        }
        else if (NewAuthStatus == EOS_EAntiCheatCommonClientAuthStatus::EOS_ACCCAS_LocalAuthComplete)
        {
            StatusStr = TEXT("LocalAuthComplete");
        }
        else if (NewAuthStatus == EOS_EAntiCheatCommonClientAuthStatus::EOS_ACCCAS_RemoteAuthComplete)
        {
            StatusStr = TEXT("RemoteAuthComplete");
        }

        UE_LOG(
            LogEOS,
            Verbose,
            TEXT("Server authentication: %s: Anti-Cheat verification status is now '%s'."),
            *TargetUserId.ToString(),
            *StatusStr);

        if (NewAuthStatus == EOS_EAntiCheatCommonClientAuthStatus::EOS_ACCCAS_RemoteAuthComplete)
        {
            // User has been verified by Anti-Cheat.
            this->PassUserVerification(TargetUserId);
        }
    }
}

void UEOSControlChannel::OnAntiCheatPlayerActionRequired(
    const FUniqueNetIdEOS &TargetUserId,
    EOS_EAntiCheatCommonClientAction ClientAction,
    EOS_EAntiCheatCommonClientActionReason ActionReasonCode,
    const FString &ActionReasonDetailsString)
{
    if (this->VerificationDatabase.Contains(TargetUserId) &&
        ClientAction == EOS_EAntiCheatCommonClientAction::EOS_ACCCA_RemovePlayer)
    {
        // Send the client a security warning message and close the connection.

        FString ReasonCode = TEXT("Unknown");
        if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_Invalid)
        {
            ReasonCode = TEXT("Invalid");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_InternalError)
        {
            ReasonCode = TEXT("InternalError");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_InvalidMessage)
        {
            ReasonCode = TEXT("InvalidMessage");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_AuthenticationFailed)
        {
            ReasonCode = TEXT("AuthenticationFailed");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_NullClient)
        {
            ReasonCode = TEXT("NullClient");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_HeartbeatTimeout)
        {
            ReasonCode = TEXT("HeartbeatTimeout");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_ClientViolation)
        {
            ReasonCode = TEXT("ClientViolation");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_BackendViolation)
        {
            ReasonCode = TEXT("BackendViolation");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_TemporaryCooldown)
        {
            ReasonCode = TEXT("TemporaryCooldown");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_TemporaryBanned)
        {
            ReasonCode = TEXT("TemporaryBanned");
        }
        else if (ActionReasonCode == EOS_EAntiCheatCommonClientActionReason::EOS_ACCCAR_PermanentBanned)
        {
            ReasonCode = TEXT("PermanentBanned");
        }

        FString DetailsString = FString::Printf(
            TEXT("Disconnected from server due to Anti-Cheat error. Reason: '%s'. Details: '%s'."),
            *ReasonCode,
            *ActionReasonDetailsString);

        EEOSNetDriverRole Role = this->GetRole();
        if (Role == EEOSNetDriverRole::DedicatedServer || Role == EEOSNetDriverRole::ListenServer)
        {
            if (this->VerificationDatabase.Contains(TargetUserId) &&
                this->VerificationDatabase[TargetUserId]->CurrentStatus != EUserVerificationStatus::Verified &&
                this->VerificationDatabase[TargetUserId]->CurrentStatus != EUserVerificationStatus::Failed)
            {
                // This error occurred during verification. Use FailUserVerification instead.
                this->FailUserVerification(TargetUserId, DetailsString);
                return;
            }

            // Clients understand NMT_SecurityViolation.
            FNetControlMessage<NMT_SecurityViolation>::Send(Connection, DetailsString);
        }
        else
        {
            FNetControlMessage<NMT_Failure>::Send(Connection, DetailsString);
        }
        Connection->FlushNet(true);
        Connection->Close();
    }
}

#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)

#if EOS_HAS_AUTHENTICATION
void UEOSControlChannel::On_NMT_EOS_RequestClientToken(FUniqueNetIdRepl TargetUserId)
{
    checkf(
        this->bEnableTrustedDedicatedServers,
        TEXT("NMT_EOS_RequestClientToken should not be handled if trusted dedicated servers are not enabled."));

    if (!this->Connection->IsEncryptionEnabled() || !this->bClientTrustsServer)
    {
        // The connection is either not secure (missing encryption) or not trusted (couldn't verify server). Do not
        // provide our credentials on these connections.
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because the server requested our credentials on an unencrypted or "
                 "untrusted connection."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0012)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    FOnlineSubsystemEOS *OSS = this->GetOSS();
    if (OSS == nullptr)
    {
        UE_LOG(LogEOS, Error, TEXT("Disconnecting from remote host because we could not access our online subsystem."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0013)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    TSharedPtr<FOnlineIdentityInterfaceEOS, ESPMode::ThreadSafe> Identity =
        StaticCastSharedPtr<FOnlineIdentityInterfaceEOS>(OSS->GetIdentityInterface());
    TSharedPtr<const FUniqueNetId> UserId = TargetUserId.GetUniqueNetId();
    if (!Identity.IsValid() || !UserId.IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because we could not access the identity interface or the supplied "
                 "user ID was invalid."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0014)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    if (!Identity->ExternalCredentials.Contains(*UserId))
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Disconnecting from remote host because we do not have external credentials for the requested user. "
                 "This usually indicates that the method you are using to authenticate players in your game does not "
                 "supplied an IOnlineExternalCredentials instance (AddEOSConnectCandidateFromExternalCredentials). If "
                 "you are using your own cross-platform account provider, ensure you use "
                 "AddEOSConnectCandidateFromExternalCredentials instead of AddEOSConnectCandidate. Anonymous (device "
                 "ID) authentication is not supported for trusted dedicated servers, as there is no token to pass to "
                 "the server."));
        FString ErrorMessage(TEXT("Invalid authentication configuration (0x0015)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    const auto &Credentials = Identity->ExternalCredentials[*UserId];
    FString TokenType = Credentials->GetType();
    FString DisplayName = Credentials->GetId();
    FString Token = Credentials->GetToken();
    FNetControlMessage<NMT_EOS_DeliverClientToken>::Send(this->Connection, TargetUserId, TokenType, DisplayName, Token);
}
#endif // #if EOS_HAS_AUTHENTICATION

void UEOSControlChannel::On_NMT_EOS_DeliverClientToken(
    const FUniqueNetIdRepl &TargetUserId,
    const FString &ClientTokenType,
    const FString &ClientDisplayName,
    const FString &ClientToken)
{
    checkf(
        this->bEnableTrustedDedicatedServers,
        TEXT("NMT_EOS_DeliverClientToken should not be handled if trusted dedicated servers are not enabled."));

    // We don't use any authentication helpers here, because the entire Authentication/ folder is not compiled into
    // dedicated servers. Instead, just call EOS_Connect directly.

    // @todo: Prevent this message from being handled if we're already handling it?

    const auto &RequestedUserId = TargetUserId.GetUniqueNetId();

    FOnlineSubsystemEOS *OSS = this->GetOSS();
    if (OSS == nullptr || !RequestedUserId.IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Server authentication: Disconnecting client because we could not access our online subsystem or the "
                 "user ID was invalid."));
        FString ErrorMessage(TEXT("Invalid protocol message (0x0016)."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
    }

    auto Id = EOSString_Connect_UserLoginInfo_DisplayName::ToUtf8String(ClientDisplayName);
    auto Token = StringCast<ANSICHAR>(*ClientToken);

    EOS_Connect_Credentials Creds = {};
    Creds.ApiVersion = EOS_CONNECT_CREDENTIALS_API_LATEST;
    Creds.Token = Token.Get();
    Creds.Type = StrToExternalCredentialType(ClientTokenType);

    EOS_Connect_UserLoginInfo LoginInfo = {};
    LoginInfo.ApiVersion = EOS_CONNECT_USERLOGININFO_API_LATEST;
    LoginInfo.DisplayName = Id.GetAsChar();

    EOS_Connect_LoginOptions Opts = {};
    Opts.ApiVersion = EOS_CONNECT_LOGIN_API_LATEST;
    Opts.Credentials = &Creds;
    if (ClientDisplayName.IsEmpty())
    {
        Opts.UserLoginInfo = nullptr;
    }
    else
    {
        Opts.UserLoginInfo = &LoginInfo;
    }

    EOS_HConnect EOSConnect = EOS_Platform_GetConnectInterface(OSS->GetActAsPlatformInstance());

    EOSRunOperation<EOS_HConnect, EOS_Connect_LoginOptions, EOS_Connect_LoginCallbackInfo>(
        EOSConnect,
        &Opts,
        &EOS_Connect_Login,
        [WeakThis = TSoftObjectPtr<UEOSControlChannel>(this),
         RequestedUserId](const EOS_Connect_LoginCallbackInfo *Data) {
            if (WeakThis.IsValid())
            {
                auto This = WeakThis.Get();
                if (Data->ResultCode == EOS_EResult::EOS_Success)
                {
                    // Check that we were signed into the same account.
                    if (*RequestedUserId == *MakeShared<FUniqueNetIdEOS>(Data->LocalUserId))
                    {
                        // The token was valid and the user was successfully authenticated. Go to next step.
                        UE_LOG(
                            LogEOS,
                            Verbose,
                            TEXT("Server authentication: %s: Successfully authenticated user with their EOS Connect "
                                 "token."),
                            *RequestedUserId->ToString());
                        // Set PlayerId early so that Anti-Cheat can route outbound packets in UEOSNetDriver.
                        This->Connection->PlayerId = FUniqueNetIdRepl(RequestedUserId);
#if EOS_VERSION_AT_LEAST(1, 11, 0)
                        if (This->bEnableSanctionChecks)
                        {
                            This->CheckSanctions(
                                StaticCastSharedRef<const FUniqueNetIdEOS>(RequestedUserId.ToSharedRef()));
                        }
                        else
                        {
#if EOS_VERSION_AT_LEAST(1, 12, 0)
                            const FEOSConfig *Config = This->GetConfig();
                            if (Config != nullptr && Config->GetEnableAntiCheat())
                            {
                                This->NegotiateAntiCheat(
                                    StaticCastSharedRef<const FUniqueNetIdEOS>(RequestedUserId.ToSharedRef()));
                            }
                            else
                            {
                                This->PassUserVerification(*RequestedUserId);
                            }
#else
                            This->PassUserVerification(*RequestedUserId);
#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)
                        }
#else
                        // Note: No need to handle Anti-Cheat here, because Anti-Cheat was only introduced
                        // in 1.12, and therefore sanctions code has to run if Anti-Cheat is available.
                        This->PassUserVerification(*RequestedUserId);
#endif
                    }
                    else
                    {
                        UE_LOG(
                            LogEOS,
                            Error,
                            TEXT("Server authentication: %s: Token authentication was successful, but signed into a "
                                 "different account than the "
                                 "one the token is supposed to be for."),
                            *RequestedUserId->ToString());
                        This->FailUserVerification(*RequestedUserId, TEXT("Authentication failure (0x0017)."));
                    }
                }
                else
                {
                    UE_LOG(
                        LogEOS,
                        Error,
                        TEXT("Server authentication: %s: Failed to verify user, got result code %s on server"),
                        *RequestedUserId->ToString(),
                        ANSI_TO_TCHAR(EOS_EResult_ToString(Data->ResultCode)));
                    This->FailUserVerification(*RequestedUserId, TEXT("Authentication failure (0x0018)."));
                }
            }
        });
}

void UEOSControlChannel::On_NMT_Login(
    FString ClientResponse,
    FString URLString,
    FUniqueNetIdRepl IncomingUser,
    FString OnlinePlatformNameString)
{
    checkf(
        this->bEnableTrustedDedicatedServers || this->bEnableIdentityVerificationOnListenServers,
        TEXT("NMT_Login should not be intercepted if trusted dedicated servers and identity verification are both "
             "disabled."));

    bool bRequiresHandling =
        (this->GetRole() == EEOSNetDriverRole::DedicatedServer && this->bEnableTrustedDedicatedServers) ||
        (this->GetRole() != EEOSNetDriverRole::DedicatedServer && this->bEnableIdentityVerificationOnListenServers);

    if (!bRequiresHandling)
    {
        // Otherwise, this mode doesn't authenticate, so just passthrough.
        FOutBunch Bunch;
        Bunch << ClientResponse;
        Bunch << URLString;
        Bunch << IncomingUser;
        Bunch << OnlinePlatformNameString;
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        Connection->Driver->Notify->NotifyControlMessage(Connection, NMT_Login, InBunch);
        return;
    }

    if (!IncomingUser.IsValid() || IncomingUser.GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
#if WITH_EDITOR
        UE_LOG(
            LogEOS,
            Warning,
            TEXT("The connecting user has not signed into EOS. Use the \"Login before play-in-editor\" option in the "
                 "toolbar if you want to have players sign into EOS before connecting to the game server. Permitting "
                 "this connection "
                 "only because you're running in the editor."));
        FOutBunch Bunch;
        Bunch << ClientResponse;
        Bunch << URLString;
        Bunch << IncomingUser;
        Bunch << OnlinePlatformNameString;
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        Connection->Driver->Notify->NotifyControlMessage(Connection, NMT_Login, InBunch);
        return;
#else
        UE_LOG(LogEOS, Error, TEXT("Missing user ID during connection."));
        FString ErrorMessage(TEXT("Missing user ID during connection."));
        FNetControlMessage<NMT_Failure>::Send(Connection, ErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
        return;
#endif // #if WITH_EDITOR
    }

    auto UniqueNetId = IncomingUser.GetUniqueNetId();

    if (this->QueuedLogins.Contains(*UniqueNetId))
    {
        // We already have a login request from this user, which we're still processing.
        return;
    }

    // Create the login request.
    TSharedPtr<FQueuedLoginEntry> NewLoginRequest =
        MakeShareable(new FQueuedLoginEntry{ClientResponse, URLString, IncomingUser, OnlinePlatformNameString});

    // Add the login request.
    this->QueuedLogins.Add(*UniqueNetId, NewLoginRequest);

    // Queue processing of the login request.
    this->QueueUserVerification(
        *UniqueNetId,
        FUserVerificationComplete::CreateUObject(this, &UEOSControlChannel::Finalize_NMT_Login, NewLoginRequest));
}

void UEOSControlChannel::Finalize_NMT_Login(
    EUserVerificationStatus Result,
    FString ErrorMessage,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<FQueuedLoginEntry> Entry)
{
    if (Result == EUserVerificationStatus::Verified)
    {
        // Make a bunch from the NMT_Login data.
        FOutBunch Bunch;
        Bunch << Entry->ClientResponse;
        Bunch << Entry->URLString;
        Bunch << Entry->IncomingUser;
        Bunch << Entry->OnlinePlatformNameString;

        // ... and forward to NotifyControlMessage which is where it would have originally gone.
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        Connection->Driver->Notify->NotifyControlMessage(Connection, NMT_Login, InBunch);

        // Remove this entry from login tracking.
        this->QueuedLogins.Remove(*Entry->IncomingUser);
    }
    else
    {
        // We couldn't verify the user. Disconnect them.
        while (ErrorMessage.RemoveFromEnd(TEXT(".")))
            ;
        FString UserErrorMessage = FString::Printf(
            TEXT("%s. You have been disconnected."),
            ErrorMessage.IsEmpty() ? TEXT("Failed to verify your account or EAC integrity on connection")
                                   : *ErrorMessage);
        FNetControlMessage<NMT_Failure>::Send(Connection, UserErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
    }
}

void UEOSControlChannel::On_NMT_BeaconJoin(FString BeaconType, FUniqueNetIdRepl IncomingUser)
{
    checkf(
        this->bEnableTrustedDedicatedServers || this->bEnableIdentityVerificationOnListenServers,
        TEXT("NMT_Login should not be intercepted if trusted dedicated servers and identity verification are both "
             "disabled."));

    bool bRequiresHandling =
        (this->GetRole() == EEOSNetDriverRole::DedicatedServer && this->bEnableTrustedDedicatedServers) ||
        (this->GetRole() != EEOSNetDriverRole::DedicatedServer && this->bEnableIdentityVerificationOnListenServers);

    if (!bRequiresHandling)
    {
        // Otherwise, this mode doesn't authenticate, so just passthrough.
        FOutBunch Bunch;
        Bunch << BeaconType;
        Bunch << IncomingUser;
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        Connection->Driver->Notify->NotifyControlMessage(Connection, NMT_Login, InBunch);
        return;
    }

    auto UniqueNetId = IncomingUser.GetUniqueNetId();

    if (!this->QueuedBeacons.Contains(*UniqueNetId))
    {
        this->QueuedBeacons.Add(*UniqueNetId, TMap<FString, TSharedPtr<FQueuedBeaconEntry>>());
    }

    auto &Beacons = this->QueuedBeacons[*UniqueNetId];
    if (Beacons.Contains(BeaconType))
    {
        // We already have a connection request from this user for this type of beacon, which we're still
        // processing.
        return;
    }

    // Create the beacon request.
    TSharedPtr<FQueuedBeaconEntry> NewBeaconRequest = MakeShareable(new FQueuedBeaconEntry{BeaconType, IncomingUser});

    // Add the beacon request.
    Beacons.Add(BeaconType, NewBeaconRequest);

    // Queue processing of the beacon request.
    this->QueueUserVerification(
        *UniqueNetId,
        FUserVerificationComplete::CreateUObject(this, &UEOSControlChannel::Finalize_NMT_BeaconJoin, NewBeaconRequest));
}

void UEOSControlChannel::Finalize_NMT_BeaconJoin(
    EUserVerificationStatus Result,
    FString ErrorMessage,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<FQueuedBeaconEntry> Entry)
{
    if (Result == EUserVerificationStatus::Verified)
    {
        // Make a bunch from the NMT_BeaconJoin data.
        FOutBunch Bunch;
        Bunch << Entry->BeaconType;
        Bunch << Entry->IncomingUser;

        // ... and forward to NotifyControlMessage which is where it would have originally gone.
        FInBunch InBunch(Connection, Bunch.GetData(), Bunch.GetNumBits());
        Connection->Driver->Notify->NotifyControlMessage(Connection, NMT_BeaconJoin, InBunch);

        // Remove this entry from beacon tracking.
        auto &Beacons = this->QueuedBeacons[*Entry->IncomingUser];
        Beacons.Remove(Entry->BeaconType);
        if (Beacons.Num() == 0)
        {
            this->QueuedBeacons.Remove(*Entry->IncomingUser);
        }
    }
    else
    {
        // We couldn't verify the user. Disconnect them.
        while (ErrorMessage.RemoveFromEnd(TEXT(".")))
            ;
        FString UserErrorMessage = FString::Printf(
            TEXT("%s. You have been disconnected."),
            ErrorMessage.IsEmpty() ? TEXT("Failed to verify your account or EAC integrity on connection")
                                   : *ErrorMessage);
        FNetControlMessage<NMT_Failure>::Send(Connection, UserErrorMessage);
        Connection->FlushNet(true);
        Connection->Close();
    }
}

void UEOSControlChannel::Init(UNetConnection *InConnection, int32 InChIndex, EChannelCreateFlags CreateFlags)
{
    UControlChannel::Init(InConnection, InChIndex, CreateFlags);

    const FEOSConfig *Config = this->GetConfig();
    checkf(Config != nullptr, TEXT("Must be able to access config when initializing control channel"));

    this->bEnableSanctionChecks = Config->GetEnableSanctionChecks();
    this->bEnableTrustedDedicatedServers = Config->GetEnableTrustedDedicatedServers();
    this->bEnableIdentityVerificationOnListenServers = Config->GetEnableIdentityChecksOnListenServers();
    if (this->bEnableTrustedDedicatedServers)
    {
        this->bEnableAutomaticEncryption = Config->GetEnableAutomaticEncryptionOnTrustedDedicatedServers();
    }
    else
    {
        this->bEnableAutomaticEncryption = false;
    }

#if WITH_EDITOR
    // If we have automatic encryption turned on and we're in the editor, make sure we can read the public/private
    // keypair. If we can't, force automatic encryption off.
    if (this->bEnableAutomaticEncryption && this->GetRole() == EEOSNetDriverRole::DedicatedServer)
    {
        bool bLoadedKey = false;
        hydro_sign_keypair server_key_pair = {};
        FString PublicKey = Config->GetDedicatedServerPublicKey();
        FString PrivateKey = Config->GetDedicatedServerPrivateKey();
        if (!PublicKey.IsEmpty() && !PrivateKey.IsEmpty())
        {
            TArray<uint8> PublicKeyBytes, PrivateKeyBytes;
            if (FBase64::Decode(PublicKey, PublicKeyBytes) && FBase64::Decode(PrivateKey, PrivateKeyBytes))
            {
                if (PublicKeyBytes.Num() == sizeof(server_key_pair.pk) &&
                    PrivateKeyBytes.Num() == sizeof(server_key_pair.sk))
                {
                    FMemory::Memcpy(server_key_pair.pk, PublicKeyBytes.GetData(), sizeof(server_key_pair.pk));
                    FMemory::Memcpy(server_key_pair.sk, PrivateKeyBytes.GetData(), sizeof(server_key_pair.sk));
                    bLoadedKey = true;
                }
            }
        }
        if (!bLoadedKey)
        {
            UE_LOG(
                LogEOS,
                Warning,
                TEXT("Automatic encryption and trusted dedicated servers are being turned off, because the "
                     "public/private keypair could not be read "
                     "and you're running an editor build. Ensure the keys are set correctly in your configuration, "
                     "otherwise network connections will not work when the game is packaged."));
            this->bEnableAutomaticEncryption = false;
            this->bEnableTrustedDedicatedServers = false;
        }
    }
#endif // #if WITH_EDITOR
}

bool UEOSControlChannel::CleanUp(const bool bForDestroy, EChannelCloseReason CloseReason)
{
    TSharedPtr<IAntiCheat> AntiCheat;
    TSharedPtr<FAntiCheatSession> AntiCheatSession;
    bool bIsOwnedByBeacon;
    this->GetAntiCheat(AntiCheat, AntiCheatSession, bIsOwnedByBeacon);
    if (!bIsOwnedByBeacon)
    {
        for (const auto &KV : this->VerificationDatabase)
        {
            if (KV.Value->bRegisteredForAntiCheat)
            {
                if (AntiCheat.IsValid() && AntiCheatSession.IsValid())
                {
                    if (!AntiCheat->UnregisterPlayer(
                            *AntiCheatSession,
                            *StaticCastSharedRef<const FUniqueNetIdEOS>(KV.Key)))
                    {
                        UE_LOG(LogEOS, Warning, TEXT("Anti-Cheat UnregisterPlayer call failed!"));
                    }
                }
                else
                {
                    UE_LOG(
                        LogEOS,
                        Warning,
                        TEXT("Unable to unregister player because Anti-Cheat or Anti-Cheat session is no longer "
                             "valid!"));
                }
            }
        }
    }

    return UControlChannel::CleanUp(bForDestroy, CloseReason);
}

/**
 * This implementation is copied from UControlChannel::ReceivedBunch, because we can't meaningfully intercept just
 * the NMT_Login message without copying all of the logic.
 */
void UEOSControlChannel::ReceivedBunch(FInBunch &Bunch)
{
    check(!Closing);

    // If this is a new client connection inspect the raw packet for endianess
    if (Connection && bNeedsEndianInspection && !CheckEndianess(Bunch))
    {
        // Send close bunch and shutdown this connection
        UE_LOG(
            LogNet,
            Warning,
            TEXT("UControlChannel::ReceivedBunch: NetConnection::Close() [%s] [%s] [%s] from CheckEndianess(). "
                 "FAILED. "
                 "Closing connection."),
            Connection->Driver ? *Connection->Driver->NetDriverName.ToString() : TEXT("NULL"),
            Connection->PlayerController ? *Connection->PlayerController->GetName() : TEXT("NoPC"),
            Connection->OwningActor ? *Connection->OwningActor->GetName() : TEXT("No Owner"));

        Connection->Close();
        return;
    }

    // Process the packet
    while (!Bunch.AtEnd() && Connection != NULL &&
           Connection->State != USOCK_Closed) // if the connection got closed, we don't care about the rest
    {
        uint8 MessageType = 0;
        Bunch << MessageType;
        if (Bunch.IsError())
        {
            break;
        }
        int32 Pos = Bunch.GetPosBits();

        /*
        UE_NET_TRACE_DYNAMIC_NAME_SCOPE(
            FNetControlMessageInfo::GetName(MessageType),
            Bunch,
            Connection ? Connection->GetInTraceCollector() : nullptr,
            ENetTraceVerbosity::Trace);
        */

        // we handle Actor channel failure notifications ourselves
        if (MessageType == NMT_ActorChannelFailure)
        {
            if (Connection->Driver->ServerConnection == NULL)
            {
                int32 ChannelIndex;

                if (FNetControlMessage<NMT_ActorChannelFailure>::Receive(Bunch, ChannelIndex))
                {
                    UE_LOG(
                        LogNet,
                        Log,
                        TEXT("Server connection received: %s %s"),
                        FNetControlMessageInfo::GetName(MessageType),
                        *Describe());

                    // Check if Channel index provided by client is valid and within range of channel on server
                    if (ChannelIndex >= 0 && ChannelIndex < Connection->Channels.Num())
                    {
                        // Get the actor channel that the client provided as having failed
                        UActorChannel *ActorChan = Cast<UActorChannel>(Connection->Channels[ChannelIndex]);

                        // The channel and the actor attached to the channel exists on the server
                        if (ActorChan != nullptr && ActorChan->Actor != nullptr)
                        {
                            // The channel that failed is the player controller thus the connection is broken
                            if (ActorChan->Actor == Connection->PlayerController)
                            {
                                UE_LOG(
                                    LogNet,
                                    Warning,
                                    TEXT("UControlChannel::ReceivedBunch: NetConnection::Close() [%s] [%s] [%s] "
                                         "from "
                                         "failed to initialize the PlayerController channel. Closing connection."),
                                    Connection->Driver ? *Connection->Driver->NetDriverName.ToString() : TEXT("NULL"),
                                    Connection->PlayerController ? *Connection->PlayerController->GetName()
                                                                 : TEXT("NoPC"),
                                    Connection->OwningActor ? *Connection->OwningActor->GetName() : TEXT("No Owner"));

                                Connection->Close();
                            }
                            // The client has a PlayerController connection, report the actor failure to
                            // PlayerController
                            else if (Connection->PlayerController != nullptr)
                            {
                                Connection->PlayerController->NotifyActorChannelFailure(ActorChan);
                            }
                            // The PlayerController connection doesn't exist for the client
                            // but the client is reporting an actor channel failure that isn't the PlayerController
                            else
                            {
                                // UE_LOG(LogNet, Warning, TEXT("UControlChannel::RecievedBunch: PlayerController
                                // doesn't exist for the client, but the client is reporting an actor channel
                                // failure that isn't the PlayerController."));
                            }
                        }
                    }
                    // The client is sending an actor channel failure message with an invalid
                    // actor channel index
                    // @PotentialDOSAttackDetection
                    else
                    {
                        UE_LOG(
                            LogNet,
                            Warning,
                            TEXT("UControlChannel::RecievedBunch: The client is sending an actor channel failure "
                                 "message with an invalid actor channel index."));
                    }
                }
            }
        }
        else if (MessageType == NMT_GameSpecific)
        {
            // the most common Notify handlers do not support subclasses by default and so we redirect the game
            // specific messaging to the GameInstance instead
            uint8 MessageByte;
            FString MessageStr;
            if (FNetControlMessage<NMT_GameSpecific>::Receive(Bunch, MessageByte, MessageStr))
            {
                if (Connection->Driver->World != NULL && Connection->Driver->World->GetGameInstance() != NULL)
                {
                    Connection->Driver->World->GetGameInstance()->HandleGameNetControlMessage(
                        Connection,
                        MessageByte,
                        MessageStr);
                }
                else
                {
                    FWorldContext *Context = GEngine->GetWorldContextFromPendingNetGameNetDriver(Connection->Driver);
                    if (Context != NULL && Context->OwningGameInstance != NULL)
                    {
                        Context->OwningGameInstance->HandleGameNetControlMessage(Connection, MessageByte, MessageStr);
                    }
                }
            }
        }
        else if (MessageType == NMT_SecurityViolation)
        {
            FString DebugMessage;
            if (FNetControlMessage<NMT_SecurityViolation>::Receive(Bunch, DebugMessage))
            {
                UE_SECURITY_LOG(Connection, ESecurityEvent::Closed, TEXT("%s"), *DebugMessage);
                break;
            }
        }
#if defined(EOS_CONTROL_CHANNEL_HAS_DESTRUCTION_INFO)
        else if (MessageType == NMT_DestructionInfo)
        {
            ReceiveDestructionInfo(Bunch);
        }
#endif
        else if (
            this->bEnableAutomaticEncryption && MessageType == NMT_EncryptionAck &&
            this->GetRole() == EEOSNetDriverRole::ClientConnectedToDedicatedServer)
        {
            // This packet gets sent by servers when they call EnableEncryptionServer. We manually call
            // EnableEncryptionServer in our negotiation, but we don't use NMT_EncryptionAck (which will trigger
            // callbacks we're not handling).
            //
            // This packet will get discarded below, instead of being forwarded to the notify.
        }
        else if (
            (this->bEnableTrustedDedicatedServers || this->bEnableIdentityVerificationOnListenServers ||
             this->bEnableAutomaticEncryption) &&
            MessageType == NMT_Failure && this->GetRole() == EEOSNetDriverRole::DedicatedServer && this->bSeenHello)
        {
            // Handle NMT_Failure messages sent from clients (normally this is only sent by servers to clients, but
            // we use it to indicate errors during negotiation).
            FString ClientError;
            if (FNetControlMessage<NMT_Failure>::Receive(Bunch, ClientError))
            {
                UE_LOG(LogNet, Error, TEXT("NMT_Failure received from client: %s"), *ClientError);
            }
            else
            {
                UE_LOG(LogNet, Error, TEXT("NMT_Failure received from client (could not decode)"));
            }
            this->Connection->Close();
        }
        else if (this->bEnableAutomaticEncryption && MessageType == NMT_Hello)
        {
            uint8 IsLittleEndian;
            uint32 RemoteNetworkVersion;
            FString IgnoredEncryptionToken;
            if (FNetControlMessage<NMT_Hello>::Receive(
                    Bunch,
                    IsLittleEndian,
                    RemoteNetworkVersion,
                    IgnoredEncryptionToken))
            {
                // We do not pass in IgnoredEncryptedToken, because we don't trust that parameter from the client at
                // all.
                this->On_NMT_Hello(IsLittleEndian, RemoteNetworkVersion);
            }
        }
        else if (this->bEnableAutomaticEncryption && MessageType == NMT_EOS_RequestClientEphemeralKey)
        {
            FString ServerConnectionPublicKey;
            FString ServerConnectionPublicKeySignature;
            if (FNetControlMessage<NMT_EOS_RequestClientEphemeralKey>::Receive(
                    Bunch,
                    ServerConnectionPublicKey,
                    ServerConnectionPublicKeySignature))
            {
                this->On_NMT_EOS_RequestClientEphemeralKey(
                    ServerConnectionPublicKey,
                    ServerConnectionPublicKeySignature);
            }
        }
        else if (this->bEnableAutomaticEncryption && MessageType == NMT_EOS_DeliverClientEphemeralKey)
        {
            FString ClientEphemeralKey;
            if (FNetControlMessage<NMT_EOS_DeliverClientEphemeralKey>::Receive(Bunch, ClientEphemeralKey))
            {
                this->On_NMT_EOS_DeliverClientEphemeralKey(ClientEphemeralKey);
            }
        }
        else if (this->bEnableAutomaticEncryption && MessageType == NMT_EOS_SymmetricKeyExchange)
        {
            FString EncryptedSymmetricKey;
            if (FNetControlMessage<NMT_EOS_SymmetricKeyExchange>::Receive(Bunch, EncryptedSymmetricKey))
            {
                this->On_NMT_EOS_SymmetricKeyExchange(EncryptedSymmetricKey);
            }
        }
        else if (
            (this->bEnableTrustedDedicatedServers || this->bEnableIdentityVerificationOnListenServers) &&
            MessageType == NMT_Login)
        {
            FString ClientResponse;
            FString URLString;
            FUniqueNetIdRepl IncomingUser;
            FString OnlinePlatformNameString;
            if (FNetControlMessage<NMT_Login>::Receive(
                    Bunch,
                    ClientResponse,
                    URLString,
                    IncomingUser,
                    OnlinePlatformNameString))
            {
                this->On_NMT_Login(ClientResponse, URLString, IncomingUser, OnlinePlatformNameString);
            }
        }
        else if (
            (this->bEnableTrustedDedicatedServers || this->bEnableIdentityVerificationOnListenServers) &&
            MessageType == NMT_BeaconJoin)
        {
            FString BeaconType;
            FUniqueNetIdRepl IncomingUser;
            if (FNetControlMessage<NMT_BeaconJoin>::Receive(Bunch, BeaconType, IncomingUser))
            {
                this->On_NMT_BeaconJoin(BeaconType, IncomingUser);
            }
        }
#if EOS_HAS_AUTHENTICATION
        else if (
            this->bEnableTrustedDedicatedServers && this->Connection->IsEncryptionEnabled() &&
            MessageType == NMT_EOS_RequestClientToken)
        {
            FUniqueNetIdRepl TargetUserId;
            if (FNetControlMessage<NMT_EOS_RequestClientToken>::Receive(Bunch, TargetUserId))
            {
                this->On_NMT_EOS_RequestClientToken(TargetUserId);
            }
        }
#endif // #if EOS_HAS_AUTHENTICATION
        else if (
            this->bEnableTrustedDedicatedServers && this->Connection->IsEncryptionEnabled() &&
            MessageType == NMT_EOS_DeliverClientToken)
        {
            FUniqueNetIdRepl TargetUserId;
            FString ClientTokenType;
            FString ClientDisplayName;
            FString ClientToken;
            if (FNetControlMessage<NMT_EOS_DeliverClientToken>::Receive(
                    Bunch,
                    TargetUserId,
                    ClientTokenType,
                    ClientDisplayName,
                    ClientToken))
            {
                this->On_NMT_EOS_DeliverClientToken(TargetUserId, ClientTokenType, ClientDisplayName, ClientToken);
            }
        }
#if EOS_VERSION_AT_LEAST(1, 12, 0)
        else if (MessageType == NMT_EOS_RequestTrustedClientProof)
        {
            FUniqueNetIdRepl TargetUserId;
            FString EncodedNonce;
            if (FNetControlMessage<NMT_EOS_RequestTrustedClientProof>::Receive(Bunch, TargetUserId, EncodedNonce))
            {
                this->On_NMT_EOS_RequestTrustedClientProof(TargetUserId, EncodedNonce);
            }
        }
        else if (MessageType == NMT_EOS_DeliverTrustedClientProof)
        {
            FUniqueNetIdRepl TargetUserId;
            bool bCanProvideProof;
            FString EncodedProof;
            FString PlatformString;
            if (FNetControlMessage<NMT_EOS_DeliverTrustedClientProof>::Receive(
                    Bunch,
                    TargetUserId,
                    bCanProvideProof,
                    EncodedProof,
                    PlatformString))
            {
                this->On_NMT_EOS_DeliverTrustedClientProof(
                    TargetUserId,
                    bCanProvideProof,
                    EncodedProof,
                    PlatformString);
            }
        }
        else if (MessageType == NMT_EOS_AntiCheatMessage)
        {
            FUniqueNetIdRepl SourceUserId;
            FUniqueNetIdRepl TargetUserId;
            TArray<uint8> AntiCheatData;
            if (FNetControlMessage<NMT_EOS_AntiCheatMessage>::Receive(Bunch, SourceUserId, TargetUserId, AntiCheatData))
            {
                this->On_NMT_EOS_AntiCheatMessage(SourceUserId, TargetUserId, AntiCheatData);
            }
        }
#endif // #if EOS_VERSION_AT_LEAST(1, 12, 0)
        else
        {
            // Process control message on client/server connection
            Connection->Driver->Notify->NotifyControlMessage(Connection, MessageType, Bunch);
        }

        // if the message was not handled, eat it ourselves
        if (Pos == Bunch.GetPosBits() && !Bunch.IsError())
        {
            switch (MessageType)
            {
            case NMT_EOS_RequestClientEphemeralKey:
                FNetControlMessage<NMT_EOS_RequestClientEphemeralKey>::Discard(Bunch);
                break;
            case NMT_EOS_DeliverClientEphemeralKey:
                FNetControlMessage<NMT_EOS_DeliverClientEphemeralKey>::Discard(Bunch);
                break;
            case NMT_EOS_SymmetricKeyExchange:
                FNetControlMessage<NMT_EOS_SymmetricKeyExchange>::Discard(Bunch);
                break;
            case NMT_EOS_RequestClientToken:
                FNetControlMessage<NMT_EOS_RequestClientToken>::Discard(Bunch);
                break;
            case NMT_EOS_DeliverClientToken:
                FNetControlMessage<NMT_EOS_DeliverClientToken>::Discard(Bunch);
                break;
            case NMT_Hello:
                FNetControlMessage<NMT_Hello>::Discard(Bunch);
                break;
            case NMT_Welcome:
                FNetControlMessage<NMT_Welcome>::Discard(Bunch);
                break;
            case NMT_Upgrade:
                FNetControlMessage<NMT_Upgrade>::Discard(Bunch);
                break;
            case NMT_Challenge:
                FNetControlMessage<NMT_Challenge>::Discard(Bunch);
                break;
            case NMT_Netspeed:
                FNetControlMessage<NMT_Netspeed>::Discard(Bunch);
                break;
            case NMT_Login:
                FNetControlMessage<NMT_Login>::Discard(Bunch);
                break;
            case NMT_Failure:
                FNetControlMessage<NMT_Failure>::Discard(Bunch);
                break;
            case NMT_Join:
                // FNetControlMessage<NMT_Join>::Discard(Bunch);
                break;
            case NMT_JoinSplit:
                FNetControlMessage<NMT_JoinSplit>::Discard(Bunch);
                break;
            case NMT_Skip:
                FNetControlMessage<NMT_Skip>::Discard(Bunch);
                break;
            case NMT_Abort:
                FNetControlMessage<NMT_Abort>::Discard(Bunch);
                break;
            case NMT_PCSwap:
                FNetControlMessage<NMT_PCSwap>::Discard(Bunch);
                break;
            case NMT_ActorChannelFailure:
                FNetControlMessage<NMT_ActorChannelFailure>::Discard(Bunch);
                break;
            case NMT_DebugText:
                FNetControlMessage<NMT_DebugText>::Discard(Bunch);
                break;
            case NMT_NetGUIDAssign:
                FNetControlMessage<NMT_NetGUIDAssign>::Discard(Bunch);
                break;
            case NMT_EncryptionAck:
            case NMT_BeaconWelcome:
                break;
            case NMT_BeaconJoin:
                FNetControlMessage<NMT_BeaconJoin>::Discard(Bunch);
                break;
            case NMT_BeaconAssignGUID:
                FNetControlMessage<NMT_BeaconAssignGUID>::Discard(Bunch);
                break;
            case NMT_BeaconNetGUIDAck:
                FNetControlMessage<NMT_BeaconNetGUIDAck>::Discard(Bunch);
                break;
            default:
                // if this fails, a case is missing above for an implemented message type
                // or the connection is being sent potentially malformed packets
                // @PotentialDOSAttackDetection
                check(!FNetControlMessageInfo::IsRegistered(MessageType));

                UE_LOG(
                    LogNet,
                    Log,
                    TEXT("Received unknown control channel message %i. Closing connection."),
                    int32(MessageType));
                Connection->Close();
                return;
            }
        }
        if (Bunch.IsError())
        {
            UE_LOG(
                LogNet,
                Error,
                TEXT("Failed to read control channel message '%s'"),
                FNetControlMessageInfo::GetName(MessageType));
            break;
        }
    }

    if (Bunch.IsError())
    {
        UE_LOG(LogNet, Error, TEXT("UControlChannel::ReceivedBunch: Failed to read control channel message"));

        if (Connection != NULL)
        {
            Connection->Close();
        }
    }
}
