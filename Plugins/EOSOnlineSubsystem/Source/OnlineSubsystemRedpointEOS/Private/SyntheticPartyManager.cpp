// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/SyntheticPartyManager.h"

#include "Containers/Ticker.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemRedpointEOS/Shared/DelegatedSubsystems.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSDefines.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlinePartyInterfaceEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"
#include "Templates/SharedPointer.h"

EOS_ENABLE_STRICT_WARNINGS

FSyntheticPartyManager::FSyntheticPartyManager(
    FName InInstanceName,
    TWeakPtr<FOnlinePartySystemEOS, ESPMode::ThreadSafe> InEOSPartySystem,
    TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> InEOSIdentitySystem,
    const TSharedRef<class FEOSConfig> &InConfig)
{
    this->EOSPartySystem = MoveTemp(InEOSPartySystem);
    this->EOSIdentitySystem = MoveTemp(InEOSIdentitySystem);
    this->Config = InConfig;

    struct W
    {
        TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> Identity;
        TWeakPtr<IOnlineSession, ESPMode::ThreadSafe> Session;
    };
    for (const auto &KV : FDelegatedSubsystems::GetDelegatedSubsystems<W>(
             InConfig,
             InInstanceName,
             [](FDelegatedSubsystems::ISubsystemContext &Ctx) -> TSharedPtr<W> {
                 auto Identity = Ctx.GetDelegatedInterface<IOnlineIdentity>([](IOnlineSubsystem *OSS) {
                     return OSS->GetIdentityInterface();
                 });
                 auto Session = Ctx.GetDelegatedInterface<IOnlineSession>([](IOnlineSubsystem *OSS) {
                     return OSS->GetSessionInterface();
                 });
                 if (Identity.IsValid() && Session.IsValid())
                 {
                     return MakeShared<W>(W{Identity, Session});
                 }
                 return nullptr;
             }))
    {
        this->WrappedSubsystems.Add({KV.Key, KV.Value->Identity, KV.Value->Session});
    }
}

void FSyntheticPartyManager::RegisterEvents()
{
    for (const auto &KV : this->WrappedSubsystems)
    {
        if (auto Session = KV.Session.Pin())
        {
            this->SessionUserInviteAcceptedDelegateHandles.Add(
                KV.Session,
                Session->AddOnSessionUserInviteAcceptedDelegate_Handle(
                    FOnSessionUserInviteAcceptedDelegate::CreateLambda(
                        [WeakThis = GetWeakThis(this)](
                            const bool bWasSuccessful,
                            const int32 ControllerId,
                            const TSharedPtr<const FUniqueNetId> &UserId,
                            const FOnlineSessionSearchResult &InviteResult) {
                            if (auto This = PinWeakThis(WeakThis))
                            {
                                UE_LOG(LogEOS, Verbose, TEXT("Received invite from synthetic party"));

                                if (bWasSuccessful && InviteResult.IsValid())
                                {
                                    FString RealLobbyId;
                                    if (InviteResult.Session.SessionSettings.Get<FString>(
                                            "EOS_RealLobbyId",
                                            RealLobbyId))
                                    {
                                        This->HandleSessionInviteAccept(ControllerId, RealLobbyId);
                                    }
                                    else
                                    {
                                        UE_LOG(
                                            LogEOS,
                                            Error,
                                            TEXT("Received invite from synthetic party, but it didn't have "
                                                 "EOS_RealLobbyId"));
                                    }
                                }
                                else
                                {
                                    UE_LOG(
                                        LogEOS,
                                        Error,
                                        TEXT("Received invite from synthetic party, but it wasn't successful"));
                                }
                            }
                        })));
        }
    }
}

void FSyntheticPartyManager::HandleSessionInviteAccept(int32 ControllerId, const FString &EOSPartyId)
{
    auto EOSParties = this->EOSPartySystem.Pin();
    auto EOSIdentity = this->EOSIdentitySystem.Pin();
    if (EOSParties && EOSIdentity)
    {
        if (EOSIdentity->GetLoginStatus(ControllerId) != ELoginStatus::LoggedIn)
        {
            // Routing of accepted invites in the editor is a bit jank, since wrapped subsystems might be singletons. If
            // they are, they'll cause both the PIE and global DefaultInstance to receive the event, and in that case
            // you'll get the message below forever (since DefaultInstance doesn't get logged into). In the editor, just
            // ignore invites if the local user isn't logged in.
#if !WITH_EDITOR
            // The user might not be authenticated yet, we need to try the invite again later.
            UE_LOG(
                LogEOS,
                Warning,
                TEXT("Received invite from synthetic party, but identity not ready yet, scheduling for later"));
            FTicker::GetCoreTicker().AddTicker(
                FTickerDelegate::CreateLambda(
                    [WeakThis = GetWeakThis(this), ControllerId, EOSPartyId](float DeltaTime) {
                        if (auto This = PinWeakThis(WeakThis))
                        {
                            This->HandleSessionInviteAccept(ControllerId, EOSPartyId);
                        }
                        return false;
                    }),
                0.2);
#endif
            return;
        }

        auto IdentityByController = EOSIdentity->GetUniquePlayerId(ControllerId);

        EPartyJoinabilityConstraint Constraint = this->Config->GetPartyJoinabilityConstraint();
        TArray<TSharedRef<const FOnlinePartyId>> JoinedParties;
        if (!EOSParties->GetJoinedParties(*IdentityByController, JoinedParties))
        {
            UE_LOG(LogEOS, Error, TEXT("GetJoinedParties returned error while processing synthetic party invite"));
            return;
        }

        if (JoinedParties.Num() >= 1 && Constraint == EPartyJoinabilityConstraint::IgnoreInvitesIfAlreadyInParty)
        {
            // This user is already in a party, so they can't join another one via a synthetic invite. Trigger the
            // appropriate event so the game developer can tell the player the invite was ignored.
            UE_LOG(
                LogEOS,
                Verbose,
                TEXT("Ignoring synthetic party invite because the local user is already in at least one party and the "
                     "party constraint configuration is IgnoreInvitesIfAlreadyInParty."));
            EOSParties->TriggerOnPartyInviteResponseReceivedDelegates(
                *IdentityByController,
                *MakeShared<FOnlinePartyIdEOS>(TCHAR_TO_ANSI(*EOSPartyId)),
                *IdentityByController,
                EInvitationResponse::Rejected);
            return;
        }

        EOSParties->LookupPartyById(
            TCHAR_TO_ANSI(*EOSPartyId),
            StaticCastSharedPtr<const FUniqueNetIdEOS>(IdentityByController)->GetProductUserId(),
            [EOSParties, IdentityByController](EOS_HLobbyDetails LobbyHandle) {
                if (LobbyHandle == nullptr)
                {
                    UE_LOG(
                        LogEOS,
                        Error,
                        TEXT("Received invite from synthetic party, but could not resolve lobby ID to lobby handle"));
                    return;
                }

                auto JoinInfo = MakeShared<FOnlinePartyJoinInfoEOS>(LobbyHandle);
                EOSParties->JoinParty(
                    IdentityByController.ToSharedRef().Get(),
                    JoinInfo.Get(),
                    FOnJoinPartyComplete::CreateLambda([](const FUniqueNetId &LocalUserId,
                                                          const FOnlinePartyId &PartyId,
                                                          const EJoinPartyCompletionResult Result,
                                                          const int32 NotApprovedReason) {
                        UE_LOG(LogEOS, Verbose, TEXT("Join party from synthetic party: %d"), Result);
                    }));
            });
    }
    else
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("Received invite from synthetic party, but EOS parties and identity were not available at the time"));
    }
}

FSyntheticPartyManager::~FSyntheticPartyManager()
{
    for (auto &KV : this->SessionUserInviteAcceptedDelegateHandles)
    {
        if (auto Session = KV.Key.Pin())
        {
            Session->ClearOnSessionUserInviteAcceptedDelegate_Handle(KV.Value);
        }
    }
    this->SessionUserInviteAcceptedDelegateHandles.Empty();
}

void FSyntheticPartyManager::CreateSyntheticParty(
    const TSharedPtr<const FOnlinePartyId> &PartyId,
    const FSyntheticPartyOnComplete &OnComplete)
{
    auto EOSPartyId = StaticCastSharedPtr<const FOnlinePartyIdEOS>(PartyId);

    // Create an online session for each of the subsystems.
    FMultiOperation<FWrappedSubsystem, bool>::Run(
        this->WrappedSubsystems,
        [WeakThis = GetWeakThis(this),
         PartyId,
         EOSPartyId](const FWrappedSubsystem &Subsystem, const std::function<void(bool)> &OnDone) -> bool {
            if (auto This = PinWeakThis(WeakThis))
            {
                if (auto Session = Subsystem.Session.Pin())
                {
                    if (auto Identity = Subsystem.Identity.Pin())
                    {
                        auto UserId = Identity->GetUniquePlayerId(0);
                        if (UserId.IsValid())
                        {
                            auto SessionName = FString::Printf(
                                TEXT("SyntheticParty_%s_%s"),
                                *Subsystem.SubsystemName.ToString(),
                                *PartyId->ToString());
                            auto CreateHandleName = FString::Printf(TEXT("%s_Create"), *SessionName);

                            FOnlineSessionSettings SessionSettings;
                            SessionSettings.bAllowJoinViaPresence = true;
                            SessionSettings.bAllowJoinViaPresenceFriendsOnly = false;
                            SessionSettings.bAllowInvites = true;
                            SessionSettings.bAllowJoinInProgress = true;
                            SessionSettings.bIsDedicated = false;
                            SessionSettings.bIsLANMatch = false;
                            SessionSettings.bShouldAdvertise = true;
                            SessionSettings.BuildUniqueId = 0;
                            SessionSettings.bUsesPresence = true;
                            SessionSettings.bUsesStats = false;
                            SessionSettings.NumPrivateConnections = 0;
                            // doesn't matter, other players never actually join the session.
                            SessionSettings.NumPublicConnections = 4;
#if defined(EOS_SESSION_HAS_LOBBY_OPTIONS)
                            SessionSettings.bAntiCheatProtected = false;
                            SessionSettings.bUseLobbiesIfAvailable = true;
                            SessionSettings.bUseLobbiesVoiceChatIfAvailable = false;
#endif
                            SessionSettings.Settings.Add(
                                TEXT("EOS_RealLobbyId"),
                                FOnlineSessionSetting(
                                    ANSI_TO_TCHAR(EOSPartyId->GetLobbyId()),
                                    EOnlineDataAdvertisementType::ViaOnlineService));

                            auto Finalize = [WeakThis = GetWeakThis(This),
                                             OnDone,
                                             CreateHandleName,
                                             WeakSession = TWeakPtr<IOnlineSession, ESPMode::ThreadSafe>(Session)](
                                                bool bWasSuccessful) {
                                if (auto This = PinWeakThis(WeakThis))
                                {
                                    OnDone(bWasSuccessful);
                                    if (This->HandlesPendingRemoval.Contains(CreateHandleName))
                                    {
                                        auto CreateHandle = This->HandlesPendingRemoval[CreateHandleName];
                                        This->HandlesPendingRemoval.Remove(CreateHandleName);
                                        if (auto Session = WeakSession.Pin())
                                        {
                                            Session->ClearOnCreateSessionCompleteDelegate_Handle(CreateHandle);
                                        }
                                    }
                                }
                            };

                            This->HandlesPendingRemoval.Add(
                                CreateHandleName,
                                Session->AddOnCreateSessionCompleteDelegate_Handle(
                                    FOnCreateSessionCompleteDelegate::CreateLambda(
                                        [SessionName, Finalize](FName CreatedSessionName, bool bWasSuccessful) {
                                            if (CreatedSessionName.IsEqual(FName(*SessionName)))
                                            {
                                                if (!bWasSuccessful)
                                                {
                                                    UE_LOG(
                                                        LogEOS,
                                                        Error,
                                                        TEXT("Failed to create synthetic party: %s"),
                                                        *SessionName);
                                                }
                                                else
                                                {
                                                    UE_LOG(
                                                        LogEOS,
                                                        Verbose,
                                                        TEXT("Synthetic party has been set up: %s"),
                                                        *SessionName);
                                                }

                                                Finalize(bWasSuccessful);
                                            }
                                        })));

                            if (!Session
                                     ->CreateSession(UserId.ToSharedRef().Get(), FName(*SessionName), SessionSettings))
                            {
                                UE_LOG(
                                    LogEOS,
                                    Error,
                                    TEXT("Failed to create synthetic party (call failed): %s"),
                                    *SessionName);
                                Finalize(false);
                            }
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        [OnComplete](const TArray<bool> &Values) {
            OnComplete.ExecuteIfBound();
        });
}

void FSyntheticPartyManager::DeleteSyntheticParty(
    const TSharedPtr<const FOnlinePartyId> &PartyId,
    const FSyntheticPartyOnComplete &OnComplete)
{
    auto EOSPartyId = StaticCastSharedPtr<const FOnlinePartyIdEOS>(PartyId);

    // Create an online session for each of the subsystems.
    FMultiOperation<FWrappedSubsystem, bool>::Run(
        this->WrappedSubsystems,
        [PartyId, EOSPartyId](const FWrappedSubsystem &Subsystem, const std::function<void(bool)> &OnDone) -> bool {
            if (auto Session = Subsystem.Session.Pin())
            {
                if (auto Identity = Subsystem.Identity.Pin())
                {
                    auto UserId = Identity->GetUniquePlayerId(0);
                    if (UserId.IsValid())
                    {
                        auto SessionName = FName(*FString::Printf(
                            TEXT("SyntheticParty_%s_%s"),
                            *Subsystem.SubsystemName.ToString(),
                            *PartyId->ToString()));
                        if (Session->GetNamedSession(SessionName) != nullptr)
                        {
                            return Session->DestroySession(
                                SessionName,
                                FOnDestroySessionCompleteDelegate::CreateLambda(
                                    [SessionName, OnDone](FName DestroyedSessionName, bool bWasSuccessful) {
                                        if (DestroyedSessionName == SessionName)
                                        {
                                            if (!bWasSuccessful)
                                            {
                                                UE_LOG(
                                                    LogEOS,
                                                    Error,
                                                    TEXT("Failed to destroy synthetic party: %s"),
                                                    *SessionName.ToString());
                                            }

                                            OnDone(bWasSuccessful);
                                        }
                                    }));
                        }
                    }
                }
            }
            return false;
        },
        [OnComplete](const TArray<bool> &Values) {
            OnComplete.ExecuteIfBound();
        });
}

void FSyntheticPartyManager::SendInvitation(
    const TSharedPtr<const FUniqueNetId> &LocalUserId,
    const TSharedPtr<const FOnlinePartyId> &PartyId,
    const TSharedPtr<const FUniqueNetId> &RecipientId,
    const FOnSendPartyInvitationComplete &Delegate)
{
    for (const auto &Subsystem : this->WrappedSubsystems)
    {
        if (!Subsystem.SubsystemName.IsEqual(RecipientId->GetType()))
        {
            continue;
        }

        auto SessionName = FName(
            *FString::Printf(TEXT("SyntheticParty_%s_%s"), *Subsystem.SubsystemName.ToString(), *PartyId->ToString()));

        if (auto SessionInterface = Subsystem.Session.Pin())
        {
            if (!SessionInterface->SendSessionInviteToFriend(
                    LocalUserId.ToSharedRef().Get(),
                    SessionName,
                    RecipientId.ToSharedRef().Get()))
            {
                UE_LOG(LogEOS, Error, TEXT("SendInvitation called for failed for delegated subsystem"));
                Delegate.ExecuteIfBound(
                    LocalUserId.ToSharedRef().Get(),
                    PartyId.ToSharedRef().Get(),
                    RecipientId.ToSharedRef().Get(),
                    ESendPartyInvitationCompletionResult::UnknownInternalFailure);
                return;
            }

            // Invitation was sent.
            Delegate.ExecuteIfBound(
                LocalUserId.ToSharedRef().Get(),
                PartyId.ToSharedRef().Get(),
                RecipientId.ToSharedRef().Get(),
                ESendPartyInvitationCompletionResult::Succeeded);
            return;
        }

        UE_LOG(LogEOS, Error, TEXT("Session interface not valid for party when SendInvitation was called"));
        Delegate.ExecuteIfBound(
            LocalUserId.ToSharedRef().Get(),
            PartyId.ToSharedRef().Get(),
            RecipientId.ToSharedRef().Get(),
            ESendPartyInvitationCompletionResult::UnknownInternalFailure);
        return;
    }

    UE_LOG(LogEOS, Error, TEXT("Session mapping does not exist for EOS party during SendInvitation"));
    Delegate.ExecuteIfBound(
        LocalUserId.ToSharedRef().Get(),
        PartyId.ToSharedRef().Get(),
        RecipientId.ToSharedRef().Get(),
        ESendPartyInvitationCompletionResult::UnknownInternalFailure);
    return;
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION