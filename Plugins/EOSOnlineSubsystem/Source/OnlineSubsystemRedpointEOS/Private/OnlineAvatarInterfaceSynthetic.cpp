// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION

#include "OnlineAvatarInterfaceSynthetic.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineFriendSynthetic.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineUserEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

FOnlineAvatarInterfaceSynthetic::FOnlineAvatarInterfaceSynthetic(
    FName InInstanceName,
    const TSharedRef<FEOSConfig> &InConfig,
    TSharedPtr<FOnlineIdentityInterfaceEOS, ESPMode::ThreadSafe> InIdentity,
    IOnlineFriendsPtr InFriends)
{
    this->InstanceName = InInstanceName;
    this->Config = InConfig;
    this->Identity = MoveTemp(InIdentity);
    this->Friends = MoveTemp(InFriends);

    // It's not safe to initialize DelegatedIdentity and DelegatedAvatar here as most OSS implementations try to use the
    // UObject system when calling GetNamedInterface (which is what Online::GetAvatarInterface uses internally). So we
    // must defer until later to ensure that the UObject system is running before we try to get the delegated
    // subsystems.
    this->bInitialized = false;
}

bool FOnlineAvatarInterfaceSynthetic::GetAvatar(
    const FUniqueNetId &LocalUserId,
    const FUniqueNetId &TargetUserId,
    TSoftObjectPtr<UTexture> DefaultTexture,
    FOnGetAvatarComplete OnComplete)
{
    if (!this->bInitialized)
    {
        this->DelegatedIdentity = FDelegatedSubsystems::GetDelegatedInterfaces<IOnlineIdentity>(
            this->Config.ToSharedRef(),
            this->InstanceName,
            [](IOnlineSubsystem *OSS) {
                return OSS->GetIdentityInterface();
            });
        this->DelegatedAvatar = FDelegatedSubsystems::GetDelegatedInterfaces<IOnlineAvatar>(
            this->Config.ToSharedRef(),
            this->InstanceName,
            [](IOnlineSubsystem *OSS) {
                return Online::GetAvatarInterface(OSS);
            });
        this->bInitialized = true;
    }

    if (LocalUserId.GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("LocalUserId is not an EOS user when calling GetAvatar."));
        OnComplete.ExecuteIfBound(false, DefaultTexture);
        return true;
    }

    int32 LocalUserNum;
    if (!this->Identity->GetLocalUserNum(LocalUserId, LocalUserNum))
    {
        UE_LOG(LogEOS, Error, TEXT("LocalUserId is not a locally signed in EOS user."));
        OnComplete.ExecuteIfBound(false, DefaultTexture);
        return true;
    }

    if (TargetUserId.GetType() == REDPOINT_EOS_SUBSYSTEM)
    {
        // Target user might be a local user.
        for (const auto &Account : this->Identity->GetAllUserAccounts())
        {
            if (*Account->GetUserId() == TargetUserId)
            {
                // Found the user as a local user.
                for (const auto &WrappedTargetUserId :
                     StaticCastSharedPtr<FUserOnlineAccountEOS>(Account)->GetExternalUserIds())
                {
                    if (this->DelegatedAvatar.Contains(WrappedTargetUserId->GetType()) &&
                        this->DelegatedIdentity.Contains(WrappedTargetUserId->GetType()))
                    {
                        if (auto WrappedIdentity =
                                this->DelegatedIdentity[WrappedTargetUserId->GetType()].Implementation.Pin())
                        {
                            TSharedPtr<const FUniqueNetId> WrappedLocalUserId =
                                WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                            if (WrappedLocalUserId.IsValid())
                            {
                                if (auto WrappedAvatar =
                                        this->DelegatedAvatar[WrappedTargetUserId->GetType()].Implementation.Pin())
                                {
                                    WrappedAvatar->GetAvatar(
                                        *WrappedLocalUserId,
                                        *WrappedTargetUserId,
                                        DefaultTexture,
                                        OnComplete);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Target user might be a friend.
        TSharedPtr<FOnlineFriend> Friend = this->Friends->GetFriend(LocalUserNum, TargetUserId, TEXT(""));
        if (Friend.IsValid() && Friend->GetUserId()->GetType() == REDPOINT_EOS_SUBSYSTEM)
        {
            // Found the user as a friend.
            for (const auto &WrappedFriend : StaticCastSharedPtr<FOnlineFriendSynthetic>(Friend)->GetWrappedFriends())
            {
                if (this->DelegatedAvatar.Contains(WrappedFriend.Key) &&
                    this->DelegatedIdentity.Contains(WrappedFriend.Key))
                {
                    if (auto WrappedIdentity = this->DelegatedIdentity[WrappedFriend.Key].Implementation.Pin())
                    {
                        TSharedPtr<const FUniqueNetId> WrappedLocalUserId =
                            WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                        if (WrappedLocalUserId.IsValid())
                        {
                            if (auto WrappedAvatar = this->DelegatedAvatar[WrappedFriend.Key].Implementation.Pin())
                            {
                                WrappedAvatar->GetAvatar(
                                    *WrappedLocalUserId,
                                    *WrappedFriend.Value->GetUserId(),
                                    DefaultTexture,
                                    OnComplete);
                                return true;
                            }
                        }
                    }
                }
            }
        }

        // @todo: Query on-demand with IOnlineUser.

        // No way to fetch the user's avatar.
        OnComplete.ExecuteIfBound(false, DefaultTexture);
        return true;
    }
    else
    {
        // Otherwise, targeting a native user ID. Just see if we have an IOnlineAvatar implementation for it.

        if (this->DelegatedAvatar.Contains(TargetUserId.GetType()) &&
            this->DelegatedIdentity.Contains(TargetUserId.GetType()))
        {
            if (auto WrappedIdentity = this->DelegatedIdentity[TargetUserId.GetType()].Implementation.Pin())
            {
                TSharedPtr<const FUniqueNetId> WrappedLocalUserId = WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                if (WrappedLocalUserId.IsValid())
                {
                    if (auto WrappedAvatar = this->DelegatedAvatar[TargetUserId.GetType()].Implementation.Pin())
                    {
                        WrappedAvatar->GetAvatar(*WrappedLocalUserId, TargetUserId, DefaultTexture, OnComplete);
                        return true;
                    }
                }
            }
        }
    }

    OnComplete.ExecuteIfBound(false, DefaultTexture);
    return true;
}

bool FOnlineAvatarInterfaceSynthetic::GetAvatarUrl(
    const FUniqueNetId &LocalUserId,
    const FUniqueNetId &TargetUserId,
    FString DefaultAvatarUrl,
    FOnGetAvatarUrlComplete OnComplete)
{
    if (!this->bInitialized)
    {
        this->DelegatedIdentity = FDelegatedSubsystems::GetDelegatedInterfaces<IOnlineIdentity>(
            this->Config.ToSharedRef(),
            this->InstanceName,
            [](IOnlineSubsystem *OSS) {
                return OSS->GetIdentityInterface();
            });
        this->DelegatedAvatar = FDelegatedSubsystems::GetDelegatedInterfaces<IOnlineAvatar>(
            this->Config.ToSharedRef(),
            this->InstanceName,
            [](IOnlineSubsystem *OSS) {
                return Online::GetAvatarInterface(OSS);
            });
        this->bInitialized = true;
    }

    if (LocalUserId.GetType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(LogEOS, Error, TEXT("LocalUserId is not an EOS user when calling GetAvatarUrl."));
        OnComplete.ExecuteIfBound(false, DefaultAvatarUrl);
        return true;
    }

    int32 LocalUserNum;
    if (!this->Identity->GetLocalUserNum(LocalUserId, LocalUserNum))
    {
        UE_LOG(LogEOS, Error, TEXT("LocalUserId is not a locally signed in EOS user."));
        OnComplete.ExecuteIfBound(false, DefaultAvatarUrl);
        return true;
    }

    if (TargetUserId.GetType() == REDPOINT_EOS_SUBSYSTEM)
    {
        // Target user might be a local user.
        for (const auto &Account : this->Identity->GetAllUserAccounts())
        {
            if (*Account->GetUserId() == TargetUserId)
            {
                // Found the user as a local user.
                for (const auto &WrappedTargetUserId :
                     StaticCastSharedPtr<FUserOnlineAccountEOS>(Account)->GetExternalUserIds())
                {
                    if (this->DelegatedAvatar.Contains(WrappedTargetUserId->GetType()) &&
                        this->DelegatedIdentity.Contains(WrappedTargetUserId->GetType()))
                    {
                        if (auto WrappedIdentity =
                                this->DelegatedIdentity[WrappedTargetUserId->GetType()].Implementation.Pin())
                        {
                            TSharedPtr<const FUniqueNetId> WrappedLocalUserId =
                                WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                            if (WrappedLocalUserId.IsValid())
                            {
                                if (auto WrappedAvatar =
                                        this->DelegatedAvatar[WrappedTargetUserId->GetType()].Implementation.Pin())
                                {
                                    WrappedAvatar->GetAvatarUrl(
                                        *WrappedLocalUserId,
                                        *WrappedTargetUserId,
                                        DefaultAvatarUrl,
                                        OnComplete);
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Target user might be a friend.
        TSharedPtr<FOnlineFriend> Friend = this->Friends->GetFriend(LocalUserNum, TargetUserId, TEXT(""));
        if (Friend.IsValid() && Friend->GetUserId()->GetType() == REDPOINT_EOS_SUBSYSTEM)
        {
            // Found the user as a friend.
            for (const auto &WrappedFriend : StaticCastSharedPtr<FOnlineFriendSynthetic>(Friend)->GetWrappedFriends())
            {
                if (this->DelegatedAvatar.Contains(WrappedFriend.Key) &&
                    this->DelegatedIdentity.Contains(WrappedFriend.Key))
                {
                    if (auto WrappedIdentity = this->DelegatedIdentity[WrappedFriend.Key].Implementation.Pin())
                    {
                        TSharedPtr<const FUniqueNetId> WrappedLocalUserId =
                            WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                        if (WrappedLocalUserId.IsValid())
                        {
                            if (auto WrappedAvatar = this->DelegatedAvatar[WrappedFriend.Key].Implementation.Pin())
                            {
                                WrappedAvatar->GetAvatarUrl(
                                    *WrappedLocalUserId,
                                    *WrappedFriend.Value->GetUserId(),
                                    DefaultAvatarUrl,
                                    OnComplete);
                                return true;
                            }
                        }
                    }
                }
            }
        }

        // @todo: Query on-demand with IOnlineUser.

        // No way to fetch the user's avatar.
        OnComplete.ExecuteIfBound(false, DefaultAvatarUrl);
        return true;
    }
    else
    {
        // Otherwise, targeting a native user ID. Just see if we have an IOnlineAvatar implementation for it.

        if (this->DelegatedAvatar.Contains(TargetUserId.GetType()) &&
            this->DelegatedIdentity.Contains(TargetUserId.GetType()))
        {
            if (auto WrappedIdentity = this->DelegatedIdentity[TargetUserId.GetType()].Implementation.Pin())
            {
                TSharedPtr<const FUniqueNetId> WrappedLocalUserId = WrappedIdentity->GetUniquePlayerId(LocalUserNum);
                if (WrappedLocalUserId.IsValid())
                {
                    if (auto WrappedAvatar = this->DelegatedAvatar[TargetUserId.GetType()].Implementation.Pin())
                    {
                        WrappedAvatar->GetAvatarUrl(*WrappedLocalUserId, TargetUserId, DefaultAvatarUrl, OnComplete);
                        return true;
                    }
                }
            }
        }
    }

    OnComplete.ExecuteIfBound(false, DefaultAvatarUrl);
    return true;
}

EOS_DISABLE_STRICT_WARNINGS

#endif