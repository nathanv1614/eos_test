// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/OnlinePresenceInterfaceSynthetic.h"

#include "OnlineError.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemRedpointEOS/Shared/DelegatedSubsystems.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineFriendsInterfaceSynthetic.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineIdentityInterfaceEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

FOnlinePresenceInterfaceSynthetic::FOnlinePresenceInterfaceSynthetic(
    FName InInstanceName,
    TSharedPtr<const FOnlineIdentityInterfaceEOS, ESPMode::ThreadSafe> InIdentity,
    const TSharedRef<const class FOnlineFriendsInterfaceSynthetic, ESPMode::ThreadSafe> &InFriends,
    const IOnlineSubsystemPtr &InSubsystemEAS,
    const TSharedRef<FEOSConfig> &InConfig)
{
    this->Config = InConfig;
    this->IdentityEOS = MoveTemp(InIdentity);
    this->FriendsSynthetic = InFriends;

    if (InSubsystemEAS != nullptr)
    {
        this->WrappedSubsystems.Add(
            {REDPOINT_EAS_SUBSYSTEM, InSubsystemEAS->GetIdentityInterface(), InSubsystemEAS->GetPresenceInterface()});
    }
    struct W
    {
        TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> Identity;
        TWeakPtr<IOnlinePresence, ESPMode::ThreadSafe> Presence;
    };
    for (const auto &KV : FDelegatedSubsystems::GetDelegatedSubsystems<W>(
             InConfig,
             InInstanceName,
             [](FDelegatedSubsystems::ISubsystemContext &Ctx) -> TSharedPtr<W> {
                 auto Identity = Ctx.GetDelegatedInterface<IOnlineIdentity>([](IOnlineSubsystem *OSS) {
                     return OSS->GetIdentityInterface();
                 });
                 auto Presence = Ctx.GetDelegatedInterface<IOnlinePresence>([](IOnlineSubsystem *OSS) {
                     return OSS->GetPresenceInterface();
                 });
                 if (Identity.IsValid() && Presence.IsValid())
                 {
                     return MakeShared<W>(W{Identity, Presence});
                 }
                 return nullptr;
             }))
    {
        this->WrappedSubsystems.Add({KV.Key, KV.Value->Identity, KV.Value->Presence});
    }
}

void FOnlinePresenceInterfaceSynthetic::RegisterEvents()
{
    for (auto Wk : this->WrappedSubsystems)
    {
        if (auto Presence = Wk.Presence.Pin())
        {
            this->OnPresenceReceivedDelegates.Add(TTuple<FWrappedSubsystem, FDelegateHandle>(
                Wk,
                Presence->AddOnPresenceReceivedDelegate_Handle(FOnPresenceReceivedDelegate::CreateLambda(
                    [WeakThis = GetWeakThis(this),
                     Wk](const class FUniqueNetId &UserId, const TSharedRef<FOnlineUserPresence> &Presence) {
                        if (auto This = PinWeakThis(WeakThis))
                        {
                            if (auto Identity = Wk.Identity.Pin())
                            {
                                UE_LOG(
                                    LogEOS,
                                    Verbose,
                                    TEXT("FOnlinePresenceInterfaceSynthetic: Forwarding OnPresenceReceived event"));
                                // The UserId is the user ID from the wrapped subsystem, not EOS. We need to try to map
                                // the native user ID back to an EOS ID if possible.
                                TSharedPtr<const FUniqueNetIdEOS> EOSId = nullptr;
                                for (int i = 0; i < MAX_LOCAL_PLAYERS; i++)
                                {
                                    TSharedPtr<const FUniqueNetId> WrappedId = Identity->GetUniquePlayerId(i);
                                    if (WrappedId.IsValid() && *WrappedId == UserId)
                                    {
                                        if (This->IdentityEOS->GetLoginStatus(i) == ELoginStatus::LoggedIn)
                                        {
                                            EOSId = StaticCastSharedPtr<const FUniqueNetIdEOS>(
                                                This->IdentityEOS->GetUniquePlayerId(i));
                                            if (EOSId.IsValid())
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!EOSId.IsValid())
                                {
                                    EOSId = This->FriendsSynthetic->TryResolveNativeIdToEOS(UserId);
                                }
                                if (EOSId.IsValid())
                                {
                                    This->TriggerOnPresenceReceivedDelegates(*EOSId, Presence);
                                }
                                else
                                {
                                    This->TriggerOnPresenceReceivedDelegates(UserId, Presence);
                                }
                            }
                        }
                    }))));
            this->OnPresenceArrayUpdatedDelegates.Add(TTuple<FWrappedSubsystem, FDelegateHandle>(
                Wk,
                Presence->AddOnPresenceArrayUpdatedDelegate_Handle(FOnPresenceArrayUpdatedDelegate::CreateLambda(
                    [WeakThis = GetWeakThis(this), Wk](
                        const class FUniqueNetId &UserId,
                        const TArray<TSharedRef<FOnlineUserPresence>> &PresenceArray) {
                        if (auto This = PinWeakThis(WeakThis))
                        {
                            if (auto Identity = Wk.Identity.Pin())
                            {
                                UE_LOG(
                                    LogEOS,
                                    Verbose,
                                    TEXT("FOnlinePresenceInterfaceSynthetic: Forwarding OnPresenceArrayUpdated "
                                         "event"));
                                // The UserId is the user ID from the wrapped subsystem, not EOS. We need to try to map
                                // the native user ID back to an EOS ID if possible.
                                TSharedPtr<const FUniqueNetIdEOS> EOSId = nullptr;
                                for (int i = 0; i < MAX_LOCAL_PLAYERS; i++)
                                {
                                    TSharedPtr<const FUniqueNetId> WrappedId = Identity->GetUniquePlayerId(i);
                                    if (WrappedId.IsValid() && *WrappedId == UserId)
                                    {
                                        if (This->IdentityEOS->GetLoginStatus(i) == ELoginStatus::LoggedIn)
                                        {
                                            EOSId = StaticCastSharedPtr<const FUniqueNetIdEOS>(
                                                This->IdentityEOS->GetUniquePlayerId(i));
                                            if (EOSId.IsValid())
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!EOSId.IsValid())
                                {
                                    EOSId = This->FriendsSynthetic->TryResolveNativeIdToEOS(UserId);
                                }
                                if (EOSId.IsValid())
                                {
                                    This->TriggerOnPresenceArrayUpdatedDelegates(*EOSId, PresenceArray);
                                }
                                else
                                {
                                    This->TriggerOnPresenceArrayUpdatedDelegates(UserId, PresenceArray);
                                }
                            }
                        }
                    }))));
        }
    }
}

FOnlinePresenceInterfaceSynthetic::~FOnlinePresenceInterfaceSynthetic()
{
    for (auto V : this->OnPresenceReceivedDelegates)
    {
        if (auto Ptr = V.Get<0>().Presence.Pin())
        {
            Ptr->ClearOnPresenceReceivedDelegate_Handle(V.Get<1>());
        }
    }
    this->OnPresenceReceivedDelegates.Empty();

    for (auto V : this->OnPresenceArrayUpdatedDelegates)
    {
        if (auto Ptr = V.Get<0>().Presence.Pin())
        {
            Ptr->ClearOnPresenceArrayUpdatedDelegate_Handle(V.Get<1>());
        }
    }
    this->OnPresenceArrayUpdatedDelegates.Empty();
}

void FOnlinePresenceInterfaceSynthetic::SetPresence(
    const FUniqueNetId &User,
    const FOnlineUserPresenceStatus &Status,
    const FOnPresenceTaskCompleteDelegate &Delegate)
{
    if (this->WrappedSubsystems.Num() == 0)
    {
        Delegate.ExecuteIfBound(User, false);
        return;
    }

    // Work out the local user num for the user, because this is what aligns local users in the other subsystems to EOS.
    int32 LocalUserNum;
    if (!this->IdentityEOS->GetLocalUserNum(User, LocalUserNum))
    {
        Delegate.ExecuteIfBound(User, false);
        return;
    }

    // For every subsystem that we are delegating to, set the presence for the same user based on the local user num.
    FMultiOperation<FWrappedSubsystem, bool>::Run(
        this->WrappedSubsystems,
        [LocalUserNum, Status](const FWrappedSubsystem &InSubsystem, const std::function<void(bool OutValue)> &OnDone) {
            if (auto Presence = InSubsystem.Presence.Pin())
            {
                if (auto Identity = InSubsystem.Identity.Pin())
                {
                    TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalUserNum);
                    if (UserId.IsValid())
                    {
                        Presence->SetPresence(
                            *UserId,
                            Status,
                            FOnPresenceTaskCompleteDelegate::CreateLambda(
                                [OnDone](const class FUniqueNetId &UserId, const bool bWasSuccessful) {
                                    OnDone(bWasSuccessful);
                                }));
                        return true;
                    }
                }
            }

            return false;
        },
        [Delegate, UserRef = User.AsShared()](const TArray<bool> &OutValues) {
            for (auto Result : OutValues)
            {
                if (Result)
                {
                    Delegate.ExecuteIfBound(*UserRef, true);
                    return;
                }
            }
            Delegate.ExecuteIfBound(*UserRef, false);
        });
    return;
}

void FOnlinePresenceInterfaceSynthetic::QueryPresence(
    const FUniqueNetId &User,
    const FOnPresenceTaskCompleteDelegate &Delegate)
{
    if (this->WrappedSubsystems.Num() == 0)
    {
        Delegate.ExecuteIfBound(User, false);
        return;
    }

    // Work out the local user num for the user, because this is what aligns local users in the other subsystems to EOS.
    int32 LocalUserNum;
    if (!this->IdentityEOS->GetLocalUserNum(User, LocalUserNum))
    {
        Delegate.ExecuteIfBound(User, false);
        return;
    }

    // For every subsystem that we are delegating to, read the presence for the same user based on the local user num.
    // This will cache the presence in each subsystem for all of the user's friends in that subsystem.
    FMultiOperation<FWrappedSubsystem, bool>::Run(
        this->WrappedSubsystems,
        [LocalUserNum](const FWrappedSubsystem &InSubsystem, const std::function<void(bool OutValue)> &OnDone) {
            if (auto Presence = InSubsystem.Presence.Pin())
            {
                if (auto Identity = InSubsystem.Identity.Pin())
                {
                    TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalUserNum);
                    if (UserId.IsValid())
                    {
                        Presence->QueryPresence(
                            *UserId,
                            FOnPresenceTaskCompleteDelegate::CreateLambda(
                                [OnDone](const class FUniqueNetId &UserId, const bool bWasSuccessful) {
                                    OnDone(bWasSuccessful);
                                }));
                        return true;
                    }
                }
            }
            return false;
        },
        [Delegate, UserRef = User.AsShared()](const TArray<bool> &OutValues) {
            for (auto Result : OutValues)
            {
                if (Result)
                {
                    Delegate.ExecuteIfBound(UserRef.Get(), true);
                    return;
                }
            }
            Delegate.ExecuteIfBound(UserRef.Get(), false);
        });
}

#if defined(UE_4_26_OR_LATER)
void FOnlinePresenceInterfaceSynthetic::QueryPresence(
    const FUniqueNetId &LocalUserId,
    const TArray<TSharedRef<const FUniqueNetId>> &UserIds,
    const FOnPresenceTaskCompleteDelegate &Delegate)
{
    if (this->WrappedSubsystems.Num() == 0)
    {
        Delegate.ExecuteIfBound(LocalUserId, false);
        return;
    }

    // Work out the local user num for the user, because this is what aligns local users in the other subsystems to EOS.
    int32 LocalUserNum;
    if (!this->IdentityEOS->GetLocalUserNum(LocalUserId, LocalUserNum))
    {
        Delegate.ExecuteIfBound(LocalUserId, false);
        return;
    }

    struct FEntry
    {
        FWrappedSubsystem WrappedSubsystemInt;
        TArray<TSharedRef<const FUniqueNetId>> UserIds;
    };

    // We have to filter the user IDs by their subsystem, so only user IDs belonging
    // to the given subsystem have presence queried by that subsystem.

    TArray<FEntry> EntriesToProcess;
    for (int i = 0; i < this->WrappedSubsystems.Num(); i++)
    {
        TArray<TSharedRef<const FUniqueNetId>> OSSUserIds;
        for (const auto &UserId : UserIds)
        {
            if (UserId->GetType().IsEqual(this->WrappedSubsystems[i].SubsystemName))
            {
                OSSUserIds.Add(UserId);
            }
        }

        FEntry Entry = {};
        Entry.WrappedSubsystemInt = this->WrappedSubsystems[i];
        Entry.UserIds = OSSUserIds;
        EntriesToProcess.Add(Entry);
    }

    FMultiOperation<FEntry, bool>::Run(
        EntriesToProcess,
        [LocalUserNum](const FEntry &InEntry, const std::function<void(bool OutValue)> &OnDone) {
            if (auto Presence = InEntry.WrappedSubsystemInt.Presence.Pin())
            {
                if (auto Identity = InEntry.WrappedSubsystemInt.Identity.Pin())
                {
                    TSharedPtr<const FUniqueNetId> UserId = Identity->GetUniquePlayerId(LocalUserNum);
                    if (UserId.IsValid())
                    {
                        Presence->QueryPresence(
                            *UserId,
                            InEntry.UserIds,
                            FOnPresenceTaskCompleteDelegate::CreateLambda(
                                [OnDone](const class FUniqueNetId &UserId, const bool bWasSuccessful) {
                                    OnDone(bWasSuccessful);
                                }));
                    }

                    return true;
                }
            }

            return false;
        },
        [Delegate, UserRef = LocalUserId.AsShared()](const TArray<bool> &OutValues) {
            for (auto Result : OutValues)
            {
                if (Result)
                {
                    Delegate.ExecuteIfBound(UserRef.Get(), true);
                    return;
                }
            }
            Delegate.ExecuteIfBound(UserRef.Get(), false);
        });
}
#endif

EOnlineCachedResult::Type FOnlinePresenceInterfaceSynthetic::GetCachedPresence(
    const FUniqueNetId &User,
    TSharedPtr<FOnlineUserPresence> &OutPresence)
{
    for (int i = 0; i < this->WrappedSubsystems.Num(); i++)
    {
        if (User.GetType().IsEqual(this->WrappedSubsystems[i].SubsystemName))
        {
            if (auto Presence = this->WrappedSubsystems[i].Presence.Pin())
            {
                return Presence->GetCachedPresence(User, OutPresence);
            }
        }
    }
    return EOnlineCachedResult::NotFound;
}

EOnlineCachedResult::Type FOnlinePresenceInterfaceSynthetic::GetCachedPresenceForApp(
    const FUniqueNetId &LocalUserId,
    const FUniqueNetId &User,
    const FString &AppId,
    TSharedPtr<FOnlineUserPresence> &OutPresence)
{
    for (int i = 0; i < this->WrappedSubsystems.Num(); i++)
    {
        if (User.GetType().IsEqual(this->WrappedSubsystems[i].SubsystemName))
        {
            if (auto Presence = this->WrappedSubsystems[i].Presence.Pin())
            {
                return Presence->GetCachedPresenceForApp(LocalUserId, User, AppId, OutPresence);
            }
        }
    }
    return EOnlineCachedResult::NotFound;
}

EOS_DISABLE_STRICT_WARNINGS