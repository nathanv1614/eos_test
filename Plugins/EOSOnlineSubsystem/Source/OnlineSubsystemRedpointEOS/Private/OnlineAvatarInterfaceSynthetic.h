// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "CoreMinimal.h"
#include "OnlineSubsystemRedpointEOS/Shared/DelegatedSubsystems.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineIdentityInterfaceEOS.h"
#include "RedpointEOSInterfaces/Private/Interfaces/OnlineAvatarInterface.h"

EOS_ENABLE_STRICT_WARNINGS

class FOnlineAvatarInterfaceSynthetic : public IOnlineAvatar,
                                        public TSharedFromThis<FOnlineAvatarInterfaceSynthetic, ESPMode::ThreadSafe>
{
private:
    TMap<FName, FDelegatedInterface<IOnlineAvatar>> DelegatedAvatar;
    TMap<FName, FDelegatedInterface<IOnlineIdentity>> DelegatedIdentity;
    FName InstanceName;
    TSharedPtr<FEOSConfig> Config;
    TSharedPtr<FOnlineIdentityInterfaceEOS, ESPMode::ThreadSafe> Identity;
    // NOLINTNEXTLINE(unreal-unsafe-storage-of-oss-pointer)
    IOnlineFriendsPtr Friends;
    bool bInitialized;

public:
    FOnlineAvatarInterfaceSynthetic(
        FName InInstanceName,
        const TSharedRef<FEOSConfig> &InConfig,
        TSharedPtr<FOnlineIdentityInterfaceEOS, ESPMode::ThreadSafe> InIdentity,
        IOnlineFriendsPtr InFriends);
    UE_NONCOPYABLE(FOnlineAvatarInterfaceSynthetic);
    virtual ~FOnlineAvatarInterfaceSynthetic(){};

    bool GetAvatar(
        const FUniqueNetId &LocalUserId,
        const FUniqueNetId &TargetUserId,
        TSoftObjectPtr<UTexture> DefaultTexture,
        FOnGetAvatarComplete OnComplete) override;

    bool GetAvatarUrl(
        const FUniqueNetId &LocalUserId,
        const FUniqueNetId &TargetUserId,
        FString DefaultAvatarUrl,
        FOnGetAvatarUrlComplete OnComplete) override;
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION