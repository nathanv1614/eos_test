// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/EOSNetConnection.h"

#include "IPAddress.h"
#include "Net/DataChannel.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketEOS.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketSubsystemEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "PacketHandlers/StatelessConnectHandlerComponent.h"
#include "TimerManager.h"

EOS_ENABLE_STRICT_WARNINGS

// According to USteamSocketsNetConnection, UNetConnection can assert if this value is greater
// than 1024 (though I can't find out where it would do so).
static const int32 MAX_PACKET = EOS_P2P_MAX_PACKET_SIZE > 1024 ? 1024 : EOS_P2P_MAX_PACKET_SIZE;

void UEOSNetConnection::ReceivePacketFromDriver(
    const TSharedPtr<FInternetAddr> &ReceivedAddr,
    uint8 *Buffer,
    int32 BufferSize)
{
    if (this->bInConnectionlessHandshake)
    {
        const ProcessedPacket RawPacket =
            this->Driver->ConnectionlessHandler->IncomingConnectionless(ReceivedAddr, Buffer, BufferSize);
        TSharedPtr<StatelessConnectHandlerComponent> StatelessConnect = this->Driver->StatelessConnectComponent.Pin();
        bool bRestartedHandshake = false;
        if (!RawPacket.bError && StatelessConnect->HasPassedChallenge(ReceivedAddr, bRestartedHandshake) &&
            !bRestartedHandshake)
        {
            if (this->StatelessConnectComponent.IsValid())
            {
                int32 ServerSequence = 0;
                int32 ClientSequence = 0;
                StatelessConnect->GetChallengeSequence(ServerSequence, ClientSequence);
                this->InitSequence(ClientSequence, ServerSequence);
            }

            if (this->Handler.IsValid())
            {
                this->Handler->BeginHandshaking();
            }

            this->bInConnectionlessHandshake = false;

            // Reset the challenge data for the future
            if (StatelessConnect.IsValid())
            {
                StatelessConnect->ResetChallengeData();
            }

            int32 RawPacketSize = FMath::DivideAndRoundUp(RawPacket.CountBits, 8);
            if (RawPacketSize == 0)
            {
                // No actual data to receive.
                return;
            }

            // Forward the data from the processed packet.
            this->ReceivedRawPacket(RawPacket.Data, RawPacketSize);
            return;
        }
        else
        {
            UE_LOG(
                LogEOS,
                Verbose,
                TEXT("UEOSNetDriver::TickDispatch: Stateless connection challenge authentication "
                     "failed (will retry in a moment)"));
        }
    }

    // Forward data onto connection.
    this->ReceivedRawPacket(Buffer, BufferSize);
}

void UEOSNetConnection::InitBase(
    UNetDriver *InDriver,
    FSocket *InSocket,
    const FURL &InURL,
    EConnectionState InState,
    int32 InMaxPacket,
    int32 InPacketOverhead)
{
    UNetConnection::InitBase(
        InDriver,
        InSocket,
        InURL,
        InState,
        ((InMaxPacket == 0) ? MAX_PACKET : InMaxPacket),
        ((InPacketOverhead == 0) ? 1 : InPacketOverhead));

    this->Socket = ((ISocketEOS *)InSocket)->AsSharedEOS();
}

void UEOSNetConnection::InitRemoteConnection(
    UNetDriver *InDriver,
    FSocket *InSocket,
    const FURL &InURL,
    const FInternetAddr &InRemoteAddr,
    EConnectionState InState,
    int32 InMaxPacket,
    int32 InPacketOverhead)
{
    this->InitBase(InDriver, InSocket, InURL, InState, InMaxPacket, InPacketOverhead);

    this->RemoteAddr = InRemoteAddr.Clone();

    this->InitSendBuffer();
    this->SetClientLoginState(EClientLoginState::LoggingIn);
    this->SetExpectedClientLoginMsgType(NMT_Hello);
}

void UEOSNetConnection::InitLocalConnection(
    UNetDriver *InDriver,
    FSocket *InSocket,
    const FURL &InURL,
    EConnectionState InState,
    int32 InMaxPacket,
    int32 InPacketOverhead)
{
    this->InitBase(InDriver, InSocket, InURL, InState, InMaxPacket, InPacketOverhead);

    // Get the remote address (the one we are connecting to) from the InURL.
    this->RemoteAddr =
        InDriver->GetSocketSubsystem()->GetAddressFromString(FString::Printf(TEXT("%s:%d"), *InURL.Host, InURL.Port));

    this->InitSendBuffer();
}

void UEOSNetConnection::LowLevelSend(void *Data, int32 CountBits, FOutPacketTraits &Traits)
{
    TSharedPtr<ISocketEOS> SocketPinned = this->Socket.Pin();
    if (SocketPinned.IsValid())
    {
        const uint8 *SendData = reinterpret_cast<const uint8 *>(Data);

        // Process any packet modifiers
        if (this->Handler.IsValid() && !this->Handler->GetRawSend())
        {
            const ProcessedPacket ProcessedData =
                this->Handler->Outgoing(reinterpret_cast<uint8 *>(Data), CountBits, Traits);

            if (!ProcessedData.bError)
            {
                SendData = ProcessedData.Data;
                CountBits = ProcessedData.CountBits;
            }
            else
            {
                CountBits = 0;
            }
        }

        int32 BytesSent = 0;
        int32 BytesToSend = FMath::DivideAndRoundUp(CountBits, 8);

        if (BytesToSend > 0)
        {
            if (!SocketPinned->SendTo(SendData, BytesToSend, BytesSent, *RemoteAddr))
            {
                UE_LOG(
                    LogNet,
                    Warning,
                    TEXT("UEOSNetConnection::LowLevelSend: Could not send %d data over socket to %s!"),
                    BytesToSend,
                    *RemoteAddr->ToString(true));
            }
            else
            {
                EOS_INC_DWORD_STAT(STAT_EOSNetP2PSentPackets);
                EOS_INC_DWORD_STAT_BY(STAT_EOSNetP2PSentBytes, BytesToSend);
                EOS_TRACE_COUNTER_INCREMENT(CTR_EOSNetP2PSentPackets);
                EOS_TRACE_COUNTER_ADD(CTR_EOSNetP2PSentBytes, BytesToSend);
            }
        }
    }
    else
    {
        UE_LOG(LogNet, Error, TEXT("UEOSNetConnection::LowLevelSend: Could not send data because the socket is gone!"));
    }
}

FString UEOSNetConnection::LowLevelGetRemoteAddress(bool bAppendPort)
{
    if (this->RemoteAddr == nullptr)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("UEOSNetConnection::LowLevelGetRemoteAddress called on connection without a valid RemoteAddr. Has the "
                 "connection been closed?"));
        return TEXT("");
    }
    return this->RemoteAddr->ToString(true);
}

FString UEOSNetConnection::LowLevelDescribe()
{
    return TEXT("EOS Net Connection");
}

EOS_DISABLE_STRICT_WARNINGS