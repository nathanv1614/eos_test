// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/EOSGetPlayerAvatar.h"

#include "EOSNativePlatform.h"
#include "Interfaces/OnlineFriendsInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineFriendSynthetic.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineSubsystemRedpointEOS.h"
#include "OnlineSubsystemUtils.h"
#include "RedpointEOSInterfaces/Private/Interfaces/OnlineAvatarInterface.h"

EOS_ENABLE_STRICT_WARNINGS

UEOSGetPlayerAvatar *UEOSGetPlayerAvatar::GetPlayerAvatar(
    const UObject *WorldContextObject,
    int32 LocalUserNum,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FUniqueNetIdRepl UserId,
    UTexture *DefaultAvatar)
{
    UEOSGetPlayerAvatar *Node = NewObject<UEOSGetPlayerAvatar>();
    Node->WorldContextObject = WorldContextObject;
    Node->LocalUserNum = LocalUserNum;
    Node->UserId = UserId;
    Node->DefaultAvatar = DefaultAvatar;
    return Node;
}

void UEOSGetPlayerAvatar::Activate()
{
    if (this->UserId.IsValid() && this->UserId.GetUniqueNetId().IsValid())
    {
        IOnlineSubsystem *OnlineSubsystemEOS =
            Online::GetSubsystem(this->WorldContextObject->GetWorld(), REDPOINT_EOS_SUBSYSTEM);
        if (OnlineSubsystemEOS == nullptr)
        {
            // EOS subsystem is not available.
            UE_LOG(LogEOS, Error, TEXT("GetPlayerAvatar called but the EOS subsystem is not available."));
            this->OnComplete.Broadcast(this->DefaultAvatar);
            return;
        }

        IOnlineIdentityPtr Identity = OnlineSubsystemEOS->GetIdentityInterface();
        checkf(Identity.IsValid(), TEXT("EOS online subsystem must implement IOnlineIdentity"));

        TSharedPtr<IOnlineAvatar, ESPMode::ThreadSafe> Avatar = Online::GetAvatarInterface(OnlineSubsystemEOS);
        checkf(Identity.IsValid(), TEXT("EOS online subsystem must implement IOnlineAvatar"));

        if (!Avatar->GetAvatar(
                *Identity->GetUniquePlayerId(this->LocalUserNum),
                *this->UserId,
                this->DefaultAvatar,
                FOnGetAvatarComplete::CreateUObject(this, &UEOSGetPlayerAvatar::HandleComplete)))
        {
            this->OnComplete.Broadcast(this->DefaultAvatar);
        }
        return;
    }

    this->OnComplete.Broadcast(this->DefaultAvatar);
}

void UEOSGetPlayerAvatar::HandleComplete(
    bool bWasSuccessful,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSoftObjectPtr<UTexture> Texture)
{
    this->OnComplete.Broadcast(Texture.Get());
}

EOS_DISABLE_STRICT_WARNINGS
