// Copyright June Rhodes. All Rights Reserved.

#include "OnlineSubsystemRedpointEOS/Shared/OnlineFriendsInterfaceSynthetic.h"

#include "OnlineError.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemRedpointEOS/Public/EOSError.h"
#include "OnlineSubsystemRedpointEOS/Public/EOSNativePlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/DelegatedSubsystems.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSConfig.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/MultiOperation.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineFriendSynthetic.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineUserEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

FString FOnlineFriendsInterfaceSynthetic::MutateListName(const FWrappedFriendsInterface &Friends, FString ListName)
{
    for (const auto &Integration : this->RuntimePlatform->GetIntegrations())
    {
        Integration->MutateFriendsListNameIfRequired(Friends.SubsystemName, ListName);
    }
    return ListName;
}

/** Operation to call ReadFriendsList on each of the delegated subsystems */
bool FOnlineFriendsInterfaceSynthetic::Op_ReadFriendsList(
    int32 LocalUserNum,
    const FString &ListName,
    FName InSubsystemName,
    const TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> &InFriends,
    const std::function<void(bool OutValue)> &OnDone)
{
    if (auto Friends = InFriends.Pin())
    {
        return Friends->ReadFriendsList(
            LocalUserNum,
            ListName,
            FOnReadFriendsListComplete::CreateLambda([OnDone, InSubsystemName](
                                                         int32 LocalUserNum,
                                                         bool bWasSuccessful,
                                                         const FString &ListName,
                                                         const FString &ErrorStr) {
                if (!bWasSuccessful)
                {
                    UE_LOG(
                        LogEOS,
                        Error,
                        TEXT("Error from delegated subsystem '%s' while performing synthetic ReadFriends: %s"),
                        *InSubsystemName.ToString(),
                        *ErrorStr);
                }
                OnDone(bWasSuccessful);
            }));
    }
    else
    {
        return false;
    }
}

/** Once we've done calling ReadFriendsList on all subsystems, proceed */
void FOnlineFriendsInterfaceSynthetic::Dn_ReadFriendsList(
    int32 LocalUserNum,
    FString ListName,
    const FOnReadFriendsListComplete &Delegate,
    const TArray<bool> &OutValues)
{
    // If none of the subsystems returned successfully, report failure.
    bool AnySuccess = false;
    for (auto Result : OutValues)
    {
        if (Result)
        {
            AnySuccess = true;
            break;
        }
    }
    if (!AnySuccess)
    {
        Delegate.ExecuteIfBound(
            LocalUserNum,
            false,
            ListName,
            FString(TEXT("None of the delegated subsystems returned successfully")));
        return;
    }

    // Now that we have called ReadFriendsList on all the subsystems, we're going to start
    // aggregating all of their friends lists together.
    FMultiOperation<FWrappedFriendsInterface, TArray<TSharedRef<FOnlineFriend>>>::Run(
        this->OnlineFriends,
        [WeakThis = GetWeakThis(this), LocalUserNum, ListName](
            const FWrappedFriendsInterface &InFriends,
            const std::function<void(TArray<TSharedRef<FOnlineFriend>> OutValue)> &OnDone) {
            if (auto This = PinWeakThis(WeakThis))
            {
                return This->Op_GetFriendsList(
                    LocalUserNum,
                    This->MutateListName(InFriends, ListName),
                    InFriends.OnlineFriends,
                    OnDone);
            }
            return false;
        },
        [WeakThis = GetWeakThis(this), LocalUserNum, ListName, Delegate](
            TArray<TArray<TSharedRef<FOnlineFriend>>> OutValues) {
            if (auto This = PinWeakThis(WeakThis))
            {
                This->Dn_GetFriendsList(LocalUserNum, ListName, Delegate, MoveTemp(OutValues));
            }
        });
}

/** Call GetFriendsList on each of the delegated subsystems */
bool FOnlineFriendsInterfaceSynthetic::Op_GetFriendsList(
    int32 LocalUserNum,
    const FString &ListName,
    const TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> &InFriends,
    const std::function<void(TArray<TSharedRef<FOnlineFriend>> OutValue)> &OnDone)
{
    if (auto Friends = InFriends.Pin())
    {
        TArray<TSharedRef<FOnlineFriend>> SubFriends;
        bool Outcome = Friends->GetFriendsList(LocalUserNum, ListName, SubFriends);
        if (Outcome)
        {
            OnDone(SubFriends);
        }
        return Outcome;
    }
    else
    {
        return false;
    }
}

/** We've got all the friends from all the delegated subsystems, now start unifying */
void FOnlineFriendsInterfaceSynthetic::Dn_GetFriendsList(
    int32 LocalUserNum,
    FString ListName,
    const FOnReadFriendsListComplete &Delegate,
    TArray<TArray<TSharedRef<FOnlineFriend>>> OutValues)
{
    // Get external account information for all of the friends, using the native platform APIs.
    TArray<TSharedPtr<FFriendInfo>> FriendInfos;
    for (auto i = 0; i < OutValues.Num(); i++)
    {
        auto List = OutValues[i];
        for (const auto &FriendRef : List)
        {
            auto FriendInfo = MakeShared<FFriendInfo>();
            FriendInfo->SubsystemName = this->OnlineFriends[i].SubsystemName;
            FriendInfo->FriendRef = FriendRef;
            FriendInfo->HasExternalInfo = false;

            if (FriendRef->GetUserId()->GetType() == REDPOINT_EAS_SUBSYSTEM)
            {
                FriendInfo->ExternalInfo.AccountId = FriendRef->GetUserId()->ToString();
                FriendInfo->ExternalInfo.AccountIdType = EOS_EExternalAccountType::EOS_EAT_EPIC;
                FriendInfo->HasExternalInfo = true;
            }
            else
            {
                for (const auto &Integration : this->RuntimePlatform->GetIntegrations())
                {
                    if (Integration->CanProvideExternalAccountId(FriendRef->GetUserId().Get()))
                    {
                        FriendInfo->ExternalInfo = Integration->GetExternalAccountId(FriendRef->GetUserId().Get());
                        FriendInfo->HasExternalInfo = true;
                        break;
                    }
                }
            }

            FriendInfos.Add(FriendInfo);
        }
    }

    // Wrangle the friends so that they're grouped by the external account type, so that
    // we can batch the operations together.
    TMap<EOS_EExternalAccountType, TArray<TSharedPtr<FFriendInfo>>> FriendsByEOSExternalAccountType =
        GroupBy<EOS_EExternalAccountType, TSharedPtr<FFriendInfo>>(
            FriendInfos,
            [](const TSharedPtr<FFriendInfo> &Info) -> EOS_EExternalAccountType {
                if (Info->HasExternalInfo)
                {
                    return (EOS_EExternalAccountType)Info->ExternalInfo.AccountIdType;
                }
                return (EOS_EExternalAccountType)-1;
            });
    TArray<FUnresolvedFriendEntries> FriendsByEOSExternalAccountTypeUnpacked =
        UnpackMap(FriendsByEOSExternalAccountType);

    // Resolve friends lists by type.
    FMultiOperation<FUnresolvedFriendEntries, TResolvedFriendEntries>::Run(
        FriendsByEOSExternalAccountTypeUnpacked,
        [WeakThis = GetWeakThis(this), LocalUserNum](
            const FUnresolvedFriendEntries &EntriesByType,
            const std::function<void(TResolvedFriendEntries)> &OnDone) {
            if (auto This = PinWeakThis(WeakThis))
            {
                return This->Op_ResolveFriendsListByType(LocalUserNum, EntriesByType, OnDone);
            }
            return false;
        },
        [WeakThis = GetWeakThis(this), LocalUserNum, ListName, Delegate](
            const TArray<TResolvedFriendEntries> &Results) {
            if (auto This = PinWeakThis(WeakThis))
            {
                This->Dn_ResolveFriendsListByType(LocalUserNum, ListName, Delegate, Results);
            }
        });
}

/** For all the friends, grouped by their EOS external account ID type, try to resolve their EOS product ID in batches
 */
bool FOnlineFriendsInterfaceSynthetic::Op_ResolveFriendsListByType(
    int32 LocalUserNum,
    const FUnresolvedFriendEntries &EntriesByType,
    const std::function<void(TResolvedFriendEntries)> &OnDone)
{
    if (EntriesByType.Key == (EOS_EExternalAccountType)-1)
    {
        // Return as-is.
        TArray<TResolvedFriendEntry> AutoFailResults;
        for (auto FriendEntry : EntriesByType.Value)
        {
            AutoFailResults.Add(TResolvedFriendEntry(nullptr, FriendEntry));
        }
        OnDone(TResolvedFriendEntries(EntriesByType.Key, AutoFailResults));
        return true;
    }

    auto EOSUser = StaticCastSharedPtr<const FUniqueNetIdEOS>(this->Identity->GetUniquePlayerId(LocalUserNum));
    if (!EOSUser.IsValid() || !EOSUser->HasValidProductUserId())
    {
        // Can't do this when the user isn't logged in or isn't valid.
        TArray<TResolvedFriendEntry> AutoFailResults;
        for (auto FriendEntry : EntriesByType.Value)
        {
            AutoFailResults.Add(TResolvedFriendEntry(nullptr, FriendEntry));
        }
        OnDone(TResolvedFriendEntries(EntriesByType.Key, AutoFailResults));
        return true;
    }

    FMultiOperation<FUnresolvedFriendEntry, TResolvedFriendEntry>::RunBatched(
        EntriesByType.Value,
        EOS_CONNECT_QUERYEXTERNALACCOUNTMAPPINGS_MAX_ACCOUNT_IDS,
        [WeakThis = GetWeakThis(this), EntriesByType, EOSUser](
            TArray<FUnresolvedFriendEntry> &Batch,
            const std::function<void(TArray<TResolvedFriendEntry> OutBatch)> &OnDone) -> bool {
            if (auto This = PinWeakThis(WeakThis))
            {
                EOS_Connect_QueryExternalAccountMappingsOptions Opts = {};
                Opts.ApiVersion = EOS_CONNECT_QUERYEXTERNALACCOUNTMAPPINGS_API_LATEST;
                Opts.AccountIdType = EntriesByType.Key;
                Opts.LocalUserId = EOSUser->GetProductUserId();
                EOSString_Connect_ExternalAccountId::AllocateToCharListViaAccessor<FUnresolvedFriendEntry>(
                    Batch,
                    [](const FUnresolvedFriendEntry &Entry) {
                        return Entry->ExternalInfo.AccountId;
                    },
                    Opts.ExternalAccountIdCount,
                    Opts.ExternalAccountIds);

                EOSRunOperation<
                    EOS_HConnect,
                    EOS_Connect_QueryExternalAccountMappingsOptions,
                    EOS_Connect_QueryExternalAccountMappingsCallbackInfo>(
                    This->EOSConnect,
                    &Opts,
                    EOS_Connect_QueryExternalAccountMappings,
                    [WeakThis = GetWeakThis(This), Opts, Batch, OnDone, EOSUser, EntriesByType](
                        const EOS_Connect_QueryExternalAccountMappingsCallbackInfo *Info) {
                        if (auto This = PinWeakThis(WeakThis))
                        {
                            TArray<TResolvedFriendEntry> Results;

                            auto FreeMem = [Opts]() {
                                EOSString_Connect_ExternalAccountId::FreeFromCharListConst(
                                    Opts.ExternalAccountIdCount,
                                    Opts.ExternalAccountIds);
                            };

                            if (Info->ResultCode != EOS_EResult::EOS_Success)
                            {
                                // Not successful, return this batch with no EOS product user IDs.
                                for (auto i = 0; i < Batch.Num(); i++)
                                {
                                    Results.Add(TResolvedFriendEntry(nullptr, Batch[i]));
                                }
                                FreeMem();
                                OnDone(Results);
                                return;
                            }

                            // Otherwise copy product IDs out.
                            for (auto i = 0; i < Batch.Num(); i++)
                            {
                                EOS_Connect_GetExternalAccountMappingsOptions GetOpts = {};
                                GetOpts.ApiVersion = EOS_CONNECT_GETEXTERNALACCOUNTMAPPINGS_API_LATEST;
                                GetOpts.LocalUserId = EOSUser->GetProductUserId();
                                GetOpts.AccountIdType = EntriesByType.Key;
                                GetOpts.TargetExternalUserId = Opts.ExternalAccountIds[i];
                                auto ProductUserId = EOS_Connect_GetExternalAccountMapping(This->EOSConnect, &GetOpts);
                                if (EOSString_ProductUserId::IsValid(ProductUserId))
                                {
                                    Results.Add(TResolvedFriendEntry(ProductUserId, Batch[i]));
                                }
                                else
                                {
                                    Results.Add(TResolvedFriendEntry(nullptr, Batch[i]));
                                }
                            }

                            FreeMem();
                            OnDone(Results);
                            return;
                        }
                    });
                return true;
            }
            else
            {
                return false;
            }
        },
        [OnDone, EntriesByType](TArray<TResolvedFriendEntry> OutResults) {
            OnDone(TResolvedFriendEntries(EntriesByType.Key, OutResults));
        });
    return true;
}

/* We have all the resolved results, now merge friends based on their EOS product user ID if present */
void FOnlineFriendsInterfaceSynthetic::Dn_ResolveFriendsListByType(
    int32 LocalUserNum,
    FString ListName,
    const FOnReadFriendsListComplete &Delegate,
    const TArray<TResolvedFriendEntries> &Results)
{
    // Flatten nested arrays into a single array of friend refs.
    TArray<TResolvedFriendEntry> ResolvedFriendRefs;
    for (const auto &KV : Results)
    {
        for (const auto &ResolvedFriendRef : KV.Value)
        {
            ResolvedFriendRefs.Add(ResolvedFriendRef);
        }
    }

    // Group by the EOS product user IDs.
    auto FriendRefsByExternalId =
        GroupBy<EOS_ProductUserId, TResolvedFriendEntry>(ResolvedFriendRefs, [](const TResolvedFriendEntry &Ref) {
            return Ref.Key;
        });

    // For friends with EOS account IDs, we now need to call FOnlineFriendEOS::NewAndWaitUntilReady to get
    // the EOS account, and then we'll use the EOS account as the first wrapped friend to the synthetic entry.
    TArray<TSharedRef<const FUniqueNetIdEOS>> UniqueIdsToResolve;
    for (const auto &KV : FriendRefsByExternalId)
    {
        if (EOSString_ProductUserId::IsNone(KV.Key))
        {
            // These are non-EOS friends.
            continue;
        }

        UniqueIdsToResolve.Add(MakeShared<FUniqueNetIdEOS>(KV.Key));
    }

    // Determine the querying user.
    auto QueryingUserIdEOS =
        StaticCastSharedPtr<const FUniqueNetIdEOS>(this->Identity->GetUniquePlayerId(LocalUserNum));
    if (!QueryingUserIdEOS.IsValid() || !QueryingUserIdEOS->HasValidProductUserId())
    {
        // Can't do this when the user isn't logged in or isn't valid.
        this->Dn_ResolveEOSAccount(
            LocalUserNum,
            ListName,
            Delegate,
            FriendRefsByExternalId,
            TArray<TSharedPtr<FOnlineUserEOS>>());
    }
    else
    {
        // Resolve EOS accounts.
        FOnlineUserEOS::Get(
            this->UserFactory.ToSharedRef(),
            QueryingUserIdEOS.ToSharedRef(),
            UniqueIdsToResolve,
            [WeakThis = GetWeakThis(this), LocalUserNum, ListName, Delegate, FriendRefsByExternalId](
                const TUserIdMap<TSharedPtr<FOnlineUserEOS>> &ResolvedEOSAccountsMap) {
                if (auto This = PinWeakThis(WeakThis))
                {
                    TArray<TSharedPtr<FOnlineUserEOS>> Array;
                    for (const auto &KV : ResolvedEOSAccountsMap)
                    {
                        Array.Add(KV.Value);
                    }
                    This->Dn_ResolveEOSAccount(LocalUserNum, ListName, Delegate, FriendRefsByExternalId, Array);
                }
            });
    }
}

/* We have now resolved all of the EOS product user IDs into FOnlineFriends to be used as the first entry for each
 * synthetic entry */
void FOnlineFriendsInterfaceSynthetic::Dn_ResolveEOSAccount(
    int32 LocalUserNum,
    const FString &ListName,
    const FOnReadFriendsListComplete &Delegate,
    TMap<EOS_ProductUserId, TArray<TResolvedFriendEntry>> FriendRefsByExternalId,
    const TArray<TSharedPtr<FOnlineUserEOS>> &ResolvedEOSAccounts)
{
    // Make a dictionary of FUniqueNetId -> ResolvedEOSAccount so that
    // we can quickly look it up.
    TUserIdMap<TSharedPtr<FOnlineUserEOS>> ResolvedEOSAccountsById;
    for (const auto &Account : ResolvedEOSAccounts)
    {
        if (Account.IsValid())
        {
            ResolvedEOSAccountsById.Add(*Account->GetUserId(), Account);
        }
    }

    // Populate the friend cache.
    this->FriendCacheById.Empty();
    this->FriendCacheByIndex.Empty();
    int32_t FriendsUnifiedCount = 0;
    int32_t FriendsNotUnifiedCount = 0;
    for (const auto &KV : FriendRefsByExternalId)
    {
        if (EOSString_ProductUserId::IsNone(KV.Key))
        {
            // These are non-EOS friends.
            continue;
        }

        TSharedRef<const FUniqueNetIdEOS> UserIdEOS = MakeShared<FUniqueNetIdEOS>(KV.Key);
        TMap<FName, TSharedPtr<FOnlineFriend>> FriendEntries;
        TSharedPtr<FOnlineUserEOS> UserEOS = nullptr;
        if (ResolvedEOSAccountsById.Contains(*UserIdEOS) && ResolvedEOSAccountsById[*UserIdEOS].IsValid())
        {
            UserEOS = ResolvedEOSAccountsById[*UserIdEOS];
        }
        for (const auto &EOSFriendRef : KV.Value)
        {
            FriendEntries.Add(EOSFriendRef.Value->SubsystemName, EOSFriendRef.Value->FriendRef);
        }
        auto SyntheticFriend = MakeShared<FOnlineFriendSynthetic>(UserEOS, FriendEntries);
        this->FriendCacheByIndex.Add(SyntheticFriend);
        this->FriendCacheById.Add(*SyntheticFriend->GetUserId(), SyntheticFriend);
        FriendsUnifiedCount++;
    }
    if (FriendRefsByExternalId.Contains(nullptr))
    {
        for (const auto &NonEOSFriend : FriendRefsByExternalId[nullptr])
        {
            TMap<FName, TSharedPtr<FOnlineFriend>> SingleFriendEntry;
            SingleFriendEntry.Add(NonEOSFriend.Value->SubsystemName, NonEOSFriend.Value->FriendRef);
            auto SyntheticFriend = MakeShared<FOnlineFriendSynthetic>(nullptr, SingleFriendEntry);
            this->FriendCacheByIndex.Add(SyntheticFriend);
            this->FriendCacheById.Add(*NonEOSFriend.Value->FriendRef->GetUserId(), SyntheticFriend);
            FriendsNotUnifiedCount++;
        }
    }

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("ReadFriendsList complete, %d unified friends, %d non-unified friends"),
        FriendsUnifiedCount,
        FriendsNotUnifiedCount);

    // We are done.
    Delegate.ExecuteIfBound(LocalUserNum, true, ListName, FString(TEXT("")));
    return;
}

FOnlineFriendsInterfaceSynthetic::FOnlineFriendsInterfaceSynthetic(
    FName InInstanceName,
    EOS_HPlatform InPlatform,
    IOnlineIdentityPtr InIdentity,
    const IOnlineSubsystemPtr &InSubsystemEAS,
    const TSharedRef<class IEOSRuntimePlatform> &InRuntimePlatform,
    const TSharedRef<FEOSConfig> &InConfig,
    const TSharedRef<class FEOSUserFactory, ESPMode::ThreadSafe> &InUserFactory)
{
    this->EOSPlatform = InPlatform;
    this->RuntimePlatform = InRuntimePlatform;

    check(InPlatform != nullptr);
    this->EOSConnect = EOS_Platform_GetConnectInterface(InPlatform);
    check(this->EOSConnect != nullptr);

    this->Identity = MoveTemp(InIdentity);
    this->UserFactory = InUserFactory;

    if (InSubsystemEAS != nullptr)
    {
        FWrappedFriendsInterface WrappedInterface = {REDPOINT_EAS_SUBSYSTEM, InSubsystemEAS->GetFriendsInterface()};
        this->OnlineFriends.Add(WrappedInterface);
    }
    struct W
    {
        TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> Friends;
    };
    for (const auto &KV : FDelegatedSubsystems::GetDelegatedSubsystems<W>(
             InConfig,
             InInstanceName,
             [](FDelegatedSubsystems::ISubsystemContext &Ctx) -> TSharedPtr<W> {
                 auto Friends = Ctx.GetDelegatedInterface<IOnlineFriends>([](IOnlineSubsystem *OSS) {
                     return OSS->GetFriendsInterface();
                 });
                 if (Friends.IsValid())
                 {
                     return MakeShared<W>(W{Friends});
                 }
                 return nullptr;
             }))
    {
        this->OnlineFriends.Add({KV.Key, KV.Value->Friends});
    }

    for (const auto &FriendsWeak : this->OnlineFriends)
    {
        if (auto Friends = FriendsWeak.OnlineFriends.Pin())
        {
            for (auto i = 0; i < MAX_LOCAL_PLAYERS; i++)
            {
                this->OnFriendsChangeDelegates.Add(TTuple<
                                                   TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe>,
                                                   int,
                                                   FDelegateHandle>(
                    Friends,
                    i,
                    Friends->AddOnFriendsChangeDelegate_Handle(
                        i,
                        FOnFriendsChangeDelegate::CreateLambda([this, i]() {
                            UE_LOG(
                                LogEOS,
                                Verbose,
                                TEXT("FOnlineFriendsInterfaceSynthetic: Forwarding OnFriendsChange event for user %d"),
                                i);
                            this->TriggerOnFriendsChangeDelegates(i);
                        }))));
            }
        }
    }
}

FOnlineFriendsInterfaceSynthetic::~FOnlineFriendsInterfaceSynthetic()
{
    for (auto V : this->OnFriendsChangeDelegates)
    {
        if (auto Friends = V.Get<0>().Pin())
        {
            Friends->ClearOnFriendsChangeDelegate_Handle(V.Get<1>(), V.Get<2>());
        }
    }
    this->OnFriendsChangeDelegates.Empty();
}

TSharedPtr<const FUniqueNetIdEOS> FOnlineFriendsInterfaceSynthetic::TryResolveNativeIdToEOS(
    const FUniqueNetId &UserId) const
{
    // @todo: This could be optimized by generating a mapping when the friends list is populated, but it's only used by
    // presence events at the moment, which aren't expected to be too frequent.
    for (const auto &Friend : this->FriendCacheById)
    {
        if (*Friend.Key == UserId && !Friend.Key->GetType().IsEqual(REDPOINT_EOS_SUBSYSTEM))
        {
            // This is a non-unified friend (because it is keyed on the native ID). Return nullptr.
            return nullptr;
        }

        if (Friend.Key->GetType().IsEqual(REDPOINT_EOS_SUBSYSTEM))
        {
            for (const auto &WrappedFriend : Friend.Value->GetWrappedFriends())
            {
                if (*WrappedFriend.Value->GetUserId() == UserId)
                {
                    return StaticCastSharedRef<const FUniqueNetIdEOS>(Friend.Key);
                }
            }
        }
    }
    return nullptr;
}

bool FOnlineFriendsInterfaceSynthetic::ReadFriendsList(
    int32 LocalUserNum,
    const FString &ListName,
    const FOnReadFriendsListComplete &Delegate)
{
    if (this->OnlineFriends.Num() == 0)
    {
        Delegate.ExecuteIfBound(LocalUserNum, true, ListName, FString(TEXT("")));
        return true;
    }

    FMultiOperation<FWrappedFriendsInterface, bool>::Run(
        this->OnlineFriends,
        [WeakThis = GetWeakThis(this),
         LocalUserNum,
         ListName](const FWrappedFriendsInterface &InFriends, std::function<void(bool OutValue)> OnDone) {
            if (auto This = PinWeakThis(WeakThis))
            {
                return This->Op_ReadFriendsList(
                    LocalUserNum,
                    This->MutateListName(InFriends, ListName),
                    InFriends.SubsystemName,
                    InFriends.OnlineFriends,
                    MoveTemp(OnDone));
            }
            return false;
        },
        [WeakThis = GetWeakThis(this), LocalUserNum, ListName, Delegate](TArray<bool> OutValues) {
            if (auto This = PinWeakThis(WeakThis))
            {
                This->Dn_ReadFriendsList(LocalUserNum, ListName, Delegate, MoveTemp(OutValues));
            }
        });
    return true;
}

bool FOnlineFriendsInterfaceSynthetic::DeleteFriendsList(
    int32 LocalUserNum,
    const FString &ListName,
    const FOnDeleteFriendsListComplete &Delegate)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::SendInvite(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName,
    const FOnSendInviteComplete &Delegate)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::AcceptInvite(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName,
    const FOnAcceptInviteComplete &Delegate)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::RejectInvite(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName)
{
    // Not supported.
    return false;
}

void FOnlineFriendsInterfaceSynthetic::SetFriendAlias(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName,
    const FString &Alias,
    const FOnSetFriendAliasComplete &Delegate)
{
    // Not supported.
    Delegate.ExecuteIfBound(LocalUserNum, FriendId, ListName, OnlineRedpointEOS::Errors::NotImplemented());
}

#if defined(HAS_FRIENDS_DELETE_FRIEND_ALIAS)
void FOnlineFriendsInterfaceSynthetic::DeleteFriendAlias(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName,
    const FOnDeleteFriendAliasComplete &Delegate)
{
    // Not supported.
    Delegate.ExecuteIfBound(LocalUserNum, FriendId, ListName, OnlineRedpointEOS::Errors::NotImplemented());
}
#endif

bool FOnlineFriendsInterfaceSynthetic::DeleteFriend(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::GetFriendsList(
    int32 LocalUserNum,
    const FString &ListName,
    TArray<TSharedRef<FOnlineFriend>> &OutFriends)
{
    if (this->OnlineFriends.Num() == 0)
    {
        OutFriends = TArray<TSharedRef<FOnlineFriend>>();
        return true;
    }

    OutFriends = this->FriendCacheByIndex;
    return true;
}

TSharedPtr<FOnlineFriend> FOnlineFriendsInterfaceSynthetic::GetFriend(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName)
{
    if (this->FriendCacheById.Contains(FriendId))
    {
        return this->FriendCacheById[FriendId];
    }

    // Friend not found.
    return nullptr;
}

bool FOnlineFriendsInterfaceSynthetic::IsFriend(
    int32 LocalUserNum,
    const FUniqueNetId &FriendId,
    const FString &ListName)
{
    return this->FriendCacheById.Contains(FriendId);
}

bool FOnlineFriendsInterfaceSynthetic::QueryRecentPlayers(const FUniqueNetId &UserId, const FString &Namespace)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::GetRecentPlayers(
    const FUniqueNetId &UserId,
    const FString &Namespace,
    TArray<TSharedRef<FOnlineRecentPlayer>> &OutRecentPlayers)
{
    // Not supported.
    return false;
}

void FOnlineFriendsInterfaceSynthetic::DumpRecentPlayers() const
{
}

bool FOnlineFriendsInterfaceSynthetic::BlockPlayer(int32 LocalUserNum, const FUniqueNetId &PlayerId)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::UnblockPlayer(int32 LocalUserNum, const FUniqueNetId &PlayerId)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::QueryBlockedPlayers(const FUniqueNetId &UserId)
{
    // Not supported.
    return false;
}

bool FOnlineFriendsInterfaceSynthetic::GetBlockedPlayers(
    const FUniqueNetId &UserId,
    TArray<TSharedRef<FOnlineBlockedPlayer>> &OutBlockedPlayers)
{
    // Not supported.
    return false;
}

void FOnlineFriendsInterfaceSynthetic::DumpBlockedPlayers() const
{
}

EOS_DISABLE_STRICT_WARNINGS