// Copyright June Rhodes. All Rights Reserved.

#include "EOSSocketRoleClient.h"
#include "SocketEOSFull.h"
#include "SocketSubsystemEOSFull.h"
#include "SocketSubsystemEOSListenManager.h"

EOS_ENABLE_STRICT_WARNINGS

bool FEOSSocketRoleClient::Close(FSocketEOSFull &Socket) const
{
    // Sockets are automatically removed from subsystem routing when Close() is called on FSocketEOSFull.
    return true;
}

bool FEOSSocketRoleClient::Listen(FSocketEOSFull &Socket, int32 MaxBacklog) const
{
    // Listen can not be called on client sockets.
    check(false);
    return false;
}

bool FEOSSocketRoleClient::Accept(
    class FSocketEOSFull &Socket,
    TSharedRef<FSocketEOSFull> InRemoteSocket,
    const FInternetAddrEOSFull &InRemoteAddr) const
{
    // Accept can not be called on client sockets.
    check(false);
    return false;
}

bool FEOSSocketRoleClient::Connect(FSocketEOSFull &Socket, const FInternetAddrEOSFull &InDestAddr) const
{
    auto State = this->GetState<FEOSSocketRoleClientState>(Socket);
    State->RemoteAddr = MakeShared<FInternetAddrEOSFull>();
    State->RemoteAddr->SetRawIp(InDestAddr.GetRawIp());
    State->bDidSendChannelReset = false;
    State->SocketResetId = Socket.SocketSubsystem->GetResetIdForOutboundConnection();
    check(State->RemoteAddr->GetSymmetricChannel() != 0);

    Socket.UpdateSocketKey(FSocketEOSKey(
        Socket.BindAddress->GetUserId(),
        State->RemoteAddr->GetUserId(),
        State->RemoteAddr->GetSymmetricSocketId(),
        State->RemoteAddr->GetSymmetricChannel()));

    UE_LOG(
        LogEOSSocket,
        Verbose,
        TEXT("%s: Client socket opened with reset ID %u."),
        *Socket.SocketKey->ToString(false),
        State->SocketResetId);

    return true;
}

bool FEOSSocketRoleClient::HasPendingData(FSocketEOSFull &Socket, uint32 &PendingDataSize) const
{
    if (!Socket.ReceivedPacketsQueue.IsEmpty())
    {
        PendingDataSize = Socket.ReceivedPacketsQueue.Peek()->Get()->GetDataLen();
        return true;
    }
    return false;
}

bool FEOSSocketRoleClient::SendTo(
    FSocketEOSFull &Socket,
    const uint8 *Data,
    int32 Count,
    int32 &BytesSent,
    const FInternetAddrEOSFull &Destination) const
{
    auto State = this->GetState<FEOSSocketRoleClientState>(Socket);
    check(State->RemoteAddr.IsValid());

    // Ensure that the address we are sending to is our remote address.
    check(Destination == *State->RemoteAddr);

    // Send the channel reset if it hasn't already been done.
    if (!State->bDidSendChannelReset)
    {
        EOS_P2P_SocketId SocketId = State->RemoteAddr->GetSymmetricSocketId();

        check(State->SocketResetId != 0);

        uint8 ChannelToResetAndId[5] = {
            State->RemoteAddr->GetSymmetricChannel(),
            ((uint8 *)(&State->SocketResetId))[0],
            ((uint8 *)(&State->SocketResetId))[1],
            ((uint8 *)(&State->SocketResetId))[2],
            ((uint8 *)(&State->SocketResetId))[3],
        };

        EOS_P2P_SendPacketOptions Opts = {};
        Opts.ApiVersion = EOS_P2P_SENDPACKET_API_LATEST;
        Opts.LocalUserId = Socket.BindAddress->GetUserId();
        Opts.RemoteUserId = State->RemoteAddr->GetUserId();
        Opts.SocketId = &SocketId;
        Opts.Channel = EOS_CHANNEL_ID_CONTROL;
        Opts.DataLengthBytes = 5;
        Opts.Data = ChannelToResetAndId;
        Opts.bAllowDelayedDelivery = true;
        Opts.Reliability = EOS_EPacketReliability::EOS_PR_ReliableOrdered;

        EOS_EResult SendResult = EOS_P2P_SendPacket(Socket.EOSP2P, &Opts);

        if (SendResult == EOS_EResult::EOS_Success)
        {
            State->bDidSendChannelReset = true;
        }
        else
        {
            UE_LOG(LogEOSSocket, Warning, TEXT("Unable to send channel reset message."));
            BytesSent = 0;
            return false;
        }
    }

    // Send the actual packet.
    {
        EOS_P2P_SocketId SocketId = State->RemoteAddr->GetSymmetricSocketId();

        EOS_P2P_SendPacketOptions Opts = {};
        Opts.ApiVersion = EOS_P2P_SENDPACKET_API_LATEST;
        Opts.LocalUserId = Socket.BindAddress->GetUserId();
        Opts.RemoteUserId = State->RemoteAddr->GetUserId();
        Opts.SocketId = &SocketId;
        Opts.Channel = State->RemoteAddr->GetSymmetricChannel();
        Opts.DataLengthBytes = Count;
        Opts.Data = Data;
        Opts.bAllowDelayedDelivery = false;
        Opts.Reliability = EOS_EPacketReliability::EOS_PR_UnreliableUnordered;

        EOS_EResult SendResult = EOS_P2P_SendPacket(Socket.EOSP2P, &Opts);

        if (SendResult == EOS_EResult::EOS_Success)
        {
            BytesSent = Count;
            return true;
        }

        BytesSent = 0;
        return false;
    }
}

bool FEOSSocketRoleClient::RecvFrom(
    FSocketEOSFull &Socket,
    uint8 *Data,
    int32 BufferSize,
    int32 &BytesRead,
    FInternetAddr &Source,
    ESocketReceiveFlags::Type Flags) const
{
    TSharedPtr<FEOSRawPacket> ReceivedPacket;
    if (Socket.ReceivedPacketsQueue.Dequeue(ReceivedPacket))
    {
        check(ReceivedPacket.IsValid());
        int32 SizeToRead = FMath::Min(BufferSize, ReceivedPacket->GetDataLen());
        FMemory::Memcpy(Data, ReceivedPacket->GetData(), SizeToRead);
        BytesRead = SizeToRead;
        Source.SetRawIp(this->GetState<FEOSSocketRoleClientState>(Socket)->RemoteAddr->GetRawIp());
        return true;
    }

    return false;
}

bool FEOSSocketRoleClient::GetPeerAddress(class FSocketEOSFull &Socket, FInternetAddrEOSFull &OutAddr) const
{
    OutAddr.SetRawIp(this->GetState<FEOSSocketRoleClientState>(Socket)->RemoteAddr->GetRawIp());
    return true;
}

EOS_DISABLE_STRICT_WARNINGS