// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

class FSocketSubsystemEOSLegacy;

#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketSubsystemEOS.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/Legacy/SocketEOSLegacy.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSDefines.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineSubsystemRedpointEOS.h"

EOS_ENABLE_STRICT_WARNINGS

class FSocketSubsystemEOSLegacy : public ISocketSubsystemEOS, public TSharedFromThis<FSocketSubsystemEOSLegacy>
{
    friend class FSocketEOSLegacy;

private:
    EOS_HP2P InP2P;
    TMap<int, TSharedPtr<FSocketEOSLegacy>> HeldSockets;
    int NextSocketId;
    TSharedPtr<FOnlineSubsystemEOS, ESPMode::ThreadSafe> Subsystem;

public:
    FSocketSubsystemEOSLegacy(
        const TSharedRef<FOnlineSubsystemEOS, ESPMode::ThreadSafe> &InSubsystem,
        EOS_HPlatform InPlatform);
    UE_NONCOPYABLE(FSocketSubsystemEOSLegacy);
    ~FSocketSubsystemEOSLegacy();

    /**
     * Returns the product user ID to use when binding P2P addresses.
     */
    EOS_ProductUserId GetBindingProductUserId_P2POnly() const;

    /**
     * Returns the product user ID for either P2P or dedicated server connections. The product user ID
     * is potentially nullptr (for dedicated servers), so the boolean result is whether the result
     * is valid.
     */
    bool GetBindingProductUserId_P2POrDedicatedServer(EOS_ProductUserId &OutPUID) const;

    virtual FString GetSocketName(bool bListening, FURL InURL) const override;

    virtual bool Init(FString &Error) override;
    virtual void Shutdown() override;

    virtual FSocket *CreateSocket(const FName &SocketType, const FString &SocketDescription, const FName &ProtocolName)
        override;
    virtual void DestroySocket(FSocket *Socket) override;

    virtual FAddressInfoResult GetAddressInfo(
        const TCHAR *HostName,
        const TCHAR *ServiceName = nullptr,
        EAddressInfoFlags QueryFlags = EAddressInfoFlags::Default,
        const FName ProtocolTypeName = NAME_None,
        ESocketType SocketType = ESocketType::SOCKTYPE_Unknown) override;
    virtual TSharedPtr<FInternetAddr> GetAddressFromString(const FString &InAddress) override;

    virtual bool RequiresChatDataBeSeparate() override;
    virtual bool RequiresEncryptedPackets() override;
    virtual bool GetHostName(FString &HostName) override;

    virtual TSharedRef<FInternetAddr> CreateInternetAddr() override;
    virtual bool HasNetworkDevice() override;

    virtual const TCHAR *GetSocketAPIName() const override;
    virtual ESocketErrors GetLastErrorCode() override;
    virtual ESocketErrors TranslateErrorCode(int32 Code) override;

    virtual bool IsSocketWaitSupported() const override;

#if defined(NEEDS_SOCKET_SUBSYSTEM_GET_LOCAL_ADAPTER_ADDRESSES)
    bool GetLocalAdapterAddresses(TArray<TSharedPtr<FInternetAddr>> &OutAddresses) override
    {
        return false;
    }
#endif
};

EOS_DISABLE_STRICT_WARNINGS