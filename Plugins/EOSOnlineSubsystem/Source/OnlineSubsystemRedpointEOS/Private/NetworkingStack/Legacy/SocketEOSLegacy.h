// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/ISocketEOS.h"
#include "OnlineSubsystemRedpointEOS/Private/NetworkingStack/Legacy/InternetAddrEOSLegacy.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "Sockets.h"

EOS_ENABLE_STRICT_WARNINGS

enum class ESocketEOSRole : uint8
{
    /* This socket doesn't have a role yet, and needs Bind called on it. */
    Role_None,

    /* This socket has been bound to a local address. */
    Role_Bound,

    /* This socket represents a remote connection. */
    Role_Remote,
};

class FSocketEOSLegacy : public ISocketEOS, public TSharedFromThis<FSocketEOSLegacy>
{
    friend class FSocketSubsystemEOSLegacy;

private:
    ESocketEOSRole Role;
    EOS_HP2P EOSP2P;
    TSharedPtr<FInternetAddrEOSLegacy> LocalAddr;
    TSharedPtr<EOSEventHandle<EOS_P2P_OnIncomingConnectionRequestInfo>> AcceptEventHandle;
    TSharedPtr<EOSEventHandle<EOS_P2P_OnRemoteConnectionClosedInfo>> CloseEventHandle;
    int SocketId;

    // When connections are accepted, they are accepted without the context of a socket ID. That is,
    // accepting a connection from a remote player allows inbound traffic on all socket IDs. Since we
    // use the socket ID as part of the address for network connections, we need to have an FSocketEOSLegacy
    // per <remote user>:<socket ID> combination. Only the FSocketEOSLegacy itself is aware of when data is
    // read from a new connection (in RecvFrom), so it fires FSocketEOSOnConnectionAccepted when
    // we actually start receiving data.
    //
    // This is a map from Remote User ID -> Socket Name -> Socket.
    TMap<EOS_ProductUserId, TMap<FString, TWeakPtr<FSocketEOSLegacy>>> RemoteSockets;

    // If this is a remote socket, this field contains the local socket that owns it. Remote sockets
    // delegate to their parent for their actual send operations.
    TWeakPtr<FSocketEOSLegacy> RemoteSocketParent;

    // Note: We don't use this field for anything, except to ensure that the socket subsystem
    // doesn't get destroyed before the socket is. This is important because the socket
    // registers events with EOS, and we need to ensure that the online subsystem doesn't
    // clean up the EOS_HPlatform handle while there are still events registered.
    TSharedPtr<class FSocketSubsystemEOSLegacy> SocketSubsystem;

public:
    FSocketEOSLegacy(
        const TSharedRef<class FSocketSubsystemEOSLegacy> &InSocketSubsystem,
        EOS_HP2P InP2P,
        int InSocketId,
        const FString &InSocketDescription)
        : ISocketEOS(ESocketType::SOCKTYPE_Datagram, InSocketDescription, REDPOINT_EOS_SUBSYSTEM),
          Role(ESocketEOSRole::Role_None), LocalAddr(nullptr), SocketId(InSocketId), SocketSubsystem(InSocketSubsystem)
    {
        check(InP2P != nullptr);
        this->EOSP2P = InP2P;
    }
    UE_NONCOPYABLE(FSocketEOSLegacy);
    ~FSocketEOSLegacy();

    virtual TSharedRef<ISocketEOS> AsSharedEOS() override;

    int GetSocketId() const
    {
        return this->SocketId;
    }

    virtual bool Shutdown(ESocketShutdownMode Mode) override;
    virtual bool Close() override;
    virtual bool Bind(const FInternetAddr &Addr) override;
    virtual bool Connect(const FInternetAddr &Addr) override;
    virtual bool Listen(int32 MaxBacklog) override;
    virtual bool WaitForPendingConnection(bool &bHasPendingConnection, const FTimespan &WaitTime) override;
    virtual bool HasPendingData(uint32 &PendingDataSize) override;
    virtual class FSocket *Accept(const FString &InSocketDescription) override;
    virtual class FSocket *Accept(FInternetAddr &OutAddr, const FString &InSocketDescription) override;
    virtual bool SendTo(const uint8 *Data, int32 Count, int32 &BytesSent, const FInternetAddr &Destination) override;
    virtual bool Send(const uint8 *Data, int32 Count, int32 &BytesSent) override;
    virtual bool RecvFrom(
        uint8 *Data,
        int32 BufferSize,
        int32 &BytesRead,
        FInternetAddr &Source,
        ESocketReceiveFlags::Type Flags = ESocketReceiveFlags::None) override;
    virtual bool Recv(
        uint8 *Data,
        int32 BufferSize,
        int32 &BytesRead,
        ESocketReceiveFlags::Type Flags = ESocketReceiveFlags::None) override;
    virtual bool RecvMulti(FRecvMulti &MultiData, ESocketReceiveFlags::Type Flags = ESocketReceiveFlags::None) override;
    virtual bool Wait(ESocketWaitConditions::Type Condition, FTimespan WaitTime) override;
    virtual ESocketConnectionState GetConnectionState() override;
    virtual void GetAddress(FInternetAddr &OutAddr) override;
    virtual bool GetPeerAddress(FInternetAddr &OutAddr) override;
    virtual bool SetNonBlocking(bool bIsNonBlocking = true) override;
    virtual bool SetBroadcast(bool bAllowBroadcast = true) override;
    virtual bool SetNoDelay(bool bIsNoDelay = true) override;
    virtual bool JoinMulticastGroup(const FInternetAddr &GroupAddress) override;
    virtual bool JoinMulticastGroup(const FInternetAddr &GroupAddress, const FInternetAddr &InterfaceAddress) override;
    virtual bool LeaveMulticastGroup(const FInternetAddr &GroupAddress) override;
    virtual bool LeaveMulticastGroup(const FInternetAddr &GroupAddress, const FInternetAddr &InterfaceAddress) override;
    virtual bool SetMulticastLoopback(bool bLoopback) override;
    virtual bool SetMulticastTtl(uint8 TimeToLive) override;
    virtual bool SetMulticastInterface(const FInternetAddr &InterfaceAddress) override;
    virtual bool SetReuseAddr(bool bAllowReuse = true) override;
    virtual bool SetLinger(bool bShouldLinger = true, int32 Timeout = 0) override;
    virtual bool SetRecvErr(bool bUseErrorQueue = true) override;
    virtual bool SetSendBufferSize(int32 Size, int32 &NewSize) override;
    virtual bool SetReceiveBufferSize(int32 Size, int32 &NewSize) override;
    virtual int32 GetPortNo() override;
};

EOS_DISABLE_STRICT_WARNINGS