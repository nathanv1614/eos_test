// Copyright June Rhodes. All Rights Reserved.

#include "./SocketEOSLegacy.h"

#include "./SocketSubsystemEOSLegacy.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "OnlineSubsystemRedpointEOS/Shared/UniqueNetIdEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"

EOS_ENABLE_STRICT_WARNINGS

// @todo: Should we treat channels like ports and socket IDs as just part of the address?
#define EOS_CHANNEL_ID 0

// If you need to debug network traffic sent over EOS sockets, uncomment the line below.
// #define EOS_ENABLE_TRAFFIC_LOGGING 1

FSocketEOSLegacy::~FSocketEOSLegacy()
{
    check(this->RemoteSockets.Num() == 0);
}

TSharedRef<ISocketEOS> FSocketEOSLegacy::AsSharedEOS()
{
    return StaticCastSharedRef<ISocketEOS>(this->AsShared());
}

bool FSocketEOSLegacy::Shutdown(ESocketShutdownMode Mode)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Shutdown is not supported."));
    return false;
}

bool FSocketEOSLegacy::Close()
{
    return true;
}

bool FSocketEOSLegacy::Bind(const FInternetAddr &Addr)
{
    if (this->Role != ESocketEOSRole::Role_None)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::Bind was called on a socket that does not have the ESocketEOSRole::Role_None "
                 "role."));
        return false;
    }
    if (Addr.GetProtocolType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::Bind was not provided with a local address of the REDPOINT_EOS_SUBSYSTEM type."));
        return false;
    }

    const FInternetAddrEOSLegacy &SourceEOS = (const FInternetAddrEOSLegacy &)Addr;
    if (!SourceEOS.IsValid())
    {
        UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Bind was not provided with a valid local address to bind to."));
        return false;
    }

    this->Role = ESocketEOSRole::Role_Bound;
    this->LocalAddr = StaticCastSharedRef<FInternetAddrEOSLegacy>(SourceEOS.Clone());

    EOS_P2P_SocketId Socket = this->LocalAddr->GetSocketId();

    EOS_P2P_AddNotifyPeerConnectionRequestOptions AddOpts = {};
    AddOpts.ApiVersion = EOS_P2P_ADDNOTIFYPEERCONNECTIONREQUEST_API_LATEST;
    AddOpts.LocalUserId = this->LocalAddr->GetUserId();
    // This field is a filter for the remote socket ID, not the locally bound socket ID.
    // We set it to null to get notifications from everyone...
    AddOpts.SocketId = nullptr;

    FString BindingUserId;
    if (EOSString_ProductUserId::ToString(AddOpts.LocalUserId, BindingUserId) != EOS_EResult::EOS_Success)
    {
        UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Bind local user ID in address is not valid."));
        return false;
    }

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("FSocketEOSLegacy::Bind registering incoming connection event for local user '%s' on socket '%s'."),
        *BindingUserId,
        ANSI_TO_TCHAR(Socket.SocketName));

    this->AcceptEventHandle = EOSRegisterEvent<
        EOS_HP2P,
        EOS_P2P_AddNotifyPeerConnectionRequestOptions,
        EOS_P2P_OnIncomingConnectionRequestInfo>(
        this->EOSP2P,
        &AddOpts,
        EOS_P2P_AddNotifyPeerConnectionRequest,
        EOS_P2P_RemoveNotifyPeerConnectionRequest,
        [WeakThis = GetWeakThis(this)](const EOS_P2P_OnIncomingConnectionRequestInfo *Info) {
            if (auto This = PinWeakThis(WeakThis))
            {
                bool Accept = true;
                if (This->OnIncomingConnection.IsBound())
                {
                    TSharedRef<FUniqueNetIdEOS> LocalUser = MakeShared<FUniqueNetIdEOS>(Info->LocalUserId);
                    TSharedRef<FUniqueNetIdEOS> RemoteUser = MakeShared<FUniqueNetIdEOS>(Info->RemoteUserId);

                    Accept = This->OnIncomingConnection.Execute(This.ToSharedRef(), LocalUser, RemoteUser);
                }

                if (Accept)
                {
                    EOS_P2P_AcceptConnectionOptions AcceptOpts = {};
                    AcceptOpts.ApiVersion = EOS_P2P_ACCEPTCONNECTION_API_LATEST;
                    AcceptOpts.LocalUserId = Info->LocalUserId;
                    AcceptOpts.RemoteUserId = Info->RemoteUserId;
                    AcceptOpts.SocketId = Info->SocketId;

                    EOS_EResult AcceptResult = EOS_P2P_AcceptConnection(This->EOSP2P, &AcceptOpts);
                    if (AcceptResult != EOS_EResult::EOS_Success)
                    {
                        UE_LOG(
                            LogEOS,
                            Error,
                            TEXT("Failed to accept connection, got status code %s"),
                            ANSI_TO_TCHAR(EOS_EResult_ToString(AcceptResult)));
                    }
                }
                else
                {
                    EOS_P2P_CloseConnectionOptions RejectOpts = {};
                    RejectOpts.ApiVersion = EOS_P2P_CLOSECONNECTION_API_LATEST;
                    RejectOpts.LocalUserId = Info->LocalUserId;
                    RejectOpts.RemoteUserId = Info->RemoteUserId;
                    RejectOpts.SocketId = Info->SocketId;

                    EOS_EResult RejectResult = EOS_P2P_CloseConnection(This->EOSP2P, &RejectOpts);
                    if (RejectResult != EOS_EResult::EOS_Success)
                    {
                        UE_LOG(
                            LogEOS,
                            Error,
                            TEXT("Failed to reject connection, got status code %s"),
                            ANSI_TO_TCHAR(EOS_EResult_ToString(RejectResult)));
                    }
                }
            }
        });

    if (!this->AcceptEventHandle->IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::Bind failed to bind for local user '%s' on socket '%s'."),
            *BindingUserId,
            ANSI_TO_TCHAR(Socket.SocketName));
    }

    UE_LOG(
        LogEOS,
        Verbose,
        TEXT("FSocketEOSLegacy::Bind registering connection closed event for local user '%s' on socket '%s'."),
        *BindingUserId,
        ANSI_TO_TCHAR(Socket.SocketName));

    EOS_P2P_AddNotifyPeerConnectionClosedOptions CloseOpts = {};
    CloseOpts.ApiVersion = EOS_P2P_ADDNOTIFYPEERCONNECTIONCLOSED_API_LATEST;
    CloseOpts.LocalUserId = this->LocalAddr->GetUserId();
    // This field is a filter for the remote socket ID, not the locally bound socket ID.
    // We set it to null to get notifications from everyone...
    CloseOpts.SocketId = nullptr;

    this->CloseEventHandle =
        EOSRegisterEvent<EOS_HP2P, EOS_P2P_AddNotifyPeerConnectionClosedOptions, EOS_P2P_OnRemoteConnectionClosedInfo>(
            this->EOSP2P,
            &CloseOpts,
            EOS_P2P_AddNotifyPeerConnectionClosed,
            EOS_P2P_RemoveNotifyPeerConnectionClosed,
            [WeakThis = GetWeakThis(this)](const EOS_P2P_OnRemoteConnectionClosedInfo *Info) {
                if (auto This = PinWeakThis(WeakThis))
                {
                    TSharedRef<FUniqueNetIdEOS> LocalUser = MakeShared<FUniqueNetIdEOS>(Info->LocalUserId);
                    TSharedRef<FUniqueNetIdEOS> RemoteUser = MakeShared<FUniqueNetIdEOS>(Info->RemoteUserId);

                    // Originally this fired an event for OnConnectionClosed into UEOSNetDriver. In the legacy
                    // networking stack, this used to be a no-op, because UEOSNetDriver didn't do anything with the
                    // event. However, UEOSNetDriver now uses this callback to correctly handle connection closing for
                    // full networking, so to retain the existing behaviour for the legacy networking, the code to
                    // broadcast the event here has been removed.
                }
            });

    if (!this->CloseEventHandle->IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::Bind failed to bind for local user '%s' on socket '%s'."),
            *BindingUserId,
            ANSI_TO_TCHAR(Socket.SocketName));
    }

    // This socket is only valid if we can accept connections for it and detect connection closed.
    return this->AcceptEventHandle->IsValid() && this->CloseEventHandle->IsValid();
}

bool FSocketEOSLegacy::Connect(const FInternetAddr &Addr)
{
    // This is called because the Full networking stack requires it, but the legacy version just ignores it.
    return true;
}

bool FSocketEOSLegacy::Listen(int32 MaxBacklog)
{
    // This is called because the Full networking stack requires it, but the legacy version just ignores it.
    return true;
}

bool FSocketEOSLegacy::WaitForPendingConnection(bool &bHasPendingConnection, const FTimespan &WaitTime)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::WaitForPendingConnection not supported."));
    return false;
}

bool FSocketEOSLegacy::HasPendingData(uint32 &PendingDataSize)
{
    EOS_SCOPE_CYCLE_COUNTER(STAT_EOSSocketHasPendingData);

    EOS_P2P_GetNextReceivedPacketSizeOptions Opts = {};
    Opts.ApiVersion = EOS_P2P_GETNEXTRECEIVEDPACKETSIZE_API_LATEST;
    Opts.LocalUserId = this->LocalAddr->GetUserId();
    Opts.RequestedChannel = nullptr;

    return EOS_P2P_GetNextReceivedPacketSize(this->EOSP2P, &Opts, &PendingDataSize) == EOS_EResult::EOS_Success;
}

FSocket *FSocketEOSLegacy::Accept(const FString &InSocketDescription)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Accept is not supported."));
    return nullptr;
}

FSocket *FSocketEOSLegacy::Accept(FInternetAddr &OutAddr, const FString &InSocketDescription)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Accept is not supported."));
    return nullptr;
}

#if defined(EOS_ENABLE_TRAFFIC_LOGGING) && EOS_ENABLE_TRAFFIC_LOGGING
FString GetTrafficDataLog(const uint8 *Data, int32 Count)
{
    FString DataLog;
    for (int i = 0; i < Count; i++)
    {
        DataLog += FString::Printf(TEXT("%02x"), Data[i]);
    }
    return DataLog;
}
#endif

bool FSocketEOSLegacy::SendTo(const uint8 *Data, int32 Count, int32 &BytesSent, const FInternetAddr &Destination)
{
    EOS_SCOPE_CYCLE_COUNTER(STAT_EOSSocketSendTo);

    if (this->Role == ESocketEOSRole::Role_Remote)
    {
        // Delegate to parent socket if we can.
        if (TSharedPtr<FSocketEOSLegacy> ParentSocket = this->RemoteSocketParent.Pin())
        {
            return ParentSocket->SendTo(Data, Count, BytesSent, Destination);
        }
        else
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("FSocketEOSLegacy::SendTo called on a socket with the ESocketEOSRole::Role_Remote role, but it no "
                     "longer had a valid remote socket parent."));
            BytesSent = 0;
            return false;
        }
    }

    if (this->Role != ESocketEOSRole::Role_Bound || !this->LocalAddr.IsValid() || !this->LocalAddr->IsValid())
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::SendTo called on a socket that was not in the ESocketEOSRole::Role_Bound role or "
                 "didn't "
                 "have a valid local address."));
        BytesSent = 0;
        return false;
    }
    if (Destination.GetProtocolType() != REDPOINT_EOS_SUBSYSTEM)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FSocketEOSLegacy::SendTo called with a destination address that was not of the "
                 "REDPOINT_EOS_SUBSYSTEM type."));
        BytesSent = 0;
        return false;
    }

    const FInternetAddrEOSLegacy &DestinationEOS = (const FInternetAddrEOSLegacy &)Destination;

    EOS_P2P_SocketId Socket = this->LocalAddr->GetSocketId();

    EOS_P2P_SendPacketOptions Opts = {};
    Opts.ApiVersion = EOS_P2P_SENDPACKET_API_LATEST;
    Opts.LocalUserId = this->LocalAddr->GetUserId();
    Opts.RemoteUserId = DestinationEOS.GetUserId();
    Opts.SocketId = &Socket;
    Opts.Channel = EOS_CHANNEL_ID;
    Opts.DataLengthBytes = Count;
    Opts.Data = Data;
    Opts.bAllowDelayedDelivery = false;
    Opts.Reliability = EOS_EPacketReliability::EOS_PR_UnreliableUnordered;

    EOS_EResult SendResult = EOS_P2P_SendPacket(this->EOSP2P, &Opts);
    if (SendResult == EOS_EResult::EOS_Success)
    {
#if defined(EOS_ENABLE_TRAFFIC_LOGGING) && EOS_ENABLE_TRAFFIC_LOGGING
        UE_LOG(
            LogEOS,
            Verbose,
            TEXT("%s -> %s: [send] [%4d bytes] %s"),
            *this->LocalAddr->ToString(true),
            *DestinationEOS.ToString(true),
            Opts.DataLengthBytes,
            *GetTrafficDataLog(Data, Opts.DataLengthBytes));
#endif

        BytesSent = Count;
        return true;
    }

    UE_LOG(
        LogEOS,
        Error,
        TEXT("FSocketEOSLegacy::SendTo got result code %s from the EOS P2P API (socket name was '%s')."),
        ANSI_TO_TCHAR(EOS_EResult_ToString(SendResult)),
        ANSI_TO_TCHAR(Opts.SocketId->SocketName));
    BytesSent = 0;
    return false;
}

bool FSocketEOSLegacy::Send(const uint8 *Data, int32 Count, int32 &BytesSent)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Send is not supported."));
    return false;
}

bool FSocketEOSLegacy::RecvFrom(
    uint8 *Data,
    int32 BufferSize,
    int32 &BytesRead,
    FInternetAddr &Source,
    ESocketReceiveFlags::Type Flags)
{
    EOS_SCOPE_CYCLE_COUNTER(STAT_EOSSocketRecvFrom);

    if (Flags != ESocketReceiveFlags::None)
    {
        UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::RecvFrom does not support calling with non-None flags."));
        return false;
    }

    EOS_P2P_ReceivePacketOptions ReceiveOpts = {};
    ReceiveOpts.ApiVersion = EOS_P2P_RECEIVEPACKET_API_LATEST;
    ReceiveOpts.LocalUserId = this->LocalAddr->GetUserId();
    ReceiveOpts.MaxDataSizeBytes = BufferSize;
    uint8_t Channel = EOS_CHANNEL_ID;
    ReceiveOpts.RequestedChannel = &Channel;

    EOS_ProductUserId PeerId = {};
    EOS_P2P_SocketId PeerSocketId = {};
    PeerSocketId.ApiVersion = EOS_P2P_SOCKETID_API_LATEST;
    uint32_t BytesReadU;
    EOS_EResult ReceiveResult =
        EOS_P2P_ReceivePacket(this->EOSP2P, &ReceiveOpts, &PeerId, &PeerSocketId, &Channel, Data, &BytesReadU);
    BytesRead = BytesReadU;

    if (ReceiveResult == EOS_EResult::EOS_Success)
    {
        TSharedRef<FInternetAddrEOSLegacy> SourceAddr =
            MakeShared<FInternetAddrEOSLegacy>(PeerId, ANSI_TO_TCHAR(PeerSocketId.SocketName));
        Source.SetRawIp(SourceAddr->GetRawIp());

        // Fire the connection accepted event if this is the first time we are receiving
        // data for a given remote user / socket ID.
        if (!this->RemoteSockets.Contains(PeerId))
        {
            this->RemoteSockets.Add(PeerId, TMap<FString, TWeakPtr<FSocketEOSLegacy>>());
        }
        FString SocketNameStr = ANSI_TO_TCHAR(PeerSocketId.SocketName);
        if (!this->RemoteSockets[PeerId].Contains(SocketNameStr))
        {
            FSocket *NewSocket = this->SocketSubsystem->CreateSocket(
                REDPOINT_EOS_SUBSYSTEM,
                TEXT("Accepted Socket"),
                REDPOINT_EOS_SUBSYSTEM);
            TSharedRef<FSocketEOSLegacy> RemoteSocket = ((FSocketEOSLegacy *)NewSocket)->AsShared();
            RemoteSocket->LocalAddr = SourceAddr;
            RemoteSocket->Role = ESocketEOSRole::Role_Remote;
            RemoteSocket->RemoteSocketParent = this->AsShared();
            this->RemoteSockets[PeerId].Add(SocketNameStr, RemoteSocket);
            this->OnConnectionAccepted.ExecuteIfBound(
                this->AsShared(),
                RemoteSocket,
                MakeShared<FUniqueNetIdEOS>(this->LocalAddr->GetUserId()),
                MakeShared<FUniqueNetIdEOS>(PeerId));
        }

#if defined(EOS_ENABLE_TRAFFIC_LOGGING) && EOS_ENABLE_TRAFFIC_LOGGING
        UE_LOG(
            LogEOS,
            Verbose,
            TEXT("%s -> %s: [recv] [%4u bytes] %s"),
            *SourceAddr->ToString(true),
            *this->LocalAddr->ToString(true),
            BytesReadU,
            *GetTrafficDataLog(Data, BytesRead));
#endif
    }

    return ReceiveResult == EOS_EResult::EOS_Success;
}

bool FSocketEOSLegacy::Recv(uint8 *Data, int32 BufferSize, int32 &BytesRead, ESocketReceiveFlags::Type Flags)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Recv is not supported."));
    return false;
}

bool FSocketEOSLegacy::RecvMulti(FRecvMulti &MultiData, ESocketReceiveFlags::Type Flags)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::RecvMulti not supported."));
    return false;
}

bool FSocketEOSLegacy::Wait(ESocketWaitConditions::Type Condition, FTimespan WaitTime)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::Wait not supported."));
    return false;
}

ESocketConnectionState FSocketEOSLegacy::GetConnectionState()
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::GetConnectionState is not supported."));
    return ESocketConnectionState::SCS_NotConnected;
}

void FSocketEOSLegacy::GetAddress(FInternetAddr &OutAddr)
{
    if (this->LocalAddr.IsValid())
    {
        TSharedPtr<FInternetAddrEOSLegacy> TargetAddr = ((FInternetAddrEOSLegacy &)OutAddr).AsShared();
        TargetAddr->SetRawIp(this->LocalAddr->GetRawIp());
        return;
    }

    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::GetAddress called for socket without local address."));
}

bool FSocketEOSLegacy::GetPeerAddress(FInternetAddr &OutAddr)
{
    if (this->LocalAddr.IsValid())
    {
        TSharedPtr<FInternetAddrEOSLegacy> TargetAddr = ((FInternetAddrEOSLegacy &)OutAddr).AsShared();
        TargetAddr->SetRawIp(this->LocalAddr->GetRawIp());
        return true;
    }

    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::GetPeerAddress called for socket without local address."));
    return false;
}

bool FSocketEOSLegacy::SetNonBlocking(bool bIsNonBlocking)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetNonBlocking is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetBroadcast(bool bAllowBroadcast)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetBroadcast is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetNoDelay(bool bIsNoDelay)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetNoDelay is not supported."));
    return false;
}

bool FSocketEOSLegacy::JoinMulticastGroup(const FInternetAddr &GroupAddress)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::JoinMulticastGroup is not supported."));
    return false;
}

bool FSocketEOSLegacy::JoinMulticastGroup(const FInternetAddr &GroupAddress, const FInternetAddr &InterfaceAddress)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::JoinMulticastGroup is not supported."));
    return false;
}

bool FSocketEOSLegacy::LeaveMulticastGroup(const FInternetAddr &GroupAddress)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::LeaveMulticastGroup is not supported."));
    return false;
}

bool FSocketEOSLegacy::LeaveMulticastGroup(const FInternetAddr &GroupAddress, const FInternetAddr &InterfaceAddress)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::LeaveMulticastGroup is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetMulticastLoopback(bool bLoopback)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetMulticastLoopback is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetMulticastTtl(uint8 TimeToLive)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetMulticastTtl is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetMulticastInterface(const FInternetAddr &InterfaceAddress)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetMulticastInterface is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetReuseAddr(bool bAllowReuse)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetReuseAddr is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetLinger(bool bShouldLinger, int32 Timeout)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetLinger is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetRecvErr(bool bUseErrorQueue)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetRecvErr is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetSendBufferSize(int32 Size, int32 &NewSize)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetSendBufferSize is not supported."));
    return false;
}

bool FSocketEOSLegacy::SetReceiveBufferSize(int32 Size, int32 &NewSize)
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::SetReceiveBufferSize is not supported."));
    return false;
}

int32 FSocketEOSLegacy::GetPortNo()
{
    UE_LOG(LogEOS, Error, TEXT("FSocketEOSLegacy::GetPortNo is not supported."));
    return 0;
}

EOS_DISABLE_STRICT_WARNINGS