// Copyright June Rhodes. All Rights Reserved.

#include "./SocketSubsystemEOSLegacy.h"

#include "./InternetAddrEOSLegacy.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineSubsystemRedpointEOS.h"
#include "OnlineSubsystemRedpointEOS/Shared/UniqueNetIdEOS.h"

EOS_ENABLE_STRICT_WARNINGS

FSocketSubsystemEOSLegacy::FSocketSubsystemEOSLegacy(
    const TSharedRef<FOnlineSubsystemEOS, ESPMode::ThreadSafe> &InSubsystem,
    EOS_HPlatform InPlatform)
{
    this->Subsystem = InSubsystem;

    check(InPlatform != nullptr);
    this->InP2P = EOS_Platform_GetP2PInterface(InPlatform);
    check(this->InP2P != nullptr);
    this->NextSocketId = 1000;
}

FSocketSubsystemEOSLegacy::~FSocketSubsystemEOSLegacy()
{
}

FString GenerateRandomSocketName(uint32 AmountOfCharacters)
{
    FString Characters = "abcdefghijklmnopqrstuvwxyz1234567890";
    FString Result = "";
    for (uint32 i = 0; i < AmountOfCharacters; ++i)
    {
        Result += Characters[FMath::RandRange(0, Characters.Len() - 1)];
    }
    return Result;
}

FString FSocketSubsystemEOSLegacy::GetSocketName(bool bListening, FURL InURL) const
{
    // Generate the socket name.
    FString SocketName;
    if (bListening)
    {
        // Bind to the port because this is for a server, where the socket name needs
        // to be deterministic.
        SocketName = FString::Printf(TEXT("s%d"), InURL.Port);
    }
    else
    {
        // Generate a random socket name on the clients, because we just want to ensure
        // uniqueness.
        SocketName =
            FString::Printf(TEXT("c%s"), *GenerateRandomSocketName(EOS_P2P_SOCKET_NAME_MAX_LENGTH - 1 /* c */));
    }
    return SocketName;
}

bool FSocketSubsystemEOSLegacy::Init(FString &Error)
{
    Error = TEXT("");
    return true;
}

void FSocketSubsystemEOSLegacy::Shutdown()
{
    TArray<int> Keys;
    this->HeldSockets.GetKeys(Keys);
    for (int Key : Keys)
    {
        // Because DestroySocket also destroys children, and those children
        // will be removed from the HeldSockets array after we've captured
        // the keys..
        if (this->HeldSockets.Contains(Key))
        {
            FSocketEOSLegacy *SocketEOS = this->HeldSockets[Key].Get();
            this->DestroySocket(SocketEOS);
        }
    }
    check(this->HeldSockets.Num() == 0);

    // So that IsUnique check will pass during OSS shutdown.
    this->Subsystem.Reset();
}

EOS_ProductUserId FSocketSubsystemEOSLegacy::GetBindingProductUserId_P2POnly() const
{
    check(this->Subsystem.IsValid());

    IOnlineIdentityPtr Identity = this->Subsystem->GetIdentityInterface();
    TArray<TSharedPtr<FUserOnlineAccount>> AllAccounts = Identity->GetAllUserAccounts();
    if (AllAccounts.Num() == 0)
    {
        if (!GIsAutomationTesting)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("EOSNetDriver: No local user is authenticated with EOS, so there is no local user "
                     "ID to use for the address"));
        }
        return nullptr;
    }
    TSharedPtr<FUserOnlineAccount> FirstAccount = AllAccounts[0];
    check(FirstAccount.IsValid());
    TSharedPtr<const FUniqueNetId> FirstPlayerId = FirstAccount->GetUserId();
    check(FirstPlayerId.IsValid());
    TSharedPtr<const FUniqueNetIdEOS> FirstPlayerIdEOS = StaticCastSharedPtr<const FUniqueNetIdEOS>(FirstPlayerId);
    check(FirstPlayerIdEOS->HasValidProductUserId());

    if (FirstPlayerIdEOS->IsDedicatedServer())
    {
        // This is a dedicated server and the product user ID will be empty (but present for API calls).
        // Return nullptr so we don't try to use EOS P2P.
        return nullptr;
    }

    EOS_ProductUserId ProductUserId = FirstPlayerIdEOS->GetProductUserId();
    return ProductUserId;
}

bool FSocketSubsystemEOSLegacy::GetBindingProductUserId_P2POrDedicatedServer(EOS_ProductUserId &OutPUID) const
{
    check(this->Subsystem.IsValid());

    IOnlineIdentityPtr Identity = this->Subsystem->GetIdentityInterface();
    TArray<TSharedPtr<FUserOnlineAccount>> AllAccounts = Identity->GetAllUserAccounts();
    if (AllAccounts.Num() == 0)
    {
        if (!GIsAutomationTesting)
        {
            UE_LOG(
                LogEOS,
                Error,
                TEXT("EOSNetDriver: No local user is authenticated with EOS, so there is no local user "
                     "ID to use for the address"));
        }
        OutPUID = nullptr;
        return false;
    }
    TSharedPtr<FUserOnlineAccount> FirstAccount = AllAccounts[0];
    check(FirstAccount.IsValid());
    TSharedPtr<const FUniqueNetId> FirstPlayerId = FirstAccount->GetUserId();
    check(FirstPlayerId.IsValid());
    TSharedPtr<const FUniqueNetIdEOS> FirstPlayerIdEOS = StaticCastSharedPtr<const FUniqueNetIdEOS>(FirstPlayerId);
    check(FirstPlayerIdEOS->HasValidProductUserId());

    if (FirstPlayerIdEOS->IsDedicatedServer())
    {
        OutPUID = nullptr;
    }
    else
    {
        OutPUID = FirstPlayerIdEOS->GetProductUserId();
    }
    return true;
}

FSocket *FSocketSubsystemEOSLegacy::CreateSocket(
    const FName &SocketType,
    const FString &SocketDescription,
    const FName &ProtocolName)
{
    int SocketId = this->NextSocketId++;
    TSharedPtr<FSocketEOSLegacy> SocketEOS =
        MakeShared<FSocketEOSLegacy>(this->AsShared(), this->InP2P, SocketId, SocketDescription);
    HeldSockets.Add(SocketId, SocketEOS);
    return SocketEOS.Get();
}

void FSocketSubsystemEOSLegacy::DestroySocket(FSocket *Socket)
{
    check(Socket != nullptr);
    check(Socket->GetProtocol() == REDPOINT_EOS_SUBSYSTEM);
    FSocketEOSLegacy *EOSSocket = (FSocketEOSLegacy *)Socket;
    TSharedPtr<FSocketEOSLegacy> EOSSocketPtr = EOSSocket->AsShared();
    HeldSockets.Remove(EOSSocketPtr->GetSocketId());
    TArray<TWeakPtr<FSocketEOSLegacy>> ChildSocketPtrs;
    for (const auto &ChildSocketsByName : EOSSocketPtr->RemoteSockets)
    {
        for (const auto &ChildSocket : ChildSocketsByName.Value)
        {
            ChildSocketPtrs.Add(ChildSocket.Value);
        }
    }
    EOSSocketPtr->RemoteSockets.Empty();
    while (ChildSocketPtrs.Num() > 0)
    {
        if (TSharedPtr<FSocketEOSLegacy> FirstChildSocket = ChildSocketPtrs[0].Pin())
        {
            ChildSocketPtrs.RemoveAt(0);
            FSocketEOSLegacy *ChildSocketRaw = FirstChildSocket.Get();
            FirstChildSocket.Reset();
            this->DestroySocket(ChildSocketRaw);
        }
    }
    check(EOSSocketPtr.IsUnique());
}

FAddressInfoResult FSocketSubsystemEOSLegacy::GetAddressInfo(
    const TCHAR *HostName,
    const TCHAR *ServiceName,
    EAddressInfoFlags QueryFlags,
    const FName ProtocolTypeName,
    ESocketType SocketType)
{
    return ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)
        ->GetAddressInfo(HostName, ServiceName, QueryFlags, ProtocolTypeName, SocketType);
}

TSharedPtr<FInternetAddr> FSocketSubsystemEOSLegacy::GetAddressFromString(const FString &InAddress)
{
    FString InHostPart = InAddress;
    int32 InPortSeparator;
    if (InAddress.FindChar(':', InPortSeparator))
    {
        InHostPart = InAddress.Mid(0, InPortSeparator);
    }

    TSharedPtr<FInternetAddrEOSLegacy> Addr = MakeShared<FInternetAddrEOSLegacy>();
    bool bIsValid;
    Addr->SetIp(*InHostPart, bIsValid);
    if (bIsValid)
    {
        return Addr;
    }

    UE_LOG(
        LogEOS,
        Error,
        TEXT(
            "FSocketSubsystemEOSLegacy::GetAddressFromString asked to generate address from string '%s', but it wasn't "
            "valid. This should never happen, as the EOS socket subsystem should not be used outside "
            "UEOSNetDriver/UEOSNetConnection."));
    return nullptr;
}

bool FSocketSubsystemEOSLegacy::RequiresChatDataBeSeparate()
{
    return false;
}

bool FSocketSubsystemEOSLegacy::RequiresEncryptedPackets()
{
    return false;
}

bool FSocketSubsystemEOSLegacy::GetHostName(FString &HostName)
{
    return false;
}

TSharedRef<FInternetAddr> FSocketSubsystemEOSLegacy::CreateInternetAddr()
{
    return MakeShared<FInternetAddrEOSLegacy>();
}

bool FSocketSubsystemEOSLegacy::HasNetworkDevice()
{
    return true;
}

const TCHAR *FSocketSubsystemEOSLegacy::GetSocketAPIName() const
{
    return TEXT("EOS");
}

ESocketErrors FSocketSubsystemEOSLegacy::GetLastErrorCode()
{
    return ESocketErrors::SE_NO_ERROR;
}

ESocketErrors FSocketSubsystemEOSLegacy::TranslateErrorCode(int32 Code)
{
    return ESocketErrors::SE_NO_ERROR;
}

bool FSocketSubsystemEOSLegacy::IsSocketWaitSupported() const
{
    return false;
}

EOS_DISABLE_STRICT_WARNINGS