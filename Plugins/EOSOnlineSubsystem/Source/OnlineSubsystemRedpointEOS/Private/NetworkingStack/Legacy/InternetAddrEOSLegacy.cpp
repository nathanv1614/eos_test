// Copyright June Rhodes. All Rights Reserved.

#include "./InternetAddrEOSLegacy.h"

#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSString.h"
#include "Serialization/MemoryReader.h"
#include "Serialization/MemoryWriter.h"

EOS_ENABLE_STRICT_WARNINGS

FInternetAddrEOSLegacy::FInternetAddrEOSLegacy(EOS_ProductUserId InUserId, const FString &InSocketName)
    : UserId(InUserId), SocketName(InSocketName)
{
    check(EOSString_ProductUserId::IsValid(this->UserId));
    check(this->SocketName.Len() > 0 && this->SocketName.Len() <= EOS_P2P_SOCKET_NAME_MAX_LENGTH);
}

FInternetAddrEOSLegacy::FInternetAddrEOSLegacy() : UserId(nullptr), SocketName(TEXT(""))
{
}

void FInternetAddrEOSLegacy::SetFromParameters(
    EOS_ProductUserId InUserId,
    const FString &InSymmetricSocketName,
    EOS_CHANNEL_ID_TYPE InSymmetricChannel)
{
    this->SetUserIdAndSocket(InUserId, InSymmetricSocketName);
}

void FInternetAddrEOSLegacy::SetUserIdAndSocket(EOS_ProductUserId InUserId, const FString &InSocketName)
{
    check(EOSString_ProductUserId::IsValid(InUserId));
    check(InSocketName.Len() > 0 && InSocketName.Len() <= EOS_P2P_SOCKET_NAME_MAX_LENGTH);
    this->UserId = InUserId;
    this->SocketName = InSocketName;
}

EOS_ProductUserId FInternetAddrEOSLegacy::GetUserId() const
{
    return this->UserId;
}

FString FInternetAddrEOSLegacy::GetSocketName() const
{
    return this->SocketName;
}

EOS_P2P_SocketId FInternetAddrEOSLegacy::GetSocketId() const
{
    EOS_P2P_SocketId Socket = {};
    Socket.ApiVersion = EOS_P2P_SOCKETID_API_LATEST;
    verify(
        EOSString_P2P_SocketName::CopyToExistingBuffer<33>(this->SocketName, Socket.SocketName) ==
        EOS_EResult::EOS_Success);
    return Socket;
}

void FInternetAddrEOSLegacy::SetIp(uint32 InAddr)
{
    UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetIp is not supported."));
}

void FInternetAddrEOSLegacy::SetIp(const TCHAR *InAddr, bool &bIsValid)
{
    FString Addr = InAddr;

    if (!Addr.EndsWith(FString::Printf(TEXT(".%s"), INTERNET_ADDR_EOS_P2P_DOMAIN_SUFFIX)))
    {
        bIsValid = false;
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetIp does not support plain IP addresses yet."));
        return;
    }

    TArray<FString> AddressComponents;
    Addr.ParseIntoArray(AddressComponents, TEXT("."), true);
    if (AddressComponents.Num() != 3)
    {
        bIsValid = false;
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetIp received an incomplete address: '%s'"), *Addr);
        return;
    }

    EOS_ProductUserId ResultProductUserId = {};
    if (EOSString_ProductUserId::FromString(AddressComponents[0], ResultProductUserId) != EOS_EResult::EOS_Success)
    {
        bIsValid = false;
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FInternetAddrEOSLegacy::SetIp did not have a valid product user ID. If you want to create an address "
                 "for binding locally, use the ID of the user that will be listening."));
        return;
    }

    FString ResultSocketName = AddressComponents[1];
    if (ResultSocketName.IsEmpty())
    {
        bIsValid = false;
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetIp did not have a valid socket name."));
        return;
    }

    if (AddressComponents[2] != INTERNET_ADDR_EOS_P2P_DOMAIN_SUFFIX)
    {
        bIsValid = false;
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FInternetAddrEOSLegacy::SetIp was missing '.%s' domain suffix."),
            INTERNET_ADDR_EOS_P2P_DOMAIN_SUFFIX);
        return;
    }

    this->UserId = ResultProductUserId;
    this->SocketName = ResultSocketName;
    bIsValid = true;
}

void FInternetAddrEOSLegacy::GetIp(uint32 &OutAddr) const
{
    UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::GetIp is not supported and will set OutAddr to 0."));
    OutAddr = 0;
}

void FInternetAddrEOSLegacy::SetPort(int32 InPort)
{
    if (!GIsAutomationTesting)
    {
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetPort is not supported."));
    }
}

int32 FInternetAddrEOSLegacy::GetPort() const
{
    if (!GIsAutomationTesting)
    {
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::GetPort is not supported and will return 0."));
    }
    return 0;
}

void FInternetAddrEOSLegacy::SetRawIp(const TArray<uint8> &RawAddr)
{
    FMemoryReader Reader(RawAddr);

    FString ProductUserIdStr;
    FString SocketNameStr;

    Reader << ProductUserIdStr;
    Reader << SocketNameStr;

    EOS_ProductUserId ProductUserId = {};
    if (EOSString_ProductUserId::FromString(ProductUserIdStr, ProductUserId) != EOS_EResult::EOS_Success)
    {
        UE_LOG(
            LogEOS,
            Error,
            TEXT("FInternetAddrEOSLegacy::SetRawIp did not have a valid product user ID. If you want to create an "
                 "address "
                 "for binding locally, use the ID of the user that will be listening."));
        return;
    }

    if (SocketNameStr.IsEmpty())
    {
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetRawIp did not have a valid socket name."));
        return;
    }

    this->UserId = ProductUserId;
    this->SocketName = SocketNameStr;
}

TArray<uint8> FInternetAddrEOSLegacy::GetRawIp() const
{
    TArray<uint8> Result;
    FMemoryWriter Writer(Result);

    FString ProductUserIdStr;
    if (EOSString_ProductUserId::ToString(this->UserId, ProductUserIdStr) != EOS_EResult::EOS_Success)
    {
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::GetRawIp could not serialize the current user ID."));
        return TArray<uint8>();
    }

    FString SocketNameStr = FString(this->SocketName);
    Writer << ProductUserIdStr;
    Writer << SocketNameStr;

    Writer.Close();

    return Result;
}

void FInternetAddrEOSLegacy::SetAnyAddress()
{
    UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetAnyAddress is not supported."));
}

void FInternetAddrEOSLegacy::SetBroadcastAddress()
{
    UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetBroadcastAddress is not supported."));
}

void FInternetAddrEOSLegacy::SetLoopbackAddress()
{
    UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::SetLoopbackAddress is not supported."));
}

FString FInternetAddrEOSLegacy::ToString(bool bAppendPort) const
{
    FString ProductUserIdStr;
    if (EOSString_ProductUserId::ToString(this->UserId, ProductUserIdStr) != EOS_EResult::EOS_Success)
    {
        UE_LOG(LogEOS, Error, TEXT("FInternetAddrEOSLegacy::ToString could not serialize the current user ID."));
        return TEXT("");
    }

    FString Result =
        FString::Printf(TEXT("%s.%s.%s"), *ProductUserIdStr, *this->SocketName, INTERNET_ADDR_EOS_P2P_DOMAIN_SUFFIX);
    return Result;
}

uint32 FInternetAddrEOSLegacy::GetTypeHash() const
{
    return ::GetTypeHash(this->ToString(true));
}

bool FInternetAddrEOSLegacy::IsValid() const
{
    return EOSString_ProductUserId::IsValid(this->UserId) && this->SocketName.Len() > 0 &&
           this->SocketName.Len() <= EOS_P2P_SOCKET_NAME_MAX_LENGTH;
}

TSharedRef<FInternetAddr> FInternetAddrEOSLegacy::Clone() const
{
    auto Clone = MakeShared<FInternetAddrEOSLegacy>();
    Clone->UserId = this->UserId;
    Clone->SocketName = this->SocketName;
    return Clone;
}

bool FInternetAddrEOSLegacy::operator==(const FInternetAddr &Other) const
{
    if (!Other.GetProtocolType().IsEqual(REDPOINT_EOS_SUBSYSTEM))
    {
        return false;
    }

    return Other.ToString(true) == this->ToString(true);
}

EOS_DISABLE_STRICT_WARNINGS