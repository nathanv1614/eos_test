// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EOSDefines.h"

EOS_ENABLE_STRICT_WARNINGS

#if defined(HAS_MALLOCZEROED)
#define Compat_MallocZeroed FMemory::MallocZeroed
#else
ONLINESUBSYSTEMREDPOINTEOS_API void *Compat_MallocZeroed(size_t Size, uint32 Alignment = 0);
#endif

EOS_DISABLE_STRICT_WARNINGS