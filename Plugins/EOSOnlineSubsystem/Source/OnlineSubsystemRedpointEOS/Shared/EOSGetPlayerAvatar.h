// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "GameFramework/OnlineReplStructs.h"
#include "Kismet/BlueprintAsyncActionBase.h"

#include "EOSGetPlayerAvatar.generated.h"

EOS_ENABLE_STRICT_WARNINGS

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEOSGetPlayerAvatarCompletePin, UTexture *, Avatar);

UCLASS(BlueprintType)
class ONLINESUBSYSTEMREDPOINTEOS_API UEOSGetPlayerAvatar : public UBlueprintAsyncActionBase
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FEOSGetPlayerAvatarCompletePin OnComplete;

public:
    // clang-format off

    // Use the "OnlineAvatarSubsystem -> GetAvatar" node from Online Subsystem Blueprints instead, or call Online::GetAvatarInterface(OSS)->GetAvatar() from C++. This blueprint node will be removed in a future release.
    UFUNCTION(
        BlueprintCallable,
        meta =
            (DeprecatedFunction,
             DeprecatedProperty,
             DeprecationMessage = "Use the \"OnlineAvatarSubsystem -> GetAvatar\" node from Online Subsystem Blueprints instead, or call Online::GetAvatarInterface(OSS)->GetAvatar() from C++. This blueprint node will be removed in a future release.",
             BlueprintInternalUseOnly = "true",
             WorldContext = "WorldContextObject"),
        Category = "Online")
    static UEOSGetPlayerAvatar *GetPlayerAvatar(
        const UObject *WorldContextObject,
        int32 LocalUserNum,
        FUniqueNetIdRepl UserId,
        UTexture *DefaultAvatar);

    // clang-format on

    virtual void Activate() override;

private:
    void HandleComplete(bool bWasSuccessful, TSoftObjectPtr<UTexture> Texture);

    UPROPERTY()
    const UObject *WorldContextObject;

    UPROPERTY()
    int32 LocalUserNum;

    UPROPERTY()
    FUniqueNetIdRepl UserId;

    UPROPERTY()
    UTexture *DefaultAvatar;
};

EOS_DISABLE_STRICT_WARNINGS