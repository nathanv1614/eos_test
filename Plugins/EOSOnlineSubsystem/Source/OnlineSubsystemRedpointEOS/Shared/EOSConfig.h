// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EOSCommon.h"
#include "Misc/ConfigCacheIni.h"
#include "OnlineSubsystemRedpointEOS/Shared/WorldResolution.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

#ifdef ONLINESUBSYSTEMEOS_PACKAGE
#include "EOSConfig.generated.h"
#endif

EOS_ENABLE_STRICT_WARNINGS

/**
 * In EOS, you can only have one object advertised with presence; this can either be a party or a session, but not both.
 * This configuration option determines what gets advertised in the presence system.
 */
UENUM()
enum class EPresenceAdvertisementType : uint8
{
    /**
     * Neither parties nor sessions will be advertised with presence.
     */
    None,

    /**
     * Parties will be the type of joinable object advertised with presence. This
     * is the default for backwards compatibility.
     */
    Party,

    /**
     * Sessions will be the type of joinable object advertised with presence.
     */
    Session,
};

/**
 * When new versions of the plugin are released, sometimes the default behaviour changes. When you first use the plugin,
 * it sets the current latest API version in the configuration so that when you upgrade, the behaviour does not change
 * for you until you decide to upgrade.
 */
UENUM()
enum class EEOSApiVersion : uint8
{
    // NOTE: When a new version is added here, it *MUST* be added to the top of
    // the enum so it becomes value 0.

    /**
     * Behaviour as of 2021-05-31. As of this release, the monolithic authentication graph is no longer the default
     * authentication graph.
     */
    v2021_05_31 UMETA(DisplayName = "2021-05-31"),

    /** Behaviour as of 2021-01-22. As of this release, the full networking stack is the default networking stack. */
    v2021_01_22 UMETA(DisplayName = "2021-01-22"),

    /** Behaviour as of 2020-12-09. As of this release, sessions that are not listening for connections are excluded by
       default from search results. */
    v2020_12_09 UMETA(DisplayName = "2020-12-09"),

    /** Behaviour as of 2020-12-02. As of this release, JoinSessionAccepted is mapped to OnSessionUserInviteAccepted. */
    v2020_12_02 UMETA(DisplayName = "2020-12-02"),

    /** Behaviour as of 2020-12-01. This is the first API version registered. */
    v2020_12_01 UMETA(DisplayName = "2020-12-01"),
};

/**
 * The EOS networking stack to use.
 */
UENUM()
enum class EEOSNetworkingStack : uint8
{
    /** Select the networking stack based on the API version. */
    Default,

    /** Use the legacy networking stack. It's been battle tested more than the full networking stack, but it doesn't
       support beacons. Eventually the legacy networking stack will be deprecated and removed. */
    Legacy,

    /** Use the full networking stack. Currently experimental, but supports both game connections and beacons. */
    Full,
};

UENUM()
enum class EPartyJoinabilityConstraint : uint8
{
    /**
     * When a player accepts an invite from the native platform and the player is already in a party, the player will
     * join the new party in addition to the existing party they are in.  IOnlinePartySystem::GetJoinedParties will
     * return multiple parties.
     */
    AllowPlayersInMultipleParties UMETA(DisplayName = "Allow players to join multiple parties at the same time"),

    /**
     * When a player accepts an invite from the native platform and the player is already in a party, the accepted
     * invite is ignored and the IOnlinePartySystem::OnPartyInviteResponseReceived event is fired to indicate an invite
     * was ignored.
     */
    IgnoreInvitesIfAlreadyInParty UMETA(DisplayName = "Ignore accepted invites if player is already in a party")
};

UENUM()
enum class EDedicatedServersDistributionMode : uint8
{
    /**
     * In developers only mode, dedicated servers can have the client ID and secret embedded in their packaged binaries.
     * You must ensure that the binaries are never distributed to end users, otherwise they will be able to run trusted
     * dedicated servers and impersonate other users on their behalf.
     */
    DevelopersOnly UMETA(DisplayName = "Dedicated server binaries are only ever distributed and run by developers"),

    /**
     * If dedicated servers are being run by both developers and players, the dedicated server secrets will be stored in
     * Build/NoRedist/DedicatedServerEngine.ini. You must then provide the values in this file at runtime when running
     * the dedicated servers on your own infrastructure. Dedicated servers run by players will be considered untrusted,
     * and will not perform user verification checks.
     */
    DevelopersAndPlayers UMETA(DisplayName = "Both developers and players will run dedicated servers"),

    /**
     * If dedicated servers are only being run by players, this turns off all the trusted dedicated server code paths.
     */
    PlayersOnly UMETA(DisplayName = "Dedicated server binaries will be distributed to players and only run by players"),
};

FORCEINLINE bool EOS_ApiVersionIsAtLeast(EEOSApiVersion InConfigVersion, EEOSApiVersion InTargetVersion)
{
    return InConfigVersion <= InTargetVersion;
}

class ONLINESUBSYSTEMREDPOINTEOS_API FEOSConfig
{
    friend class FDelegatedSubsystems;

protected:
    virtual FString GetDelegatedSubsystemsString() const = 0;

public:
    virtual ~FEOSConfig(){};

    virtual void TryLoadDependentModules();
    virtual TArray<class IEOSNativePlatform *> GetNativePlatforms() const;

    virtual FString GetFreeEditionLicenseKey() const = 0;
    virtual FString GetEncryptionKey() const = 0;
    virtual FString GetProductName() const = 0;
    virtual FString GetProductVersion() const = 0;
    virtual FString GetProductId() const = 0;
    virtual FString GetSandboxId() const = 0;
    virtual FString GetDeploymentId() const = 0;
    virtual FString GetClientId() const = 0;
    virtual FString GetClientSecret() const = 0;
    virtual FString GetDeveloperToolAddress() const = 0;
    virtual FString GetDeveloperToolDefaultCredentialName() const = 0;
    virtual FString GetWidgetClass(FString WidgetName, FString DefaultValue) const = 0;
    virtual EEOSApiVersion GetApiVersion() const = 0;
    virtual FName GetAuthenticationGraph() const = 0;
    virtual FName GetEditorAuthenticationGraph() const = 0;
    virtual FName GetCrossPlatformAccountProvider() const = 0;
    virtual bool GetRequireCrossPlatformAccount() const = 0;
    virtual EPresenceAdvertisementType GetPresenceAdvertisementType() const = 0;
    virtual bool GetAllowNativePlatformAccounts() const = 0;
    virtual bool GetAllowDeviceIdAccounts() const = 0;
    virtual bool GetDeleteDeviceIdOnLogout() const = 0;
    virtual bool GetPersistentLoginEnabled() const = 0;
    virtual bool IsAutomatedTesting() const = 0;
    virtual EEOSNetworkingStack GetNetworkingStack() const = 0;
    virtual bool GetRequireEpicGamesLauncher() const = 0;
    virtual FString GetSimpleFirstPartyLoginUrl() const = 0;
    virtual EPartyJoinabilityConstraint GetPartyJoinabilityConstraint() const = 0;
    virtual bool GetEnableSanctionChecks() const = 0;
    virtual bool GetEnableIdentityChecksOnListenServers() const = 0;
    virtual bool GetEnableTrustedDedicatedServers() const = 0;
    virtual bool GetEnableAutomaticEncryptionOnTrustedDedicatedServers() const = 0;
    virtual EDedicatedServersDistributionMode GetDedicatedServerDistributionMode() const = 0;
    virtual FString GetDedicatedServerPublicKey() const = 0;
    virtual FString GetDedicatedServerPrivateKey() const = 0;
    virtual FString GetDedicatedServerActAsClientId() const = 0;
    virtual FString GetDedicatedServerActAsClientSecret() const = 0;
    virtual bool GetEnableAntiCheat() const = 0;
    virtual FString GetTrustedClientPublicKey() const = 0;
    virtual FString GetTrustedClientPrivateKey() const = 0;
    virtual bool GetEnableVoiceChatEchoInParties() const = 0;
    virtual bool GetEnableVoiceChatPlatformAECByDefault() const = 0;
};

class ONLINESUBSYSTEMREDPOINTEOS_API FEOSConfigIni : public FEOSConfig
{
protected:
    virtual FString GetDelegatedSubsystemsString() const override;

public:
    virtual ~FEOSConfigIni(){};

    virtual FString GetFreeEditionLicenseKey() const override;
    virtual FString GetEncryptionKey() const override;
    virtual FString GetProductName() const override;
    virtual FString GetProductVersion() const override;
    virtual FString GetProductId() const override;
    virtual FString GetSandboxId() const override;
    virtual FString GetDeploymentId() const override;
    virtual FString GetClientId() const override;
    virtual FString GetClientSecret() const override;
    virtual FString GetDeveloperToolAddress() const override;
    virtual FString GetDeveloperToolDefaultCredentialName() const override;
    virtual FString GetWidgetClass(FString WidgetName, FString DefaultValue) const override;
    virtual EEOSApiVersion GetApiVersion() const override;
    virtual FName GetAuthenticationGraph() const override;
    virtual FName GetEditorAuthenticationGraph() const override;
    virtual FName GetCrossPlatformAccountProvider() const override;
    virtual bool GetRequireCrossPlatformAccount() const override;
    virtual EPresenceAdvertisementType GetPresenceAdvertisementType() const override;
    virtual bool GetAllowNativePlatformAccounts() const override;
    virtual bool GetAllowDeviceIdAccounts() const override;
    virtual bool GetDeleteDeviceIdOnLogout() const override;
    virtual bool GetPersistentLoginEnabled() const override;
    virtual bool IsAutomatedTesting() const override;
    virtual EEOSNetworkingStack GetNetworkingStack() const override;
    virtual bool GetRequireEpicGamesLauncher() const override;
    virtual FString GetSimpleFirstPartyLoginUrl() const override;
    virtual EPartyJoinabilityConstraint GetPartyJoinabilityConstraint() const override;
    virtual bool GetEnableSanctionChecks() const override;
    virtual bool GetEnableIdentityChecksOnListenServers() const override;
    virtual bool GetEnableTrustedDedicatedServers() const override;
    virtual bool GetEnableAutomaticEncryptionOnTrustedDedicatedServers() const override;
    virtual EDedicatedServersDistributionMode GetDedicatedServerDistributionMode() const override;
    virtual FString GetDedicatedServerPublicKey() const override;
    virtual FString GetDedicatedServerPrivateKey() const override;
    virtual FString GetDedicatedServerActAsClientId() const override;
    virtual FString GetDedicatedServerActAsClientSecret() const override;
    virtual bool GetEnableAntiCheat() const override;
    virtual FString GetTrustedClientPublicKey() const override;
    virtual FString GetTrustedClientPrivateKey() const override;
    virtual bool GetEnableVoiceChatEchoInParties() const override;
    virtual bool GetEnableVoiceChatPlatformAECByDefault() const override;

private:
    FString GetConfigValue(const FString &Key, const FString &DefaultValue) const;
    FString GetConfigValue(const FString &Key, const TCHAR *DefaultValue) const;
    bool GetConfigValue(const FString &Key, bool DefaultValue) const;
};

EOS_DISABLE_STRICT_WARNINGS