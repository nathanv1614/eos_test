// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "OnlineSubsystem.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "UObject/SoftObjectPtr.h"

EOS_ENABLE_STRICT_WARNINGS

struct ONLINESUBSYSTEMREDPOINTEOS_API FExternalAccountIdInfo
{
public:
    EOS_EExternalAccountType AccountIdType;
    FString AccountId;
};

class ONLINESUBSYSTEMREDPOINTEOS_API IEOSRuntimePlatformIntegration
{
public:
    /**
     * Attempts to convert the provided external user information into an online subsystem user ID. The GetType()
     * function of the user ID should map to a loaded online subsystem, so that the caller can use the user ID with that
     * online subsystem's APIs.
     */
    virtual TSharedPtr<const class FUniqueNetId> GetUserId(
        TSoftObjectPtr<class UWorld> InWorld,
        EOS_Connect_ExternalAccountInfo *InExternalInfo) = 0;

    /**
     * Returns if the specified user ID can be converted into an FExternalAccountIdInfo for the friends system to look
     * up the EOS account by external info.
     */
    virtual bool CanProvideExternalAccountId(const FUniqueNetId &InUserId) = 0;

    /**
     * Returns the external account information for the given user ID.
     */
    virtual FExternalAccountIdInfo GetExternalAccountId(const FUniqueNetId &InUserId) = 0;

    /**
     * Called when the list name is being passed into a wrapped subsystem inside OnlineFriendsInterfaceSynthetic,
     * offering the integration the chance to update the list name (in case the platform restricts valid values).
     */
    virtual void MutateFriendsListNameIfRequired(FName InSubsystemName, FString &InOutListName) const {};
};

class ONLINESUBSYSTEMREDPOINTEOS_API IEOSRuntimePlatform
{
public:
    /**
     * Load the EOS SDK runtime library.
     */
    virtual void Load() = 0;

    /**
     * Unload the EOS SDK runtime library.
     */
    virtual void Unload() = 0;

    /**
     * Return true if the EOS SDK runtime library could be loaded, and EOS can be used.
     */
    virtual bool IsValid() = 0;

    /**
     * Returns the system initialization options that are passed to EOS_InitializeOptions.
     */
    virtual void *GetSystemInitializeOptions() = 0;

#if EOS_VERSION_AT_LEAST(1, 13, 0)
    /**
     * Returns the RTC options that are passed to EOS_Platform_Options.
     */
    virtual EOS_Platform_RTCOptions *GetRTCOptions()
    {
        return nullptr;
    }
#endif

    /**
     * Returns the path to cache directory that is passed to EOS_Platform_Options.
     */
    virtual FString GetCacheDirectory() = 0;

    /**
     * DEPRECATED.
     */
    virtual void ClearStoredEASRefreshToken(int32 LocalUserNum) = 0;

    /**
     * Returns the path to the Epic Games account credentials used for automated testing.
     */
#if !UE_BUILD_SHIPPING
    virtual FString GetPathToEASAutomatedTestingCredentials() = 0;
#endif

    /**
     * Returns all platform integrations available on this platform.
     */
    virtual const TArray<TSharedPtr<IEOSRuntimePlatformIntegration>> &GetIntegrations() const = 0;

    /**
     * Returns the name of the platform as a string (as per the EOS_EAntiCheatCommonClientPlatform enumeration). This
     * value is only used on trusted platforms like consoles.
     */
    virtual FString GetAntiCheatPlatformName() const
    {
        return TEXT("Unknown");
    };
};

EOS_DISABLE_STRICT_WARNINGS