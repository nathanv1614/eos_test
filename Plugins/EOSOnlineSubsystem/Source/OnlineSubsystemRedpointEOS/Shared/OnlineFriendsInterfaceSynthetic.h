// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/OnlineFriendsInterface.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "OnlineSubsystemRedpointEOS/Public/EOSNativePlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSDefines.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSRuntimePlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/EpicGames/OnlineFriendsInterfaceEAS.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineUserEOS.h"
#include <functional>

EOS_ENABLE_STRICT_WARNINGS

struct FFriendInfo
{
public:
    bool HasExternalInfo;
    FExternalAccountIdInfo ExternalInfo;
    FName SubsystemName;
    TSharedPtr<FOnlineFriend> FriendRef;
};

typedef TSharedPtr<FFriendInfo> FUnresolvedFriendEntry;
typedef TTuple<EOS_ProductUserId, TSharedPtr<FFriendInfo>> TResolvedFriendEntry;
typedef TTuple<EOS_EExternalAccountType, TArray<FUnresolvedFriendEntry>> FUnresolvedFriendEntries;
typedef TTuple<EOS_EExternalAccountType, TArray<TResolvedFriendEntry>> TResolvedFriendEntries;

class ONLINESUBSYSTEMREDPOINTEOS_API FOnlineFriendsInterfaceSynthetic
    : public IOnlineFriends,
      public TSharedFromThis<FOnlineFriendsInterfaceSynthetic, ESPMode::ThreadSafe>
{
private:
    EOS_HPlatform EOSPlatform;
    EOS_HConnect EOSConnect;

    struct FWrappedFriendsInterface
    {
        FName SubsystemName;
        TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> OnlineFriends;
    };

    TSharedPtr<class IEOSRuntimePlatform> RuntimePlatform;
    // NOLINTNEXTLINE(unreal-unsafe-storage-of-oss-pointer)
    IOnlineIdentityPtr Identity;
    TSharedPtr<class FEOSUserFactory, ESPMode::ThreadSafe> UserFactory;
    TArray<FWrappedFriendsInterface> OnlineFriends;
    TArray<TTuple<TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe>, int, FDelegateHandle>> OnFriendsChangeDelegates;

    TUserIdMap<TSharedPtr<class FOnlineFriendSynthetic>> FriendCacheById;
    TArray<TSharedRef<FOnlineFriend>> FriendCacheByIndex;

    FString MutateListName(const FWrappedFriendsInterface &Friends, FString ListName);

    bool Op_ReadFriendsList(
        int32 LocalUserNum,
        const FString &ListName,
        FName InSubsystemName,
        const TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> &InFriends,
        const std::function<void(bool OutValue)> &OnDone);
    void Dn_ReadFriendsList(
        int32 LocalUserNum,
        FString ListName,
        const FOnReadFriendsListComplete &Delegate,
        const TArray<bool> &OutValues);
    bool Op_GetFriendsList(
        int32 LocalUserNum,
        const FString &ListName,
        const TWeakPtr<IOnlineFriends, ESPMode::ThreadSafe> &InFriends,
        const std::function<void(TArray<TSharedRef<FOnlineFriend>> OutValue)> &OnDone);
    void Dn_GetFriendsList(
        int32 LocalUserNum,
        FString ListName,
        const FOnReadFriendsListComplete &Delegate,
        TArray<TArray<TSharedRef<FOnlineFriend>>> OutValues);
    bool Op_ResolveFriendsListByType(
        int32 LocalUserNum,
        const FUnresolvedFriendEntries &EntriesByType,
        const std::function<void(TResolvedFriendEntries)> &OnDone);
    void Dn_ResolveFriendsListByType(
        int32 LocalUserNum,
        FString ListName,
        const FOnReadFriendsListComplete &Delegate,
        const TArray<TResolvedFriendEntries> &Results);
    void Dn_ResolveEOSAccount(
        int32 LocalUserNum,
        const FString &ListName,
        const FOnReadFriendsListComplete &Delegate,
        TMap<EOS_ProductUserId, TArray<TResolvedFriendEntry>> FriendRefsByExternalId,
        const TArray<TSharedPtr<FOnlineUserEOS>> &ResolvedEOSAccounts);

public:
    FOnlineFriendsInterfaceSynthetic(
        FName InInstanceName,
        EOS_HPlatform InPlatform,
        IOnlineIdentityPtr InIdentity,
        const IOnlineSubsystemPtr &InSubsystemEAS,
        const TSharedRef<class IEOSRuntimePlatform> &InRuntimePlatform,
        const TSharedRef<class FEOSConfig> &InConfig,
        const TSharedRef<class FEOSUserFactory, ESPMode::ThreadSafe> &InUserFactory);
    UE_NONCOPYABLE(FOnlineFriendsInterfaceSynthetic);
    ~FOnlineFriendsInterfaceSynthetic();

    /** Resolves the native ID to an EOS ID if possible, otherwise returns nullptr. */
    TSharedPtr<const FUniqueNetIdEOS> TryResolveNativeIdToEOS(const FUniqueNetId &UserId) const;

    virtual bool ReadFriendsList(
        int32 LocalUserNum,
        const FString &ListName,
        const FOnReadFriendsListComplete &Delegate = FOnReadFriendsListComplete()) override;
    virtual bool DeleteFriendsList(
        int32 LocalUserNum,
        const FString &ListName,
        const FOnDeleteFriendsListComplete &Delegate = FOnDeleteFriendsListComplete()) override;

    virtual bool SendInvite(
        int32 LocalUserNum,
        const FUniqueNetId &FriendId,
        const FString &ListName,
        const FOnSendInviteComplete &Delegate = FOnSendInviteComplete()) override;
    virtual bool AcceptInvite(
        int32 LocalUserNum,
        const FUniqueNetId &FriendId,
        const FString &ListName,
        const FOnAcceptInviteComplete &Delegate = FOnAcceptInviteComplete()) override;
    virtual bool RejectInvite(int32 LocalUserNum, const FUniqueNetId &FriendId, const FString &ListName) override;

    virtual void SetFriendAlias(
        int32 LocalUserNum,
        const FUniqueNetId &FriendId,
        const FString &ListName,
        const FString &Alias,
        const FOnSetFriendAliasComplete &Delegate = FOnSetFriendAliasComplete()) override;
#if defined(HAS_FRIENDS_DELETE_FRIEND_ALIAS)
    virtual void DeleteFriendAlias(
        int32 LocalUserNum,
        const FUniqueNetId &FriendId,
        const FString &ListName,
        const FOnDeleteFriendAliasComplete &Delegate = FOnDeleteFriendAliasComplete()) override;
#endif
    virtual bool DeleteFriend(int32 LocalUserNum, const FUniqueNetId &FriendId, const FString &ListName) override;

    virtual bool GetFriendsList(
        int32 LocalUserNum,
        const FString &ListName,
        TArray<TSharedRef<FOnlineFriend>> &OutFriends) override;
    virtual TSharedPtr<FOnlineFriend> GetFriend(
        int32 LocalUserNum,
        const FUniqueNetId &FriendId,
        const FString &ListName) override;
    virtual bool IsFriend(int32 LocalUserNum, const FUniqueNetId &FriendId, const FString &ListName) override;

    virtual bool QueryRecentPlayers(const FUniqueNetId &UserId, const FString &Namespace) override;
    virtual bool GetRecentPlayers(
        const FUniqueNetId &UserId,
        const FString &Namespace,
        TArray<TSharedRef<FOnlineRecentPlayer>> &OutRecentPlayers) override;
    virtual void DumpRecentPlayers() const override;

    virtual bool BlockPlayer(int32 LocalUserNum, const FUniqueNetId &PlayerId) override;
    virtual bool UnblockPlayer(int32 LocalUserNum, const FUniqueNetId &PlayerId) override;
    virtual bool QueryBlockedPlayers(const FUniqueNetId &UserId) override;
    virtual bool GetBlockedPlayers(
        const FUniqueNetId &UserId,
        TArray<TSharedRef<FOnlineBlockedPlayer>> &OutBlockedPlayers) override;
    virtual void DumpBlockedPlayers() const override;
};

EOS_DISABLE_STRICT_WARNINGS