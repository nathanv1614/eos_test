// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "CoreMinimal.h"

#include "Containers/Array.h"
#include "Interfaces/OnlineIdentityInterface.h"
#include "Interfaces/OnlinePartyInterface.h"
#include "Interfaces/OnlineSessionInterface.h"

EOS_ENABLE_STRICT_WARNINGS

DECLARE_DELEGATE(FSyntheticPartyOnComplete);

class FSyntheticPartyManager : public TSharedFromThis<FSyntheticPartyManager>
{
    friend class FOnlineSubsystemEOS;

private:
    void RegisterEvents();

    struct FWrappedSubsystem
    {
        FName SubsystemName;
        TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> Identity;
        TWeakPtr<IOnlineSession, ESPMode::ThreadSafe> Session;
    };

    TSharedPtr<class FEOSConfig> Config;
    TWeakPtr<class FOnlinePartySystemEOS, ESPMode::ThreadSafe> EOSPartySystem;
    TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> EOSIdentitySystem;
    TArray<FWrappedSubsystem> WrappedSubsystems;
    TMap<TWeakPtr<IOnlineSession, ESPMode::ThreadSafe>, FDelegateHandle> SessionUserInviteAcceptedDelegateHandles;
    TMap<FString, FDelegateHandle> HandlesPendingRemoval;

    void HandleSessionInviteAccept(int32 ControllerId, const FString &EOSPartyId);

public:
    FSyntheticPartyManager(
        FName InInstanceName,
        TWeakPtr<class FOnlinePartySystemEOS, ESPMode::ThreadSafe> InEOSPartySystem,
        TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe> InEOSIdentitySystem,
        const TSharedRef<class FEOSConfig> &InConfig);
    UE_NONCOPYABLE(FSyntheticPartyManager);
    ~FSyntheticPartyManager();

    void CreateSyntheticParty(
        const TSharedPtr<const FOnlinePartyId> &PartyId,
        const FSyntheticPartyOnComplete &OnComplete);
    void DeleteSyntheticParty(
        const TSharedPtr<const FOnlinePartyId> &PartyId,
        const FSyntheticPartyOnComplete &OnComplete);
    void SendInvitation(
        const TSharedPtr<const FUniqueNetId> &LocalUserId,
        const TSharedPtr<const FOnlinePartyId> &PartyId,
        const TSharedPtr<const FUniqueNetId> &RecipientId,
        const FOnSendPartyInvitationComplete &Delegate);
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION