// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"

EOS_ENABLE_STRICT_WARNINGS

#define EOS_AUTH_GRAPH_MONOLITHIC FName(TEXT("Monolithic"))

/**
 * This is the old authentication behaviour used in EOS Online Subsystem. It supports gathering native platform
 * credentials through IEOSNativePlatform, signing in through the native platform or Epic Games accounts, linking
 * accounts together, handling interactive UIs for you and anonymous authentication.
 *
 * However, the user experience can be complex for players to understand at times, especially when they have multiple
 * authentication sources available. It is recommended that you use the new Online Subsystem based authentication graph.
 */
class ONLINESUBSYSTEMREDPOINTEOS_API FAuthenticationGraphMonolithic : public FAuthenticationGraph
{
protected:
    virtual TSharedRef<FAuthenticationGraphNode> CreateGraph(
        const TSharedRef<FAuthenticationGraphState> &InitialState) override;

public:
    UE_NONCOPYABLE(FAuthenticationGraphMonolithic);
    FAuthenticationGraphMonolithic() = default;
    virtual ~FAuthenticationGraphMonolithic() = default;

    static void Register();
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
