// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "CoreMinimal.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraphNode.h"

EOS_ENABLE_STRICT_WARNINGS

class ONLINESUBSYSTEMREDPOINTEOS_API FAuthenticationGraphNodeUntil : public FAuthenticationGraphNode
{
private:
    TArray<TSharedRef<FAuthenticationGraphNode>> Sequence;
    TSharedPtr<FAuthenticationGraphState> CurrentState;
    int CurrentSequenceNum;
    FAuthenticationGraphNodeOnDone CurrentSequenceDone;
    FAuthenticationGraphCondition UntilCondition;
    FString ErrorMessage;

    void Continue(EAuthenticationGraphNodeResult Result);
    void HandleOnDone(EAuthenticationGraphNodeResult Result, TSharedPtr<FAuthenticationGraphNode> Node);

protected:
    FAuthenticationGraphNodeUntil() = delete;
    UE_NONCOPYABLE(FAuthenticationGraphNodeUntil);
    FAuthenticationGraphNodeUntil(FAuthenticationGraphCondition InUntilCondition, FString InErrorMessage = TEXT(""))
        : CurrentState(nullptr), CurrentSequenceNum(0), UntilCondition(MoveTemp(InUntilCondition)),
          ErrorMessage(InErrorMessage){};

    virtual void Execute(TSharedRef<FAuthenticationGraphState> State, FAuthenticationGraphNodeOnDone OnDone) final;
    virtual bool RequireConditionPass() const
    {
        return true;
    }

public:
    virtual ~FAuthenticationGraphNodeUntil() = default;

    TSharedRef<FAuthenticationGraphNodeUntil> Add(const TSharedRef<FAuthenticationGraphNode> &Node)
    {
        this->Sequence.Add(Node);
        return StaticCastSharedRef<FAuthenticationGraphNodeUntil>(this->AsShared());
    }
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
