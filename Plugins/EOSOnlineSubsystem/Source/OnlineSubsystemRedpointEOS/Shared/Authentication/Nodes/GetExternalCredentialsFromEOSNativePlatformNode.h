// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"

EOS_ENABLE_STRICT_WARNINGS

class ONLINESUBSYSTEMREDPOINTEOS_API FGetExternalCredentialsFromEOSNativePlatformNode : public FAuthenticationGraphNode
{
private:
    void GetCredentialsCallback(
        bool bWasSuccessful,
        FOnlineAccountCredentials Credentials,
        TMap<FString, FString> UserAuthAttributes,
        TSharedRef<FAuthenticationGraphState> State,
        class IEOSNativePlatform *NativePlatform,
        std::function<void(bool)> OnPlatformDone);

public:
    UE_NONCOPYABLE(FGetExternalCredentialsFromEOSNativePlatformNode);
    FGetExternalCredentialsFromEOSNativePlatformNode() = default;
    virtual ~FGetExternalCredentialsFromEOSNativePlatformNode() = default;

    virtual void Execute(TSharedRef<FAuthenticationGraphState> State, FAuthenticationGraphNodeOnDone OnDone) override;

    virtual FString GetDebugName() const override
    {
        return TEXT("FGetExternalCredentialsFromEOSNativePlatformNode");
    }
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
