// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/EOSUserInterface_LinkEOSAccountsAgainstCrossPlatform.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/UserInterfaceRef.h"

EOS_ENABLE_STRICT_WARNINGS

class ONLINESUBSYSTEMREDPOINTEOS_API FPromptToLinkEOSAccountsAgainstCrossPlatformNode : public FAuthenticationGraphNode
{
private:
    TSharedPtr<TUserInterfaceRef<
        IEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform,
        UEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform,
        UEOSUserInterface_LinkEOSAccountsAgainstCrossPlatform_Context>>
        Widget;

    virtual void SelectedCandidates(
        TArray<FEOSUserInterface_CandidateEOSAccount> SelectedCandidates,
        TSharedRef<FAuthenticationGraphState> State,
        FAuthenticationGraphNodeOnDone OnDone);

public:
    UE_NONCOPYABLE(FPromptToLinkEOSAccountsAgainstCrossPlatformNode);
    FPromptToLinkEOSAccountsAgainstCrossPlatformNode() = default;
    virtual ~FPromptToLinkEOSAccountsAgainstCrossPlatformNode() = default;

    virtual void Execute(TSharedRef<FAuthenticationGraphState> State, FAuthenticationGraphNodeOnDone OnDone) override;

    virtual FString GetDebugName() const override
    {
        return TEXT("FPromptToLinkEOSAccountsAgainstCrossPlatformNode");
    }
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
