// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/EOSUserInterface_SwitchToCrossPlatformAccount.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/UserInterface/UserInterfaceRef.h"

EOS_ENABLE_STRICT_WARNINGS

class ONLINESUBSYSTEMREDPOINTEOS_API FPromptForSwitchToCrossPlatformAccountNode : public FAuthenticationGraphNode
{
private:
    TSharedPtr<TUserInterfaceRef<
        IEOSUserInterface_SwitchToCrossPlatformAccount,
        UEOSUserInterface_SwitchToCrossPlatformAccount,
        UEOSUserInterface_SwitchToCrossPlatformAccount_Context>>
        Widget;

    virtual void SelectChoice(
        EEOSUserInterface_SwitchToCrossPlatformAccount_Choice SelectedChoice,
        TSharedRef<FAuthenticationGraphState> State,
        FAuthenticationGraphNodeOnDone OnDone);

public:
    UE_NONCOPYABLE(FPromptForSwitchToCrossPlatformAccountNode);
    FPromptForSwitchToCrossPlatformAccountNode() = default;
    virtual ~FPromptForSwitchToCrossPlatformAccountNode() = default;

    virtual void Execute(TSharedRef<FAuthenticationGraphState> State, FAuthenticationGraphNodeOnDone OnDone) override;

    virtual FString GetDebugName() const override
    {
        return TEXT("FPromptForSwitchToCrossPlatformAccountNode");
    }
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION
