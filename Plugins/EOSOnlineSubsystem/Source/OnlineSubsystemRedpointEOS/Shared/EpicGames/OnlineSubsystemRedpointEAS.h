// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION

#include "CoreMinimal.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemImpl.h"
#include "OnlineSubsystemRedpointEOS/Shared/EpicGames/OnlineFriendsInterfaceEAS.h"
#include "OnlineSubsystemRedpointEOS/Shared/EpicGames/OnlineIdentityInterfaceEAS.h"
#include "OnlineSubsystemRedpointEOS/Shared/EpicGames/OnlinePresenceInterfaceEAS.h"
#include "OnlineSubsystemRedpointEOS/Shared/EpicGames/OnlineUserInterfaceEAS.h"

EOS_ENABLE_STRICT_WARNINGS

class FOnlineSubsystemRedpointEAS : public FOnlineSubsystemImpl,
                                    public TSharedFromThis<FOnlineSubsystemRedpointEAS, ESPMode::ThreadSafe>
{
private:
    TSharedRef<class FOnlineSubsystemEOS, ESPMode::ThreadSafe> ParentEOS;
    TSharedPtr<FOnlineIdentityInterfaceEAS, ESPMode::ThreadSafe> IdentityImpl;
    TSharedPtr<FOnlineFriendsInterfaceEAS, ESPMode::ThreadSafe> FriendsImpl;
    TSharedPtr<FOnlinePresenceInterfaceEAS, ESPMode::ThreadSafe> PresenceImpl;
    TSharedPtr<FOnlineUserInterfaceEAS, ESPMode::ThreadSafe> UserImpl;

public:
    UE_NONCOPYABLE(FOnlineSubsystemRedpointEAS);
    FOnlineSubsystemRedpointEAS() = delete;
    FOnlineSubsystemRedpointEAS(
        FName InInstanceName,
        TSharedRef<class FOnlineSubsystemEOS, ESPMode::ThreadSafe> InParentEOS);
    ~FOnlineSubsystemRedpointEAS(){};

    virtual IOnlineFriendsPtr GetFriendsInterface() const override;
    virtual IOnlineIdentityPtr GetIdentityInterface() const override;
    virtual IOnlinePresencePtr GetPresenceInterface() const override;
    virtual IOnlineUserPtr GetUserInterface() const override;

    virtual bool Init() override;
    virtual bool Shutdown() override;

    virtual FString GetAppId() const override;

    virtual FText GetOnlineServiceName(void) const override;

    virtual IOnlineSessionPtr GetSessionInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineTurnBasedPtr GetTurnBasedInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineTournamentPtr GetTournamentInterface() const override
    {
        return nullptr;
    }

#if defined(NEEDS_ONLINE_SUBSYSTEM_NULL_IMPLEMENTATIONS_4_24)
    virtual IOnlinePartyPtr GetPartyInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineUserCloudPtr GetUserCloudInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineTitleFilePtr GetTitleFileInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineLeaderboardsPtr GetLeaderboardsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineAchievementsPtr GetAchievementsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineStatsPtr GetStatsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineGroupsPtr GetGroupsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineSharedCloudPtr GetSharedCloudInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineEntitlementsPtr GetEntitlementsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineVoicePtr GetVoiceInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineExternalUIPtr GetExternalUIInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineTimePtr GetTimeInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineStorePtr GetStoreInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineStoreV2Ptr GetStoreV2Interface() const override
    {
        return nullptr;
    }

    virtual IOnlinePurchasePtr GetPurchaseInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineEventsPtr GetEventsInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineSharingPtr GetSharingInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineMessagePtr GetMessageInterface() const override
    {
        return nullptr;
    }

    virtual IOnlineChatPtr GetChatInterface() const override
    {
        return nullptr;
    }
#endif
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION