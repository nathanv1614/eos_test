// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystemRedpointEOS/Shared/OnlineSubsystemImplBase.h"
#include "OnlineSubsystemRedpointSteamConstants.h"

#if EOS_STEAM_ENABLED

class FOnlineSubsystemRedpointSteam : public FOnlineSubsystemImplBase,
                                      public TSharedFromThis<FOnlineSubsystemRedpointSteam, ESPMode::ThreadSafe>
{
private:
    TSharedPtr<class FOnlineAvatarInterfaceRedpointSteam, ESPMode::ThreadSafe> AvatarImpl;
    FString WebApiKey;

public:
    UE_NONCOPYABLE(FOnlineSubsystemRedpointSteam);
    FOnlineSubsystemRedpointSteam() = delete;
    FOnlineSubsystemRedpointSteam(FName InInstanceName);
    ~FOnlineSubsystemRedpointSteam();

    virtual bool IsEnabled() const override;

    // Our custom interfaces. Note that even those these APIs return a UObject*, we return
    // non-UObjects that are TSharedFromThis.
    virtual class UObject *GetNamedInterface(FName InterfaceName) override;
    virtual void SetNamedInterface(FName InterfaceName, class UObject *NewInterface) override
    {
        checkf(false, TEXT("FOnlineSubsystemEOS::SetNamedInterface is not supported"));
    };

    virtual bool Init() override;
    virtual bool Shutdown() override;

    virtual FString GetAppId() const override;
    virtual FText GetOnlineServiceName() const override;

    FString GetWebApiKey() const;
};

#endif