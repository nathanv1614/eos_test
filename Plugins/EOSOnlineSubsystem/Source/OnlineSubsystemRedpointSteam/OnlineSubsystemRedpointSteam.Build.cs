// Copyright June Rhodes. All Rights Reserved.

using UnrealBuildTool;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Diagnostics;

public class OnlineSubsystemRedpointSteam : ModuleRules
{
    public OnlineSubsystemRedpointSteam(ReadOnlyTargetRules Target) : base(Target)
    {
        DefaultBuildSettings = BuildSettingsVersion.V2;
        bUsePrecompiled = false;

        /* PRECOMPILED REMOVE BEGIN */
        if (!bUsePrecompiled)
        {
            // This can't use OnlineSubsystemRedpointEOSConfig.GetBool because the environment variable comes
            // from the standard build scripts.
            if (Environment.GetEnvironmentVariable("BUILDING_FOR_REDISTRIBUTION") == "true")
            {
                bTreatAsEngineModule = true;
                bPrecompile = true;

                // Force the module to be treated as an engine module for UHT, to ensure UPROPERTY compliance.
#if UE_5_0_OR_LATER
                object ContextObj = this.GetType().GetProperty("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#else
                object ContextObj = this.GetType().GetField("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#endif
                ContextObj.GetType().GetField("bClassifyAsGameModuleForUHT", BindingFlags.Instance | BindingFlags.Public).SetValue(ContextObj, false);
            }

            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "Core",
                    "CoreUObject",
                    "Projects",
                    "Json",
                    "Http",
                    "Engine",
                    "OnlineSubsystem",
                    "OnlineSubsystemUtils",
                    "RedpointEOSInterfaces",

                    // Pull in WeakPtrHelpers.
                    "OnlineSubsystemRedpointEOS",
                }
            );

            if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac ||
                Target.Platform == UnrealTargetPlatform.Linux)
            {
                var bEnableByConfig = OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_STEAM", false);
                if (Target.bIsEngineInstalled || bEnableByConfig)
                {
                    PrivateDefinitions.Add("EOS_STEAM_ENABLED=1");

                    PrivateDependencyModuleNames.AddRange(
                        new string[]
                        {
                            // For encrypted app ticket authentication.
                            "OnlineSubsystemSteam",
                            "SteamShared",
                        }
                    );

                    AddEngineThirdPartyPrivateStaticDependencies(Target, "Steamworks");
                }
                else
                {
                    // NOTE: By default, we don't include Steam if the developer isn't using an
                    // installed version from the Epic Games launcher. That is because as
                    // far as I can tell, source builds don't have OnlineSubsystemSteam
                    // available by default, which will make the plugin fail to load when launching
                    // the editor.
                    //
                    // If you want to enable support, add the following to your .Target.cs file:
                    //
                    // ProjectDefinitions.Add("ONLINE_SUBSYSTEM_EOS_ENABLE_STEAM=1");
                    //
                    if (!bEnableByConfig)
                    {
                        Console.WriteLine("WARNING: Steam authentication for OnlineSubsystemEOS will not be enabled by default, because you are using a source version of the engine. Read the instructions in RedpointEOSPlatformDefault.Build.cs on how to enable support or hide this warning.");
                    }
                    PrivateDefinitions.Add("EOS_STEAM_ENABLED=0");
                }
            }
            else
            {
                PrivateDefinitions.Add("EOS_STEAM_ENABLED=0");
            }
        }
        /* PRECOMPILED REMOVE END */
    }
}