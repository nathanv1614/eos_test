// Copyright June Rhodes. All Rights Reserved.

using UnrealBuildTool;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
#if UE_5_0_OR_LATER
using EpicGames.Core;
#else
using Tools.DotNETCommon;
#endif

public class RedpointEOSPlatformDefault : ModuleRules
{
    public RedpointEOSPlatformDefault(ReadOnlyTargetRules Target) : base(Target)
    {
        DefaultBuildSettings = BuildSettingsVersion.V2;
        bUsePrecompiled = false;

        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Projects",
                "Engine",
                "OnlineSubsystem",
                "OnlineSubsystemRedpointEOS",
                "OnlineSubsystemUtils",
                "RedpointEOSSDK",
            }
        );

        if (Target.Type != TargetType.Server)
        {
            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "UMG",
                }
            );
        }

        /* PRECOMPILED REMOVE BEGIN */
        if (!bUsePrecompiled)
        {
            // This can't use OnlineSubsystemRedpointEOSConfig.GetBool because the environment variable comes
            // from the standard build scripts.
            if (Environment.GetEnvironmentVariable("BUILDING_FOR_REDISTRIBUTION") == "true")
            {
                bTreatAsEngineModule = true;
                bPrecompile = true;

                // Force the module to be treated as an engine module for UHT, to ensure UPROPERTY compliance.
#if UE_5_0_OR_LATER
                object ContextObj = this.GetType().GetProperty("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#else
                object ContextObj = this.GetType().GetField("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#endif
                ContextObj.GetType().GetField("bClassifyAsGameModuleForUHT", BindingFlags.Instance | BindingFlags.Public).SetValue(ContextObj, false);
            }

            ConfigureSteam();
            ConfigureDiscord();
            ConfigureGOG();
            ConfigureItchIo();
            ConfigureOculus();
            ConfigureGoogle();
            ConfigureApple();
        }
        /* PRECOMPILED REMOVE END */
    }

    /* PRECOMPILED REMOVE BEGIN */

    void ConfigureSteam()
    {
        if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac ||
                Target.Platform == UnrealTargetPlatform.Linux)
        {
            var bEnableByConfig = OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_STEAM", false);
            if (Target.bIsEngineInstalled || bEnableByConfig)
            {
                PrivateDefinitions.Add("EOS_STEAM_ENABLED=1");

#if UE_4_27_OR_LATER || UE_5_0_OR_LATER
                PrivateDefinitions.Add("EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE=1");
#else
                PrivateDefinitions.Add("EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE=0");
#endif

                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        // For encrypted app ticket authentication.
                        "OnlineSubsystemSteam",
                        "SteamShared",
                    }
                );

                AddEngineThirdPartyPrivateStaticDependencies(Target, "Steamworks");
            }
            else
            {
                // NOTE: By default, we don't include Steam if the developer isn't using an
                // installed version from the Epic Games launcher. That is because as
                // far as I can tell, source builds don't have OnlineSubsystemSteam
                // available by default, which will make the plugin fail to load when launching
                // the editor.
                //
                // If you want to enable support, add the following to your .Target.cs file:
                //
                // ProjectDefinitions.Add("ONLINE_SUBSYSTEM_EOS_ENABLE_STEAM=1");
                //
                if (!bEnableByConfig)
                {
                    Console.WriteLine("WARNING: Steam authentication for OnlineSubsystemEOS will not be enabled by default, because you are using a source version of the engine. Read the instructions in RedpointEOSPlatformDefault.Build.cs on how to enable support or hide this warning.");
                }
                PrivateDefinitions.Add("EOS_STEAM_ENABLED=0");
            }
        }
        else
        {
            PrivateDefinitions.Add("EOS_STEAM_ENABLED=0");
        }
    }

    void ConfigureDiscord()
    {
        if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac ||
                Target.Platform == UnrealTargetPlatform.Linux)
        {
            // RedpointDiscordGameSDK defines EOS_DISCORD_ENABLED based on whether the SDK was found.
            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "OnlineSubsystemRedpointDiscord",
                    "RedpointDiscordGameSDK",
                    "RedpointDiscordGameSDKRuntime",
                }
            );
        }
        else
        {
            PrivateDefinitions.Add("EOS_DISCORD_ENABLED=0");
        }
    }

    void ConfigureGOG()
    {
        if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac)
        {
            if (OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_GOG", false))
            {
                PrivateDefinitions.Add("EOS_GOG_ENABLED=1");

                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        "OnlineSubsystemGOG",
                        "GalaxySDK",
                    }
                );
            }
            else
            {
                PrivateDefinitions.Add("EOS_GOG_ENABLED=0");
            }
        }
        else
        {
            PrivateDefinitions.Add("EOS_GOG_ENABLED=0");
        }
    }

    void ConfigureItchIo()
    {
        if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac ||
                Target.Platform == UnrealTargetPlatform.Linux)
        {
            // OnlineSubsystemRedpointItchIo defines EOS_ITCH_IO_ENABLED based on ProjectDefinitions.
            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "OnlineSubsystemRedpointItchIo",
                }
            );
        }
        else
        {
            PrivateDefinitions.Add("EOS_ITCH_IO_ENABLED=0");
        }
    }

    void ConfigureOculus()
    {
        if (Target.Platform == UnrealTargetPlatform.Win64 ||
                Target.Platform == UnrealTargetPlatform.Android)
        {
            if (OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_OCULUS", true))
            {
                PrivateDefinitions.Add("EOS_OCULUS_ENABLED=1");

                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        "LibOVRPlatform",
                        "OnlineSubsystemOculus",
                    });
            }
            else
            {
                PrivateDefinitions.Add("EOS_OCULUS_ENABLED=0");
            }
        }
        else
        {
            PrivateDefinitions.Add("EOS_OCULUS_ENABLED=0");
        }
    }

    void ConfigureGoogle()
    {
        if (Target.Platform == UnrealTargetPlatform.Android)
        {
            if (OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_GOOGLE", false))
            {
                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        "OnlineSubsystemGoogle",
                    }
                );

                PrivateDefinitions.Add("EOS_GOOGLE_ENABLED=1");
            }
            else
            {
                PrivateDefinitions.Add("EOS_GOOGLE_ENABLED=0");
            }
        }
        else
        {
            PrivateDefinitions.Add("EOS_GOOGLE_ENABLED=0");
        }
    }

    void ConfigureApple()
    {
#if UE_4_26_OR_LATER
        if (Target.Platform == UnrealTargetPlatform.IOS)
        {
            bool bSignInWithAppleSupported;
            ConfigHierarchy PlatformGameConfig = ConfigCache.ReadHierarchy(ConfigHierarchyType.Engine, DirectoryReference.FromFile(Target.ProjectFile), UnrealTargetPlatform.IOS);
            PlatformGameConfig.GetBool("/Script/IOSRuntimeSettings.IOSRuntimeSettings", "bEnableSignInWithAppleSupport", out bSignInWithAppleSupported);

            if (bSignInWithAppleSupported)
            {
                PrivateDependencyModuleNames.AddRange(
                    new string[]
                    {
                        "OnlineSubsystemApple",
                    }
                );

                PrivateDefinitions.Add("EOS_APPLE_ENABLED=1");
            }
            else
            {
                PrivateDefinitions.Add("EOS_APPLE_ENABLED=0");
            }
        }
        else
        {
            PrivateDefinitions.Add("EOS_APPLE_ENABLED=0");
        }
#else
        PrivateDefinitions.Add("EOS_APPLE_ENABLED=0");
#endif
    }

    /* PRECOMPILED REMOVE END */
}