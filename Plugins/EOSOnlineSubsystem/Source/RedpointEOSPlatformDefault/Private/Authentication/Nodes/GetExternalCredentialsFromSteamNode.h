// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_HAS_AUTHENTICATION && EOS_STEAM_ENABLED

#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationGraph.h"

#include "Interfaces/OnlineIdentityInterface.h"

EOS_ENABLE_STRICT_WARNINGS

class FGetExternalCredentialsFromSteamNode : public FAuthenticationGraphNode
{
private:
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
    FDelegateHandle EncryptedAppTicketDelegateHandle;
#endif

    void OnEncryptedAppTicketObtained(
        bool bEncryptedDataAvailable,
        int32 ResultCode,
        TSharedRef<FAuthenticationGraphState> InState,
        FAuthenticationGraphNodeOnDone InOnDone,
        TSharedPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe> OSSEncryptedAppTicket,
        TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity);

public:
    FGetExternalCredentialsFromSteamNode() = default;
    UE_NONCOPYABLE(FGetExternalCredentialsFromSteamNode);

    virtual void Execute(TSharedRef<FAuthenticationGraphState> State, FAuthenticationGraphNodeOnDone OnDone) override;

    virtual FString GetDebugName() const override
    {
        return TEXT("FGetExternalCredentialsFromSteamNode");
    }
};

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION && EOS_STEAM_ENABLED