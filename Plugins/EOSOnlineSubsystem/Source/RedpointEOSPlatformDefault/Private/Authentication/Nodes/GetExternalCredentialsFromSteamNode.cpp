// Copyright June Rhodes. All Rights Reserved.

#if EOS_HAS_AUTHENTICATION && EOS_STEAM_ENABLED

#include "GetExternalCredentialsFromSteamNode.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/AuthenticationHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/Authentication/OnlineExternalCredentials.h"

#include "LogEOSPlatformDefault.h"
#include "OnlineSubsystemUtils.h"

#include "OnlineEncryptedAppTicketInterfaceSteam.h"
#include "OnlineSubsystemSteam.h"
THIRD_PARTY_INCLUDES_START
#include "steam/steam_api.h"
THIRD_PARTY_INCLUDES_END

EOS_ENABLE_STRICT_WARNINGS

#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
#define STEAM_OSS_BIND_DELEGATE(Handle, Class, ...)                                                                    \
    this->Handle =                                                                                                     \
        OSSEncryptedAppTicket->OnEncryptedAppTicketResultDelegate                                                      \
            .AddSP(this, &Class::OnEncryptedAppTicketObtained, __VA_ARGS__, OSSEncryptedAppTicket, OSSIdentity);
#define STEAM_OSS_UNBIND_DELEGATE(Handle)                                                                              \
    OSSEncryptedAppTicket->OnEncryptedAppTicketResultDelegate.Remove(this->Handle);
#else
#define STEAM_OSS_BIND_DELEGATE(Handle, Class, ...)                                                                    \
    OSSEncryptedAppTicket->OnEncryptedAppTicketResultDelegate = FOnEncryptedAppTicketResponseDelegate::CreateSP(       \
        this,                                                                                                          \
        &Class::OnEncryptedAppTicketObtained,                                                                          \
        __VA_ARGS__,                                                                                                   \
        OSSEncryptedAppTicket,                                                                                         \
        OSSIdentity);
#define STEAM_OSS_UNBIND_DELEGATE(Handle) OSSEncryptedAppTicket->OnEncryptedAppTicketResultDelegate.Unbind();
#endif

class FSteamExternalCredentials : public IOnlineExternalCredentials
{
private:
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
    FDelegateHandle EncryptedAppTicketDelegateHandle;
#endif
    FString AppTicket;
    TMap<FString, FString> AuthAttributes;
    void OnEncryptedAppTicketObtained(
        bool bEncryptedDataAvailable,
        int32 ResultCode,
        FOnlineExternalCredentialsRefreshComplete OnComplete,
        TSharedPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe> OSSEncryptedAppTicket,
        TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity);

public:
    FSteamExternalCredentials(const FString &InAppTicket, const TMap<FString, FString> &InAuthAttributes);
    UE_NONCOPYABLE(FSteamExternalCredentials);
    virtual ~FSteamExternalCredentials(){};
    virtual FText GetProviderDisplayName() const override;
    virtual FString GetType() const override;
    virtual FString GetId() const override;
    virtual FString GetToken() const override;
    virtual TMap<FString, FString> GetAuthAttributes() const override;
    virtual void Refresh(
        TSoftObjectPtr<UWorld> InWorld,
        int32 LocalUserNum,
        FOnlineExternalCredentialsRefreshComplete OnComplete) override;
};

FSteamExternalCredentials::FSteamExternalCredentials(
    const FString &InAppTicket,
    const TMap<FString, FString> &InAuthAttributes)
{
    this->AppTicket = InAppTicket;
    this->AuthAttributes = InAuthAttributes;
}

FText FSteamExternalCredentials::GetProviderDisplayName() const
{
    return NSLOCTEXT("OnlineSubsystemEOSAuthSteam", "Platform_Steam", "Steam");
}

FString FSteamExternalCredentials::GetType() const
{
    return TEXT("STEAM_APP_TICKET");
}

FString FSteamExternalCredentials::GetId() const
{
    return TEXT("");
}

FString FSteamExternalCredentials::GetToken() const
{
    return this->AppTicket;
}

TMap<FString, FString> FSteamExternalCredentials::GetAuthAttributes() const
{
    return this->AuthAttributes;
}

void FSteamExternalCredentials::Refresh(
    TSoftObjectPtr<UWorld> InWorld,
    int32 LocalUserNum,
    FOnlineExternalCredentialsRefreshComplete OnComplete)
{
    UWorld *World = InWorld.Get();
    if (World == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not refresh credentials with Steam, because the current world was not available."));
        OnComplete.ExecuteIfBound(false);
        return;
    }

    FOnlineSubsystemSteam *OSSSubsystem = (FOnlineSubsystemSteam *)Online::GetSubsystem(World, STEAM_SUBSYSTEM);
    if (OSSSubsystem == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not refresh credentials with Steam, because the Steam OSS was not available. Check "
                 "that Steam "
                 "OSS "
                 "is "
                 "enabled in your DefaultEngine.ini file."));
        OnComplete.ExecuteIfBound(false);
        return;
    }

    FOnlineEncryptedAppTicketSteamPtr OSSEncryptedAppTicket = OSSSubsystem->GetEncryptedAppTicketInterface();
    if (OSSEncryptedAppTicket == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not refresh credentials with Steam, because the encrypted app ticket interface was not "
                 "available."));
        OnComplete.ExecuteIfBound(false);
        return;
    }

    TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity =
        StaticCastSharedPtr<IOnlineIdentity>(OSSSubsystem->GetIdentityInterface());
    if (!OSSIdentity.IsValid())
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not refresh credentials with Steam, because the identity interface was not available."));
        OnComplete.ExecuteIfBound(false);
        return;
    }

    STEAM_OSS_BIND_DELEGATE(EncryptedAppTicketDelegateHandle, FSteamExternalCredentials, OnComplete)
    OSSEncryptedAppTicket->RequestEncryptedAppTicket(nullptr, 0);
}

EOS_EResult ConvertToAppTicket(const TArray<uint8> &Data, FString &OutAppTicket)
{
    uint8_t *Block = (uint8_t *)FMemory::Malloc(Data.Num());
    for (auto i = 0; i < Data.Num(); i++)
    {
        Block[i] = Data[i];
    }
    uint32_t TokenLen = 4096;
    char TokenBuffer[4096];
    auto Result = EOS_ByteArray_ToString(Block, Data.Num(), TokenBuffer, &TokenLen);
    FMemory::Free(Block);
    if (Result != EOS_EResult::EOS_Success)
    {
        OutAppTicket = TEXT("");
    }
    else
    {
        OutAppTicket = FString(ANSI_TO_TCHAR(TokenBuffer));
    }
    return Result;
}

void FSteamExternalCredentials::OnEncryptedAppTicketObtained(
    bool bEncryptedDataAvailable,
    int32 ResultCode,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FOnlineExternalCredentialsRefreshComplete OnComplete,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe> OSSEncryptedAppTicket,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity)
{
    if (bEncryptedDataAvailable)
    {
        TArray<uint8> Data;
        auto Success = OSSEncryptedAppTicket->GetEncryptedAppTicket(Data);
        if (Success)
        {
            FString NewAppTicket;
            EOS_EResult Result = ConvertToAppTicket(Data, NewAppTicket);
            if (Result != EOS_EResult::EOS_Success)
            {
                UE_LOG(LogEOSPlatformDefault, Error, TEXT("EOS_ByteArray_ToString call failed."));
                STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
                OnComplete.ExecuteIfBound(false);
                return;
            }

            TMap<FString, FString> UserAuthAttributes;
            UserAuthAttributes.Add(EOS_AUTH_ATTRIBUTE_AUTHENTICATEDWITH, TEXT("steam"));
            UserAuthAttributes.Add(TEXT("steam.id"), OSSIdentity->GetUniquePlayerId(0)->ToString());
            UserAuthAttributes.Add(TEXT("steam.encryptedAppTicket"), AppTicket);

            this->AppTicket = NewAppTicket;
            this->AuthAttributes = UserAuthAttributes;

            STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
            OnComplete.ExecuteIfBound(true);
            return;
        }
        else
        {
            UE_LOG(
                LogEOSPlatformDefault,
                Error,
                TEXT("Failed to GetEncryptedAppTicket after bEncryptedDataAvailable true"));
            STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
            OnComplete.ExecuteIfBound(false);
            return;
        }
    }
    else
    {
        UE_LOG(LogEOSPlatformDefault, Error, TEXT("Unable to get encrypted app ticket, result code: %d"), ResultCode);
        STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
        OnComplete.ExecuteIfBound(false);
        return;
    }
}

void FGetExternalCredentialsFromSteamNode::OnEncryptedAppTicketObtained(
    bool bEncryptedDataAvailable,
    int32 ResultCode,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedRef<FAuthenticationGraphState> InState,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    FAuthenticationGraphNodeOnDone InOnDone,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe> OSSEncryptedAppTicket,
    // NOLINTNEXTLINE(performance-unnecessary-value-param)
    TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity)
{
    if (bEncryptedDataAvailable)
    {
        TArray<uint8> Data;
        auto Success = OSSEncryptedAppTicket->GetEncryptedAppTicket(Data);
        if (Success)
        {
            FString AppTicket;
            EOS_EResult Result = ConvertToAppTicket(Data, AppTicket);
            if (Result != EOS_EResult::EOS_Success)
            {
                UE_LOG(LogEOSPlatformDefault, Error, TEXT("EOS_ByteArray_ToString call failed."));
                InState->ErrorMessages.Add(TEXT("EOS_ByteArray_ToString call failed."));
                STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
                InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
                return;
            }

            TMap<FString, FString> UserAuthAttributes;
            UserAuthAttributes.Add(EOS_AUTH_ATTRIBUTE_AUTHENTICATEDWITH, TEXT("steam"));
            UserAuthAttributes.Add(TEXT("steam.id"), OSSIdentity->GetUniquePlayerId(0)->ToString());
            UserAuthAttributes.Add(TEXT("steam.encryptedAppTicket"), AppTicket);

            UE_LOG(LogEOSPlatformDefault, Verbose, TEXT("Authenticating with app ticket: %s"), *AppTicket);

            InState->AvailableExternalCredentials.Add(
                MakeShared<FSteamExternalCredentials>(AppTicket, UserAuthAttributes));
            STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
            InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Continue);
            return;
        }
        else
        {
            UE_LOG(
                LogEOSPlatformDefault,
                Error,
                TEXT("Failed to GetEncryptedAppTicket after bEncryptedDataAvailable true"));
            STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
            InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
            return;
        }
    }
    else
    {
        UE_LOG(LogEOSPlatformDefault, Error, TEXT("Unable to get encrypted app ticket, result code: %d"), ResultCode);
        STEAM_OSS_UNBIND_DELEGATE(EncryptedAppTicketDelegateHandle);
        InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }
}

void FGetExternalCredentialsFromSteamNode::Execute(
    TSharedRef<FAuthenticationGraphState> InState,
    FAuthenticationGraphNodeOnDone InOnDone)
{
    UWorld *World = InState->GetWorld();
    if (World == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not authenticate with Steam, because the UWorld* pointer was null."));
        InState->ErrorMessages.Add(TEXT("Could not authenticate with Steam, because the UWorld* pointer was null."));
        InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    FOnlineSubsystemSteam *OSSSubsystem = (FOnlineSubsystemSteam *)Online::GetSubsystem(World, STEAM_SUBSYSTEM);
    if (OSSSubsystem == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not authenticate with Steam, because the Steam OSS was not available. Check that Steam "
                 "OSS "
                 "is "
                 "enabled in your DefaultEngine.ini file."));
        InState->ErrorMessages.Add(TEXT("Could not authenticate with online subsystem, because the subsystem was not "
                                        "available. Check that Steam OSS is "
                                        "enabled in your DefaultEngine.ini file."));
        InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    FOnlineEncryptedAppTicketSteamPtr OSSEncryptedAppTicket = OSSSubsystem->GetEncryptedAppTicketInterface();
    if (OSSEncryptedAppTicket == nullptr)
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not authenticate with Steam, because the encrypted app ticket interface was not "
                 "available."));
        InState->ErrorMessages.Add(TEXT("Could not authenticate with Steam, because the encrypted app ticket "
                                        "interface was not available."));
        InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    TSharedPtr<IOnlineIdentity, ESPMode::ThreadSafe> OSSIdentity =
        StaticCastSharedPtr<IOnlineIdentity>(OSSSubsystem->GetIdentityInterface());
    if (!OSSIdentity.IsValid())
    {
        UE_LOG(
            LogEOSPlatformDefault,
            Error,
            TEXT("Could not authenticate with Steam, because the identity interface was not available."));
        InState->ErrorMessages.Add(
            TEXT("Could not authenticate with Steam, because the identity interface was not available."));
        InOnDone.ExecuteIfBound(EAuthenticationGraphNodeResult::Error);
        return;
    }

    STEAM_OSS_BIND_DELEGATE(EncryptedAppTicketDelegateHandle, FGetExternalCredentialsFromSteamNode, InState, InOnDone)
    OSSEncryptedAppTicket->RequestEncryptedAppTicket(nullptr, 0);
}

EOS_DISABLE_STRICT_WARNINGS

#endif // #if EOS_HAS_AUTHENTICATION && EOS_STEAM_ENABLED