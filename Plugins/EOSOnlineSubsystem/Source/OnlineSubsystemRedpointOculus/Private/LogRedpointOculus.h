// Copyright June Rhodes. All Rights Reserved.

#pragma once

#if EOS_OCULUS_ENABLED

#include "CoreMinimal.h"

#include "Logging/LogMacros.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRedpointOculus, All, All);

#endif