// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#if EOS_DISCORD_ENABLED
THIRD_PARTY_INCLUDES_START
#include "cpp/RedpointPatched/achievement_manager.h"
#include "cpp/RedpointPatched/activity_manager.h"
#include "cpp/RedpointPatched/application_manager.h"
#include "cpp/RedpointPatched/core.h"
#include "cpp/RedpointPatched/event.h"
#include "cpp/RedpointPatched/image_manager.h"
#include "cpp/RedpointPatched/lobby_manager.h"
#include "cpp/RedpointPatched/network_manager.h"
#include "cpp/RedpointPatched/overlay_manager.h"
#include "cpp/RedpointPatched/relationship_manager.h"
#include "cpp/RedpointPatched/storage_manager.h"
#include "cpp/RedpointPatched/store_manager.h"
#include "cpp/RedpointPatched/types.h"
#include "cpp/RedpointPatched/user_manager.h"
#include "cpp/RedpointPatched/voice_manager.h"
THIRD_PARTY_INCLUDES_END
#endif