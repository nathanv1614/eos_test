// Copyright June Rhodes. All Rights Reserved.

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FRedpointDiscordGameSDKRuntimeModule : public IModuleInterface
{
};

IMPLEMENT_MODULE(FRedpointDiscordGameSDKRuntimeModule, RedpointDiscordGameSDKRuntime)

#if EOS_DISCORD_ENABLED
THIRD_PARTY_INCLUDES_START
#include "cpp/RedpointPatched/achievement_manager.cpp"
#include "cpp/RedpointPatched/activity_manager.cpp"
#include "cpp/RedpointPatched/application_manager.cpp"
#include "cpp/RedpointPatched/core.cpp"
#include "cpp/RedpointPatched/image_manager.cpp"
#include "cpp/RedpointPatched/lobby_manager.cpp"
#include "cpp/RedpointPatched/network_manager.cpp"
#include "cpp/RedpointPatched/overlay_manager.cpp"
#include "cpp/RedpointPatched/relationship_manager.cpp"
#include "cpp/RedpointPatched/storage_manager.cpp"
#include "cpp/RedpointPatched/store_manager.cpp"
#include "cpp/RedpointPatched/types.cpp"
#include "cpp/RedpointPatched/user_manager.cpp"
#include "cpp/RedpointPatched/voice_manager.cpp"
THIRD_PARTY_INCLUDES_END
#endif