// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include <cstddef>

extern const char UC_TestFile_Small[];
extern const size_t UC_TestFile_SmallSize;