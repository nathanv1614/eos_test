// Copyright June Rhodes. All Rights Reserved.

#include "./UserCloud_Common.h"

const char UC_TestFile_Small[] = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
const size_t UC_TestFile_SmallSize = sizeof(UC_TestFile_Small);