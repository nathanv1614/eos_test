// Copyright June Rhodes. All Rights Reserved.

using UnrealBuildTool;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Diagnostics;

public class RedpointEOSAuthSteam : ModuleRules
{
    public RedpointEOSAuthSteam(ReadOnlyTargetRules Target) : base(Target)
    {
        DefaultBuildSettings = BuildSettingsVersion.V2;
        bUsePrecompiled = false;

        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine",
                "OnlineSubsystem",
                "OnlineSubsystemRedpointEOS",
                "OnlineSubsystemUtils",
                "RedpointEOSSDK",
            }
        );

        if (Target.Type != TargetType.Server)
        {
            PrivateDependencyModuleNames.AddRange(
               new string[]
               {
                    "UMG",
               }
            );
        }

        /* PRECOMPILED REMOVE BEGIN */
        if (!bUsePrecompiled)
        {
            // This can't use OnlineSubsystemRedpointEOSConfig.GetBool because the environment variable comes
            // from the standard build scripts.
            if (Environment.GetEnvironmentVariable("BUILDING_FOR_REDISTRIBUTION") == "true")
            {
                bTreatAsEngineModule = true;
                bPrecompile = true;

                // Force the module to be treated as an engine module for UHT, to ensure UPROPERTY compliance.
#if UE_5_0_OR_LATER
                object ContextObj = this.GetType().GetProperty("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#else
                object ContextObj = this.GetType().GetField("Context", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);
#endif
                ContextObj.GetType().GetField("bClassifyAsGameModuleForUHT", BindingFlags.Instance | BindingFlags.Public).SetValue(ContextObj, false);
            }

#if UE_4_27_OR_LATER || UE_5_0_OR_LATER
            PrivateDefinitions.Add("EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE=1");
#else
            PrivateDefinitions.Add("EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE=0");
#endif

            // Add Steam support for authentication if we're on a platform
            // that supports Steam.
            if (Target.Platform == UnrealTargetPlatform.Win64 ||
#if !UE_5_0_OR_LATER
                Target.Platform == UnrealTargetPlatform.Win32 ||
#endif
                Target.Platform == UnrealTargetPlatform.Mac ||
                Target.Platform == UnrealTargetPlatform.Linux)
            {
                var bEnableByConfig = OnlineSubsystemRedpointEOSConfig.GetBool(Target, "ENABLE_STEAM", false);
                if (Target.bIsEngineInstalled || bEnableByConfig)
                {
                    PrivateDefinitions.Add("NATIVE_PLATFORM_STEAM=1");

                    PrivateDependencyModuleNames.AddRange(
                        new string[]
                        {
                        // For encrypted app ticket authentication.
                        "OnlineSubsystemSteam",
                        "SteamShared",
                        }
                    );

                    AddEngineThirdPartyPrivateStaticDependencies(Target, "Steamworks");
                }
                else
                {
                    // NOTE: By default, we don't include Steam if the developer isn't using an
                    // installed version from the Epic Games launcher. That is because as
                    // far as I can tell, source builds don't have OnlineSubsystemSteam
                    // available by default, which will make the plugin fail to load when launching
                    // the editor.
                    //
                    // If you want to enable support, add the following to your .Target.cs file:
                    //
                    // ProjectDefinitions.Add("ONLINE_SUBSYSTEM_EOS_ENABLE_STEAM=1");
                    //
                    if (!bEnableByConfig)
                    {
                        Console.WriteLine("WARNING: Steam authentication for OnlineSubsystemEOS will not be enabled by default, because you are using a source version of the engine. Read the instructions in OnlineSubsystemEOS.Build.cs on how to enable support or hide this warning.");
                    }
                    PrivateDefinitions.Add("NATIVE_PLATFORM_STEAM=0");
                }
            }
            else
            {
                PrivateDefinitions.Add("NATIVE_PLATFORM_STEAM=0");
            }
        }
        /* PRECOMPILED REMOVE END */
    }
}