// Copyright June Rhodes. All Rights Reserved.

#pragma once

#include "EOSNativePlatform.h"

#include "EOSNativePlatformSteam.generated.h"

UCLASS()
class UEOSNativePlatformSteam : public UObject, public IEOSNativePlatform
{
    GENERATED_BODY()

public:
    virtual void LoadModules() override;

    virtual FText GetDisplayName() override
    {
        return NSLOCTEXT("OnlineSubsystemEOSAuthSteam", "Platform_Steam", "Steam");
    }

    virtual bool HasCredentials(int32 LocalUserNum) override;
    virtual void GetCredentials(int32 LocalUserNum, FGetCredentialsComplete OnComplete) override;

    virtual bool HasAvatar(const FUniqueNetId &UserId) override;
    virtual void GetAvatar(const FUniqueNetId &UserId, FGetAvatarComplete OnComplete) override;

    virtual bool CanProvideExternalAccountId(const FUniqueNetId &UserId) override;
    virtual FExternalAccountIdInfoLegacy GetExternalAccountId(const FUniqueNetId &UserId) override;

    virtual bool CanRefreshCredentials(FOnlineAccountCredentials PreviousCredentials) override;
    virtual void GetRefreshCredentials(
        FOnlineAccountCredentials PreviousCredentials,
        FGetCredentialsComplete OnComplete) override;
};