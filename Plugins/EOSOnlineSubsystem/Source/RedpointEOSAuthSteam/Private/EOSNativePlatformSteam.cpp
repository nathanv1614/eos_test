// Copyright June Rhodes. All Rights Reserved.

#include "EOSNativePlatformSteam.h"

#include "LogEOSAuthSteam.h"
#include "Logging/LogMacros.h"
#include "Modules/ModuleManager.h"
#include "OnlineSubsystem.h"
#include "OnlineSubsystemRedpointEOS/Shared/CompatHelpers.h"
#include "OnlineSubsystemRedpointEOS/Shared/EOSCommon.h"
#include "OnlineSubsystemRedpointEOS/Shared/WeakPtrHelpers.h"
#if NATIVE_PLATFORM_STEAM
#include "OnlineEncryptedAppTicketInterfaceSteam.h"
#include "OnlineSubsystemSteam.h"
THIRD_PARTY_INCLUDES_START
#include "steam/steam_api.h"
THIRD_PARTY_INCLUDES_END
#endif

FString CachedAppTicket = TEXT("");
TMap<FString, FString> CachedUserAttributes;

void UEOSNativePlatformSteam::LoadModules()
{
#if NATIVE_PLATFORM_STEAM
    FModuleManager &ModuleManager = FModuleManager::Get();
    ModuleManager.LoadModule(FName(TEXT("OnlineSubsystemSteam")));
#endif
}

bool UEOSNativePlatformSteam::HasCredentials(int32 LocalUserNum)
{
#if NATIVE_PLATFORM_STEAM
    // NOLINTNEXTLINE(unreal-ionlinesubsystem-get)
    auto Subsystem = IOnlineSubsystem::Get(STEAM_SUBSYSTEM);
    return Subsystem != nullptr;
#else
    return false;
#endif
}

void UEOSNativePlatformSteam::GetCredentials(int32 LocalUserNum, FGetCredentialsComplete OnComplete)
{
#if NATIVE_PLATFORM_STEAM
    FString AppTicket = CachedAppTicket;
    if (AppTicket != TEXT(""))
    {
        UE_LOG(LogEOSAuthSteam, Verbose, TEXT("Authenticating with app ticket: %s"), *AppTicket);

        FOnlineAccountCredentials Creds;
        Creds.Type = TEXT("STEAM_APP_TICKET");
        Creds.Id = TEXT("");
        Creds.Token = AppTicket;
        OnComplete.Execute(true, Creds, CachedUserAttributes);
        return;
    }

    // NOLINTNEXTLINE(unreal-ionlinesubsystem-get)
    auto Subsystem = (FOnlineSubsystemSteam *)IOnlineSubsystem::Get(STEAM_SUBSYSTEM);
    if (Subsystem == nullptr)
    {
        UE_LOG(LogEOSAuthSteam, Error, TEXT("Steam subsystem no longer available when GetCredentials called"));
        OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
        return;
    }

    auto EncryptedAppTicketInterface = Subsystem->GetEncryptedAppTicketInterface();
    if (EncryptedAppTicketInterface == nullptr)
    {
        UE_LOG(LogEOSAuthSteam, Error, TEXT("Unable to get encrypted app ticket interface from Steam"));
        OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
        return;
    }

    auto IdentityInterface = Subsystem->GetIdentityInterface();
    if (IdentityInterface == nullptr)
    {
        UE_LOG(LogEOSAuthSteam, Error, TEXT("Unable to get identity interface from Steam"));
        OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
        return;
    }

#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
    TSharedPtr<FDelegateHandle> AppTicketHandleHolder = MakeShareable<FDelegateHandle>(new FDelegateHandle());
    *AppTicketHandleHolder = EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.AddLambda(
        [WeakIdentity = TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe>(IdentityInterface),
         WeakEncryptedAppTicket =
             TWeakPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe>(EncryptedAppTicketInterface),
         OnComplete,
         AppTicketHandleHolder](bool bEncryptedDataAvailable, int32 ResultCode) {
#else
    EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate =
        FOnEncryptedAppTicketResponseDelegate::CreateLambda(
            [WeakIdentity = TWeakPtr<IOnlineIdentity, ESPMode::ThreadSafe>(IdentityInterface),
             WeakEncryptedAppTicket =
                 TWeakPtr<class FOnlineEncryptedAppTicketSteam, ESPMode::ThreadSafe>(EncryptedAppTicketInterface),
             OnComplete](bool bEncryptedDataAvailable, int32 ResultCode) {
#endif
            if (auto IdentityInterface = WeakIdentity.Pin())
            {
                if (auto EncryptedAppTicketInterface = WeakEncryptedAppTicket.Pin())
                {
                    if (bEncryptedDataAvailable)
                    {
                        TArray<uint8> Data;
                        auto Success = EncryptedAppTicketInterface->GetEncryptedAppTicket(Data);
                        if (Success)
                        {
                            uint8_t *Block = (uint8_t *)FMemory::Malloc(Data.Num());
                            for (auto i = 0; i < Data.Num(); i++)
                            {
                                Block[i] = Data[i];
                            }
                            uint32_t TokenLen = 4096;
                            char TokenBuffer[4096];
                            auto Result = EOS_ByteArray_ToString(Block, Data.Num(), TokenBuffer, &TokenLen);
                            FMemory::Free(Block);
                            if (Result != EOS_EResult::EOS_Success)
                            {
                                UE_LOG(LogEOSAuthSteam, Error, TEXT("EOS_ByteArray_ToString called failed"));
                                OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
                                EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Remove(
                                    *AppTicketHandleHolder);
#else
                                    EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Unbind();
#endif
                                return;
                            }

                            // The API is severly rate limited, so we have to cache the token forever.
                            CachedAppTicket = FString(ANSI_TO_TCHAR(TokenBuffer));

                            CachedUserAttributes.Empty();
                            CachedUserAttributes.Add(EOS_AUTH_ATTRIBUTE_AUTHENTICATEDWITH, TEXT("steam"));
                            CachedUserAttributes.Add(
                                TEXT("steam.id"),
                                IdentityInterface->GetUniquePlayerId(0)->ToString());
                            CachedUserAttributes.Add(TEXT("steam.encryptedAppTicket"), CachedAppTicket);

                            UE_LOG(
                                LogEOSAuthSteam,
                                Verbose,
                                TEXT("Authenticating with app ticket: %s"),
                                ANSI_TO_TCHAR(TokenBuffer));

                            FOnlineAccountCredentials Creds;
                            Creds.Type = TEXT("STEAM_APP_TICKET");
                            Creds.Id = TEXT("");
                            Creds.Token = FString(ANSI_TO_TCHAR(TokenBuffer));
                            OnComplete.Execute(true, Creds, CachedUserAttributes);
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
                            EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Remove(
                                *AppTicketHandleHolder);
#else
                                EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Unbind();
#endif
                            return;
                        }
                        else
                        {
                            UE_LOG(
                                LogEOSAuthSteam,
                                Error,
                                TEXT("Failed to GetEncryptedAppTicket after bEncryptedDataAvailable true"));
                            OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
                            EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Remove(
                                *AppTicketHandleHolder);
#else
                                EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Unbind();
#endif
                            return;
                        }
                    }
                    else
                    {
                        UE_LOG(
                            LogEOSAuthSteam,
                            Error,
                            TEXT("Unable to get encrypted app ticket, result code: %d"),
                            ResultCode);
                        OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
#if EOS_STEAM_HAS_NEWER_APP_TICKET_INTERFACE
                        EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Remove(*AppTicketHandleHolder);
#else
                            EncryptedAppTicketInterface->OnEncryptedAppTicketResultDelegate.Unbind();
#endif
                        return;
                    }
                }
            }
        });

    EncryptedAppTicketInterface->RequestEncryptedAppTicket(nullptr, 0);
#else
    UE_LOG(LogEOSAuthSteam, Error, TEXT("Steam native platform not available for GetCredentials"));
    OnComplete.Execute(false, FOnlineAccountCredentials(), TMap<FString, FString>());
#endif
}

bool UEOSNativePlatformSteam::HasAvatar(const FUniqueNetId &UserId)
{
#if NATIVE_PLATFORM_STEAM
    return UserId.GetType() == STEAM_SUBSYSTEM;
#else
    return false;
#endif
}

#if NATIVE_PLATFORM_STEAM
UTexture2D *ConvertSteamPictureToAvatar(int Picture)
{
    uint32 Width;
    uint32 Height;
    SteamUtils()->GetImageSize(Picture, &Width, &Height);

    if (Width > 0 && Height > 0)
    {
        int BufferSize = Width * Height * 4;
        uint8 *AvatarRGBA = (uint8 *)Compat_MallocZeroed(BufferSize);
        SteamUtils()->GetImageRGBA(Picture, AvatarRGBA, BufferSize);
        for (uint32 i = 0; i < (Width * Height * 4); i += 4)
        {
            uint8 Temp = AvatarRGBA[i + 0];
            AvatarRGBA[i + 0] = AvatarRGBA[i + 2];
            AvatarRGBA[i + 2] = Temp;
        }

        UTexture2D *Avatar = UTexture2D::CreateTransient(Width, Height, PF_B8G8R8A8);
        uint8 *MipData = (uint8 *)Avatar->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
        FMemory::Memcpy(MipData, (void *)AvatarRGBA, BufferSize);
        Avatar->PlatformData->Mips[0].BulkData.Unlock();
        Avatar->PlatformData->SetNumSlices(1);
        Avatar->NeverStream = true;
        Avatar->UpdateResource();

        return Avatar;
    }

    return nullptr;
}
#endif

void UEOSNativePlatformSteam::GetAvatar(const FUniqueNetId &UserId, FGetAvatarComplete OnComplete)
{
#if NATIVE_PLATFORM_STEAM
    if (UserId.GetType() != STEAM_SUBSYSTEM)
    {
        OnComplete.Execute(false, nullptr);
        return;
    }

    // Cheat. We can't access FUniqueNetIdSteam directly, but we do know it returns
    // the CSteamID as a uint64 from GetBytes :)
    uint64 SteamID = *(uint64 *)UserId.GetBytes();

    int Picture = SteamFriends()->GetLargeFriendAvatar(SteamID);
    if (Picture == 0)
    {
        // User has no avatar set.
        OnComplete.Execute(false, nullptr);
        return;
    }

    if (Picture != -1)
    {
        auto Avatar = ConvertSteamPictureToAvatar(Picture);
        OnComplete.Execute(Avatar != nullptr, Avatar);
    }
    else
    {
        // Normally we'd wait for AvatarImageLoaded_t, but the Steam OSS implementation is bad
        // and it only ticks the Steam callbacks if it thinks it's expecting one internally. So
        // instead, we just check occasionally until it's done...
        auto UserIdRef = UserId.AsShared();
        FTicker::GetCoreTicker().AddTicker(
            FTickerDelegate::CreateLambda([=](float DeltaTime) {
                this->GetAvatar(UserIdRef.Get(), OnComplete);
                return false;
            }),
            0.2f);
    }
#else
    UE_LOG(LogEOSAuthSteam, Error, TEXT("Steam native platform not available for GetAvatar"));
    OnComplete.Execute(false, nullptr);
#endif
}

bool UEOSNativePlatformSteam::CanProvideExternalAccountId(const FUniqueNetId &UserId)
{
    return UserId.GetType().IsEqual(STEAM_SUBSYSTEM);
}

FExternalAccountIdInfoLegacy UEOSNativePlatformSteam::GetExternalAccountId(const FUniqueNetId &UserId)
{
    if (!UserId.GetType().IsEqual(STEAM_SUBSYSTEM))
    {
        return FExternalAccountIdInfoLegacy();
    }

    FExternalAccountIdInfoLegacy Info;
    Info.AccountIdType = (int32_t)EOS_EExternalAccountType::EOS_EAT_STEAM;
    Info.AccountId = UserId.ToString();
    return Info;
}

bool UEOSNativePlatformSteam::CanRefreshCredentials(FOnlineAccountCredentials PreviousCredentials)
{
    return PreviousCredentials.Type == FString(TEXT("STEAM_APP_TICKET"));
}

void UEOSNativePlatformSteam::GetRefreshCredentials(
    FOnlineAccountCredentials PreviousCredentials,
    FGetCredentialsComplete OnComplete)
{
    this->GetCredentials(0, OnComplete);
}
