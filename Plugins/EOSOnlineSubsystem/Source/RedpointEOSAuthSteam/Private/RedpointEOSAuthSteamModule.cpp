// Copyright June Rhodes. All Rights Reserved.

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

EOS_ENABLE_STRICT_WARNINGS

class FRedpointEOSAuthSteamModule : public IModuleInterface
{
    virtual void StartupModule() override
    {
    }
};

IMPLEMENT_MODULE(FRedpointEOSAuthSteamModule, RedpointEOSAuthSteam)

EOS_DISABLE_STRICT_WARNINGS